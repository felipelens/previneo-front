#!/bin/bash

NAME=$1
DEFAULT_DIR="./src/components"
COMPONENT_DIR=$2

if [[ "$COMPONENT_DIR" == "" ]]; then
    COMPONENT_DIR="$DEFAULT_DIR"
fi

if [[ "$NAME" == "" ]]; then
    echo "Component name is required"
    exit 1
fi

mkdir -p "$COMPONENT_DIR/$NAME"

cat << EOF > "$COMPONENT_DIR/$NAME/index.js"
export { default } from './$NAME'
EOF

cat << EOF > "$COMPONENT_DIR/$NAME/$NAME.js"
import React from 'react'

export default function $NAME () {
  return (
    <div>$NAME component</div>
  )
}
EOF
