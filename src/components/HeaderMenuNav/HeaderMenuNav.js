import React, { Component } from 'react'
import onClickOutside from 'react-onclickoutside'
import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Menu = styled.nav`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    justify-content: space-between;  
  `}

  ${mediaQuery.lessThan('large')`
    position: absolute;
    width: 200px
    right: 20px;
    top: 100px;
    overflow: hidden;
    background-color: ${props => props.theme.colors.background};
    border-radius: 3px;
    box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.15);
    ${props => !props.isMenuOpened && 'display: none;'}
    z-index: 1000;
  `}
`

class HeaderMenuNav extends Component {
  handleClickOutside = e => {
    this.props.onClickout(e)
  }

  render () {
    const { onClickout, ...props } = this.props
    return <Menu {...props} />
  }
}

export default onClickOutside(HeaderMenuNav)
