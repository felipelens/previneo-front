import React from 'react'
import styled from 'styled-components'
import Media from 'react-media'
import Carousel from 'components/Carousel'
import mediaQuery from 'utils/mediaQuery'
import theme from 'theme'

const Container = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    margin: 0 -15px;
    padding: 0 15px;

    > * {
      flex-grow: 1;
      flex-basis: 0;
    }
    > :not(:last-child){
      margin-right: 15px;
    }
  `}

  ${mediaQuery.lessThan('large')`
    > * + * {
      margin-top: 15px;
    }
  `}
`

export default function CardServiceGroup (props) {
  return (
    <Media query={`(min-width: ${theme.breakpoints.large})`}>
      {matches => matches ? (
        <Container {...props} />
      ) : (
        <Carousel>
          {props.children}
        </Carousel>
      )}
    </Media>
  )
}
