import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'
import background from 'images/section-steps.jpg'

export const Section = styled.section`
  background-color: ${props => props.theme.colors.background};

  ${mediaQuery.greaterThan('large')`
    height: 449px;
    background: url(${background}) center no-repeat;
    margin-bottom: 150px;
  `}

  ${mediaQuery.lessThan('large')`
    padding: 45px 0;
  `}
`

export const Content = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    align-items: center;
    height: 449px;

    > div {
      max-width: 565px;
    }
  `}
`
