import React from 'react'
import { FormattedHTMLMessage, injectIntl } from 'react-intl'
import Container from 'components/Container'
import SectionTitle from 'components/SectionTitle'
import CardStep from 'components/CardStep'
import CardStepGroup from 'components/CardStepGroup'
import { Section, Content } from './styles'

function SectionSteps ({ intl, ...props }) {
  return (
    <Section>
      <Container size='small'>
        <Content>
          <div>
            <SectionTitle>
              <FormattedHTMLMessage id='section.home.steps.title' />
            </SectionTitle>
          </div>
        </Content>
        <CardStepGroup lift={65}>
          <CardStep
            label={intl.formatMessage({id: 'section.home.steps.stepone.step'})}
            title={intl.formatMessage({ id: 'section.home.steps.stepone.title' })}
            description={intl.formatMessage({ id: 'section.home.steps.stepone.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.home.steps.steptwo.step' })}
            title={intl.formatMessage({ id: 'section.home.steps.steptwo.title' })}
            description={intl.formatMessage({ id: 'section.home.steps.steptwo.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.home.steps.stepthree.step' })}
            title={intl.formatMessage({ id: 'section.home.steps.stepthree.title' })}
            description={intl.formatMessage({ id: 'section.home.steps.stepthree.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.home.steps.stepfour.step' })}
            title={intl.formatMessage({ id: 'section.home.steps.stepfour.title' })}
            description={intl.formatMessage({ id: 'section.home.steps.stepfour.description' })}
          />
        </CardStepGroup>
      </Container>
    </Section>
  )
}

export default injectIntl(SectionSteps)
