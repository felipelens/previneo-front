import React from 'react'
import HeroBase from 'components/HeroBase'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import hero from 'images/hero-customers.jpg'
import { FormattedHTMLMessage } from 'react-intl'

export default function HeroCustomers () {
  return (
    <HeroBase
      bgImage={hero}
      maxWidth={490}
      minHeight={645}
    >
      <VerticalSpacer space={25}>
        <SectionTitle>
          <FormattedHTMLMessage id='section.customers.hero.title' />
        </SectionTitle>
        <Text size='18px'>
          <FormattedHTMLMessage id='section.customers.hero.description' />
        </Text>
      </VerticalSpacer>
    </HeroBase>
  )
}
