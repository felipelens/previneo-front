import React from 'react'
import styled from 'styled-components'
import Spinner from 'components/Spinner'
import Container from 'components/Container'
import Resource from 'containers/Resource'
import BlogList from 'components/BlogList'
import SectionTitle from 'components/SectionTitle'

const Section = styled.section`
  padding-top: 100px;
`

export default function RelatedPost ({ params }) {
  return (
    <Resource resource='RelatedPostsSite' autoFetch paginated params={{ limit: 3, ...params }}>
      {props => {
        if (props.records.length === 0 && props.isFetching) {
          return <Spinner text='Carregando Posts' />
        }

        return (
          <Section>
            <Container size='small'>
              <SectionTitle
                size='60px'
                style={{marginBottom: '30px'}}
              >
                Notícias relacionadas
              </SectionTitle>
            </Container>
            <Container size='blog'>
              <BlogList posts={props.records} />
            </Container>
          </Section>
        )
      }}
    </Resource>
  )
}
