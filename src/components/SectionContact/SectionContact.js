import React from 'react'
import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import FormContact from 'forms/FormContact'
import mediaQuery from 'utils/mediaQuery'
import Resource from 'containers/Resource'
import Notifications from 'containers/Notifications'

const Container = styled.section`
  ${mediaQuery.greaterThan('medium')`
    display: flex;
    justify-content: space-between;
    width: 100%;

    > :first-child {
      width: 320px;
      flex-shrink: 0;
      margin-right: 50px;
    }

    > :last-child {
      width: 700px;
      flex-shrink: 1;
    }
  `}

  ${mediaQuery.lessThan('medium')`
    > * + * {
      margin-top: 25px;
    }
  `}
`

export default function SectionContact () {
  return (
    <Container>
      <div>
        <VerticalSpacer>
          <SectionTitle>
            <FormattedMessage id='section.contact.title' />
          </SectionTitle>
          <Text>
            <FormattedMessage id='section.contact.subtitle' />
          </Text>
        </VerticalSpacer>
      </div>
      <VerticalSpacer>
        <Notifications />
        <Resource resource='Contact'>
          {props => (
            <FormContact
              onSubmit={data => props.create(data)}
              isSubmitting={props.isSubmitting}
            />
          )}
        </Resource>
      </VerticalSpacer>
    </Container>
  )
}
