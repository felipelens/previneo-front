import styled from 'styled-components'
import { hoverColor } from 'utils/colors'

export const Container = styled.div`
  display: flex;
  align-items: center;
  border-top: 1px solid ${props => props.theme.colors.border};
  padding-top: 25px;

  > * + * {
    margin-left: 7px;
  }
`

export const Button = styled.a`
  font-size: 32px;
  line-height: 0;
  color: ${props => props.theme.colors.formBorder};
  cursor: pointer;
  text-decoration: none;

  &:hover {
    color: ${props => hoverColor(props.theme.colors.formBorder)}
  }
`
