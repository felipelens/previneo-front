import React from 'react'
import PropTypes from 'prop-types'
import Icon from 'components/Icon'
import Text from 'components/Text'
import { Container, Button } from './styles'

export default function Sharebox ({ url, title, ...props }) {
  return (
    <Container {...props}>
      <Text style={{ marginRight: 5 }}>
        <strong>Compartilhe</strong>
      </Text>
      <Button
        title='Compartilhar no Google+'
        href={`https://plus.google.com/share?url=${url}`}
        target='_blank'
      >
        <Icon icon='google' />
      </Button>
      <Button
        title='Compartilhar no Twitter'
        href={`https://twitter.com/home?status=${url}`}
        target='_blank'
      >
        <Icon icon='twitter' />
      </Button>
      <Button
        title='Compartilhar no Facebook'
        href={`https://www.facebook.com/sharer/sharer.php?u=${url}`}
        target='_blank'
      >
        <Icon icon='facebook' />
      </Button>
      <Button
        title='Compartilhar no Linkedin'
        href={`https://www.linkedin.com/shareArticle?url=${url}&title=${title}`}
        target='_blank'
      >
        <Icon icon='linkedin' />
      </Button>
    </Container>
  )
}

Sharebox.propTypes = {
  /** URL que será compartilhada */
  url: PropTypes.string.isRequired,

  /** Título */
  title: PropTypes.string.isRequired
}
