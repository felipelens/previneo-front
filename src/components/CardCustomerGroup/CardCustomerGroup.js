import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
  margin: -10px;

  > * {
    width: calc(${props => (1 / props.columns) * 100}% - 20px);
    box-sizing: border-box;
    margin: 10px 10px;
  }
`

export default function CardCustomerGroup (props) {
  return (
    <Container {...props} />
  )
}

CardCustomerGroup.propTypes = {
  /** Colunas */
  columns: PropTypes.number
}

CardCustomerGroup.defaultProps = {
  columns: 2
}
