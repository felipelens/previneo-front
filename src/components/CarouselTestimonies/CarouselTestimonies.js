import React from 'react'
import PropTypes from 'prop-types'
import Carousel from 'components/Carousel'
import CardTestimony from 'components/CardTestimony'

export default function CarouselTestimonies ({ data }) {
  return (
    <Carousel
      adaptiveHeight
      autoplay
      autoPlaySpeed={5000}
    >
      {data.map((item, index) =>
        <CardTestimony key={index} {...item} />
      )}
    </Carousel>
  )
}

CarouselTestimonies.propTypes = {
  /** Depoimentos */
  data: PropTypes.array.isRequired
}
