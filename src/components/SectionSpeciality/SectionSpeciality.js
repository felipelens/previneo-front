import React from 'react'
import PropTypes from 'prop-types'
import Container from 'components/Container'
import SpecialityTitle from 'components/SpecialityTitle'
import Image from 'components/Image'
import PostContent from 'components/PostContent'
import VerticalSpacer from 'components/VerticalSpacer'
import BackLink from 'components/BackLink'
import Button from 'components/Button'
import routes from 'routes'
import { Section, Column, Figure } from './styles'
import { Link } from 'react-router-dom'
import { FormattedHTMLMessage } from 'react-intl'

export default function SectionSpeciality ({ title, description, color, image, ...props }) {
  return (
    <Container {...props}>
      <BackLink to={routes.site.specialities.index} />
      <Section>
        <Column>
          <VerticalSpacer space={25}>
            <SpecialityTitle
              title={title}
              color={color}
            />
            <PostContent dangerouslySetInnerHTML={{__html: description}} />
            <Button
              to={routes.site.contact}
              component={Link}
              arrow
            >
              <FormattedHTMLMessage id='section.contact.title' />
            </Button>
          </VerticalSpacer>
        </Column>
        <Column>
          <Figure>
            <Image src={image} alt={title} />
          </Figure>
        </Column>
      </Section>
    </Container>
  )
}

SectionSpeciality.propTypes = {
  /** Título da Especialidade */
  title: PropTypes.string.isRequired,

  /** Descrição da Especialidade */
  description: PropTypes.string.isRequired,

  /** Cor da Especialidade */
  color: PropTypes.string.isRequired,

  /** Imagem da Especialidade */
  image: PropTypes.string.isRequired
}
