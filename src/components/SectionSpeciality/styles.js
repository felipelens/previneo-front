import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Section = styled.section`
  display: flex;
  justify-content: space-between;
  margin-top: 35px;
  flex-wrap: wrap;
  ${mediaQuery.greaterThan('large')`
    margin-top: 100px;
  `}
`

export const Column = styled.div`
  width: 100%;
  ${mediaQuery.greaterThan('large')`
    &:first-child {
      width: 55%;
    }
    &:last-child {
      flex: 1;
      display: flex;
      justify-content: flex-end;
      padding-left: 20px;
    }
  `}
  ${mediaQuery.lessThan('large')`
    &:last-child {
      display: flex;
      justify-content: center;
      margin-top: 35px;
  `}
`

export const Figure = styled.figure`
  margin: 0;
  overflow: hidden;
  border-radius: 80px;
  max-width: 485px;
  align-self: flex-start;
`
