import React from 'react'
import PropTypes from 'prop-types'
import Container from 'components/Container'
import Logo from 'components/Logo'
import HeaderMenu from 'components/HeaderMenu'
import LanguageSwitch from 'components/LanguageSwitch'
import Localization from 'containers/Localization'
import { HeaderContainer, HeaderStyled } from './styles'
import { ENABLE_MULTILANG } from 'config/constants'

export default function Header ({ floated }) {
  return (
    <HeaderContainer floated={floated}>
      <Container style={{ position: 'relative', padding: 0 }}>
        <HeaderStyled floated={floated}>
          <Logo />
          <HeaderMenu />
          {ENABLE_MULTILANG &&
            <Localization>
              {props => (
                <LanguageSwitch
                  onChange={props.setLang}
                />
              )}
            </Localization>
          }
        </HeaderStyled>
      </Container>
    </HeaderContainer>
  )
}

Header.propTypes = {
  /** Deixa o header flutuando na página, como position absolute. Por enquanto, só está sendo utilizado na home */
  floated: PropTypes.bool
}
