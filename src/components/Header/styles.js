import styled, { css } from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const HeaderContainer = styled.div`
  background-color: ${props => props.floated ? 'transparent' : props.theme.colors.white};
  ${props => !props.floated && css`
    border-bottom: 1px solid ${props => props.theme.colors.border};
  `}
`

export const HeaderStyled = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 50px 20px;
  padding-bottom: 30px;
  width: 100%;
  z-index: 100;
  box-sizing: border-box;
  background-color: ${props => props.theme.colors.white};

  ${props => props.floated && mediaQuery.greaterThan('large')`
    position: absolute;
    left: 0;
    top: 0;
    background-color: transparent;
  `}
`
