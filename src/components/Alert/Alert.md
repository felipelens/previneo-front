```
<Alert
  type='success'
  message='Alguma mensagem de sucesso!'
/>
<br />
<Alert
  type='error'
  message='Ops! Algo de errado aconteceu!'
/>
```

Se passar a prop `onRemoveClick`, o botão de dismiss é adicionado:

```
<Alert
  type='success'
  message='Alguma mensagem de sucesso!'
  onRemoveClick={() => {}}
/>
<br />
<Alert
  type='error'
  message='Ops! Algo de errado aconteceu!'
  onRemoveClick={() => {}}
/>
```
