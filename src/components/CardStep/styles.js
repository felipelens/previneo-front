import styled from 'styled-components'

export const Container = styled.section`
  font-family: ${props => props.theme.fonts.primary};
  background-color: #fff;
  padding: 20px;
  border-bottom: 7px solid ${props => props.theme.colors.primary};
  border-radius: 3px;
  box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.15);
  box-sizing: border-box;
`

export const Label = styled.span`
  display: block;
  font-size: 12px;
  font-weight: 500;
  letter-spacing: 2px;
  color: ${props => props.theme.colors.primary};
  text-transform: uppercase;
  margin-bottom: 5px;
  ${props => props.border && `
    padding-bottom: 20px;
    position: relative;
    margin-bottom: 20px;
    &::after {
      content: '';
      width: 55px;
      height: 1px;
      background: ${props.theme.colors.primary};
      position: absolute;
      bottom: 0;
      left: 0;
    }
  `}
`

export const Title = styled.h3`
  display: block;
  font-size: 22px;
  font-weight: bold;
  margin: 0;
  margin-bottom: 10px;
  color: ${props => props.theme.colors.primary};
`
