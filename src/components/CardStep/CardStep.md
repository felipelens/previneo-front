```
<CardStep
  label='Etapa 1'
  title='Identificação'
  description='Anamnese online ou presencial'
/>
```

Variação sem o título

```
<CardStep
  label='Etapa 1'
  description='Anamnese online ou presencial'
/>
```
