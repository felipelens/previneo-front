import React from 'react'
import PropTypes from 'prop-types'
import Text from 'components/Text'
import { Container, Label, Title } from './styles'

export default function CardStep ({ label, title, description, ...props }) {
  return (
    <Container {...props}>
      <Label border={!title}>{label}</Label>
      {title && <Title>{title}</Title>}
      <Text>
        {description}
      </Text>
    </Container>
  )
}

CardStep.propTypes = {
  /** Label */
  label: PropTypes.string.isRequired,

  /** Título */
  title: PropTypes.string,

  /** Descrição */
  description: PropTypes.string.isRequired
}
