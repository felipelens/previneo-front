import React from 'react'
import { FormattedMessage } from 'react-intl'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import VerticalSpacer from 'components/VerticalSpacer'
import CardServiceGroup from 'components/CardServiceGroup'
import CardService from 'components/CardService'
import Resource from 'containers/Resource'

export default function SectionSpecialities () {
  return (
    <section>
      <VerticalSpacer space={45}>
        <VerticalSpacer space={15}>
          <SectionTitle>
            <FormattedMessage id='section.specialities.types.title' />
          </SectionTitle>
          <Text>
            <FormattedMessage id='section.specialities.types.subtitle' />
          </Text>
        </VerticalSpacer>
        <Resource resource='SpecialitiesSite' autoFetch params={{ limit: 5 }}>
          {props => (
            <CardServiceGroup>
              {props.records.map(record => (
                <CardService
                  key={record.id}
                  id={record.id}
                  image={record.icone.url}
                  title={record.nome}
                  legenda={record.legenda}
                  description={record.nome}
                  color={record.cor}
                />
              ))}
            </CardServiceGroup>
          )}
        </Resource>
      </VerticalSpacer>
    </section>
  )
}
