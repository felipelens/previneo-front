```
<Icon icon='facebook' />
{' '}
<Icon icon='linkedin' />
{' '}
<Icon icon='arrow-left' />
{' '}
<Icon icon='arrow-right' />
{' '}
<Icon icon='mailbox' />
{' '}
<Icon icon='print' />
{' '}
<Icon icon='whatsapp' />
```

Através das props `color` e `size`, é possível alterar as cores e tamanho do ícone:

```
<Icon icon='facebook' color='primary' size='50px' />
```
