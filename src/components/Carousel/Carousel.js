import React from 'react'
import styled from 'styled-components'
import SlickCarousel from 'react-slick'
import 'slick-carousel/slick/slick.css'
import ButtonArrow from 'components/ButtonArrow'

const Slide = styled.div`
  padding: 0 45px;
  box-sizing: border-box;
`

export default function Carousel ({ children, wrapSlides = true, innerRef, ...props }) {
  const slides = [ ...children ]
  return (
    <SlickCarousel
      ref={innerRef}
      adaptiveHeight
      prevArrow={(
        <ButtonArrow />
      )}
      nextArrow={(
        <ButtonArrow next />
      )}
      {...props}
    >
      {wrapSlides ? (
        slides.map((child, index) => (
          <Slide key={index}>
            {child}
          </Slide>
        ))
      ) : slides}
    </SlickCarousel>
  )
}
