import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Image from 'components/Image'
import Text from 'components/Text'
import PostCategory from 'components/PostCategory'
import routes from 'routes'
import getRoute from 'utils/getRoute'
import * as styles from './styles'
import { FormattedMessage } from 'react-intl'

export default function CardPost ({ id, permalink, title, thumbnail, category, content, ...props }) {
  const to = getRoute(routes.site.blog.post, { permalink, id })
  return (
    <styles.Container {...props}>
      <styles.Thumbnail>
        <Link to={to}>
          <Image src={thumbnail} alt={title} />
        </Link>
        {category && (
          <styles.Category>
            <PostCategory
              id={category.id}
              name={category.name}
            />
          </styles.Category>
        )}
      </styles.Thumbnail>
      <styles.Content to={to}>
        <styles.Title>{title.length > 60 ? title.substr(0, 60) + '...' : title }</styles.Title>
        <Text>
          {content.substr(0, 100)}...
        </Text>
        <styles.ReadMore><FormattedMessage id='admin.common.continueReading' /></styles.ReadMore>
      </styles.Content>
    </styles.Container>
  )
}

CardPost.propTypes = {
  /** ID do Post */
  id: PropTypes.any.isRequired,

  /** Título do post */
  title: PropTypes.string.isRequired,

  /** Categoria */
  category: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.any.isRequired
  }),

  /** Thumbnail */
  thumbnail: PropTypes.string.isRequired,

  /** Conteúdo do post */
  content: PropTypes.string.isRequired
}
