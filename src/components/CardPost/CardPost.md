```
<div style={{ maxWidth: 350 }}>
  <CardPost
    id={12}
    title='Título do post'
    thumbnail={'https://placehold.it/350x225'}
    category={{
      name: 'Categoria',
      id: 123
    }}
    content='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate augue sem, at pretium mauris interdum ac. Phasellus id nunc convallis leo consectetur tempor at accumsan sem. Praesent sed purus vel mi aliquam molestie nec eget nisi. Donec a nibh ac turpis hendrerit mollis nec ornare nibh. Praesent convallis lectus eget lacus maximus iaculis. Phasellus ultricies ante sed tellus auctor euismod. Ut sed nibh vehicula, accumsan metus ac, posuere magna. Vivamus in cursus mauris. In eget finibus augue, a fringilla purus.'
  />
</div>
```
