import styled from 'styled-components'
import { Link } from 'react-router-dom'
import mediaQuery from 'utils/mediaQuery'

export const Container = styled.section`
  font-family: ${props => props.theme.fonts.primary};
  height: 100%
`

export const Thumbnail = styled.div`
  position: relative;
  overflow: hidden;
  border-radius: 3px 3px 0 0;
  background-color: ${props => props.theme.colors.border};
  img {
    margin: 0 auto;
  }
`

export const Category = styled.div`
  position: absolute;
  top: 15px;
  right: 15px;
`

export const Content = styled(Link)`
  display: block;
  padding: 30px;
  text-decoration: none;
  background-color: #fff;
  border-radius: 0 0 3px 3px;
  box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.15);
  box-sizing: border-box;
`

export const Title = styled.h2`
  display: block;
  font-weight: bold;
  font-size: 25px;
  color: ${props => props.theme.colors.text};
  margin: 0;
  margin-bottom: 15px;

  ${mediaQuery.lessThan('large')`
    font-size: 20px;
  `}
`

export const ReadMore = styled.span`
  display: block;
  color: ${props => props.theme.colors.primary};
  font-size: 16px;
  font-weight: bold;
  margin-top: 35px;
`
