import React from 'react'
import { FormattedHTMLMessage } from 'react-intl'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import CardCustomerGroup from 'components/CardCustomerGroup'
import CardCustomer from 'components/CardCustomer'
import Resource from 'containers/Resource'

export default function SectionCustomers () {
  return (
    <section>
      <VerticalSpacer space={45}>
        <SectionTitle color='primary' border align='center'>
          <FormattedHTMLMessage id='section.customers.list.title' />
        </SectionTitle>
        <CardCustomerGroup columns={3}>
          <Resource resource='CustomersSite' autoFetch spinner>
            {props => props.records.map(record => (
              <CardCustomer
                key={record.id}
                title={record.nome}
                image={record.logo && record.logo.url ? record.logo.url : ''}
                url={record.url}
              />
            ))}
          </Resource>
        </CardCustomerGroup>
      </VerticalSpacer>
    </section>
  )
}
