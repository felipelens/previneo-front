Um componente útil para criar botões de submit.

```
<ButtonSubmit isSubmitting={false} />
<br />
<br />
<ButtonSubmit isSubmitting={true} />
```

```
<ButtonSubmit isSubmitting={false} label='Botão com título diferente' />
<br />
<br />
<ButtonSubmit isSubmitting={false} disabled label='Botão desabilitado' />
<br />
<br />
<ButtonSubmit isSubmitting={true} submittingLabel='Enviando dados...' />
```
