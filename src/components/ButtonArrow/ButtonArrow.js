import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Icon from 'components/Icon'

const Button = styled.button.attrs({
  type: 'button'
})`
  background-color: transparent;
  color: ${props => props.theme.colors.border};
  font-size: 24px;
  border: none;
  outline: none;
  position: absolute;
  top: 40%;
  transform: translateY(-50%);
  cursor: pointer;
  z-index: 100;
  ${props => props.next ? 'right: 0' : 'left: 0'};
`

export default function ButtonArrow ({ next, ...props }) {
  return (
    <Button
      aria-label={next ? 'Próximo' : 'Anterior'}
      next={next}
      {...props}
    >
      <Icon icon={next ? 'arrow-right' : 'arrow-left'} />
    </Button>
  )
}

ButtonArrow.propTypes = {
  /** Aria label do botão */
  next: PropTypes.bool
}
