Um componente super útil para definir espaçamento vertical entre componentes.

```
<VerticalSpacer>
  <div><button>button</button></div>
  <div><button>button</button></div>
  <div><button>button</button></div>
</VerticalSpacer>
```

```
<VerticalSpacer space={45}>
  <div><button>button</button></div>
  <div><button>button</button></div>
  <div><button>button</button></div>
</VerticalSpacer>
```
