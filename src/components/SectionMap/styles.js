import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Content = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  padding: 0 0 150px;
  ${mediaQuery.lessThan('large')`
    flex-direction: column-reverse;
    padding-bottom: 65px;
  `}
`

export const Column = styled.div`
  width: 45%;
  &:last-child {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
  ${mediaQuery.lessThan('large')`
    width: 100%;
    &:first-child {
      margin-top: 40px;
      display: flex;
      justify-content: center;
    }
  `}
`
