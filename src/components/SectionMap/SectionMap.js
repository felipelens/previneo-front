import React from 'react'
import { FormattedHTMLMessage } from 'react-intl'
import Image from 'components/Image'
import Text from 'components/Text'
import VerticalSpacer from 'components/VerticalSpacer'
import Container from 'components/Container'
import SectionTitle from 'components/SectionTitle'
import { Content, Column } from './styles'

export default function SectionMap (props) {
  return (
    <Container>
      <Content>
        <Column>
          <Image src={require('images/map.png')} alt={'Mapa'} />
        </Column>
        <Column>
          <VerticalSpacer space={35}>
            <SectionTitle size='60px' color='primary'>
              <FormattedHTMLMessage id='section.about.map.title' />
            </SectionTitle>
            <Text lineHeight={2} component='div'>
              <FormattedHTMLMessage id='section.about.map.description' />
            </Text>
          </VerticalSpacer>
        </Column>
      </Content>
    </Container>
  )
}
