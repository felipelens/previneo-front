import React from 'react'
import PropTypes from 'prop-types'
import { Container, Title, Text } from './styles'
import { FormattedHTMLMessage } from 'react-intl'

export default function SpecialityTitle ({ title, color, ...props }) {
  return (
    <Container>
      <Title color={color}>
        {title}
      </Title>
      <Text color={color}>
        <FormattedHTMLMessage id='pages.specialities.titleDescription' />
      </Text>
    </Container>
  )
}

SpecialityTitle.propTypes = {
  /** Título da página */
  title: PropTypes.string.isRequired,

  /** Cor da categoria */
  color: PropTypes.string.isRequired
}
