import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Container = styled.div`
  margin-bottom: 70px;
`

export const Title = styled.h1`
  font-family: ${props => props.theme.fonts.secondary};
  font-size: 35px;
  color: ${props => props.color};
  margin: 0 0 25px 0;
  padding: 0;
  ${mediaQuery.greaterThan('large')`
    font-size: 60px;
  `}
`

export const Text = styled.p`
  font-family: ${props => props.theme.fonts.primary};
  font-size: 16px;
  color: ${props => props.color};
  margin: 0;
  padding: 0;
  line-height: 2;
  ${mediaQuery.greaterThan('large')`
    font-size: 18px;
  `}
  &::after {
    content: '';
    width: 50px;
    height: 3px;
    background: ${props => props.color};
    display: block;
    margin-top: 25px;
  }
`
