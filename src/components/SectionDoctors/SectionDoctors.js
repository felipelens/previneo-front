import React from 'react'
import styled from 'styled-components'
import Container from 'components/Container'
import CardDoctorGroup from 'components/CardDoctorGroup'
import CardDoctor from 'components/CardDoctor'
import Resource from 'containers/Resource'
import mediaQuery from 'utils/mediaQuery'

const Section = styled.section`
  margin-top: 65px;
  ${mediaQuery.greaterThan('large')`
    margin: 190px 0;
  `}
`

export default function SectionDoctors () {
  return (
    <Section>
      <Container>
        <CardDoctorGroup>
          <Resource resource='Team' autoFetch spinner>
            {props => props.records.map(record => (
              <CardDoctor
                key={record.id}
                image={record.foto && record.foto.url ? record.foto.url : ''}
                name={record.nome}
                text={record.descricao}
              />
            ))}
          </Resource>
        </CardDoctorGroup>
      </Container>
    </Section>
  )
}
