import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import mediaQuery from 'utils/mediaQuery'

export const Container = styled.div`
  margin-left: 30px;
  flex-shrink: 0;
`

export const MenuSwitch = styled.div`
  display: none;
  justify-content: flex-end;
  margin-bottom: 10px;  

  ${mediaQuery.lessThan('large')`
    display: flex;
  `}
`

export const MenuItem = styled(NavLink)`
  display: block;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 18px;
  color: ${props => props.theme.colors.text};
  text-decoration: none;

  ${mediaQuery.greaterThan('large')`
    position: relative;

    & + & {
      margin-left: 45px;
    }

    &:hover::after,
    &.active::after {
      content: ' ';
      position: absolute;
      left: 0;
      bottom: -10px;
      width: 20px;
      height: 3px;
      background-color: ${props => props.theme.colors.text};
      margin-top: 10px;
    }  
  `}

  ${mediaQuery.lessThan('large')`
    padding: 10px;

    &.active,
    &:hover {
      background-color: ${props => props.theme.colors.primary};
      color: #fff;
    }
  `}
`
