import React from 'react'
import routes from 'routes'
import Button from 'components/Button'
import Toggle from 'components/Toggle'
import { FormattedMessage } from 'react-intl'
import HeaderMenuNav from 'components/HeaderMenuNav'
import { Container, MenuSwitch, MenuItem } from './styles'

export default function HeaderMenu () {
  return (
    <Toggle resetOnRouteChange>
      {({ toggle: toggleMenu, value: isMenuOpened }) => (
        <Container>
          <MenuSwitch>
            <Button type='button' onClick={toggleMenu}>
              Menu
            </Button>
          </MenuSwitch>
          <HeaderMenuNav
            isMenuOpened={isMenuOpened}
            onClickout={() => {
              if (isMenuOpened) {
                toggleMenu()
              }
            }}
          >
            <MenuItem exact to={routes.site.home}>
              <FormattedMessage id='site.menu.home' />
            </MenuItem>
            <MenuItem exact to={routes.site.about}>
              <FormattedMessage id='site.menu.about' />
            </MenuItem>
            <MenuItem to={routes.site.specialities.index}>
              <FormattedMessage id='site.menu.specialities' />
            </MenuItem>
            <MenuItem exact to={routes.site.customers}>
              <FormattedMessage id='site.menu.customers' />
            </MenuItem>
            <MenuItem to={routes.site.blog.index}>
              <FormattedMessage id='site.menu.blog' />
            </MenuItem>
            <MenuItem exact to={routes.site.contact}>
              <FormattedMessage id='site.menu.contact' />
            </MenuItem>
          </HeaderMenuNav>
        </Container>
      )}
    </Toggle>
  )
}
