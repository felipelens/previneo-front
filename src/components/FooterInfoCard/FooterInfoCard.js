import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Icon from 'components/Icon'
import Text from 'components/Text'

const Container = styled.div`
  display: flex;
  align-items: center;

  > * + * {
    margin-left: 20px;
  }
`

export default function FooterInfoCard ({ icon, children, ...props }) {
  return (
    <Container {...props}>
      <Icon icon={icon} color='primary' size='30px' />
      <Text color='white' size='18px' component='div'>{children}</Text>
    </Container>
  )
}

FooterInfoCard.propTypes = {
  /** Ícone */
  icon: PropTypes.string.isRequired,

  /** Conteúdo */
  children: PropTypes.any.isRequired
}
