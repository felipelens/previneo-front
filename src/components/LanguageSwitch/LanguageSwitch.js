import React from 'react'
import PropTypes from 'prop-types'
import Image from 'components/Image'
import br from 'images/br.svg'
import eng from 'images/eng.svg'
import es from 'images/es.svg'
import { Container, Button } from './styles'

export default function LanguageSwitch ({ onChange }) {
  return (
    <Container>
      <Button
        type='button'
        aria-label='Português brasileiro'
        onClick={() => {
          onChange('pt')
          window.location.reload()
        }}
      >
        <Image src={br} alt='Português brasileiro' />
      </Button>
      <Button
        type='button'
        aria-label='Inglês'
        onClick={() => {
          onChange('en')
          window.location.reload()
        }}
      >
        <Image src={eng} alt='Inglês' />
      </Button>
      <Button
        type='button'
        aria-label='Espanhol'
        onClick={() => {
          onChange('es')
          window.location.reload()
        }}
      >
        <Image src={es} alt='Espanhol' />
      </Button>
    </Container>
  )
}

LanguageSwitch.propTypes = {
  /** Callback quando o idioma for alterado */
  onChange: PropTypes.func.isRequired
}
