import React from 'react'
import Container from 'components/Container'
import FooterLinks from 'components/FooterLinks'
import FooterInfos from 'components/FooterInfos'
import FooterLogo from 'components/FooterLogo'
import { FooterStyled, Content } from './styles'

export default function Footer () {
  return (
    <FooterStyled>
      <Container size='medium'>
        <Content>
          <div>
            <FooterLinks />
          </div>
          <div>
            <FooterInfos />
          </div>
          <div>
            <FooterLogo />
          </div>
        </Content>
      </Container>
    </FooterStyled>
  )
}
