import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const FooterStyled = styled.div`
  background-color: ${props => props.theme.colors.dark};
  padding: 45px 0;
  margin-top: auto;
`

export const Content = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    align-items: center;
    justify-content: space-between;
  `}

  ${mediaQuery.lessThan('large')`
    > * + * {
      margin-top: 40px;
    }
  `}
`
