Suporta todas as props do Button, mas com as cores secundárias já definidas:

```
<ButtonSecondary>
  Botão Secundário
</ButtonSecondary>

<ButtonSecondary arrow style={{ marginLeft: 5 }}>
  Botão Secundário
</ButtonSecondary>
```
