import React from 'react'
import Button from 'components/Button'

export default function ButtonSecondary (props) {
  return (
    <Button
      color='secondary'
      textColor='text'
      {...props}
    />
  )
}
