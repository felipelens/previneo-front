import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { omit } from 'ramda'
import Base from 'components/Base'
import mediaQuery from 'utils/mediaQuery'
import { componentType } from 'types'

const filterProps = omit([
  'color',
  'size',
  'align',
  'border'
])

export const SectionTitleStyled = styled(props => <Base {...filterProps(props)} />)`
  display: block;
  font-family: ${props => props.theme.fonts.secondary};
  font-weight: normal;
  font-size: 35px;
  color: ${props => props.theme.colors[props.color]};
  text-align: ${props => props.align};
  margin: 0;
  padding: 0;
  box-sizing: border-box;

  ${mediaQuery.greaterThan('large')`
    font-size: ${props => props.size};
  `}

  ${props => props.border && css`
    &::after {
      content: ' ';
      display: block;
      background-color: #000;
      width: 45px;
      height: 3px;
      ${props => props.align === 'center' && 'margin: 0 auto;'}
      margin-top: 15px;
    }
  `}
`

export default function SectionTitle (props) {
  return <SectionTitleStyled {...props} />
}

SectionTitle.propTypes = {
  /** Título */
  children: PropTypes.any.isRequired,

  /** Cor */
  color: PropTypes.string,

  /** Componente */
  component: componentType,

  /** Tamanho da fonte */
  size: PropTypes.string,

  /** Alinhamento */
  align: PropTypes.string,

  /** Adiciona uma pequena borda em baixo do título */
  border: PropTypes.bool
}

SectionTitle.defaultProps = {
  component: 'h2',
  color: 'text',
  size: '50px',
  align: 'left',
  border: false
}
