Título para seções

```
<SectionTitle>Exemplo de título</SectionTitle>
```

Por padrão, ele renderiza com o elemento `h2`, mas é possível mudá-lo através da prop `component`. 

Abaixo, um exemplo renderizando `span` ao invés de `h2`:

```
<SectionTitle component='span'>Eu sou um span</SectionTitle>
```

É possível mudar a cor e o tamanho do SectionTitle:

```
<SectionTitle color='primary'>Tenho uma cor diferente</SectionTitle>
```
```
<SectionTitle size='70px'>Também tenho tamanho diferente</SectionTitle>
```

Alinhamento:

```
<SectionTitle align='center'>Ao centro</SectionTitle>
<SectionTitle align='right'>À direita</SectionTitle>
```

Também é possível adicionar uma borderzinha:

```
<SectionTitle border>Olá, eu sou um título</SectionTitle>
<SectionTitle align='center' border>Olá, eu sou um título centralizado</SectionTitle>
```
