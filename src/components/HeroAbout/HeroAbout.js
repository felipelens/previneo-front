import React from 'react'
import HeroBase from 'components/HeroBase'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import Button from 'components/Button'
import hero from 'images/hero-about.jpg'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import routes from 'routes'

export default function HeroAbout () {
  return (
    <HeroBase
      bgImage={hero}
      maxWidth={600}
      minHeight={643}
    >
      <VerticalSpacer space={25}>
        <SectionTitle>
          <FormattedHTMLMessage id='section.about.hero.title' />
        </SectionTitle>
        <Text size='18px'>
          <FormattedMessage id='section.about.hero.description' />
        </Text>
        <Button component={Link} to={routes.site.specialities.index} arrow>
          <FormattedMessage id='section.about.hero.button' />
        </Button>
      </VerticalSpacer>
    </HeroBase>
  )
}
