import React from 'react'
import { Link } from 'react-router-dom'
import Container from 'components/Container'
import SectionTitle from 'components/SectionTitle'
import CarouselPosts from 'components/CarouselPosts'
import Button from 'components/Button'
import routes from 'routes'
import { FormattedMessage } from 'react-intl'
import { Section, Header } from './styles'
import Resource from 'containers/Resource'

export default function SectionBlog () {
  return (
    <Section>
      <Container size='medium'>
        <Header>
          <SectionTitle>
            <FormattedMessage id='section.home.blog.title' />
          </SectionTitle>
          <Button component={Link} to={routes.site.blog.index} arrow>
            <FormattedMessage id='section.home.blog.button' />
          </Button>
        </Header>
      </Container>
      <Resource resource='PostsSite' autoFetch params={{ limit: 10 }}>
        {props => (
          <CarouselPosts posts={props.records} />
        )}
      </Resource>
    </Section>
  )
}
