import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Section = styled.section`
  background-color: ${props => props.theme.colors.background};
  padding-top: 45px;
  padding-bottom: 40px;

  ${mediaQuery.greaterThan('large')`
    padding: 100px 0;  
  `}
`

export const Header = styled.header`
  margin-bottom: 40px;

  ${mediaQuery.greaterThan('medium')`
    display: flex;
    align-items: center;
    justify-content: space-between;
  `}

  ${mediaQuery.lessThan('medium')`
    > * + * {
      margin-top: 15px;
    }
  `}
`
