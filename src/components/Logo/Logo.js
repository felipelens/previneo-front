import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Image from 'components/Image'
import logo from 'images/logo.svg'
import logoWhite from 'images/logo-white.svg'
import routes from 'routes'

const LogoLink = styled(Link)`
  display: block;

  &:hover {
    opacity: 0.7;
  }
`

export default function Logo ({ white, ...props }) {
  return (
    <LogoLink to={routes.site.home} title='Home'>
      <Image src={white ? logoWhite : logo} alt={'Home'} {...props} />
    </LogoLink>
  )
}

Logo.propTypes = {
  /** Usa a versão branca do logo */
  white: PropTypes.bool
}
