import React from 'react'
import PropTypes from 'prop-types'
import Container from 'components/Container'
import { HeroStyled, Content, Video } from './styles'

export default function HeroBase ({ children, maxWidth, minHeight, paddingTop = 0, ...props }) {
  return (
    <HeroStyled minHeight={minHeight} {...props}>
      <Container>
        <Content
          maxWidth={maxWidth}
          minHeight={minHeight}
          paddingTop={paddingTop}
        >
          {children}
        </Content>
      </Container>
      {props.isHome &&
        <Video autoPlay loop muted cover='https://d2mzh3k4dky77a.cloudfront.net/front/hero.jpg'>
          <source src='https://d2mzh3k4dky77a.cloudfront.net/front/hero.webm' type='video/webm' />
          <source src='https://d2mzh3k4dky77a.cloudfront.net/front/hero.mp4' type='video/mp4' />
        </Video>
      }
    </HeroStyled>
  )
}

HeroBase.propTypes = {
  /** Background image */
  bgImage: PropTypes.string,

  /** Largura máxima */
  maxWidth: PropTypes.number.isRequired,

  /** Altura mínima */
  minHeight: PropTypes.number.isRequired
}
