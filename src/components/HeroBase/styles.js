import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const HeroStyled = styled.div`
  background-color: ${props => props.theme.colors.background};
  box-sizing: border-box;
  position: relative;
  overflow: hidden;

  ${mediaQuery.greaterThan('large')`
    background-image: url(${props => props.bgImage});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  `}

  ${props => props.isHome && mediaQuery.lessThan('large')`
    background-image: url(${props.bgImage});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  `}

  ${mediaQuery.greaterThan('large')`
    position: relative;
    min-height: ${props => props.minHeight};
    overflow: hidden;
  `}
`

export const Content = styled.section`
  max-width: ${props => props.maxWidth}px;
  box-sizing: border-box;
  z-index: 1;

  ${mediaQuery.greaterThan('large')`
    display: flex;
    align-items: center;
    position: relative;
    height: ${props => props.minHeight}px;
    z-index: 2;
    padding-top: ${props => props.paddingTop}px;
  `}

  ${mediaQuery.lessThan('large')`
    padding: 45px 0;
  `}
`

export const Video = styled.video`
  min-width: 100%;
  min-height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 0;
  ${mediaQuery.lessThan('large')`
    display: none;
  `}
`
