```
<Input />
```

Com textarea:

```
<Input component='textarea' rows={5} />
```

Com helper tooltip:

```
<Input
  tooltip='Conteúdo qualquer do tooltip'
/>
```
