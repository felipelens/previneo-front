import React from 'react'
import HeroBase from 'components/HeroBase'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Button from 'components/Button'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import routes from 'routes'
import hero from 'images/hero-home-mobile.jpg'

export default function HeroHome () {
  return (
    <HeroBase
      maxWidth={470}
      minHeight={762}
      paddingTop={80}
      bgImage={hero}
      isHome
    >
      <VerticalSpacer space={45}>
        <SectionTitle>
          <FormattedHTMLMessage id='section.home.hero.title' />
        </SectionTitle>
        <Button component={Link} to={routes.site.about} arrow>
          <FormattedMessage id='section.home.hero.button' />
        </Button>
      </VerticalSpacer>
    </HeroBase>
  )
}
