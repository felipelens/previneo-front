import React, { Component } from 'react'
import styled from 'styled-components'
import { Switch, Route } from 'react-router-dom'
import Header from 'components/Header'
import Footer from 'components/Footer'
import * as Screens from 'screens'
import routes from 'routes'
import Storage from '../../modules/Storage'
import { connect } from 'react-redux'
import { setLang } from 'store/ducks/localization'

const SiteContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh; 
`

class Site extends Component {
  componentDidMount () {
    const storageLanguage = Storage.get('lang', 'pt')
    this.props.setLang(storageLanguage)
  }

  render () {
    return (
      <SiteContainer>
        <Header floated={this.props.location.pathname === routes.site.home} />
        <div>
          <Switch>
            <Route exact path={routes.site.home} component={Screens.HomeScreen} />
            <Route exact path={routes.site.about} component={Screens.AboutScreen} />
            <Route exact path={routes.site.specialities.index} component={Screens.SpecialitiesScreen} />
            <Route exact path={routes.site.specialities.show} component={Screens.SpecialityScreen} />
            <Route exact path={routes.site.customers} component={Screens.CustomersScreen} />
            <Route exact path={routes.site.blog.index} component={Screens.BlogIndexScreen} />
            <Route exact path={routes.site.blog.post} component={Screens.BlogPostScreen} />
            <Route exact path={routes.site.blog.category} component={Screens.BlogCategoryScreen} />
            <Route exact path={routes.site.contact} component={Screens.ContactScreen} />
            <Route component={Screens.NotFoundScreen} />
          </Switch>
        </div>
        <Footer />
      </SiteContainer>
    )
  }
}

export default connect(
  ({ localization }) => ({ lang: localization.lang }),
  { setLang }
)(Site)
