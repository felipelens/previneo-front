```
<CardDoctorGroup>
  <CardDoctor
    image={require('images/doctor.jpg')}
    name='Dr. Hélio Rubens de Oliveira Filho'
    text='Médico formado pela UFPR. 18 anos de experiência em medicina corporativa e programas de prevenção, com foco na redução da sinistralidade no benefício saúde; tendo atendido clientes como HSBC, Citibank, Kimberly-Clark, Bimbo, Amil, AON, Optum.'
  />
  <CardDoctor
    image={require('images/doctor.jpg')}
    name='Dr. Hélio Rubens de Oliveira Filho'
    text='Médico formado pela UFPR. 18 anos de experiência em medicina corporativa e programas de prevenção, com foco na redução da sinistralidade no benefício saúde; tendo atendido clientes como HSBC, Citibank, Kimberly-Clark, Bimbo, Amil, AON, Optum.'
  />
  <CardDoctor
    image={require('images/doctor.jpg')}
    name='Dr. Hélio Rubens de Oliveira Filho'
    text='Médico formado pela UFPR. 18 anos de experiência em medicina corporativa e programas de prevenção, com foco na redução da sinistralidade no benefício saúde; tendo atendido clientes como HSBC, Citibank, Kimberly-Clark, Bimbo, Amil, AON, Optum.'
  />
</CardDoctorGroup>
```
