import React from 'react'
import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

const Container = styled.section`
  > * + * {
    margin-top: 150px;
  }
  ${mediaQuery.lessThan('large')`
    > * + * {
      margin-top: 50px;
    }
  `}
`

export default function CardDoctorGroup ({ ...props }) {
  return (
    <Container {...props} />
  )
}
