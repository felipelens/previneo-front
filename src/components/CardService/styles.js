import styled from 'styled-components'
import { Link } from 'react-router-dom'
import mediaQuery from 'utils/mediaQuery'

export const Container = styled.section`
  display: block;
  border: 1px solid ${props => props.theme.colors.border};
  border-radius: 3px;
  box-sizing: border-box;

  ${mediaQuery.greaterThan('large')`
    display: flex;
  `}
`

export const Content = styled.div`
  padding: 20px;
`

export const Button = styled.div`
  display: none;
  background-color: ${props => props.color};
  color: #fff;
  padding: 15px;
  font-family: ${props => props.theme.fonts.primary};
  font-weight: bold;
  font-size: 14px;
  position: absolute;
  width: calc(100% + 2px);
  box-sizing: border-box;
  top: 100%;
  margin-top: -2px;
  left: -1px;
  border-radius: 0 0 3px 3px;
`

export const LinkStyled = styled(Link)`
  display: block;
  text-decoration: none;
  box-sizing: border-box;
  position: relative;
  transition: .2s;

  &:hover {
    background: ${props => props.color};
    border: 1px solid ${props => props.color};

    ${Content} > * {
      color: #fff;
    }

    img {
      filter: brightness(0) invert(1);
    }

    ${mediaQuery.greaterThan('large')`
      ${Button} {
        display: block;
      }
    `}
  }
`

export const Title = styled.h3`
  display: block;
  font-family: ${props => props.theme.fonts.secondary};
  font-weight: normal;
  font-size: 40px;
  color: ${props => props.theme.colors.text};
  margin: 0;
  margin-top: 20px;
  margin-bottom: 5px;

  ${mediaQuery.lessThan('medium')`
    font-size: 30px;
  `}
`
