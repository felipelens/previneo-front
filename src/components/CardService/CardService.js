import React from 'react'
import PropTypes from 'prop-types'
import Image from 'components/Image'
import Text from 'components/Text'
import * as styles from './styles'
import getRoute from 'utils/getRoute'
import routes from 'routes'
import { hoverColor } from 'utils/colors'
import { FormattedMessage } from 'react-intl'

export default function CardService ({ title, image, legenda, id, color, ...props }) {
  return (
    <styles.Container>
      <styles.LinkStyled to={getRoute(routes.site.specialities.show, { id })} color={color}>
        <styles.Content>
          <Image src={image} alt={title} />
          <styles.Title>{title}</styles.Title>
          <Text>
            <span dangerouslySetInnerHTML={{ __html: legenda.replace(/<(?:.|\n)*?>/gm, '').substr(0, 80) + '...' }} />
          </Text>
        </styles.Content>
        <styles.Button color={hoverColor(color)}>
          <FormattedMessage id='section.about.hero.button' />
        </styles.Button>
      </styles.LinkStyled>
    </styles.Container>
  )
}

CardService.propTypes = {
  /** ID */
  id: PropTypes.any.isRequired,

  /** Imagem */
  image: PropTypes.string.isRequired,

  /** Título */
  title: PropTypes.string.isRequired,

  /** Descrição */
  description: PropTypes.string.isRequired,

  /** Cor do hover */
  color: PropTypes.string.isRequired
}
