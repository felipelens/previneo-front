Este é um componente de Base que pode se transformar em outro componente ou elemento através da prop `component`.

```
const CustomComponent = props => <span style={{ color: 'red' }} {...props} />  

;<React.Fragment>
  <Base component='span'>
    Isto é um span
  </Base>
  <Base component='h1'>
    Isto é um h1
  </Base>
  <Base component={CustomComponent}>
    Isto é um componente react
  </Base>
</React.Fragment>
```
