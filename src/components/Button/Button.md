```
<Button>
  Click me
</Button>
```

```
<Button size='small'>
  Click me
</Button>
```

```
<Button arrow>
  Click me
</Button>
```

```
<Button block>
  Full button
</Button>

<Button block arrow style={{ marginTop: 15 }}>
  Full button
</Button>
```

Botão desabilitado:

```
<Button disabled>
  Botão Desabilitado
</Button>
```

### Component

Por padrão, o `Button` sempre renderiza um elemento `<button>`, mas como em vários outros componentes
da Previneo, é possível alterá-lo através da prop `component`.

Abaixo, alguns exemplos do Button renderizando
outros componentes e elementos:

```
<React.Fragment>
  <Button href='https://google.com' component='a'>
    Renderiza uma tag a
  </Button>
</React.Fragment>
```

### Estilo

```
<Button color='black'>
  Click me
</Button>
```

```
<Button color='white' textColor='black' arrow>
  Click me
</Button>
```
