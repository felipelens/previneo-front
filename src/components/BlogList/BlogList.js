import React from 'react'
import PropTypes from 'prop-types'
import CardPost from 'components/CardPost'
import Text from 'components/Text'
import { Grid } from './styles'
import { FormattedMessage } from 'react-intl'

export default function BlogList ({ posts }) {
  if (!posts.length) {
    return <Text><FormattedMessage id='admin.common.noResultsFound' /></Text>
  }

  return (
    <Grid>
      {posts.map(post => (
        <CardPost
          key={post.id}
          id={post.id}
          permalink={post.permalink}
          title={post.titulo}
          content={post.descricao}
          thumbnail={post.capa.url}
          category={{
            id: post.categoria.id,
            name: post.categoria.nome
          }}
        />
      ))}
    </Grid>
  )
}

BlogList.propTypes = {
  /** Lista de posts */
  posts: PropTypes.array.isRequired
}
