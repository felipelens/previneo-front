import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Grid = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    flex-wrap: wrap;
    margin: -15px;

    > * {
      width: calc(33.33% - 30px);
      margin: 15px;
    }
  `}

  ${mediaQuery.lessThan('large')`
    > * + * {
      margin-top: 30px;
    }
  `}
`
