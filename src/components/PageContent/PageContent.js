import React from 'react'
import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

const PageContentStyled = styled.div`
  padding: ${props => !props.blog ? '45px 0' : '0'};

  ${mediaQuery.greaterThan('medium')`
    padding: ${props => !props.blog ? '65px 0' : '0'};
  `}
`

export default function PageContent (props) {
  return (
    <PageContentStyled {...props} />
  )
}
