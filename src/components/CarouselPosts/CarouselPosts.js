import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Carousel from 'components/Carousel'
import CardPost from 'components/CardPost'
import Icon from 'components/Icon'
import theme from 'theme'
import { Container, Button, Slide } from './styles'

export default class CarouselPosts extends Component {
  static propTypes = {
    posts: PropTypes.array.isRequired
  }

  render () {
    return (
      <Container>
        {this.props.posts.length > 5 && (
          <React.Fragment>
            <Button onClick={() => this.carousel.slickPrev()}>
              <Icon icon='arrow-left' />
            </Button>
            <Button onClick={() => this.carousel.slickNext()} next>
              <Icon icon='arrow-right' />
            </Button>
          </React.Fragment>
        )}
        <Carousel
          innerRef={carousel => { this.carousel = carousel }}
          arrows={false}
          wrapSlides={false}
          slidesToShow={5}
          infinite={this.props.posts.length > 5}
          responsive={[
            {
              breakpoint: parseInt(theme.breakpoints.large, 10),
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: parseInt(theme.breakpoints.medium, 10),
              settings: {
                slidesToShow: 1
              }
            }
          ]}
        >
          {this.props.posts.map(post => (
            <Slide key={post.id}>
              <CardPost
                key={post.id}
                id={post.id}
                title={post.titulo}
                content={post.descricao}
                thumbnail={post.capa.url}
                category={{
                  id: post.categoria.id,
                  name: post.categoria.nome
                }}
              />
            </Slide>
          ))}
        </Carousel>
      </Container>
    )
  }
}
