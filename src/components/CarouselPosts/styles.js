import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Slide = styled.div`
  padding: 5px 20px;
  box-sizing: border-box;
`

export const Container = styled.div`
  position: relative;
  max-width: 1920px;
  margin: 0 auto;

  ${mediaQuery.greaterThan('large')`
    &::before,
    &::after {
      content: ' ';
      position: absolute;
      width: 600px;
      height: 100%;
      top: 0;
      pointer-events: none;
      z-index: 100;  
    }

    &::after {
      right: 0;
      background: linear-gradient(90deg, rgba(255,255,255,0), ${props => props.theme.colors.background} 80%);
    }

    &::before {
      left: 0;
      background: linear-gradient(270deg, rgba(255,255,255,0), ${props => props.theme.colors.background} 80%);  
    }
  `}

  ${mediaQuery.lessThan('large')`
    padding: 0 45px;
  `}
`

export const Button = styled.button.attrs({ type: 'button' })`
  position: absolute;
  top: 50%;
  ${props => props.next ? 'right' : 'left'}: 15px;
  transform: translateY(-50%);
  background-color: transparent;
  border: none;
  cursor: pointer;
  font-size: 20px;
  z-index: 150;
  
  ${mediaQuery.greaterThan('large')`
    font-size: 30px;
    ${props => props.next ? 'right' : 'left'}: 60px;
  `}
`
