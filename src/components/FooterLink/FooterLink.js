import React from 'react'
import Link from 'components/Link'

export default function FooterLink (props) {
  return (
    <div>
      <Link
        color='white'
        hoverColor='primary'
        {...props}
      />
    </div>
  )
}
