import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import Button from 'components/Button'
import VerticalSpacer from 'components/VerticalSpacer'
import Container from 'components/Container'
import CardCustomerGroup from 'components/CardCustomerGroup'
import CardCustomer from 'components/CardCustomer'
import Resource from 'containers/Resource'
import routes from 'routes'
import { Section, Content, Column } from './styles'

export default function SectionCustomer () {
  return (
    <Section>
      <Container size='small'>
        <Content>
          <Column>
            <VerticalSpacer space={30}>
              <SectionTitle>
                <FormattedMessage id='section.home.customers.title' />
              </SectionTitle>
              <Text size='21px'>
                <FormattedMessage id='section.home.customers.description' />
              </Text>
              <Button component={Link} to={routes.site.customers} arrow>
                <FormattedMessage id='section.home.customers.button' />
              </Button>
            </VerticalSpacer>
          </Column>
          <Column>
            <Resource resource='CustomersSite' autoFetch params={{ limit: 6 }} spinner>
              {props => (
                <CardCustomerGroup>
                  {props.records.map(record => (
                    <CardCustomer
                      key={record.id}
                      title={record.nome}
                      image={record.logo && record.logo.url ? record.logo.url : ''}
                      url={record.url}
                    />
                  ))}
                </CardCustomerGroup>
              )}
            </Resource>
          </Column>
        </Content>
      </Container>
    </Section>
  )
}
