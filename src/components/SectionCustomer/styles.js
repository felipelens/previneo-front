import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Section = styled.section`
  padding: 45px 0;

  ${mediaQuery.greaterThan('large')`
    padding: 100px 0;
    padding-bottom: 50px;
  `}
`

export const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`

export const Column = styled.div`
  ${mediaQuery.greaterThan('large')`
    width: 50%;
    &:first-child {
      max-width: 430px;
    }
  `}

  ${mediaQuery.lessThan('large')`
    width: 100%;
    + * {
      margin-top: 35px;
    }
  `}
`
