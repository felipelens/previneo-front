import React from 'react'
import routes from 'routes'
import Text from 'components/Text'
import FooterLink from 'components/FooterLink'
import { FormattedMessage } from 'react-intl'

export default function FooterLinks () {
  return (
    <Text component='div' size='18px'>
      <FooterLink to={routes.site.home}>
        <FormattedMessage id='site.menu.home' />
      </FooterLink>
      <FooterLink to={routes.site.about}>
        <FormattedMessage id='site.menu.about' />
      </FooterLink>
      <FooterLink to={routes.site.specialities.index}>
        <FormattedMessage id='site.menu.specialities' />
      </FooterLink>
      <FooterLink to={routes.site.customers}>
        <FormattedMessage id='site.menu.customers' />
      </FooterLink>
      <FooterLink to={routes.site.blog.index}>
        <FormattedMessage id='site.menu.blog' />
      </FooterLink>
      <FooterLink to={routes.site.contact}>
        <FormattedMessage id='site.menu.contact' />
      </FooterLink>
    </Text>
  )
}
