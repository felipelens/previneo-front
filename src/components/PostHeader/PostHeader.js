import React from 'react'
import PropTypes from 'prop-types'
import { Header } from './styles'
import Container from 'components/Container'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import PostCategory from 'components/PostCategory'
import Text from 'components/Text'
import { format } from 'modules/Date'

export default function PostHeader ({ title, category, date, ...props }) {
  return (
    <Header {...props}>
      <Container size='small'>
        <VerticalSpacer space={15}>
          <PostCategory
            id={category.id}
            name={category.name}
          />
          <SectionTitle size='60px'>{title}</SectionTitle>
          <Text color='textGray'>
            {format(date, 'DD [de] MMMM [de] YYYY')}
          </Text>
        </VerticalSpacer>
      </Container>
    </Header>
  )
}

PostHeader.propTypes = {
  /** Título do post */
  title: PropTypes.string.isRequired,

  /** Categoria do post */
  category: PropTypes.shape({
    id: PropTypes.any.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,

  /** Data de publicação do post */
  date: PropTypes.instanceOf(Date).isRequired
}
