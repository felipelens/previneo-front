import styled from 'styled-components'

export const Header = styled.header`
  background-color: ${props => props.theme.colors.background};
  padding: 45px 0;
`
