import React from 'react'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl'
import PropTypes from 'prop-types'
import Container from 'components/Container'
import SectionTitle from 'components/SectionTitle'
import Testimonies from 'components/Testimonies'
import VerticalSpacer from 'components/VerticalSpacer'
import Text from 'components/Text'
import Resource from 'containers/Resource'
import { Section, Content, TitleContainer } from './styles'

export default function SectionTestimonies ({ internal, ...props }) {
  return (
    <Section {...props} internal={internal}>
      <Container size='medium'>
        <Content>
          <TitleContainer>
            <VerticalSpacer>
              {internal
                ? (
                  <SectionTitle align='inherit'>
                    <FormattedHTMLMessage id='section.home.testmonies.internal.title' />
                  </SectionTitle>
                ) : (
                  <SectionTitle align='inherit'>
                    <FormattedMessage id='section.home.testmonies.title' />
                  </SectionTitle>
                )
              }
              {internal && (
                <Text align='inherit'>
                  <FormattedHTMLMessage id='section.home.testmonies.internal.subtitle' />
                </Text>
              )}
            </VerticalSpacer>
          </TitleContainer>
          <div>
            <Resource
              resource='TestimoniesSite'
              autoFetch
              params={{ limit: 5, visivel: 1 }}
              spinner
            >
              {props => {
                if (!props.records.length > 0) {
                  return <Text><FormattedHTMLMessage id='admin.common.noResultsFound' /></Text>
                }

                return (
                  <Testimonies
                    internal={internal}
                    data={props.records.map(record => ({
                      id: record.id,
                      author: record.nome,
                      company: {
                        name: record.empresa.nome,
                        logo: record.empresa.logo && record.empresa.logo.url
                          ? record.empresa.logo.url
                          : ''
                      },
                      testimony: record.descricao
                    }))}
                  />
                )
              }}
            </Resource>
          </div>
        </Content>
      </Container>
    </Section>
  )
}

SectionTestimonies.propTypes = {
  /** Localização da seção */
  internal: PropTypes.bool
}
