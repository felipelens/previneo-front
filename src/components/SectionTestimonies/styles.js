import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Section = styled.section`
  padding-bottom: 35px;
  background: ${props => props.internal ? props.theme.colors.background : 'transparent'};
  ${mediaQuery.greaterThan('large')`
    padding: ${props => props.internal ? '100px 0' : '50px 0 100px'};
  `}
`

export const Content = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    justify-content: space-between;

    > :last-child {
      flex-grow: 1;
    }
  `}

  ${mediaQuery.lessThan('large')`
    > :first-child {
      margin-bottom: 15px;
    }
  `}
`

export const TitleContainer = styled.div`
  width: 450px;
  max-width: 100%;
  flex-shrink: 0;
  margin-right: 5%;
  ${mediaQuery.greaterThan('large')`
    text-align: right;
  `}
  ${mediaQuery.lessThan('large')`
    padding-top: 45px;
  `}
`
