import React from 'react'
import { FormattedHTMLMessage } from 'react-intl'
import SectionTitle from 'components/SectionTitle'
import Container from 'components/Container'

export default function SectionPhraseAbout () {
  return (
    <Container>
      <SectionTitle size='72px' color='primary'>
        <FormattedHTMLMessage id='section.phrase.title' />
      </SectionTitle>
    </Container>
  )
}
