import React from 'react'
import { injectIntl, FormattedHTMLMessage } from 'react-intl'
import Container from 'components/Container'
import Text from 'components/Text'
import CardStepGroup from 'components/CardStepGroup'
import CardStep from 'components/CardStep'
import SectionTitle from 'components/SectionTitle'
import VerticalSpacer from 'components/VerticalSpacer'
import { About, Content } from './styles'

function SectionStepsAbout ({ intl, ...props }) {
  return (
    <About {...props}>
      <Container>
        <Content>
          <VerticalSpacer space={35}>
            <SectionTitle color={'primary'} size={'60px'}>
              <FormattedHTMLMessage id='section.about.steps.title' />
            </SectionTitle>
            <Text lineHeight={2}>
              <FormattedHTMLMessage id='section.about.steps.description' />
            </Text>
          </VerticalSpacer>
        </Content>
        <CardStepGroup lift={190}>
          <CardStep
            label={intl.formatMessage({ id: 'section.about.steps.stepone.step' })}
            description={intl.formatMessage({ id: 'section.about.steps.stepone.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.about.steps.steptwo.step' })}
            description={intl.formatMessage({ id: 'section.about.steps.steptwo.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.about.steps.stepthree.step' })}
            description={intl.formatMessage({ id: 'section.about.steps.stepthree.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.about.steps.stepfour.step' })}
            description={intl.formatMessage({ id: 'section.about.steps.stepfour.description' })}
          />
          <CardStep
            label={intl.formatMessage({ id: 'section.about.steps.stepfive.step' })}
            description={intl.formatMessage({ id: 'section.about.steps.stepfive.description' })}
          />
        </CardStepGroup>
      </Container>
    </About>
  )
}

export default injectIntl(SectionStepsAbout)
