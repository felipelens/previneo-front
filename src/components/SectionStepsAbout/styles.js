import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'
import background from 'images/bg-steps-about.jpg'

export const About = styled.section`
  background-color: ${props => props.theme.colors.background};

  ${mediaQuery.greaterThan('large')`
    height: 660px;
    background: url(${background}) center no-repeat;
    margin: 100px 0 100px;
  `}

  ${mediaQuery.lessThan('large')`
    padding: 45px 0;
  `}
`

export const Content = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    height: 660px;
    padding-top: 130px;
    box-sizing: border-box;
    > div {
      max-width: 510px;
    }
  `}
`
