import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'
import background from 'images/section-partner.jpg'

export const Section = styled.section`
  background-color: ${props => props.theme.colors.primary};
  ${mediaQuery.greaterThan('large')`
    background: url(${background}) center no-repeat;
  `}
`

export const Content = styled.div`
  ${mediaQuery.greaterThan('large')`
    height: 500px;
    display: flex;
    align-items: center;
    justify-content: flex-end;

    > * {
      max-width: 600px;
    }
  `}

  ${mediaQuery.lessThan('large')`
    padding: 45px 0;
  `}
`
