import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import Container from 'components/Container'
import Text from 'components/Text'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import ButtonSecondary from 'components/ButtonSecondary'
import routes from 'routes'
import { Section, Content } from './styles'

export default function SectionPartner () {
  return (
    <Section>
      <Container size='medium'>
        <Content>
          <VerticalSpacer>
            <SectionTitle color='white'>
              <FormattedMessage id='section.partner.title' />
            </SectionTitle>
            <Text size='21px' color='white'>
              <FormattedMessage id='section.partner.description' />
            </Text>
            <ButtonSecondary component={Link} to={routes.site.contact} arrow>
              <FormattedMessage id='section.partner.button' />
            </ButtonSecondary>
          </VerticalSpacer>
        </Content>
      </Container>
    </Section>
  )
}
