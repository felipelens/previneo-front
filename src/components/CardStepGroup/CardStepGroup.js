import React from 'react'
import styled from 'styled-components'
import Media from 'react-media'
import Carousel from 'components/Carousel'
import mediaQuery from 'utils/mediaQuery'
import theme from 'theme'

const Container = styled.div`
  ${mediaQuery.greaterThan('large')`
    display: flex;
    margin: 0 -15px;
    margin-top: -${props => props.lift}px;

    > * {
      flex-grow: 1;
      flex-basis: 0;
      margin: 0 15px;
    }  
  `}

  ${mediaQuery.lessThan('large')`
      margin-top: 35px;
  `}
`

export default function CardStepGroup (props) {
  return (
    <Media query={`(min-width: ${theme.breakpoints.large})`}>
      {matches => matches ? (
        <Container {...props} />
      ) : (
        <Container>
          <Carousel>
            {props.children}
          </Carousel>
        </Container>
      )}
    </Media>
  )
}
