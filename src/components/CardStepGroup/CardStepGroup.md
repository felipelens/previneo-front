```
const CardStep = require('components/CardStep').default

;<CardStepGroup>
  <CardStep
    label='Etapa 1'
    title='Identificação'
    description='Anamnese online ou presencial'
  />
  <CardStep
    label='Etapa 2'
    title='Rastreamento'
    description='Utilização de algoritmos para estratificação da população de risco'
  />
  <CardStep
    label='Etapa 3'
    title='Orientação'
    description='Contato e orientação'
  />
  <CardStep
    label='Etapa 4'
    title='Monitoramento'
    description='Elaboração de estratégias para redução de risco'
  />
</CardStepGroup>
```
