import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'
import quotes from 'images/quotes.svg'

export const Container = styled.div`
  padding: 20px 0;
  `

export const Testimony = styled.div`
  background: ${props => props.internal ? props.theme.colors.white : props.theme.colors.background};
  border-radius: 3px;
  padding: 25px;
  position: relative;

  &::before {
    content: ' ';
    position: absolute;
    top: -15px;
    width: 50px;
    height: 34px;
    background: url(${quotes}) center no-repeat;
    background-size: cover;
  }

  &::after {
    content: ' ';
    position: absolute;
    top: 100%;
    right: 30px;
    width: 0; 
    height: 0; 
    border-left: 7px solid transparent;
    border-right: 7px solid transparent;
    border-top: 10px solid ${props => props.theme.colors.background};
  }
`

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 15px;
  align-items: flex-start;

  ${mediaQuery.lessThan('medium')`
    justify-content: flex-end;

    img {
      display: none;
    }
  `}
`

export const Author = styled.strong`
  display: block;
  font-family: ${props => props.theme.fonts.secondary};
  font-weight: normal;
  font-size: 25px;
`

export const Company = styled.span`
  display: block;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 16px;
  text-align: right;
  font-weight: 500;
`
