Card para depoimento de clientes.

```
<CardTestimony
  author='Dr. Rodolfo Milani'
  company={{
    name: 'Aon Hewitt',
    logo: require('images/aon.png')
  }}
  testimony='Competência, ética e rigor técnico por um lado, flexibilidade e compreensão das necessidades dos clientes por outro são qualidades que experimentei ao trabalhar com a PreviNEO.'
/>
```

