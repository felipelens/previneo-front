import React from 'react'
import PropTypes from 'prop-types'
import Text from 'components/Text'
import Image from 'components/Image'
import {
  Container,
  Testimony,
  Footer,
  Author,
  Company
} from './styles'

export default function CardTestimony ({ author, company, testimony, ...props }) {
  return (
    <Container>
      <Testimony {...props}>
        <Text size='18px'>
          {testimony}
        </Text>
      </Testimony>
      <Footer>
        <Image src={company.logo} alt={company.name} width={80} />
        <div>
          <Author>{author}</Author>
          <Company>{company.name}</Company>
        </div>
      </Footer>
    </Container>
  )
}

CardTestimony.propTypes = {
  /** Nome do autor do depoimento */
  author: PropTypes.string.isRequired,

  /** Dados da empresa */
  company: PropTypes.shape({
    name: PropTypes.string.isRequired,
    logo: PropTypes.string.isRequired
  }).isRequired,

  /** Depoimento */
  testimony: PropTypes.string.isRequired
}
