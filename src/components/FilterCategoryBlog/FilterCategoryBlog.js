import React from 'react'
import styled from 'styled-components'
import PostCategory from 'components/PostCategory'
import Text from 'components/Text'
import Resource from 'containers/Resource'
import { FormattedMessage } from 'react-intl'

const Grid = styled.div`
  display: flex;
  align-self: flex-end;
  > * {
    margin-left: 10px;
  }
`

export default function FilterCategoryBlog () {
  return (
    <Resource resource='BlogCategoriesSite' autoFetch params={{ limit: 5 }}>
      {props => {
        return (
          <Grid>
            <Text><FormattedMessage id='admin.common.filter' /></Text>
            {props.records.map(cat => (
              <PostCategory
                key={cat.id}
                id={cat.id}
                name={cat.nome}
                filter={''}
              />
            ))}
          </Grid>
        )
      }}
    </Resource>
  )
}
