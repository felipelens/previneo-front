import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Content = styled.section`
  display: flex;
  flex-wrap: wrap;
`

export const Column = styled.div`
  &:first-child{
    display: flex;
    align-items: center;
    padding-right: 60px;
    ${mediaQuery.lessThan('large')`
      width: 100%;
      padding-right: 0;
      justify-content: center;
      margin-bottom: 30px;
    `}
  }
  &:last-child {
    flex: 1;
  }
  ${mediaQuery.lessThan('large')`
    width: 100%;
  `}
`

export const Figure = styled.figure`
  overflow: hidden;
  border-radius: 80px;
  padding: 0;
  margin: 0;
  align-self: flex-start;
`

export const Title = styled.h2`
  font-size: 45px;
  font-weight: 500;
  font-family: ${props => props.theme.fonts.secondary};
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  padding-bottom: 30px;
  position: relative;
  &::after {
    content: '';
    width: 50px;
    height: 3px;
    position: absolute;
    bottom: 0;
    left: 0;
    background-color: ${props => props.theme.colors.primary};
  }
  ${mediaQuery.lessThan('large')`
    font-size: 35px;
  `}
`

export const TextDoctor = styled.div`
  ${mediaQuery.greaterThan('large')`
    max-width: 545px;
  `}
`
