import React from 'react'
import PropTypes from 'prop-types'
import Image from 'components/Image'
import VerticalSpacer from 'components/VerticalSpacer'
import PostContent from 'components/PostContent'
import * as styles from './styles'

export default function CardDoctor ({ image, name, text, ...props }) {
  return (
    <styles.Content {...props}>
      <styles.Column>
        <styles.Figure>
          <Image src={image} alt={name} />
        </styles.Figure>
      </styles.Column>
      <styles.Column>
        <VerticalSpacer space={30}>
          <styles.Title>{name}</styles.Title>
          <styles.TextDoctor>
            <PostContent dangerouslySetInnerHTML={{ __html: text }} />
          </styles.TextDoctor>
        </VerticalSpacer>
      </styles.Column>
    </styles.Content>
  )
}

CardDoctor.propTypes = {
  /** Nome do Médico */
  name: PropTypes.string.isRequired,

  /** Imagem do Médico */
  image: PropTypes.string.isRequired,

  /** Descrição do Médico */
  text: PropTypes.string.isRequired
}
