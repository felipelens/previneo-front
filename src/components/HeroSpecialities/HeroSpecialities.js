import React from 'react'
import HeroBase from 'components/HeroBase'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import hero from 'images/hero-specialities.jpg'
import { FormattedHTMLMessage } from 'react-intl'

export default function HeroSpecialities () {
  return (
    <HeroBase
      bgImage={hero}
      maxWidth={560}
      minHeight={656}
      paddingTop={80}
    >
      <VerticalSpacer space={45}>
        <SectionTitle>
          <FormattedHTMLMessage id='section.specialities.hero.title' />
        </SectionTitle>
        <Text size='18px' component='div'>
          <FormattedHTMLMessage id='section.specialities.hero.description' />
        </Text>
      </VerticalSpacer>
    </HeroBase>
  )
}
