Este componente é usado no componente `<Field />` do react-final-form.

```
<CustomField
  label={'Nome'}
  meta={{}}
  input={{}}
/>
```

Exemplo com erro de validação:

```
<CustomField
  label={'Nome'}
  meta={{
    touched: true,
    error: 'Este campo é obrigatório'
  }}
  input={{}}
/>
```

Custom Field com Select:

```
<CustomField.Select
  label={'Nome'}
  meta={{}}
  input={{}}
>
  <option value='1'>Opção 1</option>
  <option value='2'>Opção 2</option>
  <option value='3'>Opção 3</option>
</CustomField.Select>
```
