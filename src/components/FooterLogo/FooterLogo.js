import React from 'react'
import styled from 'styled-components'
import Logo from 'components/Logo'
import FooterLink from 'components/FooterLink'
import Icon from 'components/Icon'
import mediaQuery from 'utils/mediaQuery'

const LinksContainer = styled.div`
  margin-top: 25px;
  display: flex;

  > * + * {
    margin-left: 10px;
  }

  ${mediaQuery.greaterThan('large')`
    justify-content: center;
  `}
`

export default function FooterLogo () {
  return (
    <div style={{ textAlign: 'center' }}>
      <Logo white />
      <LinksContainer>
        <FooterLink
          component='a'
          href='https://www.facebook.com/previneo/'
          title='Facebook da Previneo'
          target='_blank'
        >
          <Icon icon='facebook' size='30px' />
        </FooterLink>
        <FooterLink
          component='a'
          href='https://www.linkedin.com/company/previneo/'
          title='Linkedin da Previneo'
          target='_blank'
        >
          <Icon icon='linkedin' size='30px' />
        </FooterLink>
      </LinksContainer>
    </div>
  )
}
