import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Section = styled.section`
  background-color: #fff;
  padding: 45px 0;
  ${mediaQuery.greaterThan('large')`
    padding: 100px 0;
  `}
`

export const Content = styled.div`
  margin-bottom: 45px;
  ${mediaQuery.greaterThan('large')`
    display: flex;
    margin-bottom: 60px;
  `}
`

export const ColumnTitle = styled.div`
  ${mediaQuery.greaterThan('large')`
    width: 480px;
    margin-right: 80px;
  `}

  ${mediaQuery.lessThan('large')`
    margin-bottom: 30px;
  `}
`
