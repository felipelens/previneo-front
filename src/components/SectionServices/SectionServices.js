import React from 'react'
import { FormattedHTMLMessage } from 'react-intl'
import Container from 'components/Container'
import CardServiceGroup from 'components/CardServiceGroup'
import CardService from 'components/CardService'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Text from 'components/Text'
import { Section, Content, ColumnTitle } from './styles'
import Resource from 'containers/Resource'

export default function SectionServices (props) {
  return (
    <Section {...props}>
      <Container>
        <Content>
          <ColumnTitle>
            <SectionTitle>
              <FormattedHTMLMessage id='section.home.services.title' />
            </SectionTitle>
          </ColumnTitle>
          <VerticalSpacer space={30} style={{flex: 1}}>
            <Text size='18px' component='div'>
              <FormattedHTMLMessage id='section.home.services.description' />
            </Text>
          </VerticalSpacer>
        </Content>
        <Resource resource='SpecialitiesSite' autoFetch params={{ limit: 5 }}>
          {props => (
            <CardServiceGroup>
              {props.records.map(record => (
                <CardService
                  key={record.id}
                  id={record.id}
                  image={record.icone.url}
                  title={record.nome}
                  legenda={record.legenda}
                  description={record.nome}
                  color={record.cor}
                />
              ))}
            </CardServiceGroup>
          )}
        </Resource>
      </Container>
    </Section>
  )
}
