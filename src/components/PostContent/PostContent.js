import React from 'react'
import styled from 'styled-components'

const Content = styled.div`
  font-family: ${props => props.theme.fonts.primary};
  font-size: 16px;
  line-height: 2;

  p:first-child {
    margin-top: 0;
  }
`

export default function PostContent (props) {
  return (
    <Content {...props} />
  )
}
