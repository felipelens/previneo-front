```
<Toggle>
  {({ toggle, value }) => (
    <button type='button' onClick={toggle}>
      {value ? 'Ligado' : 'Desligado'}  
    </button>
  )}
</Toggle>
```
