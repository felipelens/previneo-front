```
<Option type='checkbox'>
  Meu option customizado
</Option>
```

```
<Option type='checkbox' defaultChecked>
  Meu option customizado
</Option>
```

Com radioboxes:

```
<Option type='radio' name='option'>
  Opção 1
</Option>
<Option type='radio' name='option'>
  Opção 2
</Option>
<Option type='radio' name='option' defaultChecked>
  Opção 3
</Option>
```
