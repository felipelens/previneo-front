import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import getRoute from 'utils/getRoute'
import routes from 'routes'

export const PostCategoryStyled = styled(NavLink)`
  display: inline-block;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 12px;
  font-weight: bold;
  background-color: ${props => props.filter ? props.theme.colors.white : props.theme.colors.black};
  padding: 5px 10px;
  border-radius: 3px;
  color: ${props => props.filter ? props.theme.colors.textGray : props.theme.colors.white};
  text-decoration: none;
  transition: .2s;

  &:hover, &.active {
    background: ${props => props.theme.colors.dark};
    color: ${props => props.theme.colors.white};
  }

`

export default function PostCategory ({ id, name, ...props }) {
  return (
    <PostCategoryStyled to={getRoute(routes.site.blog.category, { id })} {...props}>
      {name.length > 20 ? `${name.substr(0, 20)}...` : name}
    </PostCategoryStyled>
  )
}

PostCategory.propTypes = {
  /** ID da categoria */
  id: PropTypes.any.isRequired,

  /** Título da categoria */
  name: PropTypes.string.isRequired
}
