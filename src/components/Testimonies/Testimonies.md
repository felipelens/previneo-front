```
<Testimonies
  data={[
    {
      author: 'John Doe 1',
      company: {
        name: 'Aon',
        logo: require('images/aon.png')
      },
      testimony: 'Competência, ética e rigor técnico por um lado, flexibilidade e compreensão das necessidades dos clientes por outro são qualidades que experimentei ao trabalhar com a PreviNEO.'
    },
    {
      author: 'John Doe 2',
      company: {
        name: 'Aon',
        logo: require('images/aon.png')
      },
      testimony: 'Competência, ética e rigor técnico por um lado, flexibilidade e compreensão das necessidades dos clientes por outro são qualidades que experimentei ao trabalhar com a PreviNEO. Competência, ética e rigor técnico por um lado, flexibilidade e compreensão das necessidades dos clientes por outro são qualidades que experimentei ao trabalhar com a PreviNEO.'
    },
    {
      author: 'John Doe 3',
      company: {
        name: 'Aon',
        logo: require('images/aon.png')
      },
      testimony: 'Competência, ética e rigor técnico por um lado, flexibilidade e compreensão das necessidades dos clientes por outro são qualidades que experimentei ao trabalhar com a PreviNEO.'
    }
  ]}
/>
```
