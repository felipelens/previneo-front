import React, { Component } from 'react'
import styled from 'styled-components'
import CardTestimony from 'components/CardTestimony'
import ButtonArrow from 'components/ButtonArrow'

const Container = styled.div`
  position: relative;
  padding: 0 45px;
`

export default class Testimonies extends Component {
  state = {
    index: 0
  }

  prev = () => {
    const index = this.state.index === 0
      ? this.props.data.length - 1
      : this.state.index - 1
    this.setState({ index })
  }

  next = () => {
    const index = this.state.index === this.props.data.length - 1
      ? 0
      : this.state.index + 1
    this.setState({ index })
  }

  render () {
    return (
      <Container>
        <ButtonArrow onClick={this.prev} />
        <ButtonArrow onClick={this.next} next />
        <CardTestimony {...this.props.data[this.state.index]} {...this.props} />
      </Container>
    )
  }
}
