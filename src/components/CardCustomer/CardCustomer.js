import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  height: 97px;
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 3px;
  box-sizing: border-box;
  background-color: ${props => props.theme.colors.background};
  position: relative;
`

const Link = styled.a`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`
const Image = styled.img`
  display: block;
  max-width: 100%;
  max-height: 70px;
  margin-left: auto; 
  margin-right: auto;
`

export default function CardCustomer ({ image, title, url, ...props }) {
  return (
    <Container {...props}>
      <Link href={url} target='_blank' title={title} />
      <Image src={image} alt={title} />
    </Container>
  )
}

CardCustomer.propTypes = {
  /** Imagem */
  image: PropTypes.string.isRequired,

  /** Nome do cliente */
  title: PropTypes.string.isRequired,

  /** URL do cliente */
  url: PropTypes.string
}
