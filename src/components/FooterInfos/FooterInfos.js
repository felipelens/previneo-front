import React from 'react'
import VerticalSpacer from 'components/VerticalSpacer'
import FooterInfoCard from 'components/FooterInfoCard'
import FooterLink from 'components/FooterLink'

export default function FooterInfos () {
  return (
    <VerticalSpacer>
      <FooterInfoCard
        icon='whatsapp'
        children='41 99525-3521'
      />
      <FooterInfoCard
        icon='print'
        children={(
          <FooterLink component='a' href='mailto:contato@previneo.com.br'>
            contato@previneo.com.br
          </FooterLink>
        )}
      />
      <FooterInfoCard
        icon='mailbox'
        children={(
          <React.Fragment>
            Rua Petit Carneiro, 1122 4º andar <br /> Sala 405 Curitiba - Paraná - Brasil
          </React.Fragment>
        )}
      />
    </VerticalSpacer>
  )
}
