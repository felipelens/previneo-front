```
<Text>
  Lorem ipsum varius feugiat venenatis mattis aptent quisque aliquet proin habitasse, mi bibendum senectus ante nisl consectetur enim quam consectetur, feugiat consectetur litora posuere fermentum nullam vulputate phasellus arcu. maecenas sollicitudin commodo cursus justo imperdiet felis lacinia, torquent non ullamcorper class dapibus ut. himenaeos interdum tellus conubia est cubilia rutrum tristique libero ultricies, suspendisse donec integer sodales lectus primis sapien dui magna, quisque sed lobortis a arcu hendrerit fames phasellus. etiam tortor donec hac ultricies praesent lobortis mi tempus aliquam bibendum auctor sollicitudin sagittis netus, ad fames nunc nostra pulvinar felis aptent dapibus quisque aptent bibendum mollis. 
</Text>
```

É possível alterar a cor e o tamanho da fonte:

```
<Text size='12px' color='primary'>
  Lorem ipsum varius feugiat venenatis mattis aptent quisque aliquet proin habitasse, mi bibendum senectus ante nisl consectetur enim quam consectetur, feugiat consectetur litora posuere fermentum nullam vulputate phasellus arcu. maecenas sollicitudin commodo cursus justo imperdiet felis lacinia, torquent non ullamcorper class dapibus ut. himenaeos interdum tellus conubia est cubilia rutrum tristique libero ultricies, suspendisse donec integer sodales lectus primis sapien dui magna, quisque sed lobortis a arcu hendrerit fames phasellus. etiam tortor donec hac ultricies praesent lobortis mi tempus aliquam bibendum auctor sollicitudin sagittis netus, ad fames nunc nostra pulvinar felis aptent dapibus quisque aptent bibendum mollis. 
</Text>
```
