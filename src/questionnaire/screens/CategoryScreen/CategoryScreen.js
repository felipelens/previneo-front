import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { isEmpty } from 'ramda'
import QuestionnaireScreen from 'questionnaire/components/QuestionnaireScreen'
import CategoryWizard from 'questionnaire/containers/CategoryWizard'
import routes from 'routes'
import getRoute from 'utils/getRoute'
import { getCategory, getAuth } from 'store/ducks/questionnaire'

function CategoryScreen ({ categoria, auth, readOnly }) {
  if (isEmpty(categoria)) {
    const to = getRoute(routes.questionnaire.categories, auth)
    return <Redirect to={to} />
  }

  return (
    <QuestionnaireScreen
      title={categoria.nome}
      color={categoria.cor}
      icon={categoria.icone.url}
    >
      <CategoryWizard category={categoria} color={categoria.cor} readOnly={readOnly} />
    </QuestionnaireScreen>
  )
}

const mapStateToProps = (state, { match: { params } }) => ({
  categoria: getCategory(state, parseInt(params.id, 10)),
  auth: getAuth(state),
  readOnly: state.questionnaire.data.paciente.anamnese_finalizada
})

export default connect(
  mapStateToProps
)(CategoryScreen)
