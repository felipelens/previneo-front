import React from 'react'
import { injectIntl } from 'react-intl'
import QuestionnaireScreen from 'questionnaire/components/QuestionnaireScreen'
import Categories from 'questionnaire/containers/Categories'

function CategoriesScreen ({ intl }) {
  return (
    <QuestionnaireScreen
      title={intl.formatMessage({ id: 'admin.anamnesis.title' })}
      personal
    >
      <Categories />
    </QuestionnaireScreen>
  )
}

export default injectIntl(CategoriesScreen)
