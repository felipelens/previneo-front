import React from 'react'
import { isEmpty } from 'ramda'
import Resource from 'containers/Resource'
import getRoute from 'utils/getRoute'
import PageContent from 'components/PageContent'
import Spinner from 'components/Spinner'
import routes from 'routes'
import PatientIntro from 'questionnaire/components/PatientIntro'
import Localization from '../../../containers/Localization/Localization'

export default function CompanyScreen (props) {
  return (
    <Localization {...props}>
      {props => {
        return (
          <Resource
            resource='AnamneseCompany'
            id={props.match.params.slug}
            autoFetch
            onFetchSuccess={(data) => props.setLang(data.detailedRecord.roteiro.idioma.slug)}
          >
            {({ detailedRecord, isFetching }) => {
              if (isFetching) {
                return <PageContent><Spinner /></PageContent>
              }

              if (isEmpty(detailedRecord)) {
                window.location.href = '/'
                return null
              }

              const url = getRoute(routes.questionnaire.companyPatient, {
                companyId: detailedRecord.roteiro.empresa.id,
                hash: detailedRecord.hash
              })
              return <PatientIntro data={detailedRecord} redirect={url} />
            }}
          </Resource>
        )
      }}
    </Localization>
  )
}
