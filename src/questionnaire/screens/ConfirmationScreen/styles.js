import styled from 'styled-components'

export const Container = styled.section`
  padding: 50px 20px 100px;
  max-width: 675px;
  margin: 0 auto;
  .text {
    line-height: 2 !important;
    > h2 {
      font-weight: normal;
    }
    > ul {
      list-style-type: none;
      padding: 0;
      margin: 0 0 40px 0;
      > li {
        font-weight: bold;
        &::before {
          content: '';
          width: 7px;
          height: 7px;
          border-radius: 7px;
          display: inline-block;
          background-color: ${props => props.theme.colors.primary};
          margin: 0 10px 2px 0;
        }
      }
    }
  }
`

export const Customer = styled.span`
  color: ${props => props.theme.colors.primary};
`
