import React from 'react'
import { injectIntl } from 'react-intl'
import QuestionnaireScreen from 'questionnaire/components/QuestionnaireScreen'
import Result from 'questionnaire/containers/Result'
import VerticalSpacer from 'components/VerticalSpacer'

function CategoriesScreen ({ intl }) {
  return (
    <QuestionnaireScreen
      title={intl.formatMessage({ id: 'admin.common.result' })}
      personal
    >
      <VerticalSpacer>
        <Result />
      </VerticalSpacer>
    </QuestionnaireScreen>
  )
}

export default injectIntl(CategoriesScreen)
