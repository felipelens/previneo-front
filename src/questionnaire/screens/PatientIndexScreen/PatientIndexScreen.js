import React from 'react'
import { connect } from 'react-redux'
import * as duck from 'store/ducks/questionnaire'
import PatientIntro from 'questionnaire/components/PatientIntro'
import getRoute from 'utils/getRoute'
import routes from 'routes'

class PatientIndexScreen extends React.Component {
  render () {
    const { script, patient, session } = this.props.match.params
    const to = this.props.data.paciente.cadastro_completo
      ? getRoute(routes.questionnaire.categories, { script, patient, session })
      : getRoute(routes.questionnaire.personalData, { script, patient, session })

    return (
      <PatientIntro data={this.props.data} redirect={to} />
    )
  }
}

const mapStateToProps = state => ({
  isFetching: duck.isFetching(state),
  data: duck.getData(state),
  error: duck.getError(state)
})

const mapDispatchToProps = {
  fetchData: duck.fetchData
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientIndexScreen)
