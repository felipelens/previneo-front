import React from 'react'
import Loadable from 'react-loadable'
import PageContent from 'components/PageContent'
import Spinner from 'components/Spinner'

export default Loadable({
  loader: () => import('./containers/Questionnaire'),
  loading: () => (
    <PageContent>
      <Spinner />
    </PageContent>
  )
})
