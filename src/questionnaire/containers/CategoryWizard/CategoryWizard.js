import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { isEmpty } from 'ramda'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { Field } from 'react-final-form'
import Wizard from 'questionnaire/components/Wizard'
import CustomQuestionField from 'questionnaire/components/CustomQuestionField'
import * as duck from 'store/ducks/questionnaire'
import * as forms from 'utils/forms'
import { integerBetween } from 'utils/validators'
import getRoute from 'utils/getRoute'
import routes from 'routes'
import Text from 'components/Text'
import Button from 'components/Button'
import CategoryProgressButton from 'questionnaire/components/CategoryProgressButton'
import { SCRIPT_NODE_TYPES, TYPE_QUESTIONS, QUESTIONS_FORMATS } from 'config/constants'
import VerticalSpacer from 'components/VerticalSpacer/VerticalSpacer'

class CategoryWizard extends Component {
  static propTypes = {
    /** Categoria */
    category: PropTypes.object.isRequired
  }

  rootQuestionsId = this.props.category.script.map(q => q.id)

  state = {
    currentQuestion: this.props.category.script[0],
    parent: { children: this.props.category.script, parent: null },
    history: [],
    done: false
  }

  componentDidUpdate (prevProps) {
    if (prevProps.isSubmitting && !this.props.isSubmitting && isEmpty(this.props.error)) {
      this.nextQuestion()
    }
  }

  nextQuestion () {
    const { lastAnswer: answer, currentQuestion: question, parent } = this.state

    if (answer.children && answer.children.length > 0) {
      return this.setState({
        currentQuestion: answer.children[0],
        parent: {
          children: answer.children,
          parent: {
            ...question,
            parent
          }
        },
        history: this.state.history.concat({ question, parent })
      })
    }

    const nextQuestion = this.getNextQuestion(question, parent)

    if (nextQuestion) {
      this.setState({
        currentQuestion: nextQuestion.question,
        parent: nextQuestion.parent,
        history: this.state.history.concat({ question, parent })
      })
    } else {
      this.setState({ done: true })
    }
  }

  getNextQuestion (question, parent) {
    // Verifica se o question possui um irmão
    const questionIndex = parent.children.findIndex(q => q.id === question.id)

    if (typeof questionIndex === 'number' && parent.children[questionIndex + 1] && parent.type !== SCRIPT_NODE_TYPES.QUESTION) {
      return {
        question: parent.children[questionIndex + 1],
        parent
      }
    }

    if (parent.parent) {
      return this.getNextQuestion(parent, parent.parent)
    }
  }

  previousQuestion () {
    if (this.state.history.length > 0) {
      const lastItem = this.state.history[this.state.history.length - 1]
      this.setState({
        currentQuestion: lastItem.question,
        parent: lastItem.parent,
        history: this.state.history.slice(0, this.state.history.length - 1)
      })
    }
  }

  getCurrentQuestionObj () {
    const { currentQuestion } = this.state
    const questions = currentQuestion.typeQuestion === TYPE_QUESTIONS.GROUP.id
      ? currentQuestion.children
      : [ currentQuestion ]

    return questions.reduce((memo, question) => {
      memo[question.id] = question
      return memo
    }, {})
  }

  getAnsweredQuestionsObj () {
    const { perguntasRespondidas } = this.props
    const respostas = perguntasRespondidas

    return respostas.reduce((memo, answer) => {
      memo[answer.id] = answer
      return memo
    }, {})
  }

  handleSubmit = values => {
    const questionsObj = this.getCurrentQuestionObj()
    const data = Object.keys(questionsObj).map(id => {
      const question = questionsObj[id]
      const value = values[`question-${id}`]

      return {
        id,
        answer: question.typeQuestion === TYPE_QUESTIONS.OBJETIVA.id
          ? question.children.find(a => a.id === value)
          : { value: value.toString() }
      }
    })

    this.setState({
      lastAnswer: data[0].answer
    }, () => {
      this.props.answerQuestion({
        ...this.props.auth,
        questions: data.map(q => ({
          id: q.id,
          answer: q.answer.id || q.answer.value
        }))
      })
    })
  }

  handleBackClick = () => {
    this.previousQuestion()
  }

  canGoBack () {
    return this.state.currentQuestion.id !== this.props.category.script[0].id
  }

  getProgress () {
    const getNodeIndex = (node, parent) =>
      parent.children.findIndex(child => child.id === node.id) + 1

    const getPath = (node, parent, path = []) => {
      if (!parent) return path
      const index = getNodeIndex(node, parent)
      if (!index) {
        return getPath(parent, parent.parent, path)
      }
      return getPath(parent, parent.parent, [ index, ...path ])
    }

    return getPath(
      this.state.currentQuestion,
      this.state.parent
    ).join('.')
  }

  getInitialValues () {
    const perguntasRespondidas = this.getAnsweredQuestionsObj()
    const questionsObj = this.getCurrentQuestionObj()

    return Object.keys(questionsObj).reduce((memo, id) => {
      if (!perguntasRespondidas.hasOwnProperty(id)) {
        return memo
      }

      const question = perguntasRespondidas[id]
      const answer = question.resposta[0]
      memo[`question-${id}`] = question.tipo_resposta === TYPE_QUESTIONS.OBJETIVA.id
        ? answer.id
        : answer.resposta

      return memo
    }, {})
  }

  renderCurrentForm (question) {
    if (question.typeQuestion === TYPE_QUESTIONS.GROUP.id) {
      return (
        <VerticalSpacer>
          {question.children.map(q => this.renderCurrentForm(q))}
        </VerticalSpacer>
      )
    }

    const options = question.typeQuestion === TYPE_QUESTIONS.OBJETIVA.id
      ? (
        question.children.map(child => ({
          label: child.title,
          value: child.id
        }))
      ) : []

    const formValidation = question.format === QUESTIONS_FORMATS.integer.id
      ? {
        ...forms.integer,
        validate: integerBetween({ min: question.min, max: question.max })
      }
      : forms.required

    return (
      <div key={question.id}>
        <Field
          key={question.id}
          name={`question-${question.id}`}
          component={CustomQuestionField}
          question={question.title}
          description={question.description}
          options={options}
          color={this.props.color}
          readOnly={this.props.readOnly}
          {...formValidation}
        />
      </div>
    )
  }

  render () {
    const { intl } = this.props

    if (this.state.done) {
      const to = getRoute(routes.questionnaire.categories, this.props.auth)

      const category = this.props.category

      return (
        <React.Fragment>
          <Text
            size='25px'
            lineHeight={1}
            style={{ marginBottom: 15 }}
            color={category.cor}
          >
            <strong>
              {intl.formatMessage({ id: 'q.stepFinished' })}
            </strong>
          </Text>
          <Text size='18px' style={{ marginBottom: 50 }}>
            {intl.formatMessage({ id: 'q.youAreDoingRight' })}
          </Text>
          <CategoryProgressButton
            to={to}
            key={category.id}
            title={category.nome}
            image={category.icone ? category.icone.url : ''}
            value={1}
            color={category.cor}
          />
          <Button
            to={to}
            color={category.cor}
            style={{ width: '100%', marginTop: 50 }}
            component={Link}
          >
            {intl.formatMessage({ id: 'q.continueAvaliation' })}
          </Button>
        </React.Fragment>
      )
    }

    return (
      <Wizard
        initialValues={this.getInitialValues()}
        form={this.renderCurrentForm(this.state.currentQuestion)}
        onSubmit={this.handleSubmit}
        canGoBack={this.canGoBack()}
        onBackClick={this.handleBackClick}
        isSubmitting={this.props.isSubmitting}
        count={this.rootQuestionsId.length}
        progress={this.getProgress()}
        color={this.props.color}
      />
    )
  }
}

const mapStateToProps = state => ({
  perguntasRespondidas: duck.getData(state).paciente.perguntas_respondidas,
  auth: duck.getAuth(state),
  isSubmitting: duck.isSubmitting(state),
  error: duck.getError(state)
})

const mapDispatchToProps = {
  answerQuestion: duck.answerQuestion
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(CategoryWizard))
