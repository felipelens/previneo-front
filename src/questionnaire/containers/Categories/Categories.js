import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Link as RouterLink } from 'react-router-dom'
import { injectIntl } from 'react-intl'
import VerticalSpacer from 'components/VerticalSpacer'
import Text from 'components/Text'
import Button from 'components/Button'
import Link from 'components/Link'
import * as duck from 'store/ducks/questionnaire'
import CategoryProgressButton from 'questionnaire/components/CategoryProgressButton'
import getRoute from 'utils/getRoute'
import routes from 'routes'
import theme from 'theme'
import { setLang } from 'store/ducks/localization'

function Categories ({ data, categories, auth, intl }) {
  setLang(data.roteiro.idioma.slug)

  const to = getRoute(routes.questionnaire.personalData, auth)
  if (!data.paciente.cadastro_completo) {
    return <Redirect to={to} />
  }

  const disabled = !!categories.find(c => c.progresso < 1)

  let questionnaireResult = (
    <Text size='18px'>
      {intl.formatMessage({ id: 'q.profileUnmatch' })}
    </Text>
  )

  if (categories.length > 0) {
    questionnaireResult = (
      <div style={{textAlign: 'right'}}>
        <Button
          component={RouterLink}
          to={getRoute(routes.questionnaire.result, auth)}
          blockAtBreakpoint='small'
          onClick={e => disabled && e.preventDefault()}
          disabled={disabled}
          color={theme.colors.personal}
        >
          {intl.formatMessage({ id: 'q.finishAvaliation' })}
        </Button>
      </div>
    )
  }

  const requiredCategory = categories.find(cat => cat.exibir_antes_anamnese)

  if (requiredCategory && requiredCategory.progresso < 1) {
    return (
      <Redirect to={getRoute(routes.questionnaire.category, {
        ...auth,
        id: requiredCategory.id
      })}
      />
    )
  }

  return (
    <VerticalSpacer>
      <VerticalSpacer space={15}>
        {!data.paciente.aon_saude && (
          <Text size='18px'>
            <Link to={to}>
              {intl.formatMessage({ id: 'admin.common.clickHere' })}
            </Link>
            {' '}
            {intl.formatMessage({ id: 'q.toUpdateYourData' })}
          </Text>
        )}
      </VerticalSpacer>
      <VerticalSpacer>
        {categories.map(categoria => (
          <CategoryProgressButton
            to={getRoute(routes.questionnaire.category, {
              ...auth,
              id: categoria.id
            })}
            key={categoria.id}
            title={categoria.nome}
            image={categoria.icone ? categoria.icone.url : ''}
            value={categoria.progresso}
            color={categoria.cor}
          />
        ))}
        {questionnaireResult}
      </VerticalSpacer>
    </VerticalSpacer>
  )
}

const mapStateToProps = state => ({
  data: duck.getData(state),
  categories: duck.getCategories(state),
  auth: duck.getAuth(state)
})

export default connect(
  mapStateToProps
)(injectIntl(Categories))
