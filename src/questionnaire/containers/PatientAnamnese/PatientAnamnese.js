import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter, Switch, Route } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import { isEmpty } from 'ramda'
import Logout from 'admin/containers/Logout'
import PageContent from 'components/PageContent'
import Spinner from 'components/Spinner'
import Container from 'components/Container'
import Text from 'components/Text'
import { NotFoundScreen } from 'screens'
import * as Screens from 'questionnaire/screens'
import * as duck from 'store/ducks/questionnaire'
import routes from 'routes'
import getRoute from 'utils/getRoute'
import { isAuthenticated, getAuthType } from 'store/ducks/auth'

class PatientAnamnese extends Component {
  fetchData () {
    const { script, patient, session } = this.props.match.params
    this.props.fetchData({ script, patient, session })
  }

  componentDidMount () {
    this.fetchData()
  }

  componentDidUpdate ({ isAuth }) {
    if (!isAuth && this.props.isAuth) {
      this.fetchData()
    }
  }

  render () {
    if (this.props.isFetching) {
      return (
        <PageContent>
          <Spinner />
        </PageContent>
      )
    }

    if (isEmpty(this.props.data)) {
      return (
        <PageContent>
          <Container size='small'>
            <Text align='center'>
              <FormattedMessage id='admin.common.noResultsFound' />
            </Text>
          </Container>
        </PageContent>
      )
    }

    const { script, patient, session } = this.props.match.params

    return (
      <div>
        <Switch>
          <Route
            exact
            path={routes.questionnaire.personalData}
            component={Screens.PersonalDataScreen}
          />
          <Route
            exact
            path={routes.questionnaire.result}
            component={Screens.ResultScreen}
          />
          <Route
            exact
            path={routes.questionnaire.anamnese}
            component={Screens.AnamneseScreen}
          />
          <Route
            exact
            path={routes.questionnaire.categories}
            component={Screens.CategoriesScreen}
          />
          <Route
            exact
            path={routes.questionnaire.category}
            component={Screens.CategoryScreen}
          />
          <Route
            exact
            path={routes.questionnaire.login}
            component={Screens.LoginScreen}
          />
          <Route
            exact
            path={routes.questionnaire.logout}
            component={() => (
              <Logout
                type='patients'
                redirectTo={getRoute(routes.questionnaire.login, { script, patient, session })}
              />
            )}
          />
          <Route
            exact
            path={routes.questionnaire.home}
            component={Screens.PatientIndexScreen}
          />
          <Route component={NotFoundScreen} />
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isFetching: duck.isFetching(state),
  data: duck.getData(state),
  error: duck.getError(state),
  isAuth: isAuthenticated(state) && getAuthType(state) === 'patients'
})

const mapDispatchToProps = {
  fetchData: duck.fetchData
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientAnamnese))
