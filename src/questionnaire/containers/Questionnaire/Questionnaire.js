import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import { NotFoundScreen } from 'screens'
import * as Screens from 'questionnaire/screens'
import PatientAnamnese from 'questionnaire/containers/PatientAnamnese'
import AuthLogin from 'components/AuthLogin'
import withAuth from 'store/utils/withAuth'
import routes from 'routes'

class Questionnaire extends Component {
  state = {
    mounted: false
  }

  componentDidMount () {
    this.props.auth.logout('patients')
    this.setState({ mounted: true })
  }

  render () {
    if (!this.state.mounted) return null

    return (
      <AuthLogin type='patients'>
        <div>
          <Switch>
            <Route
              exact
              path={routes.questionnaire.confirmation}
              component={Screens.ConfirmationScreen}
            />
            <Route
              exact
              path={routes.questionnaire.company}
              component={Screens.CompanyScreen}
            />
            <Route
              exact
              path={routes.questionnaire.companyPatient}
              component={Screens.CompanyPatientScreen}
            />
            <Route
              path={routes.questionnaire.home}
              component={PatientAnamnese}
            />
            <Route
              path={routes.questionnaire.confirmation}
              component={Screens.CompanyPatientScreen}
            />
            <Route component={NotFoundScreen} />
          </Switch>
        </div>
      </AuthLogin>
    )
  }
}

export default withAuth(Questionnaire)
