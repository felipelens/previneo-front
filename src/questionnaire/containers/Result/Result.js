import React, { Component } from 'react'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { isEmpty } from 'ramda'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import Link from 'components/Link'
import CategoryResultItem from 'questionnaire/components/CategoryResultItem'
import * as duck from 'store/ducks/questionnaire'
import getRoute from 'utils/getRoute'
import routes from 'routes'
import VerticalSpacer from 'components/VerticalSpacer'
import WithModal from 'admin/components/WithModal'
import Section from 'admin/components/Section'

class Result extends Component {
  componentDidMount () {
    this.props.fetchResult(this.props.auth)
  }

  modal = ({ strategy, justifications }) => {
    const { intl } = this.props
    return (
      <React.Fragment>
        <Section
          title={intl.formatMessage({ id: 'admin.common.recommendations' })}
          wrapped={false}
        >
          {strategy && (
            <React.Fragment>
              <div dangerouslySetInnerHTML={{ __html: strategy.conteudo }} />
              {strategy.exames_recomendados.length > 0 && (
                <div>
                  <strong>
                    {intl.formatMessage({ id: 'admin.common.recommendedExams' })}:
                  </strong>
                  <ul>
                    {strategy.exames_recomendados.map((exame, index) => (
                      <li key={index}>{
                        exame.tuss
                          ? `${exame.nome} (${exame.tuss})`
                          : exame.nome
                      }</li>
                    ))}
                  </ul>
                </div>
              )}
            </React.Fragment>
          )}
          {justifications && justifications.length > 0 && (
            <React.Fragment>
              <strong>{intl.formatMessage({ id: 'admin.common.justifications' })}:</strong>
              <ul>
                {justifications.map((justification, index) => (
                  <li key={index}>
                    {justification.justificativa}
                  </li>
                ))}
              </ul>
            </React.Fragment>
          )}
        </Section>
      </React.Fragment>
    )
  }

  render () {
    const { intl } = this.props

    if (this.props.isSubmitting) {
      return <Spinner />
    }

    if (this.props.result.aon_saude && this.props.result.callback) {
      window.location.href = this.props.result.callback
      return null
    }

    if (isEmpty(this.props.result) || !this.props.result.tem_resultado) {
      const to = getRoute(routes.questionnaire.categories, this.props.auth)

      return (
        <Text>
          {intl.formatMessage({ id: 'admin.common.noResultsFound' })}.
          {' '}
          <Link to={to}>
            {intl.formatMessage({ id: 'admin.common.clickHere' })}
          </Link>
          {' '}
          {intl.formatMessage({ id: 'q.toAnswerTheQuestions' })}.
        </Text>
      )
    }

    return (
      <WithModal modal={this.modal}>
        {({ toggleModal }) => (
          <VerticalSpacer>
            <Text size='25px' style={{ fontWeight: 'bold' }}>
              {intl.formatMessage({ id: 'q.thanks' })}
            </Text>
            {this.props.script.conteudo_resultado && (
              <Text
                component='div'
                dangerouslySetInnerHTML={{ __html: this.props.script.conteudo_resultado }}
              />
            )}
            {this.props.result.tem_resultado && this.props.script.mostrar_resultado && this.props.result.resultado.map(categoria => (
              <CategoryResultItem
                key={categoria.id}
                title={categoria.nome}
                image={categoria.icone ? categoria.icone.url : ''}
                risk={categoria.risco}
                onClick={() => toggleModal({ strategy: categoria.estrategia, justifications: categoria.justificativas })}
              />
            ))}
          </VerticalSpacer>
        )}
      </WithModal>
    )
  }
}

const mapStateToProps = state => ({
  isSubmitting: duck.isSubmitting(state),
  auth: duck.getAuth(state),
  result: duck.getResult(state),
  script: duck.getData(state).roteiro
})

const mapDispatchToProps = {
  fetchResult: duck.fetchResult
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(Result))
