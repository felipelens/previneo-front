import React from 'react'
import { Link } from 'react-router-dom'
import { injectIntl } from 'react-intl'
import VerticalSpacer from 'components/VerticalSpacer'
import Button from 'components/Button'
import Footer from 'components/Footer'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import {
  Content,
  Header,
  BottomInfo,
  BottomImg,
  BottomInfoContainer,
  BottomImgHolder,
  TopImg
} from './styles'

function PatientIntro ({ data, redirect, intl }) {
  return (
    <React.Fragment>
      <PageContent>
        <VerticalSpacer space={40}>
          <Container>
            {data.roteiro.logo_esquerda && data.roteiro.logo_esquerda.url && (
              <Header justify={!!(data.roteiro.logo_direita && data.roteiro.logo_direita.url)}>
                <TopImg
                  src={data.roteiro.logo_esquerda.url}
                  alt={data.roteiro.empresa.nome}
                />
                {data.roteiro.logo_direita && data.roteiro.logo_direita.url && (
                  <TopImg
                    src={data.roteiro.logo_direita.url}
                    alt={data.roteiro.empresa.nome}
                  />
                )}
              </Header>
            )}
          </Container>
          <Container size='xsmall'>
            <Content>
              <div
                className='text'
                dangerouslySetInnerHTML={{ __html: data.roteiro.conteudo_url_aberta }}
              />
            </Content>
            <Button
              component={Link}
              to={redirect}
              arrow
              block
            >
              {intl.formatMessage({ id: 'q.startPrevention' })}
            </Button>
          </Container>
        </VerticalSpacer>
        {data.roteiro.mostrar_informacao_adicional || data.roteiro.logo_inferior_esquerda || data.roteiro.logo_inferior_direita
          ? (
            <BottomInfoContainer>
              {data.roteiro.logo_inferior_esquerda ? (
                <BottomImg
                  src={data.roteiro.logo_inferior_esquerda.url}
                  alt={data.roteiro.empresa.nome}
                />
              ) : (
                <BottomImgHolder />
              ) }
              <BottomInfo>
                <div dangerouslySetInnerHTML={{ __html: data.roteiro.conteudo_informacao_adicional }} />
              </BottomInfo>
              {data.roteiro.logo_inferior_direita ? (
                <BottomImg
                  src={data.roteiro.logo_inferior_direita.url}
                  alt={data.roteiro.empresa.nome}
                />
              ) : (
                <BottomImgHolder />
              ) }
            </BottomInfoContainer>
          ) : ''}
      </PageContent>
      {data.roteiro.mostrar_footer && (
        <Footer />
      )}
    </React.Fragment>
  )
}

export default injectIntl(PatientIntro)
