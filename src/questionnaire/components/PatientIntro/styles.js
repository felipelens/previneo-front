import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Content = styled.section`
  .text {
    line-height: 2 !important;
    > h2 {
      font-weight: normal;
    }
    > ul {
      list-style-type: none;
      padding: 0;
      margin: 0 0 40px 0;
      > li {
        font-weight: bold;
        &::before {
          content: '';
          width: 7px;
          height: 7px;
          border-radius: 7px;
          display: inline-block;
          background-color: ${props => props.theme.colors.primary};
          margin: 0 10px 2px 0;
        }
      }
    }
  }
`

export const Customer = styled.span`
  color: ${props => props.theme.colors.primary};
`

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${props => props.justify ? 'space-between' : 'center'};
  
  ${mediaQuery.lessThan('large')`
    flex-direction: column;
    > :nth-child(1) { order: 2; }
  `}

  > * + * {
    margin-left: 30px;
  }
`

export const BottomInfo = styled.div`
  display: flex;
  align-items: center;
  max-width: 560px;
  min-height: 10px;
  padding: 10px;
`

export const BottomInfoContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;

  ${mediaQuery.lessThan('large')`
    flex-direction: column;
    > :nth-child(1) { order: 2; }
  `}
`

export const BottomImg = styled.img`
  max-width: 200px;
  
  ${mediaQuery.lessThan('large')`
    margin-top: 30px;
  `}
}
`

export const TopImg = styled.img`
  display: block;
  max-width: 100%;
  height: auto;

  ${mediaQuery.lessThan('large')`
    margin-bottom: 30px;
    margin-left: 0;
  `}
}
`

export const BottomImgHolder = styled.img`
  width: 200px;
}
`
