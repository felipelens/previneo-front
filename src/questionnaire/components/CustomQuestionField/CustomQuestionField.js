import React from 'react'
import CustomField from 'components/CustomField'
import QuestionField from 'questionnaire/components/QuestionField'

export default function CustomQuestionField (props) {
  return <CustomField component={QuestionField} {...props} />
}
