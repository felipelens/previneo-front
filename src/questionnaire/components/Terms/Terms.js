import React from 'react'
import { FormattedHTMLMessage, injectIntl } from 'react-intl'
import Section from 'admin/components/Section'
import PostContent from 'components/PostContent'

function Terms ({ intl }) {
  return (
    <Section title={intl.formatMessage({ id: 'q.termsAndConditions' })} wrapped={false}>
      <PostContent>
        <FormattedHTMLMessage id='terms' />
      </PostContent>
    </Section>
  )
}
export default injectIntl(Terms)
