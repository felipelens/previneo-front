import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;

  > :first-child {
    margin-right: 15px;
  }
`
