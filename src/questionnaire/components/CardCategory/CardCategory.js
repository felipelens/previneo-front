import React from 'react'
import PropTypes from 'prop-types'
import Text from 'components/Text'
import Image from 'components/Image'
import { Container } from './styles'

export default function CardCategory ({ title, image, color, ...props }) {
  return (
    <Container {...props}>
      <Image src={image} alt={title} style={{ width: 40 }} />
      <Text color={color} component='span' size='18px'>
        <strong>{title}</strong>
      </Text>
    </Container>
  )
}

CardCategory.propTypes = {
  /** Título */
  title: PropTypes.string.isRequired,

  /** Imagem */
  image: PropTypes.string.isRequired
}
