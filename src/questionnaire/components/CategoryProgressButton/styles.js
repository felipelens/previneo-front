import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const Container = styled(Link)`
  display: flex;
  border: 1px solid ${props => props.theme.colors.formBorder};
  border-radius: 10px;
  min-height: 55px;
  text-decoration: none;
  color: ${props => props.theme.colors.text};
  transition: .2s;

  &:hover {
    background-color: ${props => props.color ? props.color : props.theme.colors.primary};
    border-color: ${props => props.color ? props.color : props.theme.colors.primary};
    color: #fff;

    img {
      filter: brightness(0) invert(1);
    }

    > div > span, > div > div > span {
      color: ${props => props.theme.colors.white} !important;
    }
  }
`

export const Content = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  padding: 0 20px;
`

export const Progress = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-left: 1px solid ${props => props.theme.colors.formBorder};
  width: 60px;
`
