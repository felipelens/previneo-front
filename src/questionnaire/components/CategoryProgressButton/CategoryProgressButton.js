import React from 'react'
import PropTypes from 'prop-types'
import CardCategory from 'questionnaire/components/CardCategory'
import Icon from 'components/Icon'
import { Container, Content, Progress } from './styles'

export default function CategoryProgressButton ({ to = '/', title, image, value, color }) {
  return (
    <Container to={to} color={color}>
      <Content>
        <CardCategory
          title={title}
          image={image}
          color={color}
        />
      </Content>
      {value === 1 &&
        <Progress>
          <Icon
            icon='check'
            size='30px'
            color={color}
          />
        </Progress>
      }
    </Container>
  )
}

CategoryProgressButton.propTypes = {
  /** Título */
  title: PropTypes.string.isRequired,

  /** Imagem */
  image: PropTypes.string.isRequired,

  /** Valor entre 0 e 1 */
  value: PropTypes.number,

  /** Cor do Background */
  color: PropTypes.string
}

CategoryProgressButton.defaultProps = {
  value: 0
}
