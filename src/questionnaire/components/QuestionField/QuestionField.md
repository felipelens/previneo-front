```
<QuestionField
  id='question-1'
  question='Você tem filhos?'
  options={[
    { label: 'Sim', value: 1 },
    { label: 'Não', value: 2 } 
  ]}
  onChange={value => console.log(value)}
/>
```

```
<QuestionField
  id='question-2'
  question='Você pretende parar de fumar?'
  options={[
    { label: 'Sim', value: 1 },
    { label: 'Talvez', value: 2 },
    { label: 'Não', value: 3 }
  ]}
  value={2}
  onChange={value => console.log(value)}
/>
```

```
<QuestionField
  id='question-3'
  question='Com qual idade veio sua primeira menstruação?'
  options={[
    { label: '7 - 11', value: 1 },
    { label: '12 - 13', value: 2 },
    { label: '14 ou mais', value: 3 },
    { label: 'Não sei', value: 4 }  
  ]}
  onChange={value => console.log(value)}
/>
```

```
<QuestionField
  id='question-4'
  question='Etnia:'
  options={[
    { label: 'Branco', value: 1 },
    { label: 'Negro', value: 2 },
    { label: 'Pardo', value: 3 },
    { label: 'Amarelo', value: 4 },
    { label: 'Indígena', value: 5 }
  ]}
  description='Sabe-se que pessoas de etnia negra apresentam risco aumentado para desenvolver câncer de próstata.'
  onChange={value => console.log(value)}
/>
```

```
<QuestionField
  id='question-5'
  question='Exemplo de pergunta discursiva:'
  onChange={value => console.log(value)}
/>
```
