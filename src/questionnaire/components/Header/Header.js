import React from 'react'
import styled, { css } from 'styled-components'
import Container from 'components/Container'
import SectionTitle from 'components/SectionTitle'
import mediaQuery from 'utils/mediaQuery'
import bg from 'images/header-anamnese.jpg'

const HeaderStyled = styled.header`
  width: 100%;
  background-color: ${props => props.color ? props.color : props.theme.colors.primary};
  ${props => props.personal && css`
    background: url(${bg}) center no-repeat #004879;
    background-size: auto 100%;
  `}
}
`

const TitleContainer = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  height: 120px;
  position: relative;

  ${mediaQuery.greaterThan('small')`
    ${props => props.icon && css`
      &::after {
        content: " ";
        position: absolute;
        top: 0;
        right: 0;
        width: 300px;
        height: 100%;
        background: url(${props.icon}) center right no-repeat;
        filter: brightness(0) invert(1);
      }
    `}
  `}
`

export default function Header ({ title, icon, ...props }) {
  return (
    <HeaderStyled {...props}>
      <Container size='xsmall'>
        <TitleContainer icon={icon}>
          <SectionTitle color='white'>{title}</SectionTitle>
        </TitleContainer>
      </Container>
    </HeaderStyled>
  )
}
