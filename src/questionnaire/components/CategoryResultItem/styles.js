import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Container = styled.button`
  display: block;
  width: 100%;
  padding: 15px;
  cursor: pointer;
  background-color: transparent;
  border: 1px solid ${props => props.theme.colors.formBorder};
  border-radius: 10px;

  &:hover {
    background-color: ${props => props.theme.colors.border};
  }

  ${mediaQuery.greaterThan('small')`
    display: flex;
    align-items: center;
    justify-content: space-between;
  `}
`

export const Risk = styled.div`
  ${mediaQuery.greaterThan('small')`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    text-align: center;
  `}

  ${mediaQuery.lessThan('small')`
    margin-top: 10px;
  `}
`

export const IconWrapper = styled.div`
  font-size: 25px;
  color: ${props => props.theme.colors.primary};

  ${mediaQuery.greaterThan('small')`
    margin-left: 10px;
   `}

  ${mediaQuery.lessThan('small')`
    margin-top: 10px;
  `}
`
