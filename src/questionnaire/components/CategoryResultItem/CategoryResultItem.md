```
<CategoryResultItem
  title='Câncer de mama'
  image={require('images/mama.svg')}
  risk='LOW'
/>
```

```
<div>
  <CategoryResultItem
    title='Câncer de mama'
    image={require('images/mama.svg')}
    risk='HIGH'
  />
  <CategoryResultItem
    title='Câncer de mama'
    image={require('images/mama.svg')}
    risk='MEDIUM'
  />
  <CategoryResultItem
    title='Câncer de mama'
    image={require('images/mama.svg')}
    risk='HIGH'
  />
  <CategoryResultItem
    title='Câncer de mama'
    image={require('images/mama.svg')}
    risk='LOW'
  />  
</div>
```
