import React from 'react'
import PropTypes from 'prop-types'
import Icon from '@fortawesome/react-fontawesome'
import faInfo from '@fortawesome/fontawesome-free-solid/faInfoCircle'
import CardCategory from 'questionnaire/components/CardCategory'
import Text from 'components/Text'
import { Container, Risk, IconWrapper } from './styles'
import { CANCER_RISKS } from 'config/constants'

export default function CategoryResultItem ({ title, image, risk, ...props }) {
  const color = CANCER_RISKS.hasOwnProperty(risk.nivel)
    ? CANCER_RISKS[risk.nivel].color
    : 'black'

  return (
    <Container type='button' {...props}>
      <CardCategory
        title={title}
        image={image}
      />
      <Risk>
        <div>
          <Text
            size='14px'
            sizeMobile='14px'
            align='center'
            color={color}
            lineHeight={1}
          >
            {risk.risco}
          </Text>
        </div>
        <IconWrapper>
          <Icon icon={faInfo} />
        </IconWrapper>
      </Risk>
    </Container>
  )
}

CategoryResultItem.propTypes = {
  /** Título */
  title: PropTypes.string.isRequired,

  /** Imagem */
  image: PropTypes.string.isRequired,

  /** Risco de câncer */
  risk: PropTypes.object.isRequired
}
