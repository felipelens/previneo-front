import React from 'react'
import PropTypes from 'prop-types'
import Icon from 'components/Icon'
import {
  Wrapper,
  Container,
  Bar,
  Check
} from './styles'

export default function ProgressBar ({ value, color }) {
  return (
    <Wrapper>
      <Container>
        <Bar value={value} color={color} />
        <Check filled={{filled: value === 1, color: color}}>
          <Icon icon='check' color='inherit' />
        </Check>
      </Container>
    </Wrapper>
  )
}

ProgressBar.propTypes = {
  /** Valor entre 0 e 1 */
  value: PropTypes.number,

  /** Cor da barra */
  color: PropTypes.string
}

ProgressBar.defaultProps = {
  value: 0
}
