```
<ProgressBar value={0} />
```

30%

```
<ProgressBar value={0.3} />
```

50%

```
<ProgressBar value={0.5} />
```

100%

```
<ProgressBar value={1} />
```

```
const Toggle = require('components/Toggle').default
;<Toggle>
  {({ toggle, value }) => (
    <div>
      <ProgressBar value={value ? 1 : 0} />
      <button type='button' onClick={toggle}>click me</button>
    </div>
  )}
</Toggle>
```
