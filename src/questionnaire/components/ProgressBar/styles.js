import styled from 'styled-components'

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  height: 30px;
`

export const Container = styled.div`
  width: 100%;
  height: 10px;
  background-color: #e0e0e0;
  border-radius: 4px;
`

export const Bar = styled.div`
  width: calc(${props => props.value * 100}% - 1px);
  height: 10px;
  border-radius: 4px;
  background-color: ${props => props.color ? props.color : props.theme.colors.primary};
  transition: all 0.4s ease;
`

export const Check = styled.div`
  position: absolute;
  top: 50%;
  right: 0;
  margin-top: -15px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  border-radius: 100%;
  background-color: ${props => props.filled.filled ? props.filled.color : '#e0e0e0'};
  color: ${props => props.filled ? '#fff' : '#bbbabb'};
  line-height: 0;
  transition: all 0.4s ease;
`
