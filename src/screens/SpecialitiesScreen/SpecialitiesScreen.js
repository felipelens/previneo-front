import React from 'react'
import { injectIntl } from 'react-intl'
import Screen from 'components/Screen'
import HeroSpecialities from 'components/HeroSpecialities'
import SectionPartner from 'components/SectionPartner'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import SectionSpecialities from 'components/SectionSpecialities'

function SpecialitiesScreen ({ intl }) {
  return (
    <Screen
      title={intl.formatMessage({ id: 'pages.specialities.title' })}
      description={intl.formatMessage({ id: 'pages.specialities.description' })}
    >
      <HeroSpecialities />
      <PageContent>
        <Container>
          <SectionSpecialities />
        </Container>
      </PageContent>
      <SectionPartner />
    </Screen>
  )
}

export default injectIntl(SpecialitiesScreen)
