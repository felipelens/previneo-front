import React from 'react'
import { injectIntl } from 'react-intl'
import Screen from 'components/Screen'
import HeroCustomers from 'components/HeroCustomers'
import SectionPartner from 'components/SectionPartner'
import SectionCustomers from 'components/SectionCustomers'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import SectionTestimonies from 'components/SectionTestimonies'

function CustomersScreen ({ intl }) {
  return (
    <Screen
      title={intl.formatMessage({ id: 'pages.customers.title' })}
      description={intl.formatMessage({ id: 'pages.customers.description' })}
    >
      <HeroCustomers />
      <PageContent>
        <Container>
          <SectionCustomers />
        </Container>
      </PageContent>
      <SectionTestimonies internal />
      <SectionPartner />
    </Screen>
  )
}

export default injectIntl(CustomersScreen)
