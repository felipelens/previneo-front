import React from 'react'
import { injectIntl } from 'react-intl'
import Screen from 'components/Screen'
import HeroHome from 'components/HeroHome'
import SectionServices from 'components/SectionServices'
import SectionSteps from 'components/SectionSteps'
import SectionCustomer from 'components/SectionCustomer'
import SectionTestimonies from 'components/SectionTestimonies'
import SectionBlog from 'components/SectionBlog'
import SectionPartner from 'components/SectionPartner'

function HomeScreen ({ intl }) {
  return (
    <Screen
      title={intl.formatMessage({ id: 'pages.home.title' })}
      description={intl.formatMessage({ id: 'pages.home.description' })}
    >
      <HeroHome />
      <SectionServices id='section-services' />
      <SectionSteps />
      <SectionCustomer />
      <SectionTestimonies
        title='Nossos clientes falam melhor que a gente'
      />
      <SectionBlog />
      <SectionPartner />
    </Screen>
  )
}

export default injectIntl(HomeScreen)
