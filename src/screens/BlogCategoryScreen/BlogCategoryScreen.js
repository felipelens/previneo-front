import React from 'react'
import styled from 'styled-components'
import Screen from 'components/Screen'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import VerticalSpacer from 'components/VerticalSpacer'
import SectionTitle from 'components/SectionTitle'
import Posts from 'containers/Posts'
import FilterCategoryBlog from 'components/FilterCategoryBlog'

const Content = styled.div`
  background: ${props => props.theme.colors.background};
  padding: 65px 0;
`

const Grid = styled.div`
  display: flex;
  justify-content: space-between;
`

export default function BlogCategoryScreen ({ match: { params } }) {
  const id = parseInt(params.id, 10)
  return (
    <Screen title='Blog'>
      <PageContent blog>
        <Content>
          <Container size='blog'>
            <VerticalSpacer space={45} component='section'>
              <Grid>
                <SectionTitle size='60px'>
                  Blog
                </SectionTitle>
                <FilterCategoryBlog />
              </Grid>
              <Posts params={{categoria: id}} />
            </VerticalSpacer>
          </Container>
        </Content>
      </PageContent>
    </Screen>
  )
}
