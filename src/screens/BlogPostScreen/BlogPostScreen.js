import React from 'react'
import { isEmpty } from 'ramda'
import Helmet from 'react-helmet'
import Screen from 'components/Screen'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import PostHeader from 'components/PostHeader'
import PostContent from 'components/PostContent'
import VerticalSpacer from 'components/VerticalSpacer'
import Sharebox from 'components/Sharebox'
import Resource from 'containers/Resource'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import { parse } from 'date-fns'
import RelatedPosts from 'components/RelatedPosts'
import { FormattedMessage } from 'react-intl'

export default function BlogPostScreen ({ match: { params } }) {
  const id = parseInt(params.id, 10)
  return (
    <Screen title='Post'>
      <Resource resource='PostsSite' id={id} autoFetch>
        {({ isFetching, detailedRecord: post }) => {
          if (isFetching) {
            return (
              <PageContent>
                <Spinner />
              </PageContent>
            )
          }

          if (isEmpty(post)) {
            return (
              <PageContent>
                <Container size='small'>
                  <Text align='center'><FormattedMessage id='admin.common.noResultsFound' /></Text>
                </Container>
              </PageContent>
            )
          }

          return (
            <React.Fragment>
              <Helmet>
                <title>{post.titulo} - PreviNEO</title>
              </Helmet>
              <PostHeader
                title={post.titulo}
                category={{
                  id: post.categoria.id,
                  name: post.categoria.nome
                }}
                date={parse(post.created_at)}
              />
              <PageContent>
                <Container size='small'>
                  <VerticalSpacer space={25}>
                    <PostContent dangerouslySetInnerHTML={{ __html: post.conteudo }} />
                    <Sharebox
                      title={post.titulo}
                      url={window.location.href}
                    />
                  </VerticalSpacer>
                </Container>
                <RelatedPosts />
              </PageContent>
            </React.Fragment>
          )
        }}
      </Resource>
    </Screen>
  )
}
