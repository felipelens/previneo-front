import React from 'react'
import { isEmpty } from 'ramda'
import { injectIntl } from 'react-intl'
import Screen from 'components/Screen'
import PageContent from 'components/PageContent'
import SectionSpeciality from 'components/SectionSpeciality'
import Text from 'components/Text'
import Resource from 'containers/Resource'

function SpecialityScreen ({ intl, match: { params } }) {
  const id = parseInt(params.id, 10)
  return (
    <Screen
      title={intl.formatMessage({ id: 'pages.specialities.title' })}
      description={intl.formatMessage({ id: 'pages.specialities.description' })}
    >
      <PageContent>
        <Resource resource='Specialities' id={id} autoFetch spinner>
          {({ detailedRecord: record }) => {
            if (isEmpty(record)) {
              return <Text>Conteúdo não encontrado!</Text>
            }

            return (
              <SectionSpeciality
                title={record.nome}
                description={record.descricao}
                color={record.cor}
                image={record.imagem && record.imagem.url ? record.imagem.url : ''}
              />
            )
          }}
        </Resource>
      </PageContent>
    </Screen>
  )
}

export default injectIntl(SpecialityScreen)
