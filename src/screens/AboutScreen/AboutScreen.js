import React from 'react'
import { injectIntl } from 'react-intl'
import Screen from 'components/Screen'
import HeroAbout from 'components/HeroAbout'
import SectionPartner from 'components/SectionPartner'
import SectionStepsAbout from 'components/SectionStepsAbout'
import SectionMap from 'components/SectionMap'
import SectionPhraseAbout from 'components/SectionPhraseAbout'
import SectionDoctors from 'components/SectionDoctors'
import PageContent from 'components/PageContent'

function AboutScreen ({ intl }) {
  return (
    <Screen
      title={intl.formatMessage({ id: 'pages.about.title' })}
      description={intl.formatMessage({ id: 'pages.about.description' })}
    >
      <HeroAbout />
      <SectionStepsAbout id='section-steps-about' />
      <PageContent>
        <SectionMap />
        <SectionPhraseAbout />
        <SectionDoctors />
      </PageContent>
      <SectionPartner />
    </Screen>
  )
}

export default injectIntl(AboutScreen)
