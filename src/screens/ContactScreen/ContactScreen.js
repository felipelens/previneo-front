import React from 'react'
import { injectIntl } from 'react-intl'
import Screen from 'components/Screen'
import Container from 'components/Container'
import PageContent from 'components/PageContent'
import SectionContact from 'components/SectionContact'

function ContactScreen ({ intl }) {
  return (
    <Screen
      title={intl.formatMessage({ id: 'pages.contact.title' })}
      description={intl.formatMessage({ id: 'pages.contact.description' })}
    >
      <PageContent>
        <Container>
          <SectionContact />
        </Container>
      </PageContent>
    </Screen>
  )
}

export default injectIntl(ContactScreen)
