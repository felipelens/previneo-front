/**
 * Dependencies.
 */

import { fit } from 'modules/msk'
import { format, parse } from 'date-fns'

/**
 * Number
 */

export const number = (value = '') =>
  fit(value, '999999999')

/**
 * Phone.
 */

export const phone = (value = '') => value.length === 11
  ? fit(value, '(99) 99999-9999')
  : fit(value, '(99) 9999-9999')

/**
 * CPF.
 */

export const cpf = (value = '') =>
  fit(value, '999.999.999-99')

/**
 * CNPJ.
 */

export const cnpj = (value = '') =>
  fit(value, '99.999.999/9999-99')

/**
 * CEP.
 */

export const cep = (value = '') =>
  fit(value, '99999-999')

/**
 * Date.
 */

export const date = (value = '') => {
  const date = /\d{4}-\d{2}-\d{2}/.test(value)
    ? format(parse(value), 'DD/MM/YYYY')
    : value

  return fit(date, '99/99/9999')
}
