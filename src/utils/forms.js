/**
 * Dependencies.
 */

import * as formatters from 'utils/formatters'
import * as validators from 'utils/validators'
import * as parsers from 'utils/parsers'

/**
 * createRequired helper.
 */

const createRequired = ({ parse, format, validate, ...props }) => ({
  parse,
  format,
  validate: value => validate
    ? validators.required(value) || validate(value)
    : validators.required(value),
  ...props
})

/**
 * Required.
 */

export const required = {
  validate: validators.required
}

/**
 * Integer.
 */

export const integer = {
  type: 'tel',
  format: formatters.number,
  validate: validators.integer,
  parse: v => parseInt(v || 0, 10)
}

export const integerRequired = createRequired(integer)

/**
 * Weight.
 */

export const weight = {
  type: 'tel',
  min: 20,
  max: 300,
  validate: validators.integerBetween({ min: 20, max: 300 }),
  parse: v => parseInt(v || 0, 10)
}

export const weightRequired = createRequired(weight)

/**
 * Height
 */

export const height = {
  type: 'tel',
  min: 50,
  max: 300,
  validate: validators.integerBetween({ min: 50, max: 300 }),
  parse: v => parseInt(v || 0, 10)
}

export const heightRequired = createRequired(height)

/**
 * Password.
 */

export const password = {
  type: 'password'
}

export const passwordRequired = createRequired(password)

/**
 * E-mail.
 */

export const email = {
  type: 'email',
  validate: validators.email
}

export const emailRequired = createRequired(email)

/**
 * URL.
 */

export const url = {
  type: 'url'
}

export const urlRequired = createRequired(url)

/**
 * Phone.
 */

export const phone = {
  type: 'tel',
  format: formatters.phone,
  parse: parsers.phone,
  validate: validators.phone
}

export const phoneRequired = createRequired(phone)

/**
 * CPF.
 */

export const cpf = {
  type: 'tel',
  format: formatters.cpf,
  parse: parsers.cpf,
  validate: validators.cpf
}

export const cpfRequired = createRequired(cpf)

/**
 * CNPJ
 */

export const cnpj = {
  type: 'tel',
  format: formatters.cnpj,
  parse: parsers.cnpj,
  validate: validators.cnpj
}

export const cnpjRequired = createRequired(cnpj)

/**
 * CEP.
 */

export const cep = {
  type: 'tel',
  format: formatters.cep,
  parse: parsers.cep,
  validate: validators.cep
}

export const cepRequired = createRequired(cep)

/**
 * Date.
 */

export const date = {
  type: 'tel',
  format: formatters.date,
  parse: parsers.date,
  validate: validators.date
}

export const dateRequired = createRequired(date)
