export default function createResourceRoute (route) {
  return {
    index: `${route}`,
    show: `${route}/:id`,
    new: `${route}/new`,
    edit: `${route}/:id/edit`
  }
}
