export default (entity, extra) => ({
  viewAll: `view_all_${entity}`,
  view: `view_${entity}`,
  create: `create_${entity}`,
  update: `edit_${entity}`,
  delete: `destroy_${entity}`,
  ...extra
})
