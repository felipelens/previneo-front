import { darken } from 'polished'

export const hoverColor = darken(0.05)
