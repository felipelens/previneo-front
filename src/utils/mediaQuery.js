import { generateMedia } from 'styled-media-query'
import theme from 'theme'

export default generateMedia(theme.breakpoints)
