export default function getDigits (value) {
  return value && value.length
    ? value.replace(/\D/g, '')
    : value
}
