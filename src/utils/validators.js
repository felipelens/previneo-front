/**
 * Dependencies.
 */

import isEmail from 'is-email'
import isCPF from 'iscpf'
import CNPJ from 'cnpj'
import { isValid, parse } from 'date-fns'
import { memoize, isEmpty } from 'ramda'
import PasswordValidator from 'password-validator'
import getDigits from 'utils/getDigits'

/**
 * Required.
 */

export const required = value => {
  if (value === 0) return
  if (typeof value === 'boolean') return
  return typeof input !== 'number' && (!value || isEmpty(value)) && { id: 'validations.required' }
}

/**
 * Integer.
 */

export const integer = value => value && parseInt(value, 10) !== value &&
  { id: 'validations.integer' }

/**
 * Integer Between.
 */

export const integerBetween = memoize(({ max, min }) => value => {
  const parsedValue = parseInt(value, 10)

  if (typeof parsedValue !== 'number' || !(parsedValue >= min && parsedValue <= max)) {
    return { id: 'validations.integerBetween', max, min }
  }
})

/**
 * E-mail.
 */

export const email = value => value && !isEmail(value) &&
  { id: 'validations.email' }

/**
 * Phone.
 */

export const phone = value => value && !/\d{10,11}/.test(value) &&
  { id: 'validations.phone' }

/**
 * CPF.
 */

export const cpf = value => value && !isCPF(value) &&
  { id: 'validations.cpf' }

/**
 * CNPJ.
 */

export const cnpj = value => value && !CNPJ.validate(value) &&
  { id: 'validations.cnpj' }

/**
 * CEP.
 */

export const cep = value => value && !/\d{8}/.test(value) &&
  { id: 'validations.cep' }

/**
 * Date.
 */

export const date = value => value && getDigits(value).length === 8 && !isValid(parse(value)) &&
  { id: 'validations.date' }

/**
 * Equal to.
 */

export const equalTo = memoize(({ field, message }) => (value, allValues) =>
  value !== allValues[field] && message
)

/**
 * Password.
 */

const passwordSchema = new PasswordValidator()
passwordSchema
  .is().min(8)
  .has().uppercase()
  .has().lowercase()
  .has().digits()

export const password = value => value && !passwordSchema.validate(value) &&
  { id: 'validations.password' }

/**
 * File Type.
 */

export const fileType = memoize(types => file =>
  file && !types.includes(file.type) &&
  { id: 'validations.fileType' }
)
