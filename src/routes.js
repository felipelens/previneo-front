import createResourceRoute from 'utils/createResourceRoute'

export default {
  site: {
    home: '/',
    about: '/quem-somos',
    customers: '/clientes',
    contact: '/contato',

    specialities: {
      index: '/especialidades',
      show: '/especialidades/:id'
    },

    blog: {
      index: '/blog',
      category: '/blog/categories/:id',
      post: '/blog/posts/:id/:permalink'
    }
  },

  questionnaire: {
    index: '/anamnese',
    home: '/anamnese/:script/:patient/:session',
    anamnese: '/anamnese/:script/:patient/:session/anamnese',
    login: '/anamnese/:script/:patient/:session/login',
    logout: '/anamnese/:script/:patient/:session/logout',
    personalData: '/anamnese/:script/:patient/:session/personal',
    categories: '/anamnese/:script/:patient/:session/categories',
    category: '/anamnese/:script/:patient/:session/categories/:id',
    result: '/anamnese/:script/:patient/:session/result',
    company: '/anamnese/company/:slug',
    companyPatient: '/anamnese/company/:companyId/patient/:hash',
    confirmation: '/anamnese/confirmation/:script/:patient/:session'
  },

  admin: {
    index: '/admin',
    login: '/admin/login',
    logout: '/admin/logout',
    passwordRecovery: '/admin/password-recovery',

    dashboard: {
      index: '/admin/dashboard',
      anamnesis: '/admin/dashboard/anamenesis',
      anamnesisByGender: '/admin/dashboard/anamnesis-by-gender',
      highRisk: {
        index: '/admin/dashboard/high-risk',
        type: '/admin/dashboard/high-risk/:tipo_cancer_id'
      },
      normalRisk: {
        index: '/admin/dashboard/normal-risk',
        type: '/admin/dashboard/normal-risk/:tipo_cancer_id'
      }
    },

    myAccount: '/admin/my-account',

    users: {
      index: '/admin/users',
      users: createResourceRoute('/admin/users/users'),
      deleting: '/admin/users/deleting'
    },

    companies: createResourceRoute('/admin/companies'),
    healthInsurances: createResourceRoute('/admin/health-insurances'),
    roles: createResourceRoute('/admin/roles'),
    languages: createResourceRoute('/admin/languages'),
    genders: createResourceRoute('/admin/genders'),
    risks: createResourceRoute('/admin/risks'),
    strategy: createResourceRoute('/admin/strategy'),
    exams: createResourceRoute('/admin/exams'),
    permissions: createResourceRoute('/admin/permissoes'),
    reports: '/admin/reports',

    patients: {
      index: '/admin/patients',
      patients: createResourceRoute('/admin/patients/patients'),
      importing: '/admin/patients/import',
      exporting: '/admin/patients/exporting',
      deleting: '/admin/patients/deleting'
    },

    patientsHistory: {
      index: '/admin/manage-patients-history',
      patientsHistory: {
        ...createResourceRoute('/admin/manage-patients-history/patients-history'),
        manage: '/admin/manage-patients-history/patients-history/:id/manage/:patientHistoryId'
      },
      contact: '/admin/manage-patients-history/contact',
      contactReason: createResourceRoute('/admin/manage-patients-history/contact-reason'),
      statuses: createResourceRoute('/admin/manage-patients-history/statuses')
    },

    questionnaire: {
      index: '/admin/questionnaire',
      scripts: createResourceRoute('/admin/questionnaire/scripts'),
      questions: createResourceRoute('/admin/questionnaire/questions'),
      categories: createResourceRoute('/admin/questionnaire/categories'),
      typesCancer: createResourceRoute('/admin/questionnaire/types-cancer'),
      scriptSend: '/admin/questionnaire/send',
      emails: createResourceRoute('/admin/questionnaire/emails'),
      layouts: createResourceRoute('/admin/questionnaire/layouts'),
      justifications: createResourceRoute('/admin/questionnaire/justifications')
    },

    site: {
      index: '/admin/site',
      team: createResourceRoute('/admin/site/team'),
      testimonies: createResourceRoute('/admin/site/testimonies'),
      specialities: createResourceRoute('/admin/site/specialities'),
      customers: createResourceRoute('/admin/site/customers'),
      contactInfos: createResourceRoute('/admin/site/contact-infos')
    },

    blog: {
      index: '/admin/blog',
      posts: createResourceRoute('/admin/blog/posts'),
      categories: createResourceRoute('/admin/blog/categories'),
      tags: createResourceRoute('/admin/blog/tags')
    }
  }
}
