import React from 'react'
import Link from 'components/Link'
import FilterSelect from 'admin/components/FilterSelect'
import routes from 'routes'
import getRoute from 'utils/getRoute'
import Resource from 'containers/Resource'
import { FormattedMessage } from 'react-intl'
import debounce from 'lodash.debounce'

export default props => ({
  Header: <FormattedMessage id='admin.users.title' />,
  accessor: 'user.name',
  id: 'user',
  Cell: ({ original }) => {
    const user = original.user || original
    return (
      <Link color='text' hoverColor='primary' to={getRoute(routes.admin.users.users.edit, { id: user.id })}>
        {user.name}
      </Link>
    )
  },
  Filter: ({ onChange, filter }) =>
    <Resource resource='Users'>
      {props => (
        <FilterSelect
          onChange={onChange}
          filter={filter}
          isLoading={props.isFetching}
          onInputChange={debounce(value => {
            if (value) props.fetchAll({ nome: value })
            return value
          }, 600)}
          options={props.records}
        />
      )}
    </Resource>,
  ...props
})
