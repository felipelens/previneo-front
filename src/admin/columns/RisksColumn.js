import React from 'react'
import { FormattedMessage } from 'react-intl'
import FilterSelect from 'admin/components/FilterSelect'
import Resource from 'containers/Resource'
import debounce from 'lodash.debounce'

export default props => ({
  Header: <FormattedMessage id='admin.common.risk' />,
  accessor: 'risco.risco',
  id: 'risco',
  filterable: true,
  Filter: ({ onChange, filter }) =>
    <Resource autoFetch resource='Risks'>
      {props => (
        <FilterSelect
          onChange={onChange}
          filter={filter}
          isLoading={props.isFetching}
          labelKey='risco'
          valueKey='id'
          onInputChange={debounce(value => {
            if (value) props.fetchAll({ risco: value })
            return value
          }, 600)}
          options={props.records}
        />
      )}
    </Resource>,
  ...props
})
