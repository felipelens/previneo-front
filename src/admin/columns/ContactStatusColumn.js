import React from 'react'
import { FormattedMessage } from 'react-intl'
import FilterSelect from 'admin/components/FilterSelect'
import Resource from 'containers/Resource'
import theme from '../../theme'
import debounce from 'lodash.debounce'

export default props => ({
  Header: <FormattedMessage id='admin.common.contactStatus' />,
  accessor: 'statuses',
  id: 'status',
  filterable: true,
  Cell: ({ original }) => {
    const statuses = original.statuses || original
    return (
      <span>
        <span style={{
          color: statuses[0].comparavel === 'DONE' ? theme.colors['danger']
            : statuses[0].comparavel === 'IN_PROGRESS' ? theme.colors['warning']
              : theme.colors['success']
        }}>
      &#x25cf; &nbsp;
        </span>
        { statuses[0].status }
      </span>
    )
  },
  Filter: ({ onChange, filter }) =>
    <Resource autoFetch resource='ContactStatus'>
      {props => (
        <FilterSelect
          onChange={onChange}
          filter={filter}
          isLoading={props.isFetching}
          labelKey='status'
          valueKey='id'
          onInputChange={debounce(value => {
            if (value) props.fetchAll({ nome: value })
            return value
          }, 600)}
          options={props.records}
        />
      )}
    </Resource>,
  ...props
})
