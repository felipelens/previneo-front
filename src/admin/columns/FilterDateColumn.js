import React from 'react'
import { format } from 'modules/Date'
import FilterDatePicker from 'admin/components/FilterDatePicker'
import { FormattedMessage } from 'react-intl'

export default (props = { dateAccessor: 'created_at', message: 'admin.common.createdAt' }) => ({
  Header: <FormattedMessage id={props.message} />,
  accessor: props.dateAccessor,
  filterable: true,
  width: 175,
  Cell: cellProps => format(cellProps.value, props.dateFormat),
  Filter: ({ onChange, filter }) => (
    <FilterDatePicker
      onChange={onChange}
      filter={filter}
      value={filter ? filter.value : {}}
    />
  ),
  ...props
})
