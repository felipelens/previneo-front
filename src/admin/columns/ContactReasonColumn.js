import React from 'react'
import { FormattedMessage } from 'react-intl'
import FilterSelect from 'admin/components/FilterSelect'
import Resource from 'containers/Resource'
import debounce from 'lodash.debounce'

export default props => ({
  Header: <FormattedMessage id='admin.common.motivo' />,
  accessor: 'motivoContato.motivo',
  id: 'motivo',
  filterable: true,
  Filter: ({ onChange, filter }) =>
    <Resource autoFetch resource='ContactReason'>
      {props => (
        <FilterSelect
          onChange={onChange}
          filter={filter}
          placeholder='Pesquisar motivo'
          isLoading={props.isFetching}
          labelKey='motivo'
          valueKey='id'
          onInputChange={debounce(value => {
            if (value) props.fetchAll({ nome: value })
            return value
          }, 600)}
          options={props.records}
        />
      )}
    </Resource>,
  ...props
})
