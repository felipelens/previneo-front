import React from 'react'
import { FormattedMessage } from 'react-intl'
import FilterSelect from 'admin/components/FilterSelect'
import { GENDERS } from 'config/constants'

export default props => ({
  Header: <FormattedMessage id='admin.common.gender' />,
  accessor: 'genero.nome',
  id: 'genero',
  filterable: true,
  Filter: ({ onChange, filter }) =>
    <FilterSelect
      onChange={onChange}
      filter={filter}
      labelKey='nome'
      valueKey='id'
      options={Object.values(GENDERS).map(item => ({
        nome: <FormattedMessage id={item.name} />,
        id: item.id
      }))}
    />,
  ...props
})
