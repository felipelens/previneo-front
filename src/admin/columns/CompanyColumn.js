import React from 'react'
import { FormattedMessage } from 'react-intl'
import FilterSelect from 'admin/components/FilterSelect'
import Resource from 'containers/Resource'
import debounce from 'lodash.debounce'

export default props => ({
  Header: <FormattedMessage id='admin.common.company' />,
  accessor: 'empresa.nome',
  id: 'empresa_id',
  filterable: true,
  Filter: ({ onChange, filter }) =>
    <Resource autoFetch resource='Companies'>
      {props => (
        <FilterSelect
          onChange={onChange}
          filter={filter}
          placeholder='Pesquisar empresa'
          isLoading={props.isFetching}
          labelKey='nome'
          valueKey='id'
          onInputChange={debounce(value => {
            if (value) props.fetchAll({ nome: value })
            return value
          }, 600)}
          options={props.records}
        />
      )}
    </Resource>,
  ...props
})
