import React from 'react'
import { format } from 'modules/Date'
import FilterDateRange from 'admin/components/FilterDateRange'
import { FormattedMessage } from 'react-intl'

export default (props = { dateAccessor: 'created_at', message: 'admin.common.createdAt' }) => ({
  Header: <FormattedMessage id={props.message} />,
  accessor: props.dateAccessor,
  width: 235,
  filterable: true,
  Cell: cellProps => format(cellProps.value, props.dateFormat),
  Filter: ({ onChange, filter }) => (
    <FilterDateRange
      onChange={onChange}
      filter={filter}
      value={filter ? filter.value : {}}
    />
  ),
  ...props
})
