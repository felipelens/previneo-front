import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormContactStatus'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function ContactStatusesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='ContactStatus'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.contactStatus' }),
        intl.formatMessage({ id: 'admin.common.contactStatuses' })
      ]}
      routeBase={routes.admin.patientsHistory.statuses}
      permissions={PERMISSIONS.statuses}
      canAdd={false}
      canRemove={false}
      Form={Form}
      columns={[
        {
          accessor: 'status',
          Header: intl.formatMessage({ id: 'admin.common.contactStatus' })
        }
      ]}
    />
  )
}

export default injectIntl(ContactStatusesScreen)
