import React from 'react'
import { injectIntl } from 'react-intl'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import Auth from 'admin/containers/Auth'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import FormUserPersonal from 'admin/forms/FormUserPersonal'
import FormUserPassword from 'admin/forms/FormUserPassword'
import { LANGUAGES } from 'config/constants'
import Localization from 'containers/Localization/Localization'

function MyAccountScreen ({ intl }) {
  return (
    <ProtectedScreen title={intl.formatMessage({ id: 'admin.common.myAccount' })}>
      <Auth>
        {({ user: { id } }) => (
          <Resource
            resource='Users'
            id={id}
            autoFetch
            spinner
          >
            {props => (
              <VerticalSpacer space={45}>
                <Localization {...props}>
                  {props => {
                    return (
                      <FormUserPersonal
                        initialValues={props.detailedRecord}
                        onSubmit={async values => {
                          const data = await props.update(id, values, {myAccount: true})
                          props.setLang(LANGUAGES[data.value.data.language])
                          window.location.reload()
                        }}
                        isSubmitting={props.isSubmitting}
                        intl={intl}
                      />
                    )
                  }
                  }
                </Localization>
                <FormUserPassword
                  onSubmit={data => props.updatePassword(id, data)}
                  isSubmitting={props.isSubmitting}
                  intl={intl}
                />
              </VerticalSpacer>
            )}
          </Resource>
        )}
      </Auth>
    </ProtectedScreen>
  )
}

export default injectIntl(MyAccountScreen)
