import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormScript'
import { CompanyColumn, LanguageColumn } from 'admin/columns'
import FilterSelect from 'admin/components/FilterSelect'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function ScriptsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Scripts'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.script.title' }),
        intl.formatMessage({ id: 'admin.script.title.plural' })
      ]}
      routeBase={routes.admin.questionnaire.scripts}
      permissions={PERMISSIONS.scripts}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          filterable: true
        },
        LanguageColumn({
          accessor: 'idioma.nome'
        }),
        CompanyColumn(),
        {
          accessor: 'status_roteiro',
          id: 'status',
          Header: 'Status',
          filterable: true,
          Filter: ({ onChange, filter }) =>
            <FilterSelect
              onChange={onChange}
              filter={filter}
              options={[
                {
                  name: intl.formatMessage({ id: 'admin.common.active' }),
                  id: 'Ativo'
                },
                {
                  name: intl.formatMessage({ id: 'admin.common.inactive' }),
                  id: 'Inativo'
                },
                {
                  name: intl.formatMessage({ id: 'admin.common.draft' }),
                  id: 'Rascunho'
                }
              ]}
            />
        }
      ]}
    />
  )
}

export default injectIntl(ScriptsScreen)
