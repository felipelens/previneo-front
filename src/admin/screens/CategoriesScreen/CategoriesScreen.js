import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormCategory'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import FilterSelect from 'admin/components/FilterSelect'
import { CompanyColumn, LanguageColumn } from 'admin/columns'
import { GENDERS } from 'config/constants'

function CategoriesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Categories'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.categories.title' }),
        intl.formatMessage({ id: 'admin.categories.title.plural' })
      ]}
      routeBase={routes.admin.questionnaire.categories}
      permissions={PERMISSIONS.categories}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          filterable: true
        },
        LanguageColumn({
          accessor: 'idioma.nome'
        }),
        {
          accessor: 'genero.nome',
          id: 'genero',
          Header: intl.formatMessage({ id: 'admin.common.gender' }),
          filterable: true,
          Filter: ({ onChange, filter }) =>
            <FilterSelect
              onChange={onChange}
              filter={filter}
              options={Object.values(GENDERS).map(g => ({ ...g, name: intl.formatMessage({ id: g.name }) }))}
            />
        },
        {
          accessor: 'roteiro.nome',
          id: 'roteiro_nome',
          Header: intl.formatMessage({ id: 'admin.common.script' }),
          filterable: true
        },
        CompanyColumn({
          accessor: 'roteiro.empresa.nome'
        })
      ]}
    />
  )
}

export default injectIntl(CategoriesScreen)
