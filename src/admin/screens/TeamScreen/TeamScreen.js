import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormTeam'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function TeamScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Team'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.team.title' }),
        intl.formatMessage({ id: 'admin.team.title.plural' })
      ]}
      routeBase={routes.admin.site.team}
      permissions={PERMISSIONS.team}
      resourceListProps={{
        params: {
          all_languages: true
        }
      }}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(TeamScreen)
