import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormHealthInsurance'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function HealthInsurancesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='HealthInsurances'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.healthInsurance.title' }),
        intl.formatMessage({ id: 'admin.healthInsurance.title.plural' })
      ]}
      routeBase={routes.admin.healthInsurances}
      permissions={PERMISSIONS.healthInsurances}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          filterable: true
        }
      ]}
    />
  )
}

export default injectIntl(HealthInsurancesScreen)
