import React, { Component } from 'react'
import { injectIntl } from 'react-intl'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import PERMISSIONS from 'config/permissions'
import FormScriptSend from 'admin/forms/FormScriptSend'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import DataTable from 'admin/components/DataTable'
import Section from 'admin/components/Section'
import Link from 'components/Link'
import Button from 'components/Button'

class ScriptSendScreen extends Component {
  state = {
    data: []
  }

  render () {
    const { intl } = this.props

    return (
      <ProtectedScreen
        title={intl.formatMessage({ id: 'admin.scriptSend.title' })}
        permission={PERMISSIONS.anamnesis.create}
      >
        <Resource
          resource='ScriptSend'
          notifications={{
            create: { success: false, errors: true }
          }}
        >
          {props => (
            <React.Fragment>
              <FormScriptSend
                onSubmit={data => {
                  return props.create(data).then(data => {
                    this.setState({ data: data.value.data })
                  })
                }}
                isSubmitting={props.isSubmitting}
              />
              {this.state.data.length > 0 && (
                <Section title={intl.formatMessage({ id: 'admin.scriptSend.sentScripts' })}>
                  <VerticalSpacer>
                    <DataTable
                      filterable={false}
                      data={this.state.data}
                      showPagination={false}
                      columns={[
                        {
                          accessor: 'nome',
                          Header: intl.formatMessage({ id: 'admin.common.name' })
                        },
                        {
                          accessor: 'status',
                          Header: intl.formatMessage({ id: 'admin.common.status' })
                        },
                        {
                          accessor: 'link',
                          Header: intl.formatMessage({ id: 'admin.common.link' }),
                          Cell: ({ original }) => (
                            <div>
                              <Link component='a' href={original.link} target='_blank'>
                                {original.link}
                              </Link>
                            </div>
                          )
                        }
                      ]}
                    />
                    <Resource resource='DataExports'>
                      {({ create, createdRecord, isSubmitting }) => (
                        <React.Fragment>
                          <Button
                            size='small'
                            onClick={() => create({
                              fields: {
                                nome: intl.formatMessage({ id: 'admin.common.name' }),
                                status: intl.formatMessage({ id: 'admin.common.status' }),
                                link: intl.formatMessage({ id: 'admin.common.link' })
                              },
                              content: props.createdRecord.data
                            })}
                            disabled={isSubmitting}
                          >
                            {isSubmitting
                              ? intl.formatMessage({ id: 'admin.common.processing' })
                              : intl.formatMessage({ id: 'admin.common.export' })
                            }
                          </Button>
                          {createdRecord.data && (
                            <div>
                              <Link
                                component='a'
                                href={createdRecord.data}
                                download
                              >
                                {intl.formatMessage({ id: 'admin.scriptSend.exportCsvFile' })}
                              </Link>
                            </div>
                          )}
                        </React.Fragment>
                      )}
                    </Resource>
                  </VerticalSpacer>
                </Section>
              )}
            </React.Fragment>
          )}
        </Resource>
      </ProtectedScreen>
    )
  }
}

export default injectIntl(ScriptSendScreen)
