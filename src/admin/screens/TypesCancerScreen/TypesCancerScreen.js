import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen/ResourceScreen'
import routes from 'routes'
import Form from 'admin/forms/FormTypeCancer'
import PERMISSIONS from 'config/permissions'

function TypesCancerScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='TypesCancer'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.cancerTypes.title' }),
        intl.formatMessage({ id: 'admin.cancerTypes.title.plural' })
      ]}
      routeBase={routes.admin.questionnaire.typesCancer}
      permissions={PERMISSIONS.typesCancer}
      resourceUpdateProps={{style: { overflow: 'visible' }}}
      resourceNewProps={{style: { overflow: 'visible' }}}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        },
        {
          accessor: 'genero.nome',
          Header: intl.formatMessage({ id: 'admin.common.gender' })
        }
      ]}
    />
  )
}

export default injectIntl(TypesCancerScreen)
