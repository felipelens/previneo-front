import React from 'react'
import DeletedScreen from 'admin/containers/DeletedScreen'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { CompanyColumn } from 'admin/columns'
import { injectIntl } from 'react-intl'

function UsersDeletingScreen ({ intl }) {
  return (
    <DeletedScreen
      title={intl.formatMessage({ id: 'admin.users.deletedUsers' })}
      resource='UsersRestore'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.users.title' }),
        intl.formatMessage({ id: 'admin.users.title.plural' })
      ]}
      routeBase={routes.admin.users.users}
      permissions={PERMISSIONS.users}
      resourceDetailsProps={{ wrapped: false }}
      params={{ lixeira: true }}
      columns={[
        {
          accessor: 'name',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          id: 'nome',
          filterable: true
        },
        {
          accessor: 'email',
          Header: intl.formatMessage({ id: 'admin.common.email' }),
          filterable: true
        },
        CompanyColumn()
      ]}
    />
  )
}

export default injectIntl(UsersDeletingScreen)
