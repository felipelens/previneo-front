import React from 'react'
import { injectIntl } from 'react-intl'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import PERMISSIONS from 'config/permissions'
import Section from 'admin/components/Section'
import Text from 'components/Text'
import VerticalSpacer from 'components/VerticalSpacer'
import Resource from 'containers/Resource'
import FormPatientsSelect from 'admin/forms/FormPatientsSelect'
import QueueTable from 'admin/components/QueueTable'

function PatientsExportingScreen ({ intl }) {
  return (
    <ProtectedScreen title={intl.formatMessage({ id: 'admin.patients.exportPatients' })} permission={PERMISSIONS.patients.create}>
      <VerticalSpacer space={45}>
        <Section title={intl.formatMessage({ id: 'admin.patients.exportPatients' })}>
          <VerticalSpacer>
            <Text>
              {intl.formatMessage({ id: 'admin.patients.exportText' })}
            </Text>
            <Resource resource='PatientsExporting'>
              {props => (
                <FormPatientsSelect
                  isSubmitting={props.isSubmitting}
                  onSubmit={(data, form) => {
                    props.create(data).then(() => form.reset())
                  }}
                  buttonTitle={intl.formatMessage({ id: 'admin.common.export' })}
                />
              )}
            </Resource>
          </VerticalSpacer>
        </Section>
        <Section title={intl.formatMessage({ id: 'admin.common.export' })}>
          <QueueTable resource='PatientsExporting' />
        </Section>
      </VerticalSpacer>
    </ProtectedScreen>
  )
}

export default injectIntl(PatientsExportingScreen)
