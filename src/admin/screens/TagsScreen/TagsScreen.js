import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormTag'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function TagsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Tags'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.tag' }),
        intl.formatMessage({ id: 'admin.common.tags' })
      ]}
      routeBase={routes.admin.blog.tags}
      permissions={PERMISSIONS.tags}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(TagsScreen)
