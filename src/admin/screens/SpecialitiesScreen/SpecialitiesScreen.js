import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormSpeciality'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function SpecialitiesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Specialities'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.specialities.title' }),
        intl.formatMessage({ id: 'admin.specialities.title.plural' })
      ]}
      routeBase={routes.admin.site.specialities}
      permissions={PERMISSIONS.specialities}
      resourceListProps={{
        params: {
          all_languages: true
        }
      }}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(SpecialitiesScreen)
