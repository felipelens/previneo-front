import React from 'react'
import { injectIntl } from 'react-intl'
import AdminWrapper from 'admin/components/AdminWrapper'
import GuestScreen from 'admin/components/GuestScreen'
import Login from 'admin/containers/Login'

function LoginScreen ({ intl }) {
  return (
    <AdminWrapper>
      <GuestScreen
        title={`${intl.formatMessage({ id: 'admin.forms.login.title' })} - PreviNEO`}
      >
        <Login />
      </GuestScreen>
    </AdminWrapper>
  )
}

export default injectIntl(LoginScreen)
