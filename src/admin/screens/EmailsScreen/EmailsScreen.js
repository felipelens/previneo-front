import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormEmails'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { CompanyColumn } from 'admin/columns'

function EmailsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Emails'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.emailContent' }),
        intl.formatMessage({ id: 'admin.common.emailContents' })
      ]}
      routeBase={routes.admin.questionnaire.emails}
      permissions={PERMISSIONS.emails}
      Form={Form}
      columns={[
        {
          accessor: 'assunto',
          Header: intl.formatMessage({ id: 'admin.common.subject' }),
          filterable: true
        },
        CompanyColumn({
          accessor: 'empresa.nome',
          id: 'empresa'
        })
      ]}
    />
  )
}

export default injectIntl(EmailsScreen)
