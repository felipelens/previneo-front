import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormTestimony'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { CompanyColumn } from 'admin/columns'
import { injectIntl } from 'react-intl'

function TestimoniesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Testimonies'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.testimonies.title' }),
        intl.formatMessage({ id: 'admin.testimonies.title.plural' })
      ]}
      routeBase={routes.admin.site.testimonies}
      permissions={PERMISSIONS.testimonies}
      resourceListProps={{
        params: {
          all_languages: true
        }
      }}
      Form={Form}
      columns={[
        CompanyColumn(),
        {
          accessor: 'nome',
          filterable: true,
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          width: 200
        },
        {
          accessor: 'descricao',
          Header: intl.formatMessage({ id: 'admin.testimonies.title.plural' })
        }
      ]}
    />
  )
}

export default injectIntl(TestimoniesScreen)
