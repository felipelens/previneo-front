import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormContactInfo'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function ContactInfosScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='ContactInfos'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.contactInfo' }),
        intl.formatMessage({ id: 'admin.common.contactInfos' })
      ]}
      routeBase={routes.admin.site.contactInfos}
      permissions={PERMISSIONS.contactInfos}
      Form={Form}
      columns={[
        {
          accessor: 'tipo',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(ContactInfosScreen)
