import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormUser'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { UserColumn } from 'admin/columns'

function UsersScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Users'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.users.title' }),
        intl.formatMessage({ id: 'admin.users.title.plural' })
      ]}
      routeBase={routes.admin.users.users}
      permissions={PERMISSIONS.users}
      Form={props => <Form {...props} />}
      resourceUpdateProps={{
        wrapped: false,
        style: { overflow: 'visible' }
      }}
      resourceDetailsProps={{style: { overflow: 'visible' }}}
      resourceNewProps={{style: { overflow: 'visible' }}}
      dataTableProps={{ filterable: true }}
      columns={[
        UserColumn(),
        {
          accessor: 'email',
          Header: intl.formatMessage({ id: 'admin.common.email' }),
          width: 300,
          filterable: true
        }
      ]}
    />
  )
}

export default injectIntl(UsersScreen)
