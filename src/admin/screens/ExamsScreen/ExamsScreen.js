import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormExams'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function ExamsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='RecomendedExams'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.exams.title' }),
        intl.formatMessage({ id: 'admin.exams.title.plural' })
      ]}
      routeBase={routes.admin.exams}
      permissions={PERMISSIONS.exams}
      canRemove={false}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          filterable: true
        }
      ]}
    />
  )
}

export default injectIntl(ExamsScreen)
