import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormPermission'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function PermissionsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Permissions'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.permissions.title' }),
        intl.formatMessage({ id: 'admin.permissions.title.plural' })
      ]}
      routeBase={routes.admin.permissions}
      permissions={PERMISSIONS.permissions}
      canAdd={false}
      canRemove={false}
      Form={Form}
      columns={[
        {
          accessor: 'description',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        },
        {
          accessor: 'entity',
          Header: intl.formatMessage({ id: 'admin.common.entity' })
        }
      ]}
    />
  )
}

export default injectIntl(PermissionsScreen)
