import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormGender'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function GendersScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Genders'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.gender' }),
        intl.formatMessage({ id: 'admin.common.genders' })
      ]}
      routeBase={routes.admin.genders}
      permissions={PERMISSIONS.genders}
      canAdd={false}
      canRemove={false}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(GendersScreen)
