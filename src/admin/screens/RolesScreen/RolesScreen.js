import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormRole'
import FilterInput from 'admin/components/FilterInput'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function RolesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Roles'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.roles.title' }),
        intl.formatMessage({ id: 'admin.roles.title.plural' })
      ]}
      routeBase={routes.admin.roles}
      permissions={PERMISSIONS.roles}
      Form={Form}
      columns={[
        {
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          accessor: 'name',
          id: 'nome',
          filterable: true,
          Filter: ({ filter, onChange }) =>
            <FilterInput
              filter={filter}
              onChange={onChange}
            />
        }
      ]}
    />
  )
}

export default injectIntl(RolesScreen)
