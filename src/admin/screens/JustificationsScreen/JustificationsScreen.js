import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen/ResourceScreen'
import routes from 'routes'
import Form from 'admin/forms/FormJustification'
import PERMISSIONS from 'config/permissions'

function JustificationsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Justifications'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.justifications.title' }),
        intl.formatMessage({ id: 'admin.justifications.title.plural' })
      ]}
      routeBase={routes.admin.questionnaire.justifications}
      permissions={PERMISSIONS.justifications}
      resourceUpdateProps={{style: { overflow: 'visible' }}}
      resourceNewProps={{style: { overflow: 'visible' }}}
      canAdd={false}
      canRemove={false}
      Form={Form}
      columns={[
        {
          accessor: 'justificativa',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        },
        {
          accessor: 'risco.risco',
          Header: intl.formatMessage({ id: 'admin.common.risk' })
        },
        {
          accessor: 'tipo_cancer.nome',
          Header: intl.formatMessage({ id: 'admin.cancerTypes.title' })
        }
      ]}
    />
  )
}

export default injectIntl(JustificationsScreen)
