import React from 'react'
import DeletedScreen from 'admin/containers/DeletedScreen'
import Form from 'admin/forms/FormPatient'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { CompanyColumn } from 'admin/columns'
import { injectIntl } from 'react-intl'

function PatientsDeletingScreen ({ intl }) {
  return (
    <DeletedScreen
      title={intl.formatMessage({ id: 'admin.patients.deletedPatients' })}
      resource='PatientsRestore'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.patients.title' }),
        intl.formatMessage({ id: 'admin.patients.title.plural' })
      ]}
      routeBase={routes.admin.patients.patients}
      permissions={PERMISSIONS.patients}
      resourceDetailsProps={{ wrapped: false }}
      params={{ lixeira: true }}
      Form={Form}
      columns={[
        {
          accessor: 'name',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          id: 'nome',
          filterable: true
        },
        {
          accessor: 'email',
          Header: intl.formatMessage({ id: 'admin.common.email' }),
          filterable: true
        },
        CompanyColumn()
      ]}
    />
  )
}

export default injectIntl(PatientsDeletingScreen)
