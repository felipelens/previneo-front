import React from 'react'
import { Form, Field } from 'react-final-form'
import { injectIntl } from 'react-intl'
import { startsWith } from 'ramda'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import withAuth from 'store/utils/withAuth'
import DashboardCharts from 'admin/containers/DashboardCharts/DashboardCharts'
import { Box } from './style'
import DashboardButton from 'admin/components/DashboardButton'
import Grid from '../../components/Grid/Grid'
import routes from 'routes'
import getRoute from '../../../utils/getRoute'

class DashboardScreen extends React.Component {
  state = {
    name: this.props.auth.user.empresa.nome,
    companyId: this.props.auth.user.empresa.id
  }

  shouldComponentUpdate (nextProps, nextState) {
    return this.state.companyId !== nextState.companyId
  }

  render () {
    return (
      <VerticalSpacer space={10}>
        <Form
          onSubmit={() => {}}
          initialValues={{
            empresa: this.props.auth.user.empresa.id
          }}
        >
          {({ values: { empresa } }) => (
            <ProtectedScreen title='Dashboard'>
              <VerticalSpacer space={10}>
                <Resource resource='Companies' autoFetch>
                  {props => (
                    <Field
                      name='empresa'
                      label={this.props.intl.formatMessage({ id: 'admin.common.company' })}
                      id='form_dashboard_company'
                      component={CustomFieldSelect}
                      onChangeOption={value => {
                        const record = props.records.find(record => record.id === value)
                        if (record && this.state.name !== record.name) {
                          this.setState({ name: record.nome, companyId: record.id })
                        }
                      }}
                      customSelectProps={{
                        options: props.records.concat(this.props.auth.user.empresa),
                        labelKey: 'nome',
                        valueKey: 'id',
                        simpleValue: true,
                        isLoading: props.isFetching,
                        placeholder: this.props.intl.formatMessage({ id: 'admin.common.searchForCompany' }),
                        onInputChange: value => {
                          if (value) props.fetchAll({ nome: value })
                          return value
                        }
                      }}
                    />
                  )}
                </Resource>
              </VerticalSpacer>
            </ProtectedScreen>
          )}
        </Form>
        <Box style={{ padding: '10px' }}>
          <div style={{ minWidth: '500px' }}>
            <Grid spacing={10} breakpoint='small'>
              <DashboardButton
                isActive={checkIndexPage}
                to={routes.admin.dashboard.index}
                title={this.props.intl.formatMessage({ id: 'admin.dashboard.charts.summary' })}
              />
              <DashboardButton
                to={routes.admin.dashboard.anamnesis}
                title={this.props.intl.formatMessage({ id: 'admin.dashboard.charts.totalFilled' })}
              />
              <DashboardButton
                to={routes.admin.dashboard.anamnesisByGender}
                title={this.props.intl.formatMessage({ id: 'admin.dashboard.charts.totalFilledByGender' })}
              />
              <DashboardButton
                isActive={checkIndexHigh}
                to={getRoute(routes.admin.dashboard.highRisk.type, { tipo_cancer_id: 1 })}
                title={this.props.intl.formatMessage({ id: 'admin.dashboard.charts.highRisk' })}
              />
              <DashboardButton
                isActive={checkIndexNormal}
                to={getRoute(routes.admin.dashboard.normalRisk.type, { tipo_cancer_id: 1 })}
                title={this.props.intl.formatMessage({ id: 'admin.dashboard.charts.normalRisk' })}
              />
            </Grid>
          </div>
        </Box>
        <DashboardCharts name={this.state.name} company={this.state.companyId} />
      </VerticalSpacer>
    )
  }
}

const checkIndexPage = (match, location) => {
  const current = location && location.pathname

  if (current === routes.admin.dashboard.index || current === routes.admin.index) {
    return true
  }
  return false
}

const checkIndexHigh = (match, location) => {
  const current = location && location.pathname

  return startsWith(routes.admin.dashboard.highRisk.index, current)
}

const checkIndexNormal = (match, location) => {
  const current = location && location.pathname

  return startsWith(routes.admin.dashboard.normalRisk.index, current)
}

export default withAuth(injectIntl(DashboardScreen))
