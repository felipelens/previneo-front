import styled from 'styled-components'
import Base from '../../../components/Base/Base'
import React from 'react'

export const Box = styled(props => <Base {...props} />)`
  background-color: #ffffff;
  border: 1px solid #e5e5e5;
  padding: 20px 35px;
  box-sizing: border-box;
  position: relative;
  overflow: auto;
`
