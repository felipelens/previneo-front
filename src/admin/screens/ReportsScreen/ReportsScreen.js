import React from 'react'
import { injectIntl } from 'react-intl'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import PERMISSIONS from 'config/permissions'
import PatientsFilter from 'admin/containers/PatientsFilter'

function ReportsScreen ({ intl }) {
  return (
    <ProtectedScreen
      title={intl.formatMessage({ id: 'admin.common.reports' })}
      permission={PERMISSIONS.reports.viewAll}
    >
      <PatientsFilter />
    </ProtectedScreen>
  )
}

export default injectIntl(ReportsScreen)
