import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormPatient'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { CompanyColumn } from 'admin/columns'
import { injectIntl } from 'react-intl'

function PatientsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Patients'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.patients.title' }),
        intl.formatMessage({ id: 'admin.patients.title.plural' })
      ]}
      routeBase={routes.admin.patients.patients}
      permissions={PERMISSIONS.patients}
      resourceDetailsProps={{ wrapped: false }}
      resourceUpdateProps={{style: { overflow: 'visible' }}}
      Form={Form}
      columns={[
        {
          accessor: 'name',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          id: 'nome',
          filterable: true
        },
        {
          accessor: 'email',
          Header: intl.formatMessage({ id: 'admin.common.email' }),
          filterable: true
        },
        CompanyColumn()
      ]}
    />
  )
}

export default injectIntl(PatientsScreen)
