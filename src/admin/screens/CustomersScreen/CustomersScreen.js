import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormCustomer'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function UsersScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Customers'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.customer' }),
        intl.formatMessage({ id: 'admin.common.customers' })
      ]}
      routeBase={routes.admin.site.customers}
      permissions={PERMISSIONS.customers}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' }),
          filterable: true
        }
      ]}
    />
  )
}

export default injectIntl(UsersScreen)
