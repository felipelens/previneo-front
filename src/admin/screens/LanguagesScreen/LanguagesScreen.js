import React from 'react'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormLanguage'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { injectIntl } from 'react-intl'

function LanguagesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Languages'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.languages.title' }),
        intl.formatMessage({ id: 'admin.languages.title.plural' })
      ]}
      routeBase={routes.admin.languages}
      permissions={PERMISSIONS.languages}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        },
        {
          accessor: 'slug',
          Header: intl.formatMessage({ id: 'admin.languages.slug' })
        }
      ]}
    />
  )
}

export default injectIntl(LanguagesScreen)
