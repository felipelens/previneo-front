import React from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormPatientHistory'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { ContactStatusColumn, ContactReasonColumn, FilterDateRangeColumn, RisksColumn } from 'admin/columns'
import { FormattedMessage, injectIntl } from 'react-intl'
import Button from 'components/Button'
import ResourceUpdate from 'admin/containers/ResourceUpdate'
import getRoute from 'utils/getRoute'
import Auth from 'admin/containers/Auth'
import Text from 'components/Text'
import { CompanyColumn } from '../../columns'

const ActionButton = props => (
  <React.Fragment>
    <Button
      icon={require('@fortawesome/fontawesome-free-solid/faEdit')}
      component={Link}
      target='_blank'
      to={getRoute(
        routes.admin.patientsHistory.patientsHistory.manage,
        { id: props.paciente.id, patientHistoryId: props.id })
      }
      size='small'
    />
  </React.Fragment>
)

function PatientsHistoryScreen ({ intl }) {
  return (
    <Auth>
      {({ permissions }) => {
        const viewPermission = permissions.includes(PERMISSIONS.patientsHistoryContact.view)

        return (
          <ResourceScreen
            resource='PatientsHistory'
            readableResourceName={[
              intl.formatMessage({ id: 'admin.medicalRecords.title' }),
              intl.formatMessage({ id: 'admin.patientsHistory.titlePlural' })
            ]}
            routeBase={routes.admin.patientsHistory.patientsHistory}
            permissions={PERMISSIONS.patientsHistoryContact}
            resourceListProps={
              { autoRefresh: true,
                autoRefreshTime: 1800000,
                showActionColumns: viewPermission,
                minRows: 15
              }
            }
            resourceDetailsProps={{ wrapped: false }}
            Form={Form}
            canAdd={false}
            canRemove={false}
            canShowDetails={false}
            canEdit={false}
            actionColumnWidth={100}
            extraActionButton={ActionButton}
            showActionColumns={false}
            columns={[
              FilterDateRangeColumn({
                dateAccessor: 'data_exibir_notificacao',
                message: 'admin.common.nextContact'
              }),
              {
                accessor: 'paciente.name',
                Header: intl.formatMessage({ id: 'admin.patients.title' }),
                id: 'nome',
                filterable: true
              },
              CompanyColumn(),
              ContactReasonColumn(),
              RisksColumn(),
              ContactStatusColumn()
            ]}
          />
        )
      }
      }
    </Auth>
  )
}

const PatientsHistoryScreenWithIntl = injectIntl(PatientsHistoryScreen)

function PatientsHistoryScreenWithCustomRoute ({ intl }) {
  return (
    <Auth>
      {({ permissions }) => {
        return (
          <Switch>
            <Route
              exact
              path={routes.admin.patientsHistory.patientsHistory.manage}
              render={({match}) => {
                return (
                  permissions.includes(PERMISSIONS.patientsHistoryContact.view)
                    ? (
                      <ResourceUpdate
                        resource={'PatientsHistoryLogs'}
                        title=''
                        routeBase={routes.admin.patientsHistory.patientsHistory}
                        Form={props =>
                          <Form {...props}
                            patientHistoryId={parseInt(match.params.patientHistoryId, 10)}
                          />
                        }
                        id={parseInt(match.params.id, 10)}
                        redirectAfterSuccess={false}
                        useDiff
                      />
                    ) : <Text><FormattedMessage id='admin.common.noPermission' /></Text>
                )
              }
              }
            />
            <Route
              exact
              path={routes.admin.patientsHistory.patientsHistory.index}
              component={PatientsHistoryScreenWithIntl}
            />
          </Switch>
        )
      }}
    </Auth>
  )
}

export default injectIntl(PatientsHistoryScreenWithCustomRoute)
