import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormLayouts'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { CompanyColumn } from 'admin/columns'

function LayoutsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Layouts'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.emailLayout' }),
        intl.formatMessage({ id: 'admin.common.emailLayouts' })
      ]}
      routeBase={routes.admin.questionnaire.layouts}
      permissions={PERMISSIONS.layouts}
      Form={Form}
      columns={[
        CompanyColumn({
          accessor: 'empresa.nome',
          id: 'empresa'
        })
      ]}
    />
  )
}

export default injectIntl(LayoutsScreen)
