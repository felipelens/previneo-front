import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormStrategy'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import { GenderColumn, CompanyColumn } from 'admin/columns'

function StrategyScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Strategies'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.strategies.title' }),
        intl.formatMessage({ id: 'admin.strategies.title.plural' })
      ]}
      routeBase={routes.admin.strategy}
      permissions={PERMISSIONS.strategy}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          id: 'tipo_cancer_nome',
          Header: intl.formatMessage({ id: 'admin.cancerTypes.title' }),
          filterable: true
        },
        {
          accessor: 'risco.risco',
          id: 'risco_nome',
          Header: intl.formatMessage({ id: 'admin.common.risk' }),
          filterable: true
        },
        CompanyColumn({
          accessor: 'empresa.nome',
          id: 'empresa'
        }),
        GenderColumn()
      ]}
    />
  )
}

export default injectIntl(StrategyScreen)
