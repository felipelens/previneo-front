import React from 'react'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import PERMISSIONS from 'config/permissions'
import Section from 'admin/components/Section'
import Resource from 'containers/Resource'
import FormPatientImport from 'admin/forms/FormPatientImport'
import { injectIntl } from 'react-intl'

function PatientsImportingScreen ({ intl }) {
  return (
    <ProtectedScreen title={intl.formatMessage({ id: 'admin.patients.importPatients' })} permission={PERMISSIONS.patients.create}>
      <Section title={intl.formatMessage({ id: 'admin.patients.importPatients' })}>
        <Resource resource='PatientsImporting'>
          {props => (
            <FormPatientImport
              onSubmit={(values, form) => {
                props.create(values).then(() => form.reset())
              }}
              isSubmitting={props.isSubmitting}
            />
          )}
        </Resource>
      </Section>
    </ProtectedScreen>
  )
}

export default injectIntl(PatientsImportingScreen)
