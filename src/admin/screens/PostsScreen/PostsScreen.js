import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormPost'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function PostsScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Posts'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.post' }),
        intl.formatMessage({ id: 'admin.common.posts' })
      ]}
      routeBase={routes.admin.blog.posts}
      permissions={PERMISSIONS.posts}
      resourceListProps={{
        params: {
          all_languages: true
        }
      }}
      Form={Form}
      columns={[
        {
          accessor: 'titulo',
          filterable: true,
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(PostsScreen)
