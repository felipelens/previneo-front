import React from 'react'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import Companies from 'admin/containers/Companies'
import PERMISSIONS from 'config/permissions'

export default function CompaniesScreen () {
  return (
    <ProtectedScreen title='Empresas' permission={PERMISSIONS.companies.viewAll}>
      <Companies />
    </ProtectedScreen>
  )
}
