import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormContactReason'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function ContactReasonScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='ContactReason'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.contactReason' }),
        intl.formatMessage({ id: 'admin.patientsHistory.contactReasonPlural' })
      ]}
      routeBase={routes.admin.patientsHistory.contactReason}
      permissions={PERMISSIONS.patientsHistoryContactReason}
      resourceUpdateProps={{style: { overflow: 'visible' }}}
      resourceNewProps={{style: { overflow: 'visible' }}}
      Form={Form}
      columns={[{
        accessor: 'motivo',
        Header: intl.formatMessage({ id: 'admin.common.motivo' })
      }]}
    />
  )
}

export default injectIntl(ContactReasonScreen)
