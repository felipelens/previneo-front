import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen/ResourceScreen'
import routes from 'routes'
import Form from 'admin/forms/FormRisk'
import PERMISSIONS from 'config/permissions'

function RisksScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='Risks'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.risk' }),
        intl.formatMessage({ id: 'admin.common.risks' })
      ]}
      routeBase={routes.admin.risks}
      permissions={PERMISSIONS.risks}
      resourceUpdateProps={{style: { overflow: 'visible' }}}
      resourceNewProps={{style: { overflow: 'visible' }}}
      canAdd={false}
      canRemove={false}
      Form={Form}
      columns={[
        {
          accessor: 'risco',
          Header: intl.formatMessage({ id: 'admin.common.risk' })
        }
      ]}
    />
  )
}

export default injectIntl(RisksScreen)
