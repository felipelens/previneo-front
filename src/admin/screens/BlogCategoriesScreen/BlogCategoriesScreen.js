import React from 'react'
import { injectIntl } from 'react-intl'
import ResourceScreen from 'admin/containers/ResourceScreen'
import Form from 'admin/forms/FormBlogCategory'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'

function BlogCategoriesScreen ({ intl }) {
  return (
    <ResourceScreen
      resource='BlogCategories'
      readableResourceName={[
        intl.formatMessage({ id: 'admin.common.blogCategory' }),
        intl.formatMessage({ id: 'admin.common.blogCategories' })
      ]}
      routeBase={routes.admin.blog.categories}
      permissions={PERMISSIONS.blogCategories}
      resourceListProps={{
        params: {
          all_languages: true
        }
      }}
      Form={Form}
      columns={[
        {
          accessor: 'nome',
          Header: intl.formatMessage({ id: 'admin.common.name' })
        }
      ]}
    />
  )
}

export default injectIntl(BlogCategoriesScreen)
