import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import Section from 'admin/components/Section'
import CustomField from 'components/CustomField'
import VerticalSpacer from 'components/VerticalSpacer'
import FormGrid from 'components/FormGrid'
import ButtonSubmit from 'components/ButtonSubmit'
import * as forms from 'utils/forms'
import { equalTo } from 'utils/validators'

export default function FormUserPassword ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Section title={intl.formatMessage({ id: 'admin.common.editPassword' })}>
      <Form {...props}>
        {({ handleSubmit, pristine }) => (
          <form onSubmit={handleSubmit}>
            <VerticalSpacer>
              <FormGrid>
                <Field
                  name='old_password'
                  label={intl.formatMessage({ id: 'admin.common.oldPassword' })}
                  id='form_user_old_password'
                  component={CustomField}
                  {...forms.passwordRequired}
                />
                <Field
                  name='password'
                  label={intl.formatMessage({ id: 'admin.common.password' })}
                  id='form_user_password'
                  component={CustomField}
                  {...forms.passwordRequired}
                />
                <Field
                  name='password_confirmation'
                  label={intl.formatMessage({ id: 'admin.common.confirmPassword' })}
                  id='form_user_password_confirmation'
                  component={CustomField}
                  {...forms.password}
                  validate={equalTo({
                    field: 'password',
                    message: intl.formatMessage({ id: 'validations.passwordConfirm' })
                  })}
                />
              </FormGrid>
              {!readOnly && (
                <ButtonSubmit
                  isSubmitting={isSubmitting}
                  disabled={pristine}
                  label={intl.formatMessage({ id: 'admin.common.save' })}
                />
              )}
            </VerticalSpacer>
          </form>
        )}
      </Form>
    </Section>
  )
}

FormUserPassword.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}
