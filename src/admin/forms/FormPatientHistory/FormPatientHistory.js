import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import Base from 'components/Base'
import VerticalSpacer from 'components/VerticalSpacer'
import Section from 'admin/components/Section'
import PatientHistoryMedicalRecords from 'admin/containers/PatientHistoryMedicalRecords'
import PatientHistoryLogs from 'admin/containers/PatientHistoryLogs'
import { isEmpty } from 'ramda'
import FormPatientHistoryActions from 'admin/forms/FormPatientHistoryActions'
import Auth from 'admin/containers/Auth'
import PERMISSIONS from 'config/permissions'

function FormPatientHistory ({ intl, isSubmitting, readOnly, title, ...props }) {
  const userName = !isEmpty(props.initialValues.logs)
    ? props.initialValues.paciente.name
    : intl.formatMessage({ id: 'admin.patientsHistory.contact' })

  return (
    <Auth>
      {({ permissions }) => {
        const readOnly = !permissions.includes(PERMISSIONS.patientsHistoryContact.update)
        const side = !readOnly ? (<FormPatientHistoryActions {...props} />) : null
        return (
          <VerticalSpacer space={45}>
            <Base
              component={Section}
              title={userName}
              side={side}
            >
              <PatientHistoryLogs readOnly={readOnly} patientId={props.resource.id} {...props} />
            </Base>
            <PatientHistoryMedicalRecords patientId={props.resource.id} {...props} />
          </VerticalSpacer>
        )
      }
      }
    </Auth>
  )
}

FormPatientHistory.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormPatientHistory)
