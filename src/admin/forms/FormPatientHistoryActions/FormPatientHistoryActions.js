import React from 'react'
import Resource from 'containers/Resource'
import { Form } from 'react-final-form'
import ButtonSubmit from 'components/ButtonSubmit'
import { CONTACT_STATUS } from 'config/constants'
import Grid from 'admin/components/Grid'
import { injectIntl } from 'react-intl'
import Text from 'components/Text'
import Icon from '@fortawesome/react-fontawesome'

function ActionButtons ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <ButtonSubmit
            disabled={props.disabled}
            label={intl.formatMessage({ id: props.label })}
            color={props.color}
            isSubmitting={isSubmitting}
            submittingLabel={intl.formatMessage({ id: props.label })}
          />
        </form>
      )}
    </Form>
  )
}

const ActionButtonsWithIntl = injectIntl(ActionButtons)

function ActionForms ({ intl, color, label, action, patientHistoryId, disabled }) {
  return (
    <Resource resource='PatientsHistoryActions'>
      {props => (
        <ActionButtonsWithIntl
          onSubmit={() => {
            props.create(patientHistoryId, action)
          }}
          label={label}
          color={color}
          patientHistoryId={patientHistoryId}
          disabled={disabled}
          isSubmitting={props.isSubmitting}
        />
      )}
    </Resource>
  )
}

/**
 * @return {null}
 */
function ButtonEditMode ({ intl, mode, patientHistoryId, disabled }) {
  return (
    <ActionForms
      color={CONTACT_STATUS[mode].color}
      label={CONTACT_STATUS[mode].label}
      action={CONTACT_STATUS[mode].action}
      patientHistoryId={patientHistoryId}
      disabled={disabled}
    />
  )
}

export default function FormPatientHistoryActions ({ ...props }) {
  return (
    <Resource
      resource='PatientsHistoryActions'
      patientHistoryId={props.patientHistoryId}
      autoFetch
      id={props.patientHistoryId}
    >
      {(props) => {
        const status = props.hasDetailedRecord &&
          props.detailedRecord.statuses[0].comparavel

        return (
          <div>
            {props.hasDetailedRecord ? (
              status === 'DONE'
                ? (
                  <Grid spacing={10} >
                    <ButtonEditMode mode={'PENDING'} patientHistoryId={props.patientHistoryId} />
                    <ButtonEditMode mode='DONE' patientHistoryId={props.patientHistoryId} disabled />
                  </Grid>
                )
                : (
                  <div>
                    <Grid spacing={10} >
                      <ButtonEditMode mode={status} patientHistoryId={props.patientHistoryId} />
                      <ButtonEditMode
                        mode='DONE'
                        patientHistoryId={props.patientHistoryId}
                        disabled={status === 'PENDING'}
                      />
                    </Grid>
                    {status === 'IN_PROGRESS' && (
                      <Text color='warning'>
                        <Icon icon={require('@fortawesome/fontawesome-free-solid/faUserMd')} />
                        &nbsp;{props.detailedRecord.statuses[0].causer.name}
                      </Text>
                    )}
                  </div>
                )
            ) : null
            }
          </div>
        )
      }}
    </Resource>
  )
}
