import React from 'react'
import { isEmpty } from 'ramda'
import Resource from 'containers/Resource'
import { Field } from 'react-final-form'
import { FieldArray } from 'react-final-form-arrays'
import Icon from '@fortawesome/react-fontawesome'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomField from 'components/CustomField'
import VerticalSpacer from 'components/VerticalSpacer'
import Button from 'components/Button'
import { QuestionFieldContainer } from './styles'

const getQuestionsOptions = categories => {
  return categories.reduce((prev, value) => {
    return prev.concat(value.perguntas.map(p => ({
      id: p.id,
      name: `${p.nome} / ${value.nome}`,
      respostas: p.respostas
    })))
  }, [])
}

export default function ScriptQuestions ({ values, intl, form }) {
  return (
    <Resource
      resource='ScriptQuestions'
      id={values.roteiro}
      autoFetch
      spinner
    >
      {({ detailedRecord }) => {
        if (isEmpty(detailedRecord)) {
          return null
        }

        return (
          <FieldArray name='perguntas'>
            {({ fields }) => (
              <VerticalSpacer>
                {fields.map((name, index) => {
                  const questions = getQuestionsOptions(detailedRecord.categorias)
                  const selectedQuestion = questions.find(q => {
                    if (values.perguntas[index] && values.perguntas[index].pergunta) {
                      return q.id === values.perguntas[index].pergunta
                    }

                    return {}
                  })

                  return (
                    <QuestionFieldContainer key={name}>
                      <Field
                        name={`${name}.pergunta`}
                        component={CustomFieldSelect}
                        label={intl.formatMessage({ id: 'admin.script.question' })}
                        onChangeOption={value => form.change(`${name}.resposta`, null)}
                        customSelectProps={{
                          options: questions,
                          labelKey: 'name',
                          valueKey: 'id'
                        }}
                      />
                      {!isEmpty(selectedQuestion) && selectedQuestion.respostas.length > 0 ? (
                        <Field
                          name={`${name}.resposta`}
                          component={CustomFieldSelect}
                          label={intl.formatMessage({ id: 'admin.script.answer' })}
                          customSelectProps={{
                            options: selectedQuestion.respostas,
                            labelKey: 'resposta',
                            valueKey: 'resposta'
                          }}
                        />
                      ) : (
                        <Field
                          name={`${name}.resposta`}
                          component={CustomField}
                          label={intl.formatMessage({ id: 'admin.script.answer' })}
                        />
                      )}
                      <Button
                        type='button'
                        color='danger'
                        size='small'
                        onClick={() => fields.remove(index)}
                        block
                      >
                        <Icon icon={require('@fortawesome/fontawesome-free-solid/faTimes')} />
                      </Button>
                    </QuestionFieldContainer>
                  )
                })}
                <Button
                  type='button'
                  onClick={() => fields.push({ pergunta: '', resposta: '' })}
                  color='success'
                  size='small'
                >
                  <Icon icon={require('@fortawesome/fontawesome-free-solid/faPlus')} />
                </Button>
              </VerticalSpacer>
            )}
          </FieldArray>
        )
      }}
    </Resource>
  )
}
