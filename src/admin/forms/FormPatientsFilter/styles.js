import styled from 'styled-components'

export const QuestionFieldContainer = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 0 -10px;

  > * {
    width: 50%;
    margin: 0 10px;
  }
  
  > :last-child {
    width: 100px;
    flex-shrink: 0;
    margin-top: 24px;
    height: 40px;
    font-size: 20px;
  }
`
