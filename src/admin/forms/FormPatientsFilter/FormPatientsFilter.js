import React from 'react'
import PropTypes from 'prop-types'
import { isEmpty } from 'ramda'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import VerticalSpacer from 'components/VerticalSpacer'
import FormGrid from 'components/FormGrid'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomDateRange from 'admin/components/CustomDateRange'
import SubmissionContainer from 'admin/components/SubmissionContainer'
import ButtonSubmit from 'components/ButtonSubmit'
import Button from 'components/Button'
import Resource from 'containers/Resource'
import * as forms from 'utils/forms'
import { GENDERS } from 'config/constants'
import ScriptQuestions from './ScriptQuestions'

const normalize = (values, fields) => {
  if (fields.length === 0) {
    return values
  }

  if (values[fields[0]] && values[fields[0]].length > 0) {
    values[fields[0]] = values[fields[0]].map(v => v.id)
  }

  fields.shift()

  return normalize(values, fields)
}

function FormPatientsFilter ({ onSubmit, isSubmitting, intl, readOnly, ...props }) {
  return (
    <Form
      onSubmit={(values, form) => {
        const data = normalize(
          { ...values },
          ['empresas', 'estados', 'cidades', 'tipos_cancer', 'riscos_cancer']
        )

        if (data.date && !isEmpty(data.date)) {
          data.date_start = data.date.start_date
          data.date_end = data.date.end_date
          delete data.date
        }

        return onSubmit(data, form)
      }}
      mutators={arrayMutators}
      {...props}
    >
      {({ handleSubmit, pristine, form, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <FormGrid>
              <Resource resource='Companies' autoFetch>
                {props => (
                  <Field
                    name='empresas'
                    label={intl.formatMessage({ id: 'admin.common.company' })}
                    id='form_filter_companies'
                    component={CustomFieldSelect}
                    disabled={readOnly}
                    customSelectProps={{
                      options: props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      multi: true,
                      simpleValue: false,
                      isLoading: props.isFetching,
                      onInputChange: value => {
                        if (value) props.fetchAll({ nome: value })
                        return value
                      }
                    }}
                  />
                )}
              </Resource>
              <Field
                name='date'
                component={CustomDateRange}
                label={intl.formatMessage({ id: 'admin.common.interval' })}
                id='form_patient_date'
              />
            </FormGrid>
            <FormGrid>
              <Field
                name='genero'
                label={intl.formatMessage({ id: 'admin.common.gender' })}
                id='form_patient_gender'
                component={CustomFieldSelect}
                disabled={readOnly}
                customSelectProps={{
                  options: [
                    {
                      nome: intl.formatMessage({ id: GENDERS.male.name }),
                      id: GENDERS.male.id
                    },
                    {
                      nome: intl.formatMessage({ id: GENDERS.female.name }),
                      id: GENDERS.female.id
                    }
                  ],
                  labelKey: 'nome',
                  valueKey: 'id'
                }}
              />
              <Field
                name='idade.min'
                label={intl.formatMessage({ id: 'admin.common.minimumAge' })}
                component={CustomField}
                {...forms.integer}
              />
              <Field
                name='idade.max'
                label={intl.formatMessage({ id: 'admin.common.maximumAge' })}
                component={CustomField}
                {...forms.integer}
              />
            </FormGrid>
            <FormGrid>
              <Resource resource='States' autoFetch>
                {props => (
                  <Field
                    name='estados'
                    label={intl.formatMessage({ id: 'admin.common.state' })}
                    id='form_filter_states'
                    component={CustomFieldSelect}
                    disabled={readOnly}
                    customSelectProps={{
                      options: props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      multi: true,
                      simpleValue: false,
                      isLoading: props.isFetching,
                      onInputChange: value => {
                        if (value) props.fetchAll({ nome: value })
                        return value
                      }
                    }}
                  />
                )}
              </Resource>
              <Resource resource='Cities' autoFetch>
                {props => (
                  <Field
                    name='cidades'
                    label={intl.formatMessage({ id: 'admin.common.city' })}
                    id='form_filter_cities'
                    component={CustomFieldSelect}
                    disabled={readOnly}
                    customSelectProps={{
                      options: props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      multi: true,
                      simpleValue: false,
                      isLoading: props.isFetching,
                      onInputChange: value => {
                        if (value) props.fetchAll({ nome: value })
                        return value
                      }
                    }}
                  />
                )}
              </Resource>
            </FormGrid>
            <FormGrid>
              <Resource resource='TypesCancer' autoFetch>
                {props => (
                  <Field
                    name='tipos_cancer'
                    label={intl.formatMessage({ id: 'admin.cancerTypes.title.plural' })}
                    id='form_filter_cancer_types'
                    component={CustomFieldSelect}
                    disabled={readOnly}
                    customSelectProps={{
                      options: props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      multi: true,
                      simpleValue: false,
                      isLoading: props.isFetching
                    }}
                  />
                )}
              </Resource>
              <Resource resource='Risks' autoFetch>
                {props => (
                  <Field
                    name='riscos_cancer'
                    label={intl.formatMessage({ id: 'admin.common.risks' })}
                    id='form_filter_risks'
                    component={CustomFieldSelect}
                    disabled={readOnly}
                    customSelectProps={{
                      options: props.records,
                      labelKey: 'risco',
                      valueKey: 'id',
                      multi: true,
                      simpleValue: false,
                      isLoading: props.isFetching
                    }}
                  />
                )}
              </Resource>
            </FormGrid>
            <Resource resource='Scripts' autoFetch>
              {props => (
                <Field
                  name='roteiro'
                  label={intl.formatMessage({ id: 'admin.common.script' })}
                  id='form_filter_script'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching,
                    onInputChange: value => {
                      if (value) props.fetchAll({ nome: value })
                      return value
                    }
                  }}
                />
              )}
            </Resource>
            {values.roteiro && (
              <ScriptQuestions
                form={form}
                values={values}
                intl={intl}
              />
            )}
            {!readOnly && (
              <SubmissionContainer>
                <ButtonSubmit
                  isSubmitting={isSubmitting}
                  disabled={pristine}
                  label={intl.formatMessage({ id: 'admin.common.send' })}
                />
                <Button
                  type='button'
                  size='small'
                  onClick={() => form.reset()}
                  disabled={pristine}
                >
                  Reset
                </Button>
              </SubmissionContainer>
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormPatientsFilter.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool.isRequired,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormPatientsFilter)
