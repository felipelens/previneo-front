import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import { injectIntl } from 'react-intl'
import Section from 'admin/components/Section'
import VerticalSpacer from 'components/VerticalSpacer'
import CustomField from 'components/CustomField'
import SubmissionContainer from 'admin/components/SubmissionContainer'
import Notifications from 'containers/Notifications'
import ButtonSubmit from 'components/ButtonSubmit'
import Link from 'components/Link'
import * as forms from 'utils/forms'
import routes from 'routes'

function FormLogin ({ isSubmitting, intl, hideForgetPasswordLink, hideNotifications, patient, ...props }) {
  return (
    <Section title={intl.formatMessage({ id: 'admin.forms.login.title' })}>
      <Form {...props}>
        {({ handleSubmit, pristine }) => (
          <form onSubmit={handleSubmit}>
            <VerticalSpacer>
              {!hideNotifications && (
                <Notifications />
              )}
              {!patient ? (
                <Field
                  name='email'
                  label={intl.formatMessage({ id: 'admin.forms.login.email' })}
                  id='form_login_email'
                  component={CustomField}
                  {...forms.emailRequired}
                />
              ) : (
                <Field
                  name='email'
                  label={intl.formatMessage({ id: 'admin.common.cpf' })}
                  id='form_login_email'
                  tooltip={intl.formatMessage({ id: 'hints.cpfNumberOnly' })}
                  component={CustomField}
                  {...forms.required}
                />
              )}
              <Field
                name='password'
                label={intl.formatMessage({ id: 'admin.forms.login.password' })}
                id='form_login_password'
                tooltip={patient ? intl.formatMessage({ id: 'hints.number' }) : ''}
                component={CustomField}
                {...forms.passwordRequired}
              />
              <SubmissionContainer>
                <ButtonSubmit
                  label={intl.formatMessage({ id: 'admin.forms.login.button' })}
                  isSubmitting={isSubmitting}
                  disabled={pristine}
                />
                {!hideForgetPasswordLink && (
                  <Link to={routes.admin.passwordRecovery}>
                    {intl.formatMessage({ id: 'admin.forms.login.passwordRecovery' })}
                  </Link>
                )}
              </SubmissionContainer>
            </VerticalSpacer>
          </form>
        )}
      </Form>
    </Section>
  )
}

FormLogin.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool
}

export default injectIntl(FormLogin)
