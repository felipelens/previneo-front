import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomFieldSelect from '../../components/CustomFieldSelect'
import Resource from '../../../containers/Resource'

function FormTag ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_blog_tag_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={190}
              {...forms.required}
            />
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='idioma.id'
                  label={intl.formatMessage({ id: 'admin.common.language' })}
                  id='form_team_language'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormTag.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormTag)
