import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import Section from 'admin/components/Section'
import { Form, Field } from 'react-final-form'
import VerticalSpacer from 'components/VerticalSpacer'
import Text from 'components/Text'
import CustomField from 'components/CustomField'
import SubmissionContainer from 'admin/components/SubmissionContainer'
import ButtonSubmit from 'components/ButtonSubmit'
import Link from 'components/Link'
import * as forms from 'utils/forms'
import routes from 'routes'

function FormPasswordRecovery ({ isSubmitting, intl, ...props }) {
  return (
    <Section title={intl.formatMessage({ id: 'admin.forms.passwordRecovery.title' })}>
      <Form {...props}>
        {({ handleSubmit, pristine }) => (
          <form onSubmit={handleSubmit}>
            <VerticalSpacer>
              <Text>
                Após completar o formulário abaixo, você receberá por email um link para redefinir sua senha.
              </Text>
              <Field
                name='email'
                id='form_password_recovery_email'
                label={intl.formatMessage({ id: 'admin.forms.login.email' })}
                component={CustomField}
                {...forms.emailRequired}
              />
              <SubmissionContainer>
                <ButtonSubmit
                  label={intl.formatMessage({ id: 'admin.forms.passwordRecovery.button' })}
                  isSubmitting={isSubmitting}
                  disabled={pristine}
                />
                <Link to={routes.admin.login}>
                  {intl.formatMessage({ id: 'admin.common.back' })}
                </Link>
              </SubmissionContainer>
            </VerticalSpacer>
          </form>
        )}
      </Form>
    </Section>
  )
}

FormPasswordRecovery.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool
}

export default injectIntl(FormPasswordRecovery)
