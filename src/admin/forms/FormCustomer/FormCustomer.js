import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import CustomImageUploader from 'admin/components/CustomUploader'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'

function FormCustomer ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_customers_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='url'
              label={intl.formatMessage({ id: 'admin.common.link' })}
              id='form_customers_url'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='logo'
              label={intl.formatMessage({ id: 'admin.common.image' })}
              id='form_customers_logo'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
              {...forms.required}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormCustomer.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormCustomer)
