import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import CustomImageUploader from 'admin/components/CustomUploader'
import CustomEditor from 'admin/components/CustomEditor'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomFieldSelect from '../../components/CustomFieldSelect'
import Resource from '../../../containers/Resource'

function FormTeam ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_team_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='descricao'
              label={intl.formatMessage({ id: 'admin.common.description' })}
              id='form_team_descricao'
              component={CustomEditor}
              readOnly={readOnly}
              {...forms.required}
            />
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='idioma.id'
                  label={intl.formatMessage({ id: 'admin.common.language' })}
                  id='form_team_language'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Field
              name='foto'
              label={intl.formatMessage({ id: 'admin.common.image' })}
              id='form_team_image'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
              {...forms.required}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormTeam.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormTeam)
