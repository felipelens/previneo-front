import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import Resource from 'containers/Resource'
import CustomField from 'components/CustomField'
import CustomEditor from 'admin/components/CustomEditor'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomImageUploader from 'admin/components/CustomUploader'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import * as forms from 'utils/forms'

function FormPost ({ isSubmitting, readOnly, updateResource, intl, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='titulo'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_post_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            {(readOnly || updateResource) && (
              <Field
                name='permalink'
                disabled
                label={intl.formatMessage({ id: 'admin.common.permalink' })}
                id='form_post_permalink'
                component={CustomField}
                readOnly={readOnly}
                maxLength={255}
              />
            )}
            <Field
              name='descricao'
              label={intl.formatMessage({ id: 'admin.common.description' })}
              id='form_post_description'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='conteudo'
              label={intl.formatMessage({ id: 'admin.common.content' })}
              id='form_post_content'
              component={CustomEditor}
              readOnly={readOnly}
              {...forms.required}
            />
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='idioma.id'
                  label={intl.formatMessage({ id: 'admin.common.language' })}
                  id='form_post_language'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Resource resource='BlogCategories' autoFetch>
              {props => (
                <Field
                  name='categoria.id'
                  label={intl.formatMessage({ id: 'admin.common.category' })}
                  id='form_post_category'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Resource resource='Tags' autoFetch>
              {props => (
                <Field
                  name='tags'
                  label={intl.formatMessage({ id: 'admin.common.tags' })}
                  id='form_post_tags'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    multi: true,
                    simpleValue: false,
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Field
              name='capa'
              label={intl.formatMessage({ id: 'admin.common.image' })}
              id='form_post_thumbnail'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
              {...forms.required}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormPost.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormPost)
