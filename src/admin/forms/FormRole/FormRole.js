import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomPermissionsManager from 'admin/components/CustomPermissionsManager'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Resource from 'containers/Resource'
import { injectIntl } from 'react-intl'

function FormRole ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='name'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_role_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={190}
              {...forms.required}
            />
            <Resource resource='Companies' autoFetch>
              {props => (
                <Field
                  name='empresa.id'
                  label={intl.formatMessage({ id: 'admin.common.company' })}
                  id='form_role_empresa'
                  component={CustomFieldSelect}
                  readOnly={readOnly}
                  customSelectProps={{
                    options: values.empresa && !props.records.find(r => r.id === values.empresa.id)
                      ? props.records.concat(values.empresa)
                      : props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching,
                    placeholder: 'Pesquisar empresa',
                    onInputChange: value => {
                      if (value) props.fetchAll({ nome: value })
                      return value
                    }
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Resource resource='Permissions' autoFetch spinner>
              {props => (
                <Field
                  name='permissions'
                  label={intl.formatMessage({ id: 'admin.formRole.permissions' })}
                  id='form_role_permissions'
                  permissions={props.records}
                  component={CustomPermissionsManager}
                  readOnly={readOnly}
                  {...forms.required}
                />
              )}
            </Resource>
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label='Salvar'
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormRole.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormRole)
