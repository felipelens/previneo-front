import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import { getFlatDataFromTree } from 'react-sortable-tree'
import FormGrid from 'components/FormGrid'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomEditor from 'admin/components/CustomEditor'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Resource from 'containers/Resource'
import isValidTreeScript from 'admin/components/TreeScript/isValidTreeScript'
import CustomScriptEditor from 'admin/components/CustomScriptEditor'
import { SCRIPT_NODE_TYPES } from 'config/constants'
import CustomImageUploader from 'admin/components/CustomUploader'
import Toggle from 'components/Toggle'
import Collapse from 'react-smooth-collapse'

function FormScript ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form
      {...props}
      onSubmit={data => {
        if (isValidTreeScript(data.categorias)) {
          const tree = getFlatDataFromTree({
            treeData: data.categorias,
            getNodeKey: ({ node }) => node.key,
            ignoreCollapsed: false
          })
            .map(({ node }) => node)
            .filter(node => node.type === SCRIPT_NODE_TYPES.CATEGORY)

          const isMedicalHistoryRequired = !!tree.find(node => (
            node.requer_historico_medico
          ))

          if (isMedicalHistoryRequired) {
            const cats = tree.filter(node => node.historico_medico)

            if (!(cats.length === 2 && cats[0].genero.nome !== cats[1].genero.nome)) {
              return window.alert(intl.formatMessage({ id: 'admin.common.requiredHistoryMedicalCategories' }))
            }
          }

          props.onSubmit(data)
        }
      }}
    >
      {({ handleSubmit, pristine, values, form }) => (
        <form onSubmit={e => e.preventDefault()}>
          <VerticalSpacer>
            <FormGrid>
              <Field
                name='nome'
                label={intl.formatMessage({ id: 'admin.common.name' })}
                id='form_script_name'
                component={CustomField}
                readOnly={readOnly}
                maxLength={190}
                {...forms.required}
              />
              <Resource resource='Companies' autoFetch>
                {props => (
                  <Field
                    name='empresa.id'
                    label={intl.formatMessage({ id: 'admin.common.company' })}
                    id='form_user_empresa'
                    component={CustomFieldSelect}
                    readOnly={readOnly}
                    customSelectProps={{
                      options: values.empresa && !props.records.find(r => r.id === values.empresa.id)
                        ? props.records.concat(values.empresa)
                        : props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      isLoading: props.isFetching,
                      placeholder: intl.formatMessage({ id: 'admin.common.searchForCompany' }),
                      onInputChange: value => {
                        if (value) props.fetchAll({ nome: value })
                        return value
                      }
                    }}
                    {...forms.required}
                  />
                )}
              </Resource>
            </FormGrid>
            <FormGrid>
              <Field
                name='status_roteiro'
                label={intl.formatMessage({ id: 'admin.script.status' })}
                id='form_script_status'
                disabled={readOnly}
                component={CustomFieldSelect}
                customSelectProps={{
                  options: [
                    {
                      name: intl.formatMessage({ id: 'admin.common.active' }),
                      id: 'Ativo'
                    },
                    {
                      name: intl.formatMessage({ id: 'admin.common.inactive' }),
                      id: 'Inativo'
                    },
                    {
                      name: intl.formatMessage({ id: 'admin.common.draft' }),
                      id: 'Rascunho'
                    }
                  ],
                  labelKey: 'name',
                  valueKey: 'id'
                }}
                {...forms.required}
              />
              <Resource resource='Languages' autoFetch>
                {props => (
                  <Field
                    name='idioma.id'
                    label={intl.formatMessage({ id: 'admin.common.language' })}
                    id='form_script_language'
                    component={CustomFieldSelect}
                    readOnly={readOnly}
                    customSelectProps={{
                      options: props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      isLoading: props.isFetching
                    }}
                    {...forms.required}
                  />
                )}
              </Resource>
            </FormGrid>
            <FormGrid>
              <Field
                name='matricula'
                label={intl.formatMessage({ id: 'admin.script.requiredRegistration' })}
                id='form_script_formfields_code'
                disabled={readOnly}
                component={CustomFieldSelect}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.yes' }), value: 1 },
                    { label: intl.formatMessage({ id: 'admin.common.no' }), value: 0 }
                  ]
                }}
                {...forms.required}
              />
              <Field
                name='celular'
                label={intl.formatMessage({ id: 'admin.script.requiredCellphone' })}
                id='form_script_formfields_phone'
                disabled={readOnly}
                component={CustomFieldSelect}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.yes' }), value: 1 },
                    { label: intl.formatMessage({ id: 'admin.common.no' }), value: 0 }
                  ]
                }}
                {...forms.required}
              />
            </FormGrid>
            <FormGrid>
              <Field
                name='plano_saude'
                label={intl.formatMessage({ id: 'admin.script.requiredHealthcareInsurance' })}
                id='form_script_formfields_helthcare_insurance'
                disabled={readOnly}
                component={CustomFieldSelect}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.yes' }), value: 1 },
                    { label: intl.formatMessage({ id: 'admin.common.no' }), value: 0 }
                  ]
                }}
                {...forms.required}
              />
              <Field
                name='periodo'
                label={intl.formatMessage({ id: 'admin.script.minimumPeriod' })}
                id='form_script_period'
                component={CustomField}
                readOnly={readOnly}
                type='number'
                min='0'
              />
            </FormGrid>
            <FormGrid>
              <Field
                name='mostrar_resultado'
                label={intl.formatMessage({ id: 'admin.script.displayResultPageWithRisks' })}
                id='form_script_show_result'
                component={CustomFieldSelect}
                readOnly={readOnly}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.yes' }), value: true },
                    { label: intl.formatMessage({ id: 'admin.common.no' }), value: false }
                  ]
                }}
                {...forms.required}
              />
              <Field
                name='mostrar_footer'
                label={intl.formatMessage({ id: 'admin.script.displayFooter' })}
                id='form_script_display_footer'
                component={CustomFieldSelect}
                readOnly={readOnly}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.yes' }), value: true },
                    { label: intl.formatMessage({ id: 'admin.common.no' }), value: false }
                  ]
                }}
                {...forms.required}
              />
            </FormGrid>
            <FormGrid>
              <Toggle defaultValue={values.email_resultado}>
                {({ toggle, value }) => (
                  <VerticalSpacer>
                    <Field
                      name='email_resultado'
                      id='form_script_email_result'
                      onClick={toggle}
                      component={CustomField.Option}
                      type='checkbox'
                    >
                      {intl.formatMessage({ id: 'admin.script.sendResultForEmail' })}
                    </Field>
                    <Collapse expanded={value}>
                      <Field
                        name='email_resultado_periodo_minimo'
                        label={intl.formatMessage({ id: 'admin.script.sendResultForEmailPeriod' })}
                        id='form_script_email_result_period'
                        component={CustomField}
                        readOnly={readOnly}
                        type='number'
                        min='1'
                      />
                    </Collapse>
                  </VerticalSpacer>
                )}
              </Toggle>
              <Toggle defaultValue={values.email_lembrete}>
                {({ toggle, value }) => (
                  <VerticalSpacer>
                    <Field
                      name='email_lembrete'
                      id='form_script_email_reminder'
                      onClick={toggle}
                      component={CustomField.Option}
                      type='checkbox'
                    >
                      {intl.formatMessage({ id: 'admin.script.sendReminderEmail' })}
                    </Field>
                    <Collapse expanded={value}>
                      <FormGrid>
                        <Field
                          name='email_lembrete_periodo_minimo'
                          label={intl.formatMessage({ id: 'admin.script.sendReminderEmailPeriod' })}
                          id='form_script_email_reminder_period'
                          component={CustomField}
                          readOnly={readOnly}
                          type='number'
                          min='1'
                        />
                        <Field
                          name='email_lembrete_quantidade_maxima'
                          label={intl.formatMessage({ id: 'admin.script.sendReminderEmailQuantity' })}
                          id='form_script_email_reminder_quantity'
                          component={CustomField}
                          readOnly={readOnly}
                          type='number'
                          min='1'
                        />
                      </FormGrid>
                    </Collapse>
                  </VerticalSpacer>
                )}
              </Toggle>
            </FormGrid>
            <FormGrid>
              <Field
                name='logo_esquerda'
                label={intl.formatMessage({ id: 'admin.common.logoTopLeft' })}
                id='form_company_logo_left'
                component={CustomImageUploader}
                accept='image/*'
                readOnly={readOnly}
                {...forms.required}
              />
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Field
                  name='logo_direita'
                  label={intl.formatMessage({ id: 'admin.common.logoTopRight' })}
                  id='form_company_logo_right'
                  component={CustomImageUploader}
                  accept='image/*'
                  readOnly={readOnly}
                />
              </div>
            </FormGrid>
            <FormGrid>
              <Field
                name='logo_inferior_esquerda'
                label={intl.formatMessage({ id: 'admin.common.logoBottomLeft' })}
                id='form_company_logo_bottom_left'
                component={CustomImageUploader}
                accept='image/*'
                readOnly={readOnly}
              />
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Field
                  name='logo_inferior_direita'
                  label={intl.formatMessage({ id: 'admin.common.logoBottomRight' })}
                  id='form_company_logo_bottom_right'
                  component={CustomImageUploader}
                  accept='image/*'
                  readOnly={readOnly}
                />
              </div>
            </FormGrid>
            <Field
              name='url_restrita'
              id='form_url_restrita'
              component={CustomField.Option}
              type='checkbox'
            >
              {intl.formatMessage({ id: 'admin.common.restrictedURL' })}
            </Field>
            <Field
              name='conteudo_url_aberta'
              label={intl.formatMessage({ id: 'admin.script.initialPageContent' })}
              id='form_script_content_open_url'
              component={CustomEditor}
              readOnly={readOnly}
            />
            <Field
              name='conteudo_resultado'
              label={intl.formatMessage({ id: 'admin.script.resultPageContent' })}
              id='form_script_content_result'
              component={CustomEditor}
              readOnly={readOnly}
            />
            <Toggle defaultValue={values.mostrar_informacao_adicional}>
              {({ toggle, value }) => (
                <VerticalSpacer>
                  <Field
                    name='mostrar_informacao_adicional'
                    id='form_display_script_content_additional_info'
                    onClick={toggle}
                    component={CustomField.Option}
                    type='checkbox'
                  >
                    {intl.formatMessage({ id: 'admin.script.displayAdditionalInfoPageContent' })}
                  </Field>
                  <Collapse expanded={value}>
                    <Field
                      name='conteudo_informacao_adicional'
                      label={intl.formatMessage({ id: 'admin.script.additionalInfoPageContent' })}
                      id='form_script_content_additional_info'
                      component={CustomEditor}
                      readOnly={readOnly}
                    />
                  </Collapse>
                </VerticalSpacer>
              )}
            </Toggle>
            <Field
              name='categorias'
              label={intl.formatMessage({ id: 'admin.script.title' })}
              id='form_category_script'
              component={CustomScriptEditor}
              includesCategory
              readOnly={readOnly}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
                onClick={handleSubmit}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormScript.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormScript)
