import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form } from 'react-final-form'
import CustomField from 'components/CustomField'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import FieldTranslations from '../../components/FieldTranslations/FieldTranslations'
import arrayMutators from 'final-form-arrays'

function FormRisk ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form
      {...props}
      mutators={arrayMutators}
    >
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <FieldTranslations
              values={values}
              disabled={readOnly}
              additional={[
                { component: CustomField,
                  name: 'descricao',
                  label: intl.formatMessage({ id: 'admin.common.risk' })
                }
              ]}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormRisk.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormRisk)
