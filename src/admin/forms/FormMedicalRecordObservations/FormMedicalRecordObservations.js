import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import { isEmpty } from 'ramda'
import CustomField from 'components/CustomField'
import * as forms from 'utils/forms'
import { phone } from 'utils/formatters'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Grid from 'admin/components/Grid'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import Resource from 'containers/Resource'
import CustomDate from 'admin/components/CustomDate'
import DataTable from 'admin/components/DataTable'
import WithModal from 'admin/components/WithModal'
import Button from 'components/Button'
import FieldListener from 'admin/components/FieldListener'
import Section from 'admin/components/Section'
import { CANCER_RISKS } from 'config/constants'
import Text from 'components/Text'
import { ModalRisksContainer, ModalRisksImg } from './styles'
import { format } from '../../../modules/Date'

const Table = props =>
  <DataTable
    filterable={false}
    showPagination={false}
    pageSize={1000}
    {...props}
  />

const FieldResource = props =>
  <Resource
    resource={props.resource}
    autoFetch
    spinner
    {...props}
  />

function FormMedicalRecordObservations ({ isSubmitting, readOnly, intl, patientId, ...props }) {
  return (
    <Grid>
      <FieldResource resource='PatientsHistoryCurrent' id={patientId}>
        {props => (
          <Section
            title={intl.formatMessage({ id: 'admin.common.details' })}
            titleSize='30px'
          >
            {props.detailedRecord.data_preenchimento_anamnese && (
              <Text>
                {intl.formatMessage({ id: 'admin.common.lastAnamnesis' })} {': '}
                {format(props.detailedRecord.data_preenchimento_anamnese, 'DD/MM/YYYY')}
              </Text>
            )}
            {props.detailedRecord.idade && (
              <Text>{intl.formatMessage({ id: 'admin.common.age' })}: {props.detailedRecord.idade}</Text>
            )}
            {props.detailedRecord.celular && (
              <Text>{intl.formatMessage({ id: 'admin.common.cellphone' })}: {phone(props.detailedRecord.celular)}</Text>
            )}
            {props.detailedRecord.telefone && (
              <Text>{intl.formatMessage({ id: 'admin.common.phone' })}: {phone(props.detailedRecord.telefone)}</Text>

            )}
            <br />
            <TableCategories original={props.detailedRecord.prontuarios} />
          </Section>
        )}
      </FieldResource>
      <Section
        title={intl.formatMessage({ id: 'admin.patientsHistory.contact' })}
        titleSize='30px'
      >
        <Form {...props}>
          {({ handleSubmit, pristine, form, values }) => (
            <form onSubmit={handleSubmit}>
              <VerticalSpacer>
                <VerticalSpacer>
                  <Grid>
                    <Field
                      name='proximo_contato'
                      label={intl.formatMessage({ id: 'admin.common.nextContact' })}
                      id='form_patient_history_next_contact'
                      component={props => <CustomDate {...props} formatDefault='YYYY-MM-DD 00:00:00' />}
                      disabled={readOnly}
                      numberOfMonths={1}
                    />
                    <FieldResource resource='ContactReason'>
                      {props => (
                        <Field
                          name='motivos_proximo_contato'
                          label={intl.formatMessage({ id: 'admin.common.contactReason' })}
                          id='form_patient_history_contact_reason'
                          component={CustomFieldSelect}
                          disabled={readOnly}
                          customSelectProps={{
                            options: props.records,
                            labelKey: 'motivo',
                            valueKey: 'id',
                            multi: true,
                            simpleValue: false,
                            isLoading: props.isFetching
                          }}
                        />
                      )}
                    </FieldResource>
                  </Grid>
                  <Grid>
                    <FieldResource resource='TypesCancer'>
                      {props => (
                        <Field
                          name='tipos_cancer'
                          label={intl.formatMessage({ id: 'admin.common.typesCancer' })}
                          id='form_patient_history_types_cancer'
                          component={CustomFieldSelect}
                          disabled={readOnly}
                          customSelectProps={{
                            options: props.records,
                            labelKey: 'nome',
                            valueKey: 'id',
                            multi: true,
                            simpleValue: false,
                            isLoading: props.isFetching
                          }}
                        />
                      )}
                    </FieldResource>
                    <FieldResource resource='RecomendedExams'>
                      {props => (
                        <Field
                          name='exames_realizados'
                          label={intl.formatMessage({ id: 'admin.common.patientHistoryExams' })}
                          id='form_patient_history_recommended_exams'
                          component={CustomFieldSelect}
                          disabled={readOnly}
                          customSelectProps={{
                            options: props.records,
                            labelKey: 'nome',
                            valueKey: 'id',
                            multi: true,
                            simpleValue: false,
                            isLoading: props.isFetching
                          }}
                        />
                      )}
                    </FieldResource>
                  </Grid>
                  <VerticalSpacer>
                    <FieldListener
                      name='tres_tentativas_contato'
                      onChange={(newValue, oldValue) => {
                        newValue && form.change('contato_realizado', false)
                      }}
                    />
                    <FieldListener
                      name='contato_realizado'
                      onChange={(newValue, oldValue) => {
                        newValue && form.change('tres_tentativas_contato', false)
                      }}
                    />
                    <FieldListener
                      name='nao_requer_novo_contato'
                      onChange={(newValue, oldValue) => {
                        newValue && form.change('proximo_contato', null)
                        newValue && form.change('motivos_proximo_contato', [])
                      }}
                    />
                    <FieldListener
                      name='motivos_proximo_contato'
                      onChange={(newValue, oldValue) => {
                        !isEmpty(newValue) && form.change('nao_requer_novo_contato', false)
                      }}
                    />
                    <FieldListener
                      name='nao_requer_novo_contato'
                      onChange={(newValue, oldValue) => {
                        newValue && form.change('proximo_contato', null)
                      }}
                    />
                    <FieldListener
                      name='proximo_contato'
                      onChange={(newValue, oldValue) => {
                        newValue && form.change('nao_requer_novo_contato', false)
                      }}
                    />
                    <Field
                      name='contato_realizado'
                      id='form_medical_contato_realizado'
                      component={CustomField.Option}
                      type='checkbox'
                    >
                      {intl.formatMessage({ id: 'admin.medicalRecords.madeContact' })}
                    </Field>
                    <Field
                      name='tres_tentativas_contato'
                      id='form_medical_tres_tentativas_contato'
                      component={CustomField.Option}
                      type='checkbox'
                    >
                      {intl.formatMessage({ id: 'admin.medicalRecords.threeContactsMessage' })}
                    </Field>
                    <Field
                      name='nao_requer_novo_contato'
                      id='form_medical_nao_requer_novo_contato'
                      component={CustomField.Option}
                      type='checkbox'
                    >
                      {intl.formatMessage({ id: 'admin.medicalRecords.noMoreContact' })}
                    </Field>
                    <Field
                      name='descricao'
                      label={intl.formatMessage({ id: 'admin.common.observations' })}
                      id='form_medical_record_observations'
                      component={CustomField.Textarea}
                      readOnly={readOnly}
                      {...forms.required}
                    />
                    {!readOnly && (
                      <ButtonSubmit
                        isSubmitting={isSubmitting}
                        disabled={pristine}
                        label={intl.formatMessage({ id: 'admin.common.saveContact' })}
                        size='small'
                      />
                    )}
                  </VerticalSpacer>
                </VerticalSpacer>
              </VerticalSpacer>
            </form>
          )}
        </Form>
      </Section>
    </Grid>
  )
}

function TableCategories ({ original }) {
  return (
    <Table
      minRows={6}
      data={original}
      columns={[{
        Header: <FormattedMessage id='admin.patientsHistory.currentSituation' />,
        columns: [
          {
            accessor: 'categoria.nome',
            Header: <FormattedMessage id='admin.cancerTypes.title' />
          },
          {
            accessor: 'risco.risco',
            Header: <FormattedMessage id='admin.common.risk' />,
            Cell: (row) => {
              const risco = row.original.risco
              if (!risco) return ''
              const color = CANCER_RISKS.hasOwnProperty(risco.nivel)
                ? CANCER_RISKS[risco.nivel].color
                : 'black'

              return (
                <Text color={color}>{risco.risco}</Text>
              )
            }
          },
          {
            width: 50,
            filterable: false,
            Cell: (row) => {
              return (
                <WithModal width={700} modal={() => (
                  <ModalRisksContainer>
                    <ModalRisksImg style={{}}>
                      <img src={row.original.categoria.icone.url} alt={row.original.categoria.nome} />
                      <br />
                      <span>{row.original.categoria.title}</span>
                    </ModalRisksImg>
                    <div>
                      {!isEmpty(row.original.estrategia) && (
                        <Text
                          component='div'
                          size='16px'
                          lineHeight={1.3}
                          dangerouslySetInnerHTML={{__html: row.original.estrategia.conteudo}}
                        />
                      )}
                      {!isEmpty(row.original.examesRecomendados) && (
                        <div>
                          <Text weight='bold' component='p'>
                            <FormattedMessage id='admin.exams.title.plural' />
                          </Text>
                          <ul>
                            {row.original.examesRecomendados.map(item => <li>{item.nome}</li>)}
                          </ul>
                        </div>
                      )}
                      {!isEmpty(row.original.justificativas) && (
                        <div>
                          <Text weight='bold' component='p'>
                            <FormattedMessage id='admin.common.justifications.title.plural' />
                          </Text>
                          <ul>
                            {row.original.justificativas.map(item => <li>{item.justificativa}</li>)}
                          </ul>
                        </div>
                      )}
                    </div>
                  </ModalRisksContainer>
                )} >
                  {({ toggleModal }) => (
                    <Button
                      onClick={toggleModal}
                      size='small'
                      icon={require('@fortawesome/fontawesome-free-solid/faInfoCircle')}
                      color='white'
                      textColor='primary'
                      block
                    />
                  )}
                </WithModal>
              )
            }
          }
        ]
      }]}
    />
  )
}

FormMedicalRecordObservations.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormMedicalRecordObservations)
