import styled from 'styled-components'

export const ModalRisksContainer = styled.div`
  display: flex;
`

export const ModalRisksImg = styled.div`
   width: 120px;
   margin-right: 20px;
   text-align: center;
   margin-top: 1em;
`
