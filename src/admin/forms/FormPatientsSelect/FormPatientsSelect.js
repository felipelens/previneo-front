import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import FormGrid from 'components/FormGrid'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomDateRange from 'admin/components/CustomDateRange'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import Resource, { filterParams } from 'containers/Resource'
import CustomField from 'components/CustomField'
import CustomCheckDataTable from 'admin/components/CustomCheckDataTable'
import queryToObject from 'utils/queryToObject'
import { injectIntl } from 'react-intl'

class FormPatientsSelect extends Component {
  state = {
    firstLoaded: false
  }

  render () {
    const { onSubmit, isSubmitting, readOnly, buttonTitle, ...props } = this.props
    return (
      <Form
        onSubmit={(data, ...rest) => {
          const filteredData = filterParams(data)
          if (!filteredData.manual_select) {
            delete filteredData.usuarios
          }
          onSubmit(filteredData, ...rest)
        }}
        {...props}
      >
        {({ handleSubmit, pristine, values, form }) => (
          <form onSubmit={handleSubmit}>
            <VerticalSpacer>
              <FormGrid>
                <Resource resource='Companies' autoFetch>
                  {props => (
                    <Field
                      name='empresa_id'
                      label={this.props.intl.formatMessage({ id: 'admin.common.company' })}
                      id='form_patient_company'
                      component={CustomFieldSelect}
                      disabled={readOnly}
                      customSelectProps={{
                        options: props.records,
                        labelKey: 'nome',
                        valueKey: 'id',
                        isLoading: props.isFetching,
                        placeholder: this.props.intl.formatMessage({ id: 'admin.common.companyPlaceHolder' }),
                        onInputChange: value => {
                          if (value) props.fetchAll({ nome: value })
                          return value
                        }
                      }}
                    />
                  )}
                </Resource>
                <Field
                  name='date'
                  component={CustomDateRange}
                  label={this.props.intl.formatMessage({ id: 'admin.common.interval' })}
                  id='form_patient_date'
                />
              </FormGrid>
              <Field
                name='manual_select'
                component={CustomField.Option}
                type='checkbox'
              >
                {this.props.intl.formatMessage({ id: 'admin.common.selectPatient' })}
              </Field>
              {values.manual_select && (
                <Resource
                  resource='Patients'
                  params={{
                    empresa_id: values.empresa_id || null,
                    date_start: values.date ? values.date.start_date : null,
                    date_end: values.date ? values.date.end_date : null
                  }}
                  autoFetch
                  onFetchSuccess={() => {
                    if (!this.state.firstLoaded) {
                      this.setState({firstLoaded: true})
                    }

                    form.change('usuarios', [])
                  }}
                >
                  {props => (
                    <Field
                      name='usuarios'
                      label={this.props.intl.formatMessage({ id: 'admin.patients.title.plural' })}
                      id='form_script_send_patients'
                      component={CustomCheckDataTable}
                      records={props.records}
                      pages={props.pagination.last_page}
                      loading={props.isFetching}
                      onFetchData={({ page, pageSize, sorted, filtered }) => {
                        if (!this.state.firstLoaded) return
                        const filteredParams = queryToObject(filtered)
                        props.fetchAll({
                          page: page + 1,
                          limit: pageSize,
                          ...filteredParams,
                          ...props.params
                        })
                      }}
                      columns={[
                        {
                          accessor: 'name',
                          id: 'nome',
                          Header: this.props.intl.formatMessage({ id: 'admin.common.name' })
                        },
                        {
                          accessor: 'email',
                          Header: this.props.intl.formatMessage({ id: 'admin.common.email' }),
                          width: 300
                        }
                      ]}
                      {...forms.required}
                    />
                  )}
                </Resource>
              )}
              {!readOnly && (
                <ButtonSubmit
                  isSubmitting={isSubmitting}
                  disabled={pristine}
                  label={buttonTitle}
                />
              )}
            </VerticalSpacer>
          </form>
        )}
      </Form>
    )
  }

  static propTypes = {
    /** Callback para o form submit */
    onSubmit: PropTypes.func.isRequired,

    /** Form sendo processado */
    isSubmitting: PropTypes.bool,

    /** Exibe o form em modo leitura */
    readOnly: PropTypes.bool,

    /** Título do botão */
    buttonTitle: PropTypes.string
  }

  static defaultProps = {
    buttonTitle: 'Selecionar'
  }
}

export default injectIntl(FormPatientsSelect)
