import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import Resource from 'containers/Resource'
import BackLink from 'components/BackLink'
import Section from 'admin/components/Section'
import VerticalSpacer from 'components/VerticalSpacer'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import FormGrid from 'components/FormGrid'
import * as forms from 'utils/forms'
import { equalTo } from 'utils/validators'
import ButtonSubmit from 'components/ButtonSubmit'
import routes from 'routes'

const FormWrapper = props => !props.updateResource
  ? <React.Fragment children={props.children} />
  : (
    <VerticalSpacer space={45}>
      <Section
        title={props.intl.formatMessage({ id: 'admin.common.editUser' })}
        side={<BackLink to={routes.admin.users.users.index} />}
        children={props.children}
        style={{ overflow: 'visible' }}
      />
    </VerticalSpacer>
  )

function FormUser ({ intl, isSubmitting, readOnly, updateResource, ...props }) {
  return (
    <FormWrapper
      isSubmitting={isSubmitting}
      updateResource={updateResource}
      {...props}
      intl={intl}
    >
      <Form {...props}>
        {({ handleSubmit, pristine, values }) => (
          <form onSubmit={handleSubmit}>
            <VerticalSpacer>
              <FormGrid>
                <Field
                  name='name'
                  label={intl.formatMessage({ id: 'admin.common.name' })}
                  id='form_user_name'
                  readOnly={readOnly}
                  component={CustomField}
                  {...forms.required}
                />
                <Field
                  name='email'
                  label={intl.formatMessage({ id: 'admin.common.email' })}
                  id='form_user_email'
                  readOnly={readOnly}
                  component={CustomField}
                  {...forms.emailRequired}
                />
              </FormGrid>
              {!(readOnly || updateResource) && (
                <FormGrid>
                  <Field
                    name='password'
                    label={intl.formatMessage({ id: 'admin.common.password' })}
                    id='form_user_password'
                    component={CustomField}
                    {...forms.passwordRequired}
                  />
                  <Field
                    name='password_confirmation'
                    label={intl.formatMessage({ id: 'admin.common.confirmPassword' })}
                    id='form_user_password_confirmation'
                    component={CustomField}
                    {...forms.password}
                    validate={equalTo({ field: 'password', message: 'As senhas precisam ser iguais' })}
                  />
                </FormGrid>
              )}
              <FormGrid>
                <Resource resource='Companies' autoFetch>
                  {props => (
                    <Field
                      name='empresa.id'
                      label={intl.formatMessage({ id: 'admin.common.company' })}
                      id='form_user_empresa'
                      component={CustomFieldSelect}
                      readOnly={readOnly}
                      customSelectProps={{
                        options: values.empresa && !props.records.find(r => r.id === values.empresa.id)
                          ? props.records.concat(values.empresa)
                          : props.records,
                        labelKey: 'nome',
                        valueKey: 'id',
                        isLoading: props.isFetching,
                        placeholder: 'Pesquisar empresa',
                        onInputChange: value => {
                          if (value) props.fetchAll({ nome: value })
                          return value
                        }
                      }}
                      {...forms.required}
                    />
                  )}
                </Resource>
                <Resource resource='Roles' autoFetch>
                  {props => (
                    <Field
                      name='roles'
                      label={intl.formatMessage({ id: 'admin.common.accessLevels' })}
                      id='form_user_role'
                      component={CustomFieldSelect}
                      disabled={readOnly}
                      customSelectProps={{
                        options: props.records,
                        labelKey: 'name',
                        valueKey: 'id',
                        multi: true,
                        simpleValue: false,
                        isLoading: props.isFetching
                      }}
                      {...forms.required}
                    />
                  )}
                </Resource>
              </FormGrid>
              <FormGrid>
                <Resource resource='Languages' autoFetch>
                  {props => (
                    <Field
                      name='language'
                      label={intl.formatMessage({ id: 'admin.common.language' })}
                      id='form_user_language'
                      component={CustomFieldSelect}
                      readOnly={readOnly}
                      customSelectProps={{
                        options: props.records,
                        labelKey: 'nome',
                        valueKey: 'slug',
                        isLoading: props.isFetching,
                        placeholder: intl.formatMessage({ id: 'admin.common.search' }),
                        onInputChange: value => {
                          if (value) props.fetchAll({ language: value })
                          return value
                        }
                      }}
                      {...forms.required}
                    />
                  )}
                </Resource>
                <div />
              </FormGrid>
              {!readOnly && (
                <React.Fragment>
                  <ButtonSubmit
                    isSubmitting={isSubmitting}
                    disabled={pristine}
                    label={intl.formatMessage({ id: 'admin.common.save' })}
                  />
                </React.Fragment>
              )}
            </VerticalSpacer>
          </form>
        )}
      </Form>
    </FormWrapper>
  )
}

FormUser.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormUser)
