import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import Section from 'admin/components/Section'
import CustomField from 'components/CustomField'
import VerticalSpacer from 'components/VerticalSpacer'
import FormGrid from 'components/FormGrid'
import ButtonSubmit from 'components/ButtonSubmit'
import * as forms from 'utils/forms'
import Resource from 'containers/Resource'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'

export default function FormUserPersonal ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Section style={{ overflow: 'visible' }} title={intl.formatMessage({ id: 'admin.common.personalData' })}>
      <Form {...props}>
        {({ handleSubmit, pristine }) => (
          <form onSubmit={handleSubmit}>
            <VerticalSpacer>
              <FormGrid>
                <Field
                  name='name'
                  label={intl.formatMessage({ id: 'admin.common.name' })}
                  id='form_user_personal_data_name'
                  component={CustomField}
                  readOnly={readOnly}
                  maxLength={255}
                  {...forms.required}
                />
                <Field
                  name='email'
                  label={intl.formatMessage({ id: 'admin.common.email' })}
                  id='form_user_email'
                  component={CustomField}
                  {...forms.emailRequired}
                />
                <Resource resource='Languages' autoFetch>
                  {props => (
                    <Field
                      name='language'
                      label={intl.formatMessage({ id: 'admin.common.language' })}
                      id='form_user_language'
                      component={CustomFieldSelect}
                      readOnly={readOnly}
                      customSelectProps={{
                        options: props.records,
                        labelKey: 'nome',
                        valueKey: 'slug',
                        isLoading: props.isFetching,
                        placeholder: intl.formatMessage({ id: 'admin.common.search' }),
                        onInputChange: value => {
                          if (value) props.fetchAll({ language: value })
                          return value
                        }
                      }}
                      {...forms.required}
                    />
                  )}
                </Resource>
              </FormGrid>
              {!readOnly && (
                <ButtonSubmit
                  isSubmitting={isSubmitting}
                  disabled={pristine}
                  label={intl.formatMessage({ id: 'admin.common.save' })}
                />
              )}
            </VerticalSpacer>
          </form>
        )}
      </Form>
    </Section>
  )
}

FormUserPersonal.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}
