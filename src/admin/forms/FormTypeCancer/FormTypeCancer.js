import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import CustomField from 'components/CustomField'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import Resource from 'containers/Resource'
import FieldTranslations from '../../components/FieldTranslations/FieldTranslations'

function FormTypeCancer ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form
      {...props}
      mutators={arrayMutators}
    >
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Resource resource='Genders' autoFetch>
              {props => (
                <Field
                  name='genero.id'
                  label={intl.formatMessage({ id: 'admin.common.gender' })}
                  id='form_category_gender'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <FieldTranslations
              values={values}
              disabled={readOnly}
              label={intl.formatMessage({ id: 'admin.cancerTypes.title' })}
              component={CustomField}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormTypeCancer.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormTypeCancer)
