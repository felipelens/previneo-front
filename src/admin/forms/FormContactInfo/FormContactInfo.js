import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'

function FormContactInfo ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='tipo'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_contact_info_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={190}
              {...forms.required}
            />
            <Field
              name='conteudo'
              label={intl.formatMessage({ id: 'admin.common.value' })}
              id='form_contact_info_slug'
              component={CustomField}
              readOnly={readOnly}
              maxLength={100}
              {...forms.required}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormContactInfo.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormContactInfo)
