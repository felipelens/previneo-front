import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import { isEmpty } from 'ramda'
import { Link } from 'react-router-dom'
import debounce from 'lodash.debounce'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomCheckDataTable from 'admin/components/CustomCheckDataTable'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Section from 'admin/components/Section'
import Resource from 'containers/Resource'
import Button from 'components/Button'
import queryToObject from 'utils/queryToObject'
import getRoute from 'utils/getRoute'
import routes from 'routes'
import CustomField from 'components/CustomField'

class FormScriptSend extends Component {
  state = {
    firstLoaded: false
  }

  render () {
    const { isSubmitting, readOnly, intl, ...props } = this.props
    return (
      <Section style={{ overflow: 'visible' }} title={intl.formatMessage({ id: 'admin.scriptSend.title' })}>
        <Form
          initialValues={{
            enviar_email: false
          }}
          {...props}
        >
          {({ handleSubmit, pristine, values, form }) => (
            <form onSubmit={e => e.preventDefault()}>
              <VerticalSpacer>
                <Resource
                  namespace='scriptResource'
                  resource='Scripts'
                  params={{ status_roteiro: 'Ativo' }}
                  autoFetch
                >
                  {props => (
                    <Field
                      name='script'
                      label={intl.formatMessage({ id: 'admin.common.script' })}
                      id='form_script_send_script'
                      component={CustomFieldSelect}
                      readOnly={readOnly}
                      customSelectProps={{
                        options: props.records,
                        labelKey: 'nome',
                        valueKey: 'id',
                        isLoading: props.isFetching,
                        placeholder: intl.formatMessage({ id: 'admin.common.search' }),
                        onInputChange: debounce((value) => {
                          if (value) props.fetchAll({ nome: value })
                          return value
                        }, 300)
                      }}
                      {...forms.required}
                    />
                  )}
                </Resource>
                <Field
                  name='enviar_email'
                  id='form_script_send_send_email'
                  component={CustomField.Option}
                  type='checkbox'
                >
                  {intl.formatMessage({ id: 'admin.scriptSend.sendEmail' })}
                </Field>
                {!!values.script && (
                  <Resource
                    resource='Scripts'
                    id={values.script}
                    autoFetch
                    spinner
                  >
                    {({ detailedRecord }) => {
                      if (isEmpty(detailedRecord)) {
                        return intl.formatMessage({ id: 'admin.common.noResultsFound' })
                      }

                      return (
                        <Resource
                          resource='Patients'
                          params={{ empresa_id: detailedRecord.empresa.id }}
                          autoFetch
                          onFetchSuccess={() => {
                            if (!this.state.firstLoaded) {
                              this.setState({firstLoaded: true})
                            }

                            form.change('usuarios', [])
                          }}
                        >
                          {props => (
                            <Field
                              name='usuarios'
                              label={intl.formatMessage({ id: 'admin.patients.title.plural' })}
                              id='form_script_send_patients'
                              component={CustomCheckDataTable}
                              records={props.records}
                              pages={props.pagination.last_page}
                              loading={props.isFetching}
                              onFetchData={({ page, pageSize, sorted, filtered }) => {
                                if (!this.state.firstLoaded) return
                                const filteredParams = queryToObject(filtered)
                                props.fetchAll({
                                  page: page + 1,
                                  limit: pageSize,
                                  ...filteredParams,
                                  ...props.params
                                })
                              }}
                              columns={[
                                {
                                  accessor: 'name',
                                  id: 'nome',
                                  Header: intl.formatMessage({ id: 'admin.common.name' })
                                },
                                {
                                  accessor: 'email',
                                  Header: intl.formatMessage({ id: 'admin.common.email' }),
                                  width: 300
                                },
                                {
                                  width: 100,
                                  filterable: false,
                                  Header: intl.formatMessage({ id: 'admin.common.details' }),
                                  Cell: ({ original }) => (
                                    <div>
                                      <Button
                                        component={Link}
                                        to={getRoute(routes.admin.patients.patients.show, { id: original.id })}
                                        color='success'
                                        block
                                        size='small'
                                        target='_blank'
                                      >
                                        {intl.formatMessage({ id: 'admin.common.details' })}
                                      </Button>
                                    </div>
                                  )
                                }
                              ]}
                              {...forms.required}
                            />
                          )}
                        </Resource>
                      )
                    }}
                  </Resource>
                )}
                {!readOnly && (
                  <ButtonSubmit
                    isSubmitting={isSubmitting}
                    disabled={pristine}
                    label={intl.formatMessage({ id: 'admin.common.send' })}
                    onClick={handleSubmit}
                  />
                )}
              </VerticalSpacer>
            </form>
          )}
        </Form>
      </Section>
    )
  }
}

FormScriptSend.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormScriptSend)
