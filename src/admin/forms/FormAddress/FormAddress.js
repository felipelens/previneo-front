import React, { Component } from 'react'
import { isEmpty } from 'ramda'
import { Field } from 'react-final-form'
import VerticalSpacer from 'components/VerticalSpacer'
import FormGrid from 'components/FormGrid'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import { Addresses } from 'store/api'
import * as forms from 'utils/forms'
import { injectIntl } from 'react-intl'

class FormAddress extends Component {
  state = {
    loading: false,
    blockStreetField: this.props.values && this.props.values.rua
  }

  fillAddress (address) {
    this.props.form.batch(() => {
      this.props.form.change('pais', address.pais)
      this.props.form.change('estado', address.estado)
      this.props.form.change('cidade', address.cidade)
      this.props.form.change('rua', address.rua)
      if (this.state.blockStreetField !== !!address.rua) {
        this.setState({
          blockStreetField: !!address.rua,
          loading: false
        })
      }
    })
  }

  queryCep (cep) {
    this.setState({ loading: true }, async () => {
      try {
        const response = await Addresses.fetchOne(cep)

        if (response && response.data && !this.unmounted) {
          const { data: address } = response
          if (address && !isEmpty(address)) {
            this.fillAddress(address)
          } else {
            this.fillAddress({})
          }
        }
      } catch (err) {
        window.alert('CEP não encontrado')
        this.fillAddress({})
        this.setState({ loading: false })
      }
    })
  }

  componentDidUpdate ({ values: { cep: previousCep } }) {
    const { cep: currentCep = '' } = this.props.values

    if (currentCep && currentCep.length === 8 && currentCep !== previousCep) {
      this.queryCep(currentCep)
    }
  }

  componentWillUnmount () {
    this.unmounted = true
  }

  render () {
    const { loading: isLoadingAddress } = this.state
    const { intl: { formatMessage } } = this.props
    return (
      <div>
        <VerticalSpacer>
          <FormGrid>
            <Field
              name='cep'
              label={formatMessage({ id: 'admin.common.cep' })}
              id='form_address_cep'
              tooltip={formatMessage({ id: 'admin.common.cepTooltip' })}
              component={CustomField}
              disabled={isLoadingAddress}
              {...forms.cepRequired}
              autoFocus={this.props.autoFocus}
            />
            <Field
              name='pais'
              label={formatMessage({ id: 'admin.common.country' })}
              id='form_address_country'
              disabled
              component={CustomFieldSelect}
              customSelectProps={{
                options: this.props.values.pais
                  ? [ this.props.values.pais ]
                  : [],
                labelKey: 'nome',
                valueKey: 'id',
                isLoading: isLoadingAddress,
                placeholder: formatMessage({ id: 'admin.common.country' })
              }}
              {...forms.required}
            />
          </FormGrid>
          <FormGrid>
            <Field
              name='estado'
              label={formatMessage({ id: 'admin.common.state' })}
              id='form_address_state'
              disabled
              component={CustomFieldSelect}
              customSelectProps={{
                options: this.props.values.estado
                  ? [ this.props.values.estado ]
                  : [],
                labelKey: 'nome',
                valueKey: 'id',
                isLoading: isLoadingAddress,
                placeholder: formatMessage({ id: 'admin.common.state' })
              }}
              {...forms.required}
            />
            <Field
              name='cidade'
              label={formatMessage({ id: 'admin.common.city' })}
              id='form_patient_city'
              disabled
              component={CustomFieldSelect}
              customSelectProps={{
                options: this.props.values.cidade
                  ? [ this.props.values.cidade ]
                  : [],
                labelKey: 'nome',
                valueKey: 'id',
                isLoading: isLoadingAddress,
                placeholder: formatMessage({ id: 'admin.common.city' })
              }}
              {...forms.required}
            />
          </FormGrid>
          <Field
            name='rua'
            component={CustomField}
            label={formatMessage({ id: 'admin.common.address' })}
            disabled={isLoadingAddress || this.state.blockStreetField}
          />
          <FormGrid>
            <Field
              name='numero'
              component={CustomField}
              label={formatMessage({ id: 'admin.common.addressNumber' })}
              {...forms.required}
            />
            <Field
              name='complemento'
              component={CustomField}
              label={formatMessage({ id: 'admin.common.addressComplement' })}
            />
          </FormGrid>
        </VerticalSpacer>
      </div>
    )
  }
}

export default injectIntl(FormAddress)
