import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import * as forms from 'utils/forms'
import CustomUploader from 'admin/components/CustomUploader'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import Link from 'components/Link'
import Text from 'components/Text'
import Resource from 'containers/Resource'

const Help = ({ children, ...props }) =>
  <Text
    component='span'
    color='grayText'
    size='14px'
    sizeMobile='14px'
    style={{ display: 'inline' }}
    {...props}
  >
    {' '}
    - {children}
  </Text>

function FormPatientImport ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Resource resource='Companies' autoFetch>
              {props => (
                <Field
                  name='empresa.id'
                  label={intl.formatMessage({ id: 'admin.common.company' })}
                  id='form_patient_company'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching,
                    placeholder: intl.formatMessage({ id: 'admin.common.companyPlaceHolder' }),
                    onInputChange: value => {
                      if (value) props.fetchAll({ nome: value })
                      return value
                    }
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='language'
                  label={intl.formatMessage({ id: 'admin.common.language' })}
                  id='form_patient_language'
                  component={CustomFieldSelect}
                  readOnly={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'slug',
                    isLoading: props.isFetching,
                    placeholder: intl.formatMessage({ id: 'admin.common.search' }),
                    onInputChange: value => {
                      if (value) props.fetchAll({ language: value })
                      return value
                    }
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Field
              name='file'
              label={intl.formatMessage({ id: 'admin.common.fileImport' })}
              id='form_file'
              component={CustomUploader}
              accept='text/csv,application/vnd.ms-excel'
              readOnly={readOnly}
              {...forms.required}
            />
            <div>
              <Link
                component='a'
                href={`${process.env.PUBLIC_URL}/modelo.csv`}
                download
              >
                {intl.formatMessage({ id: 'admin.common.downloadModel' })}
              </Link>
            </div>
            <div>
              <Text>
                {intl.formatMessage({ id: 'admin.common.requiredFields' })}:
              </Text>
              <Text component='ul'>
                <li>
                  {intl.formatMessage({ id: 'admin.common.name' })}
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.cpf' })}
                  <Help>
                    {intl.formatMessage({ id: 'admin.patients.fields.cpf' })}
                  </Help>
                </li>
              </Text>
              <br />
              <Text>
                {intl.formatMessage({ id: 'admin.common.optionalFields' })}:
              </Text>
              <Text component='ul'>
                <li>
                  {intl.formatMessage({ id: 'admin.common.gender' })}
                  <Help>
                    {intl.formatMessage({ id: 'admin.patients.fields.gender' })}
                  </Help>
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.email' })}
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.phone' })}
                  <Help>
                    {intl.formatMessage({ id: 'admin.patients.fields.phone' })}
                  </Help>
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.cellphone' })}
                  <Help>
                    {intl.formatMessage({ id: 'admin.patients.fields.phone' })}
                  </Help>
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.idNumber' })}
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.dateOfBirth' })}
                  <Help>
                    {intl.formatMessage({ id: 'admin.patients.fields.birthday' })}
                  </Help>
                </li>
                <li>
                  {intl.formatMessage({ id: 'admin.common.cep' })}
                  <Help>
                    {intl.formatMessage({ id: 'admin.patients.fields.cep' })}
                  </Help>
                </li>
              </Text>
            </div>
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.import' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormPatientImport.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormPatientImport)
