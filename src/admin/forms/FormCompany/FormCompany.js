import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import CustomDate from 'admin/components/CustomDate'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomImageUploader from 'admin/components/CustomUploader'
import Resource from 'containers/Resource'
import { injectIntl } from 'react-intl'

function FormCompany ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.common.companyName' })}
              id='form_company_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='cnpj'
              label={intl.formatMessage({ id: 'admin.common.cnpj' })}
              id='form_company_cnpj'
              component={CustomField}
              readOnly={readOnly}
            />
            {props.initialValues && props.initialValues.parent && (
              <Field
                name='parent.id'
                label={intl.formatMessage({ id: 'admin.common.parentCompany' })}
                id='form_company_parent'
                component={CustomField.Select}
                disabled
                maxLength={255}
                {...forms.integerRequired}
              >
                <option value={props.initialValues.parent.id}>
                  {props.initialValues.parent.name}
                </option>
              </Field>
            )}
            <Resource resource='HealthInsurances' autoFetch>
              {props => (
                <Field
                  name='planos_saude'
                  label={intl.formatMessage({ id: 'admin.healthInsurance.title' })}
                  id='form_company_plano_de_saude'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    multi: true,
                    simpleValue: false,
                    isLoading: props.isFetching
                  }}
                />
              )}
            </Resource>
            <Field
              name='expired_at'
              label={intl.formatMessage({ id: 'admin.common.expirationDate' })}
              id='form_company_expired_at'
              component={CustomDate}
              disabled={readOnly}
            />
            <Resource resource='Risks' autoFetch>
              {props => (
                <Field
                  name='auto_emails'
                  label={intl.formatMessage({ id: 'admin.common.selectRiskForEmails' })}
                  id='form_company_risks'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'risco',
                    valueKey: 'id',
                    multi: true,
                    simpleValue: false,
                    isLoading: props.isFetching
                  }}
                />
              )}
            </Resource>
            <Field
              name='quantidade_prevista'
              label={intl.formatMessage({ id: 'admin.common.numberOfEmployees' })}
              id='form_company_quantidade_prevista'
              component={CustomField}
              readOnly={readOnly}
              type='number'
              min='0'
            />
            <Field
              name='logo'
              label={intl.formatMessage({ id: 'admin.common.logo' })}
              id='form_company_logo'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
              {...forms.required}
            />
            <Field
              name='anamnese_url'
              id='form_anamnese_url'
              component={CustomField.Option}
              type='checkbox'
            >
              {intl.formatMessage({ id: 'admin.common.openURL' })}
            </Field>
            {(values.anamnese_url || props.initialValues.anamnese_url) ? (
              <Field
                name='slug'
                component={CustomField}
                label={intl.formatMessage({ id: 'admin.common.slugURL' })}
                id='form_slug'
                {...forms.required}
              />
            ) : null}
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormCompany.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormCompany)
