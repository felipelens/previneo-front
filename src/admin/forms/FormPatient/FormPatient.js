import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import Base from 'components/Base'
import VerticalSpacer from 'components/VerticalSpacer'
import Section from 'admin/components/Section'
import FormGrid from 'components/FormGrid'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import BackLink from 'components/BackLink'
import Resource from 'containers/Resource'
import * as forms from 'utils/forms'
import PatientMedicalRecords from 'admin/containers/PatientMedicalRecords'
import FormAddress from 'admin/forms/FormAddress'
import routes from 'routes'
import { PATIENTS_ETNIAS } from 'config/constants'

function FormPatient ({ intl, isSubmitting, readOnly, title, isInfoOnly = false, ...props }) {
  const isDetails = readOnly && props.initialValues && props.initialValues.id
  const form = (
    <Form {...props}>
      {({ handleSubmit, pristine, ...formProps }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <FormGrid>
              <Resource resource='Companies'>
                {props => (
                  <Field
                    name='empresa.id'
                    label={intl.formatMessage({ id: 'admin.common.company' })}
                    id='form_patient_company'
                    component={CustomFieldSelect}
                    readOnly={readOnly}
                    customSelectProps={{
                      options: props.records.find(r =>
                        formProps.values && formProps.values.empresa && formProps.values.empresa.id === r.id
                      )
                        ? props.records
                        : formProps.values.empresa
                          ? props.records.concat(formProps.values.empresa)
                          : props.records,
                      labelKey: 'nome',
                      valueKey: 'id',
                      isLoading: props.isFetching,
                      placeholder: intl.formatMessage({ id: 'admin.common.companyPlaceHolder' }),
                      onInputChange: value => {
                        if (value) props.fetchAll({ nome: value })
                        return value
                      }
                    }}
                    {...forms.required}
                  />
                )}
              </Resource>
              <Resource resource='Languages' autoFetch={!readOnly} initialValues={props.initialValues} >
                {props => (
                  <Field
                    name='language'
                    label={intl.formatMessage({ id: 'admin.common.language' })}
                    id='form_patient_language'
                    component={CustomFieldSelect}
                    readOnly={readOnly}
                    customSelectProps={{
                      options: readOnly ? [{
                        nome: props.initialValues.idioma.nome,
                        slug: props.initialValues.idioma.slug}]
                        : props.records,
                      labelKey: 'nome',
                      valueKey: 'slug',
                      isLoading: props.isFetching,
                      placeholder: intl.formatMessage({ id: 'admin.common.search' }),
                      onInputChange: value => {
                        if (value) props.fetchAll({ language: value })
                        return value
                      }
                    }}
                    {...forms.required}
                  />
                )}
              </Resource>
              <Field
                name='matricula'
                label={intl.formatMessage({ id: 'admin.common.idNumber' })}
                id='form_patient_code'
                readOnly={readOnly}
                component={CustomField}
              />
            </FormGrid>
            <FormGrid>
              <Field
                name='name'
                label={intl.formatMessage({ id: 'admin.common.name' })}
                id='form_patient_name'
                readOnly={readOnly}
                component={CustomField}
                maxLength={255}
                {...forms.required}
              />
              <Field
                name='sexo'
                label={intl.formatMessage({ id: 'admin.common.gender' })}
                id='form_patient_gender'
                disabled={readOnly}
                component={CustomFieldSelect}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.genderMale' }), value: 'M' },
                    { label: intl.formatMessage({ id: 'admin.common.genderFemale' }), value: 'F' }
                  ]
                }}
                {...forms.required}
              />
            </FormGrid>
            <FormGrid>
              <Field
                name='data_nascimento'
                label={intl.formatMessage({ id: 'admin.common.dateOfBirth' })}
                id='form_patient_birthday'
                readOnly={readOnly}
                component={CustomField}
                placeholder={intl.formatMessage({ id: 'admin.common.birthDatePlaceHolder' })}
                {...forms.dateRequired}
              />
              <Field
                name='cpf'
                label={intl.formatMessage({ id: 'admin.common.cpf' })}
                id='form_patient_cpf'
                readOnly={readOnly}
                component={CustomField}
                {...forms.cpfRequired}
              />
            </FormGrid>
            <Field
              name='email'
              label={intl.formatMessage({ id: 'admin.common.email' })}
              id='form_patient_email'
              readOnly={readOnly}
              component={CustomField}
              {...forms.emailRequired}
            />
            <FormGrid>
              <Field
                name='etnia'
                label={intl.formatMessage({ id: 'q.ethnicity.question' })}
                id='form_patient_etnia'
                disabled={readOnly}
                component={CustomFieldSelect}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: `admin.common.ethnicity.${PATIENTS_ETNIAS.white.label}` }), value: PATIENTS_ETNIAS.white.value },
                    { label: intl.formatMessage({ id: `admin.common.ethnicity.${PATIENTS_ETNIAS.black.label}` }), value: PATIENTS_ETNIAS.black.value },
                    { label: intl.formatMessage({ id: `admin.common.ethnicity.${PATIENTS_ETNIAS.brown.label}` }), value: PATIENTS_ETNIAS.brown.value },
                    { label: intl.formatMessage({ id: `admin.common.ethnicity.${PATIENTS_ETNIAS.yellow.label}` }), value: PATIENTS_ETNIAS.yellow.value },
                    { label: intl.formatMessage({ id: `admin.common.ethnicity.${PATIENTS_ETNIAS.indigenous.label}` }), value: PATIENTS_ETNIAS.indigenous.value }
                  ]
                }}
              />
              <Field
                name='telefone'
                label={intl.formatMessage({ id: 'admin.common.phone' })}
                id='form_patient_phone'
                readOnly={readOnly}
                component={CustomField}
                {...forms.phone}
              />
              <Field
                name='celular'
                label={intl.formatMessage({ id: 'admin.common.cellphone' })}
                id='form_patient_cellphone'
                readOnly={readOnly}
                component={CustomField}
                {...forms.phone}
              />
              <Field
                name='aceita_contato'
                label={intl.formatMessage({ id: 'admin.common.acceptContact' })}
                id='form_patient_accepts_contact'
                component={CustomFieldSelect}
                disabled={readOnly}
                customSelectProps={{
                  options: [
                    { label: intl.formatMessage({ id: 'admin.common.yes' }), value: true },
                    { label: intl.formatMessage({ id: 'admin.common.no' }), value: false }
                  ]
                }}
                {...forms.required}
              />
            </FormGrid>
            <FormAddress {...formProps} />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )

  return (
    <VerticalSpacer space={45}>
      {isDetails && !isInfoOnly ? (
        <Base
          component={Section}
          title={title}
          side={<BackLink to={routes.admin.patients.patients.index} />}
        >
          {form}
        </Base>
      ) : isInfoOnly ? (
        <Base
          component={Section}
          title={title}
        >
          {form}
        </Base>
      ) : form}
      {isDetails && !isInfoOnly &&
        <PatientMedicalRecords patientId={props.initialValues.id} />
      }
    </VerticalSpacer>
  )
}

FormPatient.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool,

  /** Exibe o form em modo leitura, sem título e historico médico */
  isInfoOnly: PropTypes.bool
}

export default injectIntl(FormPatient)
