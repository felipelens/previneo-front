import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import FormGrid from 'components/FormGrid'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import { TYPE_QUESTIONS, QUESTIONS_FORMATS } from 'config/constants'

function FormQuestion ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='typeQuestion'
              label={intl.formatMessage({ id: 'admin.script.questionType' })}
              id='form_question_type'
              component={CustomField.Select}
              disabled={props.initialValues}
              {...forms.required}
            >
              <option value='' disabled>
                {intl.formatMessage({ id: 'admin.script.selectTheQuestionType' })}
              </option>
              {Object.values(TYPE_QUESTIONS).map(t => (
                <option key={t.id} value={t.id}>
                  {intl.formatMessage({ id: t.name })}
                </option>
              ))}
            </Field>
            {values && values.typeQuestion === TYPE_QUESTIONS.DISCURSIVA.id && (
              <React.Fragment>
                <Field
                  name='format'
                  label={intl.formatMessage({ id: 'admin.common.format' })}
                  component={CustomField.Select}
                >
                  {Object.values(QUESTIONS_FORMATS).map((option, index) => (
                    <option key={index} value={option.id}>
                      {intl.formatMessage({ id: option.name })}
                    </option>
                  ))}
                </Field>
                {values.format === QUESTIONS_FORMATS.integer.id && (
                  <FormGrid>
                    <Field
                      name='min'
                      component={CustomField}
                      label={intl.formatMessage({ id: 'admin.common.min' })}
                      {...forms.integerRequired}
                    />
                    <Field
                      name='max'
                      component={CustomField}
                      label={intl.formatMessage({ id: 'admin.common.max' })}
                      {...forms.integerRequired}
                    />
                  </FormGrid>
                )}
              </React.Fragment>
            )}
            <Field
              name='title'
              id='form_translate_title'
              label={intl.formatMessage({ id: 'admin.script.questionTitle' })}
              component={CustomField}
              {...forms.required}
            />
            <Field
              name='description'
              id='form_translate_description'
              label={intl.formatMessage({ id: 'admin.script.questionDescription' })}
              component={CustomField.Textarea}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormQuestion.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormQuestion)
