import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import CustomField from 'components/CustomField'
import CustomEditor from 'admin/components/CustomEditor'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import FieldTranslations from '../../components/FieldTranslations/FieldTranslations'

function FormEmails ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form {...props} mutators={arrayMutators}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            {
              values.empresa
                ? (
                  <Field
                    name='empresa.nome'
                    label={intl.formatMessage({ id: 'admin.common.company' })}
                    component={CustomField}
                    id='form_user_empresa'
                    disabled
                  />
                )
                : null
            }
            <FieldTranslations
              values={values}
              disabled={readOnly}
              wrapped
              additional={[
                { component: CustomField,
                  name: 'assunto',
                  label: intl.formatMessage({ id: 'admin.common.subject' })
                },
                { component: CustomEditor,
                  name: 'conteudo',
                  label: intl.formatMessage({ id: 'admin.common.content' })
                }
              ]}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormEmails.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormEmails)
