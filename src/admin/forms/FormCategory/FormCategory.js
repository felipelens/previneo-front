import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import CustomScriptEditor from 'admin/components/CustomScriptEditor'
import CustomImageUploader from 'admin/components/CustomUploader'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import isValidTreeScript from 'admin/components/TreeScript/isValidTreeScript'
import CustomColorPicker from 'admin/components/CustomColorPicker'
import Resource from 'containers/Resource'

function FormCategory ({ isSubmitting, readOnly, intl, initialValues, ...props }) {
  return (
    <Form
      {...props}
      initialValues={{
        exibir_antes_anamnese: false,
        ...initialValues
      }}
      onSubmit={data => {
        if (isValidTreeScript(data.script)) {
          return props.onSubmit(data)
        }
      }}
    >
      {({ handleSubmit, pristine }) => (
        <form onSubmit={e => e.preventDefault()}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_category_name'
              component={CustomField}
              disabled={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Resource resource='Genders' autoFetch>
              {props => (
                <Field
                  name='genero.id'
                  label={intl.formatMessage({ id: 'admin.common.gender' })}
                  id='form_category_gender'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Resource resource='TypesCancer' autoFetch>
              {props => (
                <Field
                  name='tipo_cancer.id'
                  label={intl.formatMessage({ id: 'admin.cancerTypes.title' })}
                  id='form_category_language'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                />
              )}
            </Resource>
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='idioma.id'
                  label={intl.formatMessage({ id: 'admin.languages.title' })}
                  id='form_category_language'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Field
              name='exibir_antes_anamnese'
              id='form_anamnese_url'
              component={CustomField.Option}
              type='checkbox'
              disabled={readOnly}
            >
              {intl.formatMessage({ id: 'admin.common.displayBeforeAnamnesis' })}
            </Field>
            <Field
              name='icone'
              label={intl.formatMessage({ id: 'admin.common.icon' })}
              id='form_category_icon'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
              {...forms.required}
            />
            <Field
              name='cor'
              label={intl.formatMessage({ id: 'admin.common.categoryColor' })}
              id='form_category_cor'
              component={CustomColorPicker}
              disabled={readOnly}
              {...forms.required}
            />
            <Field
              name='script'
              label={intl.formatMessage({ id: 'admin.common.script' })}
              id='form_category_data'
              component={CustomScriptEditor}
              readOnly={readOnly}
            />
            {!readOnly && (
              <ButtonSubmit
                type='button'
                isSubmitting={isSubmitting}
                disabled={pristine}
                onClick={handleSubmit}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormCategory.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormCategory)
