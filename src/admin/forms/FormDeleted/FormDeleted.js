import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomCheckDataTable from 'admin/components/CustomCheckDataTable'
import * as forms from 'utils/forms'
import ButtonSubmit from 'components/ButtonSubmit'
import Resource from 'containers/Resource'
import Button from 'components/Button'
import queryToObject from 'utils/queryToObject'
import { CompanyColumn } from 'admin/columns'
import WithModal from '../../components/WithModal/WithModal'
import ConfirmModal from '../../components/ConfirmModal'

class FormDeleted extends Component {
  state = {
    firstLoaded: false
  }

  render () {
    const { isSubmitting, readOnly, intl, resource, params, ...props } = this.props
    return (
      <Form {...props}>
        {({ handleSubmit, pristine, form }) => (
          <form onSubmit={e => e.preventDefault()}>
            <Resource
              resource={resource}
              autoFetch
              params={params}
              onFetchSuccess={() => {
                if (!this.state.firstLoaded) {
                  this.setState({ firstLoaded: true })
                }

                form.change('usuarios', [])
              }}
            >
              {props => (
                <Field
                  name='usuarios'
                  label={intl.formatMessage({ id: 'admin.common.select' })}
                  id='form_script_send_patients'
                  component={CustomCheckDataTable}
                  records={props.records}
                  pages={props.pagination.last_page}
                  loading={props.isFetching}
                  onFetchData={({ page, pageSize, sorted, filtered }) => {
                    if (!this.state.firstLoaded) return
                    const filteredParams = queryToObject(filtered)
                    props.fetchAll({
                      page: page + 1,
                      limit: pageSize,
                      ...filteredParams,
                      ...props.params
                    })
                  }}
                  columns={[
                    {
                      accessor: 'name',
                      id: 'nome',
                      Header: intl.formatMessage({ id: 'admin.common.name' })
                    },
                    {
                      accessor: 'email',
                      Header: intl.formatMessage({ id: 'admin.common.email' }),
                      width: 300
                    },
                    CompanyColumn(),
                    {
                      width: 150,
                      filterable: false,
                      Header: intl.formatMessage({ id: 'admin.common.details' }),
                      Cell: ({ original }) => {
                        const itemId = original.id
                        return (
                          <WithModal
                            modal={({ closeModal }) => (
                              <ConfirmModal
                                title={this.props.intl.formatMessage({ id: 'admin.common.AreYouSureABoutIt' })}
                                text=''
                                onConfirm={() => {
                                  props.remove({
                                    usuarios: [itemId]
                                  }).then(() => {
                                    props.fetchAll({
                                      ...props.params
                                    }).then(closeModal())
                                  })
                                }
                                }
                                onCancel={closeModal}
                              />
                            )}
                          >
                            {({ toggleModal }) => (
                              <Button
                                component='button'
                                color='success'
                                block
                                size='small'
                                type='button'
                                onClick={toggleModal}
                              >
                                <FormattedMessage id='admin.common.restore' />
                              </Button>
                            )}
                          </WithModal>
                        )
                      }
                    }
                  ]}
                  {...forms.required}
                />
              )}
            </Resource>
            {!readOnly && (
              <WithModal
                modal={({ closeModal }) => (
                  <ConfirmModal
                    title={this.props.intl.formatMessage({ id: 'admin.common.AreYouSureABoutIt' })}
                    text=''
                    onConfirm={() => {
                      handleSubmit()
                      closeModal()
                    }}
                    onCancel={closeModal}
                  />
                )}
              >
                {({ toggleModal }) => (
                  <ButtonSubmit
                    isSubmitting={isSubmitting}
                    disabled={pristine}
                    label={this.props.intl.formatMessage({ id: 'admin.common.restoreSelected' })}
                    onClick={toggleModal}
                    style={{ marginTop: 25 }}
                  />
                )
                }
              </WithModal>
            )}
          </form>
        )}
      </Form>
    )
  }
}

FormDeleted.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormDeleted)
