import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Resource from 'containers/Resource'

function FormCategorySelect ({ isSubmitting, readOnly, intl, categories, ...props }) {
  const medicalHistoryCategories = categories.filter(c => c.historico_medico)
  return (
    <Form {...props}>
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Resource resource='Categories' params={{ disponivel: true }} autoFetch>
              {props => (
                <Field
                  name='categoria'
                  label={intl.formatMessage({ id: 'admin.categories.title' })}
                  id='form_category_select'
                  component={CustomFieldSelect}
                  readOnly={readOnly}
                  customSelectProps={{
                    options: props.records
                      .map(r => ({
                        ...r,
                        nome: `${r.nome} (${r.genero.nome})`
                      }))
                      .filter(r =>
                        !(r.historico_medico && !!medicalHistoryCategories.find(c => c.genero.id === r.genero.id))
                      ),
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormCategorySelect.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormCategorySelect)
