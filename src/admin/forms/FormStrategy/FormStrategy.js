import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import FieldTranslations from 'admin/components/FieldTranslations'
import CustomField from 'components/CustomField'
import CustomEditor from 'admin/components/CustomEditor'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Grid from 'admin/components/Grid'

function FormStrategy ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props} mutators={arrayMutators}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='empresa.nome'
              label={intl.formatMessage({ id: 'admin.company.title' })}
              id='form_empresa'
              component={CustomField}
              disabled
            />
            <Grid>
              <Field
                name='nome'
                label={intl.formatMessage({ id: 'admin.cancerTypes.title' })}
                id='form_tipo_cancer'
                component={CustomField}
                disabled
              />
              <Field
                name='risco.risco'
                label={intl.formatMessage({ id: 'admin.common.risk' })}
                id='form_tipo_cancer'
                component={CustomField}
                disabled
              />
              <Field
                name='genero.nome'
                label={intl.formatMessage({ id: 'admin.common.gender' })}
                id='form_gender'
                component={CustomField}
                disabled
              />
            </Grid>
            <FieldTranslations
              values={values}
              label={intl.formatMessage({ id: 'admin.common.description' })}
              component={CustomEditor}
              disabled={readOnly}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormStrategy.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormStrategy)
