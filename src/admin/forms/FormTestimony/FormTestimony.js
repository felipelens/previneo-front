import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import Resource from 'containers/Resource'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import Auth from 'admin/containers/Auth'
import { injectIntl } from 'react-intl'

function FormTestimony ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.testimonies.author' })}
              id='form_testimonies_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='descricao'
              label={intl.formatMessage({ id: 'admin.testimonies.title' })}
              id='form_testimonies_description'
              component={CustomField.Textarea}
              readOnly={readOnly}
              rows={5}
              {...forms.required}
            />
            <Auth>
              {({ isSuperAdmin }) => isSuperAdmin && (
                <Resource resource='Companies' autoFetch>
                  {props => (
                    <Field
                      name='empresa.id'
                      label={intl.formatMessage({ id: 'admin.common.company' })}
                      id='form_user_empresa'
                      component={CustomFieldSelect}
                      readOnly={readOnly}
                      customSelectProps={{
                        options: values.empresa && !props.records.find(r => r.id === values.empresa.id)
                          ? props.records.concat(values.empresa)
                          : props.records,
                        labelKey: 'nome',
                        valueKey: 'id',
                        isLoading: props.isFetching,
                        placeholder: intl.formatMessage({ id: 'admin.common.companyPlaceHolder' }),
                        onInputChange: value => {
                          if (value) props.fetchAll({ nome: value })
                          return value
                        }
                      }}
                      {...forms.required}
                    />
                  )}
                </Resource>
              )}
            </Auth>
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='idioma.id'
                  id='form_idioma.id'
                  label={intl.formatMessage({ id: 'admin.languages.title' })}
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Auth>
              {({ isSuperAdmin }) => isSuperAdmin && (
                <Field
                  name='visivel'
                  id='form_visivel'
                  component={CustomField.Option}
                  type='checkbox'
                >
                  {intl.formatMessage({ id: 'admin.common.visible' })}
                </Field>
              )}
            </Auth>
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormTestimony.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormTestimony)
