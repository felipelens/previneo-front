import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import CustomField from 'components/CustomField'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import * as forms from 'utils/forms'
import Resource from 'containers/Resource'
import FieldTranslations from '../../components/FieldTranslations/FieldTranslations'

function FormExams ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props} mutators={arrayMutators}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Resource resource='Strategies' autoFetch>
              {props => {
                const estrategias = props.records.map(estrategia => ({
                  id: estrategia.id,
                  nome: `${estrategia.nome} - ${estrategia.risco.risco} - ${estrategia.genero.nome}`
                }))

                return (
                  <Field
                    name='estrategia.id'
                    label={intl.formatMessage({ id: 'admin.strategies.title' })}
                    id='form_estrategia'
                    component={CustomFieldSelect}
                    disabled={readOnly}
                    customSelectProps={{
                      options: estrategias,
                      labelKey: 'nome',
                      valueKey: 'id',
                      isLoading: props.isFetching
                    }}
                    {...forms.required}
                  />
                )
              }}
            </Resource>
            <Field
              name='tuss'
              label={intl.formatMessage({ id: 'admin.common.tuss' })}
              id='form_TUSS'
              component={CustomField}
              disabled={readOnly}
              {...forms.required}
            />
            <Field
              name='idade_minima'
              type='tel'
              component={CustomField}
              label={intl.formatMessage({ id: 'admin.common.minimumAge' })}
              id='minimum_age'
              disabled={readOnly}
              {...forms.integerRequired}
            />
            <FieldTranslations
              values={values}
              disabled={readOnly}
              label={intl.formatMessage({ id: 'admin.common.name' })}
              component={CustomField}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormExams.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormExams)
