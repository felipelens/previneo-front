import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import CustomImageUploader from 'admin/components/CustomUploader'
import CustomColorPicker from 'admin/components/CustomColorPicker'
import CustomEditor from 'admin/components/CustomEditor'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import { injectIntl } from 'react-intl'
import CustomFieldSelect from '../../components/CustomFieldSelect'
import Resource from '../../../containers/Resource'

function FormSpeciality ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form
      {...props}
    >
      {({ handleSubmit, pristine }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Field
              name='nome'
              label={intl.formatMessage({ id: 'admin.common.name' })}
              id='form_specialities_name'
              component={CustomField}
              readOnly={readOnly}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='legenda'
              label={intl.formatMessage({ id: 'admin.common.subtitle' })}
              id='form_specialities_legenda'
              component={CustomField.Textarea}
              readOnly={readOnly}
              row={2}
              maxLength={255}
              {...forms.required}
            />
            <Field
              name='icone'
              label={intl.formatMessage({ id: 'admin.common.icon' })}
              id='form_specialities_icon'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
            />
            <Field
              name='descricao'
              label={intl.formatMessage({ id: 'admin.common.description' })}
              id='form_specialities_descricao'
              component={CustomEditor}
              readOnly={readOnly}
              {...forms.required}
            />
            <Resource resource='Languages' autoFetch>
              {props => (
                <Field
                  name='idioma.id'
                  label={intl.formatMessage({ id: 'admin.common.language' })}
                  id='form_specialty_language'
                  component={CustomFieldSelect}
                  disabled={readOnly}
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Field
              name='cor'
              label={intl.formatMessage({ id: 'admin.common.specialtyColor' })}
              id='form_specialities_cor'
              component={CustomColorPicker}
              readOnly={readOnly}
            />
            <Field
              name='imagem'
              label={intl.formatMessage({ id: 'admin.common.image' })}
              id='form_specialities_image'
              component={CustomImageUploader}
              accept='image/*'
              readOnly={readOnly}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormSpeciality.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormSpeciality)
