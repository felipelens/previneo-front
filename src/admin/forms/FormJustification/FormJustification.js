import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { Form, Field } from 'react-final-form'
import CustomField from 'components/CustomField'
import * as forms from 'utils/forms'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import CustomFieldSelect from 'admin/components/CustomFieldSelect'
import Resource from 'containers/Resource'
import FieldTranslations from '../../components/FieldTranslations/FieldTranslations'
import arrayMutators from 'final-form-arrays'

function FormJustification ({ isSubmitting, readOnly, intl, ...props }) {
  return (
    <Form
      {...props}
      mutators={arrayMutators}
    >
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <Resource resource='TypesCancer' autoFetch>
              {props => (
                <Field
                  name='tipo_cancer.id'
                  label={intl.formatMessage({ id: 'admin.cancerTypes.title' })}
                  id='form_justification_cancer'
                  component={CustomFieldSelect}
                  disabled
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'nome',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <Resource resource='Risks' autoFetch>
              {props => (
                <Field
                  name='risco.id'
                  label={intl.formatMessage({ id: 'admin.common.risk' })}
                  id='form_justification_cancer'
                  component={CustomFieldSelect}
                  disabled
                  customSelectProps={{
                    options: props.records,
                    labelKey: 'risco',
                    valueKey: 'id',
                    isLoading: props.isFetching
                  }}
                  {...forms.required}
                />
              )}
            </Resource>
            <FieldTranslations
              values={values}
              disabled={readOnly}
              additional={[
                { component: CustomField,
                  name: 'descricao',
                  label: intl.formatMessage({ id: 'admin.justifications.title' })
                }
              ]}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormJustification.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormJustification)
