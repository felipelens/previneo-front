import React from 'react'
import PropTypes from 'prop-types'
import { Form } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import VerticalSpacer from 'components/VerticalSpacer'
import ButtonSubmit from 'components/ButtonSubmit'
import { injectIntl } from 'react-intl'
import FieldTranslations from '../../components/FieldTranslations/FieldTranslations'
import CustomField from '../../../components/CustomField/CustomField'

function FormPermission ({ intl, isSubmitting, readOnly, ...props }) {
  return (
    <Form {...props} mutators={arrayMutators}>
      {({ handleSubmit, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <VerticalSpacer>
            <FieldTranslations
              values={values}
              disabled={readOnly}
              wrapped
              additional={[
                { component: CustomField,
                  name: 'entity',
                  label: intl.formatMessage({ id: 'admin.common.entity' })
                },
                { component: CustomField,
                  name: 'description',
                  label: intl.formatMessage({ id: 'admin.common.description' })
                }
              ]}
            />
            {!readOnly && (
              <ButtonSubmit
                isSubmitting={isSubmitting}
                disabled={pristine}
                label={intl.formatMessage({ id: 'admin.common.save' })}
              />
            )}
          </VerticalSpacer>
        </form>
      )}
    </Form>
  )
}

FormPermission.propTypes = {
  /** Callback para o form submit */
  onSubmit: PropTypes.func.isRequired,

  /** Form sendo processado */
  isSubmitting: PropTypes.bool,

  /** Exibe o form em modo leitura */
  readOnly: PropTypes.bool
}

export default injectIntl(FormPermission)
