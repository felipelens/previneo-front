import React from 'react'
import Loadable from 'react-loadable'
import PageContent from 'components/PageContent'
import Spinner from 'components/Spinner'

export default Loadable({
  loader: () => import('./containers/Admin'),
  loading: () => (
    <PageContent>
      <Spinner />
    </PageContent>
  )
})
