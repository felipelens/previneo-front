import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Grid = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  ${mediaQuery.lessThan('medium')`
    flex-direction: column;
  `}
  > *:nth-child(odd) {
    ${mediaQuery.greaterThan('medium')`
      width: calc(50% - 130px);
    `}
  }

  > *:nth-child(2) {
    ${mediaQuery.greaterThan('medium')`
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    `}
    ${mediaQuery.lessThan('medium')`
      margin: 20px 0;
    `}
  }
`

export const GridRisk = styled.div`
  display: flex;
  ${mediaQuery.greaterThan('large')`
    > *:first-child {
      width: 42%;
    }
    > *:last-child {
      flex: 1;
      margin-left: 20px;
    }
  `}
  ${mediaQuery.lessThan('large')`
    flex-direction: column;
    > *:last-child {
      margin-top: 20px;
    }
  `}
`
