import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import routes from 'routes'
import DashboardSummary from 'admin/components/DashboardSummary'
import DashboardAnamnesis from 'admin/components/DashboardAnamnesis'
import DashboardAnamnesisByGender from 'admin/components/DashboardAnamnesisByGender'
import DashboardRisk from 'admin/components/DashboardRisk'

function DashboardCharts ({ company, name }) {
  return (
    <Switch>
      <Route
        exact
        path={routes.admin.dashboard.index}
        component={() => <DashboardSummary company={company} name={name} />}
      />
      <Route
        exact
        path={routes.admin.index}
        component={() => <DashboardSummary company={company} name={name} />}
      />
      <Route
        exact
        path={routes.admin.dashboard.anamnesis}
        component={() => <DashboardAnamnesis company={company} name={name} />}
      />
      <Route
        exact
        path={routes.admin.dashboard.anamnesisByGender}
        component={() => <DashboardAnamnesisByGender company={company} name={name} />}
      />
      <Route
        path={routes.admin.dashboard.highRisk.type}
        component={({match}) => (<DashboardRisk highRisk company={company} name={name} params={match.params} />)}
      />
      <Route
        path={routes.admin.dashboard.normalRisk.type}
        component={({match}) => (<DashboardRisk company={company} name={name} params={match.params} />)}
      />
      <Route render={() => <Redirect to={routes.admin.dashboard.index} />} />
    </Switch>
  )
}

DashboardCharts.propTypes = {
  /** ID da empresa */
  company: PropTypes.number
}

export default DashboardCharts
