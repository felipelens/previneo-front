import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { isEmpty } from 'ramda'
import Section from 'admin/components/Section'
import Text from 'components/Text'
import Resource from 'containers/Resource'
import MedicalRecords from 'admin/components/MedicalRecords'

function PatientMedicalRecords ({ patientId, intl }) {
  return (
    <Section title={intl.formatMessage({ id: 'admin.medicalRecords.title' })}>
      <Resource
        resource='MedicalRecords'
        id={patientId}
        autoFetch
        spinner
      >
        {({ detailedRecord }) => !isEmpty(detailedRecord) ? (
          <MedicalRecords patientId={patientId} data={detailedRecord.roteiros} />
        ) : (
          <Text>
            {intl.formatMessage({ id: 'admin.medicalRecords.noInformation' })}
          </Text>
        )}
      </Resource>
    </Section>
  )
}

PatientMedicalRecords.propTypes = {
  /** ID do paciente */
  patientId: PropTypes.number.isRequired
}

export default injectIntl(PatientMedicalRecords)
