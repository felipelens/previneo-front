import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { isEmpty } from 'ramda'
import Text from 'components/Text'
import HistoryLogs from 'admin/components/HistoryLogs'

function PatientHistoryLogs ({ patientId, intl, ...props }) {
  return (
    !isEmpty(props.resource.detailedRecord) ? (
      <HistoryLogs readOnly={props.readOnly} patientId={patientId} data={props.resource.detailedRecord.logs} />
    ) : (
      <Text>
        {intl.formatMessage({ id: 'admin.medicalRecords.noInformation' })}
      </Text>
    )
  )
}

PatientHistoryLogs.propTypes = {
  /** ID do paciente */
  patientId: PropTypes.number.isRequired
}

export default injectIntl(PatientHistoryLogs)
