import React from 'react'
import { Switch, Route } from 'react-router-dom'
import routes from 'routes'
import { LoginScreen, PasswordRecoveryScreen } from 'admin/screens'
import Dashboard from 'admin/components/Dashboard'
import Logout from 'admin/containers/Logout'
import AuthLogin from 'components/AuthLogin'

export default function Admin () {
  return (
    <AuthLogin type='users'>
      <Switch>
        <Route exact path={routes.admin.login} component={LoginScreen} />
        <Route exact path={routes.admin.passwordRecovery} component={PasswordRecoveryScreen} />
        <Route exact path={routes.admin.logout} component={Logout} />
        <Route path={routes.admin.index} component={Dashboard} />
      </Switch>
    </AuthLogin>
  )
}
