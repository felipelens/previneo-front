import React, { Component } from 'react'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import Section from 'admin/components/Section'
import Button from 'components/Button/Button'
import Tree from 'admin/components/Tree'
import WithModal from 'admin/components/WithModal'
import Notifications from 'containers/Notifications'
import VerticalSpacer from 'components/VerticalSpacer'
import ConfirmModal from 'admin/components/ConfirmModal'
import FormCompany from 'admin/forms/FormCompany'
import * as companiesDuck from 'store/ducks/companies'
import withAuth from 'store/utils/withAuth'
import handleFormSubmission from 'utils/handleFormSubmission'
import PERMISSIONS from 'config/permissions'

class Companies extends Component {
  rootId = 0

  componentDidMount () {
    if (!this.props.nodes[this.rootId]) {
      this.props.registerRootNode(
        this.rootId,
        this.props.auth.user.empresa.nome
      )
    }
  }

  formModal = props => {
    if (props.initialValues) {
      return (
        <Section title={this.props.intl.formatMessage({ id: 'admin.common.editCompany' })} wrapped={false}>
          <VerticalSpacer>
            <Notifications notificationsId='companies' />
            <FormCompany
              onSubmit={data => {
                const promise = this.props.update(props.initialValues.id, data)
                  .then(() => {
                    props.closeModal()
                  })

                return handleFormSubmission(promise)
              }}
              isSubmitting={this.props.isSubmitting}
              initialValues={props.initialValues}
            />
          </VerticalSpacer>
        </Section>
      )
    }

    return (
      <Section title={this.props.intl.formatMessage({ id: 'admin.common.registerCompany' })} wrapped={false}>
        <VerticalSpacer>
          <Notifications notificationsId='companies' />
          <FormCompany
            onSubmit={data => {
              const promise = this.props.create(data)
                .then(() => {
                  props.closeModal()
                })

              return handleFormSubmission(promise)
            }}
            isSubmitting={this.props.isSubmitting}
            initialValues={{
              parent: props.parent
            }}
          />
        </VerticalSpacer>
      </Section>
    )
  }

  confirmModal = props => (
    <ConfirmModal
      title={this.props.intl.formatMessage({ id: 'admin.common.removeMessage' })}
      text=''
      onConfirm={() => {
        this.props.remove(props.node.id, props.parentId)
        props.closeModal()
      }}
      onCancel={props.closeModal}
    />
  )

  render () {
    if (!this.props.nodes[this.rootId]) {
      return this.props.intl.formatMessage({ id: 'admin.common.rootNohNotFound' })
    }

    const { permissions } = this.props.auth

    return (
      <WithModal modal={this.formModal}>
        {({ toggleModal: toggleFormModal }) => (
          <Section
            title={this.props.intl.formatMessage({ id: 'admin.company.title.plural' })}
            side={permissions.includes(PERMISSIONS.companies.create) ? (
              <Button
                type='button'
                color='success'
                onClick={() => {
                  toggleFormModal(
                    this.rootId !== 0
                      ? { parent: { id: this.rootId, name: this.props.auth.user.empresa.nome } }
                      : undefined
                  )
                }}>
                {this.props.intl.formatMessage({ id: 'admin.common.register' })}
              </Button>
            ) : null}
          >
            <WithModal modal={this.confirmModal}>
              {({ toggleModal: toggleConfirmModal }) => (
                <Tree
                  parentId={this.rootId}
                  nodes={this.props.nodes}
                  canCreate={permissions.includes(PERMISSIONS.companies.create)}
                  canUpdate={permissions.includes(PERMISSIONS.companies.update)}
                  canDelete={permissions.includes(PERMISSIONS.companies.delete)}
                  onRequestNodes={this.props.fetchNodes}
                  onRequestNodeCreation={node => toggleFormModal({
                    parent: {
                      id: node.id,
                      name: node.name
                    }
                  })}
                  onRequestNodeDeletion={(node, parentNode) => toggleConfirmModal({
                    node,
                    parentId: parentNode.id
                  })}
                  onRequestNodeEdition={(node, parentNode) => toggleFormModal({
                    initialValues: {
                      ...node.data,
                      parent: parentNode.id === 0
                        ? null
                        : parentNode
                    }
                  })}
                />
              )}
            </WithModal>
          </Section>
        )}
      </WithModal>
    )
  }
}

const mapStateToProps = state => ({
  isSubmitting: companiesDuck.isSubmitting(state),
  nodes: companiesDuck.getNodes(state),
  error: companiesDuck.getError(state)
})

const mapDispatchToProps = {
  create: companiesDuck.create,
  update: companiesDuck.update,
  remove: companiesDuck.remove,
  fetchNodes: companiesDuck.fetchNodes,
  registerRootNode: companiesDuck.registerRootNode
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withAuth(injectIntl(Companies)))
