import React from 'react'
import { injectIntl } from 'react-intl'
import VerticalSpacer from 'components/VerticalSpacer'
import Section from 'admin/components/Section'
import FormPatientsFilter from 'admin/forms/FormPatientsFilter'
import Resource from 'containers/Resource'
import QueueTable from 'admin/components/QueueTable'

function PatientsFilter ({ intl }) {
  return (
    <VerticalSpacer space={45}>
      <Section style={{ overflow: 'visible' }} title={intl.formatMessage({ id: 'admin.common.patientsFilter' })}>
        <Resource resource='Reports'>
          {props => (
            <FormPatientsFilter
              isSubmitting={props.isSubmitting}
              onSubmit={(values, form) => {
                return props.create(values).then(() => form.reset())
              }}
            />
          )}
        </Resource>
      </Section>
      <Section title={intl.formatMessage({ id: 'admin.common.queue' })}>
        <QueueTable resource='Reports' />
      </Section>
    </VerticalSpacer>
  )
}

export default injectIntl(PatientsFilter)
