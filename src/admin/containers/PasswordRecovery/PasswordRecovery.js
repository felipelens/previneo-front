import React from 'react'
import qs from 'querystringify'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import VerticalSpacer from 'components/VerticalSpacer'
import FormPasswordRecovery from 'admin/forms/FormPasswordRecovery'
import FormPasswordReset from 'admin/forms/FormPasswordReset'
import Notifications from 'containers/Notifications'
import Resource from 'containers/Resource'

export default function PasswordRecovery ({ location }) {
  const query = qs.parse(location.search)
  return (
    <PageContent>
      <Container size='xsmall'>
        <VerticalSpacer>
          <Notifications />
          <Resource
            resource='PasswordToken'
            notifications={{
              create: {
                success: true,
                errors: true,
                defaultSuccessMessage: 'Um e-mail com o link para redefinir sua senha foi enviado para você!'
              },
              update: {
                success: true,
                errors: true,
                defaultSuccessMessage: 'Sua senha foi atualizada com sucesso!'
              }
            }}
          >
            {props => {
              if (query.hasOwnProperty('token')) {
                return (
                  <FormPasswordReset
                    onSubmit={data => props.update(query.token, data)}
                    isSubmitting={props.isSubmitting}
                  />
                )
              }

              return (
                <FormPasswordRecovery
                  onSubmit={props.create}
                  isSubmitting={props.isSubmitting}
                />
              )
            }}
          </Resource>
        </VerticalSpacer>
      </Container>
    </PageContent>
  )
}
