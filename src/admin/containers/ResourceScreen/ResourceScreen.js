import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'
import ProtectedScreen from 'admin/components/ProtectedScreen'
import ResourceList from 'admin/containers/ResourceList'
import ResourceNew from 'admin/containers/ResourceNew'
import ResourceUpdate from 'admin/containers/ResourceUpdate'
import ResourceDetails from 'admin/containers/ResourceDetails'
import Auth from 'admin/containers/Auth'
import Text from 'components/Text'
import { injectIntl } from 'react-intl'

function ResourceScreen ({ intl, resource, readableResourceName, routeBase, extraActionButton, ...props }) {
  return (
    <Auth>
      {({ permissions }) => {
        if (!permissions.includes(props.permissions.viewAll)) {
          return (
            <Text>Você não tem permissão para acessar essa página</Text>
          )
        }

        return (
          <Switch>
            <Route
              exact
              path={routeBase.index}
              render={() => (
                <ProtectedScreen title={`${intl.formatMessage({ id: 'admin.common.listOf' })} ${readableResourceName[1]}`}>
                  <ResourceList
                    resource={resource}
                    title={`${intl.formatMessage({ id: 'admin.common.listOf' })} ${readableResourceName[1]}`}
                    routeBase={routeBase}
                    columns={props.columns}
                    dataTableProps={props.dataTableProps}
                    canAdd={props.canAdd && permissions.includes(props.permissions.create)}
                    canEdit={props.canEdit && permissions.includes(props.permissions.update)}
                    canRemove={props.canRemove && permissions.includes(props.permissions.delete)}
                    canShowDetails={props.canShowDetails && permissions.includes(props.permissions.view)}
                    extraActionButton={extraActionButton}
                    actionColumnWidth={props.actionColumnWidth}
                    {...props.resourceListProps}
                  />
                </ProtectedScreen>
              )}
            />
            {props.canAdd && (
              <Route
                path={routeBase.new}
                render={() => (
                  <ProtectedScreen title={`${intl.formatMessage({ id: 'admin.common.register' })} ${readableResourceName[0]}`}>
                    <ResourceNew
                      resource={resource}
                      title={`${intl.formatMessage({ id: 'admin.common.register' })} ${readableResourceName[0]}`}
                      routeBase={routeBase}
                      Form={props.Form}
                      {...props.resourceNewProps}
                    />
                  </ProtectedScreen>
                )}
              />
            )}
            {props.canEdit && (
              <Route
                exact
                path={routeBase.edit}
                render={({ match: { params: { id } } }) => (
                  <ProtectedScreen title={`${intl.formatMessage({ id: 'admin.common.edit' })} ${readableResourceName[0]}`}>
                    <ResourceUpdate
                      resource={resource}
                      title={`${intl.formatMessage({ id: 'admin.common.edit' })} ${readableResourceName[0]}`}
                      routeBase={routeBase}
                      Form={props.Form}
                      id={parseInt(id, 10)}
                      {...props.resourceUpdateProps}
                    />
                  </ProtectedScreen>
                )}
              />
            )}
            {props.canShowDetails && (
              <Route
                exact
                path={routeBase.show}
                render={({ match: { params: { id } } }) => (
                  <ProtectedScreen title={`${intl.formatMessage({ id: 'admin.common.details' })} - ${readableResourceName[0]}`}>
                    <ResourceDetails
                      resource={resource}
                      title={`${intl.formatMessage({ id: 'admin.common.details' })} - ${readableResourceName[0]}`}
                      routeBase={routeBase}
                      Details={props.Details || (p => <props.Form onSubmit={v => {}} isSubmitting={false} initialValues={p.detailedRecord} title={p.title} readOnly />)}
                      id={parseInt(id, 10)}
                      {...props.resourceDetailsProps}
                    />
                  </ProtectedScreen>
                )}
              />
            )}
          </Switch>
        )
      }}
    </Auth>
  )
}

ResourceScreen.propTypes = {
  /** Recurso */
  resource: PropTypes.string.isRequired,

  /** Nome legível do recurso */
  readableResourceName: PropTypes.array.isRequired,

  /** Rota base */
  routeBase: PropTypes.object.isRequired,

  /** Formulário que será usado para cadastro e edição do recurso */
  Form: PropTypes.func,

  /** Componente que será utilizado para exibir o registro detalhado */
  Details: PropTypes.func,

  /** Lista de colunas para a listagem de recursos */
  columns: PropTypes.array,

  /** Props para o DataTable */
  dataTableProps: PropTypes.object,

  /** Props para Resource List */
  resourceListProps: PropTypes.object,

  /** Props para Resource Create */
  resourceNewProps: PropTypes.object,

  /** Props para Resource Update */
  resourceUpdateProps: PropTypes.object,

  /** Props para o Resource Details */
  resourceDetailsProps: PropTypes.object,

  /** Permite o cadastro de novos registros */
  canAdd: PropTypes.bool,

  /** Permite a edição de registros */
  canEdit: PropTypes.bool,

  /** Permite a exclusão de registros */
  canRemove: PropTypes.bool,

  /** Permite a visualização do registro detalhado */
  canShowDetails: PropTypes.bool,

  /** Objeto de permissões do recurso */
  permissions: PropTypes.object.isRequired,

  /** Adicionar ações adicionais */
  extraActionButton: PropTypes.func
}

ResourceScreen.defaultProps = {
  canAdd: true,
  canEdit: true,
  canRemove: true,
  canShowDetails: true
}

export default injectIntl(ResourceScreen)
