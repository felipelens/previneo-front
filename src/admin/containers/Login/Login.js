import React from 'react'
import PageContent from 'components/PageContent'
import Container from 'components/Container'
import FormLogin from 'admin/forms/FormLogin'
import Auth from 'admin/containers/Auth'

export default function Login () {
  return (
    <PageContent>
      <Container size='xsmall'>
        <Auth>
          {props => (
            <FormLogin
              onSubmit={data => props.login(data.email, data.password, 'users')}
              isSubmitting={props.isSubmitting}
            />
          )}
        </Auth>
      </Container>
    </PageContent>
  )
}
