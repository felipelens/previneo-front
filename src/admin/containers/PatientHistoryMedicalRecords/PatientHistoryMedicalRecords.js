import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { isEmpty } from 'ramda'
import Section from 'admin/components/Section'
import Text from 'components/Text'
import MedicalRecords from 'admin/components/MedicalRecords'

function PatientHistoryMedicalRecords ({ patientId, intl, ...props }) {
  return (
    <Section title={intl.formatMessage({ id: 'admin.medicalRecords.title' })}>
      {!isEmpty(props.resource.detailedRecord) ? (
        <MedicalRecords includeDetails patientId={patientId} data={props.resource.detailedRecord.roteiros} {...props} />
      ) : (
        <Text>
          {intl.formatMessage({ id: 'admin.medicalRecords.noInformation' })}
        </Text>
      )}
    </Section>
  )
}

PatientHistoryMedicalRecords.propTypes = {
  /** ID do paciente */
  patientId: PropTypes.number.isRequired
}

export default injectIntl(PatientHistoryMedicalRecords)
