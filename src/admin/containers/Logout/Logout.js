import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import withAuth from 'store/utils/withAuth'
import routes from 'routes'

class Logout extends Component {
  componentDidMount () {
    this.props.auth.logout(this.props.type)
  }

  render () {
    return <Redirect to={this.props.redirectTo} />
  }

  static defaultProps = {
    type: 'users',
    redirectTo: routes.admin.login
  }
}

export default withAuth(Logout)
