import React from 'react'
import Section from 'admin/components/Section'
import FormDeleted from 'admin/forms/FormDeleted'
import Resource from 'containers/Resource'

export default function DeletedScreen (props) {
  return (
    <Resource resource={props.resource}>
      {({ remove, fetchAll }) => (
        <Section
          title={props.title}
        >
          <FormDeleted
            onSubmit={data => {
              remove(data).then(() => fetchAll(props.params))
            }}
            isSubmitting={props.isSubmitting}
            {...props}
          />
        </Section>
      )}
    </Resource>
  )
}
