import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl'
import Text from 'components/Text'
import Box from 'admin/components/Box'
import { COLORS } from 'config/constants'
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid
} from 'recharts'

const TooltipContent = (props) => {
  if (!props.payload[0]) return null

  const genders = [
    'admin.common.men',
    'admin.common.women'
  ]
  const total = props.payload[0].payload[`qntd`]
  const name = props.payload[0].payload[`nome`]
  const currentGenderIndex = props.type === 'feminino' ? 1 : 0

  return (
    <Box style={{ padding: 10 }}>
      <Text
        style={{ opacity: 0.6 }}
      >
        {name}
      </Text>
      <Text
        color={COLORS[currentGenderIndex]}
      >
        {<FormattedMessage id={genders[currentGenderIndex]} />}: {<FormattedNumber value={total} />}
      </Text>
    </Box>
  )
}

class ChartAnswersByAge extends Component {
  state = {
    isTooltipVisible: false,
    type: ''
  }

  handleMouseEnter (type) {
    this.setState({
      isTooltipVisible: true,
      type
    })
  }

  handleMouseLeave () {
    this.setState({
      isTooltipVisible: false,
      type: ''
    })
  }

  render () {
    const data = this.props.data

    const currentGender = this.props.female ? 'feminino' : 'masculino'
    const genderTitle = this.props.female
      ? this.props.intl.formatMessage({ id: 'admin.common.genderFemale' })
      : this.props.intl.formatMessage({ id: 'admin.common.genderMale' })
    const currentColor = this.props.female ? 1 : 0

    return (
      <Box
        arrow
        color={this.props.female ? 'highRisk' : 'primary'}
      >
        <Text
          size='18px'
          sizeMobile='18px'
          color={this.props.female ? 'highRisk' : 'primary'}
          lineHeight={1.2}
          weight='bold'
          alignMobile='center'
          style={{ marginBottom: 20 }}
        >
          {`${this.props.intl.formatMessage({ id: 'admin.dashboard.charts.totalFilledByAge' })} - ${genderTitle}` }
        </Text>
        <ResponsiveContainer width='100%' height={250}>
          <BarChart data={data} stackOffset='sign'>
            <CartesianGrid
              strokeDasharray='4 2'
              stroke='#aab8c2'
            />
            <XAxis type='category' dataKey={`nome`} />
            <YAxis allowDecimals={false} interval={1} />
            {this.state.isTooltipVisible && (
              <Tooltip
                content={props => (
                  <TooltipContent
                    {...props}
                    type={currentGender}
                  />
                )}
              />
            )}
            {
              <Bar
                key={`${currentGender}-value`}
                dataKey={`qntd`}
                fill={COLORS[currentColor]}
                stackId={`stack${currentColor}`}
                isAnimationActive={false}
                onMouseEnter={() => this.handleMouseEnter(currentGender)}
                onMouseLeave={() => this.handleMouseLeave()}
              />
            }
          </BarChart>
        </ResponsiveContainer>
      </Box>
    )
  }

  static propTypes = {
    /** Dados */
    data: PropTypes.array.isRequired
  }
}

export default injectIntl(ChartAnswersByAge)
