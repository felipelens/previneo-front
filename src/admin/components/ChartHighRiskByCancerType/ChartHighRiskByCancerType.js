import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import Card from 'admin/components/Card'
import { COLORS } from 'config/constants'
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  LabelList
} from 'recharts'

function ChartHighRiskByCancerType ({ intl, data: rawData }) {
  const data = rawData.map(item => ({
    name: item.nome,
    value: item.qntd
  }))

  return (
    <Card title={intl.formatMessage({ id: 'admin.dashboard.charts.highRiskByCancerType' })}>
      <ResponsiveContainer width='100%' height={400}>
        <BarChart data={data}>
          <Bar
            dataKey='value'
            isAnimationActive={false}
          >
            <LabelList
              dataKey='value'
              position='center'
              fill='#000'
              formatter={intl.formatNumber}
            />
            {data.map((entry, index) => (
              <Cell key={entry.name} fill={COLORS[index]} />
            ))}
          </Bar>
          <XAxis dataKey='name' />
          <YAxis />
        </BarChart>
      </ResponsiveContainer>
    </Card>
  )
}

ChartHighRiskByCancerType.propTypes = {
  /** Dados */
  data: PropTypes.array.isRequired
}

export default injectIntl(ChartHighRiskByCancerType)
