import React from 'react'
import Input from 'components/Input'

export default function FilterInput ({ filter, onChange, ...props }) {
  return (
    <Input
      className='custom-filter-input'
      type='text'
      value={filter ? filter.value : ''}
      onChange={e => onChange(e.target.value)}
      {...props}
    />
  )
}
