import React from 'react'
import CustomField from 'components/CustomField'
import ScriptEditor from 'admin/components/ScriptEditor'

export default props => {
  props.input.value = props.input.value || []
  return <CustomField component={ScriptEditor} {...props} />
}
