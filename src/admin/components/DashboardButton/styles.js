import React from 'react'
import styled from 'styled-components'
import media from 'utils/mediaQuery'
import Base from 'components/Base'

export const ButtonContainer = styled(({ small, narrow, ...rest }) => <Base {...rest} />)`
  padding: 10px 15px;
  padding-left: 15px;
  font-size: 14px;
  display: inline-flex;
  width: ${props => props.block ? '100%' : 'auto'};
  
  ${media.lessThan('large')`
    display: table-cell;
    text-align: center;
    padding: 5px 7px;
    padding-left: 7px;
  `}
  
  color: ${props => props.theme.dashboard.colors.primary};
  background-color: ${props => props.theme.dashboard.colors.grayLight};
  border: 1px solid ${props => props.theme.dashboard.colors.primary};
  border-radius: 2px;
  text-decoration: none;
  font-family: ${props => props.theme.fonts.primary};
  cursor: pointer;


  &:hover {
    background-color: ${props => props.theme.dashboard.colors.gray};
    color: ${props => props.theme.dashboard.colors.primary};
  }

  &.active {
    background-color: ${props => props.theme.dashboard.colors.primary};
    font-weight: bold;
    color: #fff;
  }

`
