import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { ButtonContainer } from './styles'

export default function DashboardButton ({ title, narrow, ...props }) {
  return (
    <ButtonContainer
      narrow={narrow}
      component={NavLink}
      {...props}
    >
      <span narrow={narrow}>{title}</span>
    </ButtonContainer>
  )
}

DashboardButton.propTypes = {
  /** Título do botão */
  title: PropTypes.string.isRequired,

  /** Oculta o texto do Título */
  narrow: PropTypes.bool
}
