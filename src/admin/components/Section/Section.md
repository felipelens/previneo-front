Section é uma `<Box />` com um `<SectionTitle />`.

```
<Section title='Título da Section'>
    Conteúdo da Section
</Section>
```

Através da prop `side`, você consegue adicionar elementos para ficar ao lado do título:

```
const Button = require('components/Button').default

;<Section title='Usuários' side={(
  <Button color='success'>Cadastrar</Button>
)}>
    Conteúdo da Section
</Section>
```

