import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import DatePicker from 'admin/components/DatePicker'

export default function FilterDatePicker ({ value, onChange, ...props }) {
  return (
    <DatePicker
      selected={value ? moment(value) : value}
      onChange={date => onChange(date ? date.format('YYYY-MM-DD') : '')}
      {...props}
    />
  )
}

FilterDatePicker.propTypes = {
  /** Change handler */
  onChange: PropTypes.func.isRequired
}
