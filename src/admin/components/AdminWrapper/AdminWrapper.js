import styled from 'styled-components'

export default styled.div`
  min-height: 100vh;
  background-color: ${props => props.theme.colors.background};
`
