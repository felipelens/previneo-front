import React from 'react'
import { injectIntl } from 'react-intl'
import PropTypes from 'prop-types'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import CardChartInfoBar from 'admin/components/CardChartInfoBar'
import ChartAnswersByAge from 'admin/components/ChartAnswersByAge'
import Grid from 'admin/components/Grid'

const ChartResource = props =>
  <Resource
    resource='Charts'
    autoFetch
    spinner
    {...props}
  />

function DashboardAnamnesisByGender ({ company, name, intl }) {
  return (
    <VerticalSpacer space={20}>
      <ChartResource
        namespace='planned'
        params={{ empresa_id: company, request: '/previsto' }}
      >
        {({ records }) => {
          return (<CardChartInfoBar data={records} name={name} />)
        }}
      </ChartResource>
      <Grid>
        <ChartResource
          namespace='answersMale'
          params={{ empresa_id: company, request: '/faixa-etaria-preenchidos', sexo: 'M' }}
        >
          {({ records }) => (
            <ChartAnswersByAge data={records.resultado} />
          )}
        </ChartResource>
        <ChartResource
          namespace='answersFemale'
          params={{ empresa_id: company, request: '/faixa-etaria-preenchidos', sexo: 'F' }}
        >
          {({ records }) => (
            <ChartAnswersByAge data={records.resultado} right female />
          )}
        </ChartResource>
      </Grid>
    </VerticalSpacer>
  )
}

DashboardAnamnesisByGender.propTypes = {
  /** ID da empresa */
  company: PropTypes.number
}

export default injectIntl(DashboardAnamnesisByGender)
