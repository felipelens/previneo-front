import React from 'react'
import { injectIntl } from 'react-intl'
import { DateColumn } from 'admin/columns'
import ResourceDataTable from 'admin/containers/ResourceDataTable'
import Button from 'components/Button'
import WithModal from 'admin/components/WithModal'
import ConfirmModal from 'admin/components/ConfirmModal'
import Resource from 'containers/Resource'
import Auth from 'admin/containers/Auth'
import PERMISSIONS from 'config/permissions'

function QueueTable ({ intl, ...props }) {
  return (
    <ResourceDataTable
      autoRefresh
      columns={[
        {
          accessor: 'titulo',
          Header: intl.formatMessage({ id: 'admin.common.title' })
        },
        {
          accessor: 'status',
          Header: intl.formatMessage({ id: 'admin.common.status' }),
          width: 150
        },
        DateColumn({ accessor: 'created_at', dateFormat: 'DD/MM/YYYY H:m:s', message: 'admin.common.createdAt' }),
        DateColumn({
          accessor: 'updated_at',
          message: 'admin.common.lastUpdate',
          dateFormat: 'DD/MM/YYYY H:m:s'
        }),
        {
          accessor: 'url',
          Header: intl.formatMessage({ id: 'admin.common.download' }),
          width: 100,
          Cell: ({ row: { url } }) => (
            <Button
              component='a'
              href={url}
              size='small'
              download
              target='_blank'
              disabled={!url}
              block
            >
              {url
                ? intl.formatMessage({ id: 'admin.common.download' })
                : intl.formatMessage({ id: 'admin.common.unavailable' })
              }
            </Button>
          )
        },
        {
          accessor: 'id',
          Header: intl.formatMessage({ id: 'admin.common.remove' }),
          width: 100,
          Cell: ({ row: { id, url } }) => (
            <Resource resource='Reports'>
              {resourceProps => (
                <WithModal
                  modal={({ closeModal }) => (
                    <ConfirmModal
                      title={intl.formatMessage({ id: 'admin.common.warningMessage' })}
                      text={intl.formatMessage({ id: 'admin.common.AreYouSureABoutIt' })}
                      onConfirm={() => {
                        resourceProps.remove(id)
                        closeModal()
                      }}
                      onCancel={closeModal}
                    />
                  )}
                >
                  {({ toggleModal }) => (
                    <Auth>
                      {authProps => (
                        <Button
                          onClick={toggleModal}
                          size='small'
                          disabled={!url || resourceProps.removingRecords.includes(id) || !authProps.permissions.includes(PERMISSIONS.reports.delete)}
                          block
                          color='danger'
                        >
                          {url && authProps.permissions.includes(PERMISSIONS.reports.delete)
                            ? intl.formatMessage({ id: 'admin.common.remove' })
                            : intl.formatMessage({ id: 'admin.common.unavailable' })
                          }
                        </Button>
                      )}
                    </Auth>
                  )}
                </WithModal>
              )}
            </Resource>
          )
        }
      ]}
      {...props}
    />
  )
}

export default injectIntl(QueueTable)
