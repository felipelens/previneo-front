import React from 'react'
import { Redirect } from 'react-router-dom'
import Auth from 'admin/containers/Auth'
import Screen from 'components/Screen'
import routes from 'routes'

export default function GuestScreen (props) {
  return (
    <Auth>
      {({ isAuthenticated }) => !isAuthenticated
        ? <Screen {...props} />
        : <Redirect to={routes.admin.index} />
      }
    </Auth>
  )
}
