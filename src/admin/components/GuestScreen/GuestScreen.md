Uma Screen extendida que só é acessível para usuários não logados. Útil para telas de Login por exemplo.

Caso o usuário já esteja logada, ele é redirecionado para o Dashboard.
