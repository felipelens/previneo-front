Componente de DataTable baseado no [React Table](https://react-table.js.org/#/story/readme).

```
<DataTable
  data={[
    { id: 1, name: 'Registro de exemplo 1' },
    { id: 2, name: 'Registro de exemplo 2' },
    { id: 3, name: 'Registro de exemplo 3' }  
  ]}
  columns={[
    {
      accessor: 'id',
      Header: 'ID'
    },
    {
      accessor: 'name',
      Header: 'Nome'
    }
  ]}
/>
````
