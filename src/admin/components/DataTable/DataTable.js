import React from 'react'
import ReactTable from 'react-table'
import styled, { injectGlobal } from 'styled-components'
import './DataTable.css'
import Input from 'components/Input'
import { injectIntl } from 'react-intl'

injectGlobal`
  .ReactTable .rt-thead.-filters .rt-th {
    overflow: visible;
  }
  .ReactTable .rt-noData {
    z-index: 0 !important;
  }
`

const Container = styled.div`
  background-color: #fff;
  font-family: ${props => props.theme.fonts.primary};
`

function DataTable ({ intl, minRows = 6, ...props }) {
  return (
    <Container>
      <ReactTable
        showPageJump={false}
        minRows={minRows}
        filterable
        previousText={intl.formatMessage({ id: 'admin.common.previous' })}
        nextText={intl.formatMessage({ id: 'admin.common.next' })}
        loadingText={intl.formatMessage({ id: 'admin.common.loading' })}
        noDataText={intl.formatMessage({ id: 'admin.common.noResultsFound' })}
        pageText={intl.formatMessage({ id: 'admin.common.page' })}
        ofText={intl.formatMessage({ id: 'admin.common.Of' })}
        rowsText={intl.formatMessage({ id: 'admin.common.results' })}
        className={'-striped -highlight'}
        FilterComponent={({ onChange, filter }) => (
          <Input
            className='custom-filter-input'
            value={filter ? filter.value : ''}
            onChange={e => onChange(e.target.value)}
          />
        )}
        {...props}
      />
    </Container>
  )
}

export default injectIntl(DataTable)
