import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import { Grid, Column } from './styles'
import { FormattedNumber, injectIntl } from 'react-intl'

function CardChartExams ({ data, intl }) {
  return (
    <Box width='100%' arrow>
      <Text
        size='18px'
        weight='bold'
        lineHeight={1.2}
        style={{ marginBottom: 20 }}
      >
        {intl.formatMessage({id: 'admin.dashboard.charts.examsPending'})}
      </Text>
      <Grid>
        {data.map((item, index) => (
          <Column key={index}>
            <Text
              size='20px'
              lineHeight={2}
              style={{ marginBottom: 30 }}
              align='center'
            >
              {item.name}
            </Text>
            <Text
              size='25px'
              weight='bold'
              lineHeight={1.2}
            >
              <FormattedNumber value={item.qntd} />
            </Text>
            <Text
              size='12px'
              style={{ opacity: 0.6 }}
              lineHeight={1.2}
            >
              {item.porcentagem}%
            </Text>
          </Column>
        ))}
      </Grid>
    </Box>
  )
}

export default injectIntl(CardChartExams)
