import React from 'react'
import PropTypes from 'prop-types'
import Box from 'admin/components/Box'
import VerticalSpacer from 'components/VerticalSpacer'
import Text from 'components/Text'

export default function Card ({ title, children, space, ...props }) {
  return (
    <Box component='section' {...props}>
      <VerticalSpacer space={space}>
        <Text component='h3' size='24px' sizeMobile='20px'>
          <strong>{title}</strong>
        </Text>
        <div>
          {children}
        </div>
      </VerticalSpacer>
    </Box>
  )
}

Card.propTypes = {
  /** Título */
  title: PropTypes.string.isRequired,

  /** Espaçamento entre o título e o conteúdo */
  space: PropTypes.number,

  /** Conteúdo do card */
  children: PropTypes.any.isRequired
}

Card.defaultProps = {
  space: 15
}
