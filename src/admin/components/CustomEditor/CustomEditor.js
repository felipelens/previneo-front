import React, { Component } from 'react'
import { API_URL } from 'config/constants'
import CustomField from 'components/CustomField'
import Storage from 'modules/Storage'
import { getTokenKey } from 'store/api/Auth'
import { htmlAllowedTags } from 'admin/components/CustomEditor/htmlAllowedTags'

window.$ = require('jquery')

const FroalaEditor = require('react-froala-wysiwyg').default

require('froala-editor/js/froala_editor.pkgd.min.js')
// 'froala_style.min.css' removido para evitar que o editor adicione regras indesejadas ao html dos emails
// require('froala-editor/css/froala_style.min.css')
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')

class CustomEditor extends Component {
  state = {
    mounted: false
  }

  componentDidMount () {
    this.config = {
      key: '1F4D3C7C5eF5C4B3D4E2C2C4D6E4D3xutbI-7c1dncF5mh1A-7I1pa==',
      useClasses: true,
      htmlRemoveTags: ['script', 'base'],
      htmlAllowedTags: htmlAllowedTags,
      imageUploadURL: `${API_URL}/api/media?saveMediaInPublicFolder=1`,
      requestHeaders: {
        Authorization: `Bearer ${Storage.get(getTokenKey('users'))}`
      },
      events: {
        'froalaEditor.image.uploaded': (e, editor, response) => {
          const { data } = JSON.parse(response)

          editor.image.insert(data.url, false, null, editor.image.get(), response)

          return false
        }
      }
    }

    this.setState({ mounted: true })
  }

  render () {
    if (!this.state.mounted) return null

    return (
      <FroalaEditor
        tag='textarea'
        model={this.props.value}
        config={this.config}
        onModelChange={this.props.onChange}
      />
    )
  }
}

export default props =>
  <CustomField component={CustomEditor} {...props} />
