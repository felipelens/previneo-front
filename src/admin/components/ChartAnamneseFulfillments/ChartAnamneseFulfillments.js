import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import Card from 'admin/components/Card'
import { COLORS } from 'config/constants'
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip
} from 'recharts'

function ChartAnamneseFulfillments ({ data: rawData, intl }) {
  const data = rawData.map(item => ({
    name: `${item.abbreviation}/${item.ano}`,
    h: item.masculino,
    m: item.feminino
  }))

  const genders = [
    intl.formatMessage({ id: 'admin.common.men' }),
    intl.formatMessage({ id: 'admin.common.women' })
  ]

  return (
    <Card title={intl.formatMessage({ id: 'admin.dashboard.charts.anamnesisFulfillments' })}>
      <ResponsiveContainer width='100%' height={400}>
        <LineChart data={data}>
          <XAxis dataKey='name' />
          <YAxis />
          <CartesianGrid horizontal={false} />
          {['h', 'm'].map((name, index) => (
            <Line
              key={name}
              name={genders[index]}
              type='monotone'
              dataKey={name}
              stroke={COLORS[index]}
              strokeWidth={2}
              legendType='square'
              dot={{ r: 5 }}
              isAnimationActive={false}
            />
          ))}
          <Tooltip />
        </LineChart>
      </ResponsiveContainer>
    </Card>
  )
}

ChartAnamneseFulfillments.propTypes = {
  /** Dados */
  data: PropTypes.array.isRequired
}

export default injectIntl(ChartAnamneseFulfillments)
