import React from 'react'
import { TwitterPicker } from 'react-color'
import theme from 'theme'
import CustomField from 'components/CustomField'

const ColorPicker = ({ onChange, value }) =>
  <TwitterPicker
    width='240px'
    triangle='hide'
    color={value}
    colors={[
      theme.colors.personal,
      ...Object.values(theme.colors.specialities)
    ]}
    onChangeComplete={color => onChange(color.hex)}
  />

export default props =>
  <CustomField component={ColorPicker} {...props} />
