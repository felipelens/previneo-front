import React from 'react'
import CustomField from 'components/CustomField'
import PermissionsManager from 'admin/components/PermissionsManager'

export default props => {
  props.input.value = props.input.value || []
  return <CustomField component={PermissionsManager} {...props} />
}
