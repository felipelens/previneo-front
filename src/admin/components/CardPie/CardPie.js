import React from 'react'
import PropTypes from 'prop-types'
import { FormattedNumber, FormattedMessage } from 'react-intl'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import theme from 'theme'
import { Container } from './styles'
import {
  PieChart,
  Pie,
  Label,
  Cell
} from 'recharts'

const getPerc = (value, total) => {
  if (total === 0) return 0
  return (value / total) * 100
}

const round = value => value === parseInt(value, 10)
  ? value
  : value.toFixed(2)

export default function CardPie ({ title, value, total, color = 'blue' }) {
  const valuePerc = getPerc(value, total)
  const valuePercRounded = round(valuePerc)

  const data = [
    { name: 'Casos', value: valuePerc, color },
    { name: 'Total', value: getPerc(total - value, total), color: theme.colors.border }
  ]

  return (
    <Box component='section'>
      <Container>
        <div>
          <Text size='60px' sizeMobile='30px' lineHeight={1.2}>
            <strong>
              <FormattedNumber value={value} />
            </strong>
          </Text>
          <Text component='h1'>
            {title}
            {' '}
            <FormattedMessage id='admin.dashboard.charts.ofTotal'>
              {text => (
                <strong>({valuePercRounded}% {text})</strong>
              )}
            </FormattedMessage>
          </Text>
        </div>
        <div>
          <PieChart width={160} height={160}>
            <Pie
              data={data}
              innerRadius={60}
              outerRadius={80}
              fill='#8884d8'
              paddingAngle={5}
              dataKey='value'
              isAnimationActive={false}
            >
              {data.map((entry, index) => (
                <Cell key={index} fill={entry.color} />
              ))}
              <Label
                value={`${valuePercRounded}%`}
                position='center'
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  fill: color
                }}
              />
            </Pie>
          </PieChart>
        </div>
      </Container>
    </Box>
  )
}

CardPie.propTypes = {
  /** Título */
  title: PropTypes.string.isRequired,

  /** Valor do dado específico */
  value: PropTypes.number.isRequired,

  /** Total */
  total: PropTypes.number.isRequired,

  /** Cor do progresso do gráfico e da label da % */
  color: PropTypes.string
}
