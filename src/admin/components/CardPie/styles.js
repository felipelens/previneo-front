import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Container = styled.div`
  ${mediaQuery.greaterThan('small')`
    display: flex;
    align-items: center;
    justify-content: space-between;
    
    > * + * {
      margin-left: 15px;
    }
  `}

  ${mediaQuery.lessThan('small')`
    > * + * {
      display: flex;
      width: 100%;
      justify-content: center;
      margin-top: 15px;
    }
  `}
`
