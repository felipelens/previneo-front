import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import Auth from 'admin/containers/Auth'
import Screen from 'components/Screen'
import routes from 'routes'

export default function ProtectedScreen ({ permission, title, ...props }) {
  return (
    <Auth>
      {({ isAuthenticated, permissions }) => {
        if (!isAuthenticated) {
          const url = `${routes.admin.login}?goto=${props.location.pathname}`
          return <Redirect to={url} />
        }

        if (permission && !permissions.includes(permission)) {
          return <Redirect to={routes.admin.index} />
        }

        return <Screen title={`${title} - PreviNEO`} {...props} />
      }}
    </Auth>
  )
}

ProtectedScreen.propTypes = {
  /** Permissão exigida para acessar a screen */
  permission: PropTypes.string
}
