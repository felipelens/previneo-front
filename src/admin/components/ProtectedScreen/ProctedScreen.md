Um componente que serve para criar Screens que só são visíveis para usuários logados.

Caso um usuário não logado tente acessá-la, ela o redirecionará para a página de Login.
