import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import { Detail, Column, Wrap } from './styles'
import { FormattedNumber, injectIntl } from 'react-intl'

function CardGenderFilled ({ data, female, intl, ...props }) {
  const maleText = intl.formatMessage({ id: 'admin.common.gender.male' })
  const femaleText = intl.formatMessage({ id: 'admin.common.gender.female' })
  return (
    <Box
      arrow
      style={
        props.right
          ? { padding: '20px 15px 0 40px', borderWidth: '1px 1px 1px 0' }
          : { padding: '20px 40px 0 40px', borderWidth: '1px 0 1px 1px' }
      }
      color={female ? 'highRisk' : 'primary'}
      right={!female}
    >
      <Text
        size='18px'
        sizeMobile='18px'
        color={female ? 'highRisk' : 'primary'}
        lineHeight={1.2}
        weight='bold'
        align={female ? 'left' : 'right'}
        alignMobile='center'
        style={{ marginBottom: 20 }}
      >
        {`${intl.formatMessage({ id: 'admin.dashboard.charts.totalDone' })} ${female ? femaleText : maleText}`}
      </Text>
      <Wrap right={!props.right}>
        <Column>
          <Text
            size='50px'
            sizeMobile='40px'
            weight='bold'
            color={female ? 'highRisk' : 'primary'}
            lineHeight={1}
          >
            <FormattedNumber value={female ? data.mulheres_total : data.homens_total} />
          </Text>
          <Text
            size='16px'
            style={{ opacity: 0.4 }}
            lineHeight={1}
          >
            {`(${female ? data.mulheres_porcentagem : data.homens_porcentagem}%)`}
          </Text>
        </Column>
      </Wrap>
      <Detail right={props.right} />
    </Box>
  )
}

export default injectIntl(CardGenderFilled)
