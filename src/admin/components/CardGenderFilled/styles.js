import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 200px;
  padding: 60px 20px 0 20px;
  
  ${mediaQuery.lessThan('medium')`
    padding-top: 10px;
    height: 90px;
  `}
`

export const Wrap = styled.div`
  display: flex;
   
  ${mediaQuery.lessThan('medium')`
    justify-content: center;
  `}
  
  ${mediaQuery.greaterThan('medium')`
    justify-content: ${props => props.right ? 'flex-end' : 'flex-start'};
  `}
`

export const Detail = styled.div`
  width: 250px;
  height: 280px;
  background: ${props => props.theme.colors.background};
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  border-radius: 50%;
  left: calc(100% - 30px);
  box-shadow: inset 7px 0 9px -7px #0000000c;
  ${props => props.right && `
    left: auto;
    right: calc(100% - 30px);
    box-shadow: inset -7px 0 9px -7px #0000000c;
  `}
  ${mediaQuery.lessThan('medium')`
    display: none;
  `}
`
