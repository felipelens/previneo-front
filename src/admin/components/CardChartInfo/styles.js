import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Grid = styled.div`
  display: flex;
  ${mediaQuery.lessThan('medium')`
    flex-wrap: wrap;
  `}
`

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  ${props => props.center && `
    align-items: center;
  `}
  ${props => props.total && `
    padding: 0 40px 0 70px;
  `}
  ${mediaQuery.lessThan('medium')`
    width: 50%;
    padding: 0;
  `}
`

export const Info = styled.div`
  display: flex;
  padding: 0 55px 0 35px;
  > *:first-child {
    margin-right: 10px;
  }
  ${props => props.border && `
    border-width: 0 1px
    border-color: #e1e1e1;
    border-style: solid;
  `}
  ${mediaQuery.lessThan('medium')`
    width: 50%;
    padding: 0;
    border: none;
    justify-content: center;
    margin: 20px 0;
    > .icon {
      display: none;
    }
  `}
`
