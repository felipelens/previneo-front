import React from 'react'
import { FormattedMessage, FormattedNumber } from 'react-intl'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import Icon from 'components/Icon'
import withAuth from 'store/utils/withAuth'
import { Grid, Column, Info } from './styles'

function CardChartInfo ({ data, name, ...props }) {
  return (
    <Box width='100%' arrow>
      <Grid>
        <Column>
          <Text
            size='18px'
            weight='bold'
            lineHeight={1.2}
          >
            <FormattedMessage id='admin.dashboard.chart.foreseen' />
          </Text>
          <Text
            size='15px'
            sizeMobile='15px'
            lineHeight={1.2}
          >
            {name}
          </Text>
        </Column>
        <Column center total>
          <Text
            size='14px'
            sizeMobile='14px'
            lineHeight={1.2}
          >
            Total
          </Text>
          <Text
            size='18px'
            weight='bold'
            lineHeight={1.2}
          >
            <FormattedNumber value={data.total} />
          </Text>
        </Column>
        <Info border>
          <Icon
            icon='checkmark'
            size='20px'
            sizeMobile='14px'
            color='success'
          />
          <Column center>
            <Text
              size='14px'
              sizeMobile='14px'
              lineHeight={1.2}
            >
              <FormattedMessage id='admin.dashboard.chart.fulfilled' />
            </Text>
            <Text
              size='18px'
              weight='bold'
              lineHeight={1.2}
            >
              <FormattedNumber value={data.realizado} />
            </Text>
            <Text
              size='12px'
              sizeMobile='12px'
              style={{ opacity: 0.6 }}
              lineHeight={1.2}
            >
              {data.porcentagem_realizado}%
            </Text>
          </Column>
        </Info>
        <Info>
          <Icon
            icon='close'
            size='20px'
            sizeMobile='14px'
            color='danger'
          />
          <Column center>
            <Text
              size='14px'
              sizeMobile='14px'
              lineHeight={1.2}
            >
              <FormattedMessage id='admin.dashboard.chart.not.fulfilled' />
            </Text>
            <Text
              size='18px'
              weight='bold'
              lineHeight={1.2}
            >
              <FormattedNumber value={data.nao_realizado} />
            </Text>
            <Text
              size='12px'
              sizeMobile='12px'
              style={{ opacity: 0.6 }}
              lineHeight={1.2}
            >
              {data.porcentagem_nao_realizado}%
            </Text>
          </Column>
        </Info>
      </Grid>
    </Box>
  )
}

export default withAuth(CardChartInfo)
