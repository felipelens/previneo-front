import React from 'react'
import { injectIntl } from 'react-intl'
import PropTypes from 'prop-types'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import CardChartInfoBar from 'admin/components/CardChartInfoBar'
import CardGenderFilled from 'admin/components/CardGenderFilled'
import CardChartGender from 'admin/components/CardChartGender'
import { Grid } from './styles'

const ChartResource = props =>
  <Resource
    resource='Charts'
    autoFetch
    spinner
    {...props}
  />

function DashboardAnamnesis ({ company, name, intl }) {
  let recordsTotal = {}
  return (
    <VerticalSpacer space={20}>
      <ChartResource
        namespace='planned'
        params={{ empresa_id: company, request: '/previsto' }}
      >
        {({ records }) => {
          recordsTotal = records
          return (<CardChartInfoBar data={records} name={name} />)
        }}
      </ChartResource>
      <ChartResource
        namespace='gender'
        params={{ empresa_id: company, request: '/realizado' }}
      >
        {({ records }) => (
          <Grid>
            <CardGenderFilled data={records} />
            <CardChartGender data={{ ...records, ...recordsTotal }} total />
            <CardGenderFilled data={records} right female />
          </Grid>
        )}
      </ChartResource>
    </VerticalSpacer>
  )
}

DashboardAnamnesis.propTypes = {
  /** ID da empresa */
  company: PropTypes.number
}

export default injectIntl(DashboardAnamnesis)
