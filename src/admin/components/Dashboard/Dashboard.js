import React from 'react'
import { Redirect, Switch, Route } from 'react-router-dom'
import ReactTooltip from 'react-tooltip'
import { injectIntl } from 'react-intl'
import Auth from 'admin/containers/Auth'
import Toggle from 'components/Toggle'
import Notifications from 'containers/Notifications'
import VerticalSpacer from 'components/VerticalSpacer'
import DashboardHeader from 'admin/components/DashboardHeader'
import SidebarButton from 'admin/components/SidebarButton'
import SidebarButtonCollapse from 'admin/components/SidebarButtonCollapse'
import { Container, Content, Sidebar, HorizontalCollapseArrow } from './styles'
import * as Screens from 'admin/screens'
import { NotFoundScreen } from 'screens'
import routes from 'routes'
import PERMISSIONS from 'config/permissions'
import Icon from '@fortawesome/react-fontawesome'
import { startsWith } from 'ramda'

function Dashboard ({ intl, location }) {
  return (
    <Auth>
      {props => {
        if (!props.isAuthenticated) {
          const url = `${routes.admin.login}?goto=${location.pathname}`
          return <Redirect to={url} />
        }
        return (
          <Toggle resetOnRouteChange>
            {({ toggle: toggleMenu, value: isMenuOpened }) => (
              <Toggle defaultValue >
                {({ toggle: toggleHorizontal, value: isNarrowMenu }) => (
                  <div>
                    <ReactTooltip disable={!isNarrowMenu} id='dashboard-tooltip' />
                    <HorizontalCollapseArrow narrow={isNarrowMenu} onClick={toggleHorizontal}>
                      <Icon
                        icon={
                          isNarrowMenu
                            ? require('@fortawesome/fontawesome-free-solid/faAngleRight')
                            : require('@fortawesome/fontawesome-free-solid/faAngleLeft')
                        }
                      />
                    </HorizontalCollapseArrow>
                    <DashboardHeader intl={intl} onMenuButtonClick={toggleMenu} />
                    <Container>
                      <Sidebar narrow={isNarrowMenu} isMenuOpened={isMenuOpened}>
                        <div>
                          <SidebarButton
                            narrow={isNarrowMenu}
                            exact
                            isActive={checkIndexDash}
                            to='/admin'
                            title='Dashboard'
                            icon={require('@fortawesome/fontawesome-free-solid/faChartBar'
                            )}
                          />
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.companies.index}
                            title={intl.formatMessage({ id: 'admin.company.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faBuilding')}
                            permission={PERMISSIONS.companies.viewAll}
                          />
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.healthInsurances.index}
                            title={intl.formatMessage({ id: 'admin.healthInsurance.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faBriefcaseMedical')}
                            permission={PERMISSIONS.healthInsurances.viewAll}
                          />
                          <SidebarButtonCollapse
                            narrow={isNarrowMenu}
                            route={routes.admin.users.index}
                            title={intl.formatMessage({ id: 'admin.users.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faUser')}
                            permissions={[
                              PERMISSIONS.users.viewAll
                            ]}
                          >
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.users.users.index}
                              title={intl.formatMessage({ id: 'admin.users.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUsers')}
                              permission={PERMISSIONS.users.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.users.deleting}
                              title={intl.formatMessage({ id: 'admin.users.deletedUsers' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUserTimes')}
                              permission={PERMISSIONS.users.viewAll}
                              small
                            />
                          </SidebarButtonCollapse>
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.roles.index}
                            title={intl.formatMessage({ id: 'admin.roles.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faUnlockAlt')}
                            permission={PERMISSIONS.roles.viewAll}
                          />
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.permissions.index}
                            title={intl.formatMessage({ id: 'admin.permissions.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faKey')}
                            permission={PERMISSIONS.permissions.viewAll}
                          />
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.languages.index}
                            title={intl.formatMessage({ id: 'admin.languages.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faLanguage')}
                            permission={PERMISSIONS.languages.viewAll}
                          />
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.reports}
                            title={intl.formatMessage({ id: 'admin.common.reports' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faFileAlt')}
                            permission={PERMISSIONS.reports.viewAll}
                          />
                          <SidebarButtonCollapse
                            narrow={isNarrowMenu}
                            route={routes.admin.patients.index}
                            title={intl.formatMessage({ id: 'admin.patients.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faMale')}
                            permissions={[
                              PERMISSIONS.patients.viewAll
                            ]}
                          >
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.patients.patients.index}
                              title={intl.formatMessage({ id: 'admin.patients.listOfPatients' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUsers')}
                              permission={PERMISSIONS.patients.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.patients.importing}
                              title={intl.formatMessage({ id: 'admin.patients.importPatients' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUserPlus')}
                              permission={PERMISSIONS.patients.create}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.patients.deleting}
                              title={intl.formatMessage({ id: 'admin.patients.deletedPatients' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUserTimes')}
                              permission={PERMISSIONS.patients.viewAll}
                              small
                            />
                          </SidebarButtonCollapse>
                          <SidebarButtonCollapse
                            narrow={isNarrowMenu}
                            route={routes.admin.patientsHistory.index}
                            title={intl.formatMessage({ id: 'admin.patientsHistory.titlePlural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faStethoscope')}
                            permissions={[
                              PERMISSIONS.patientsHistory.viewAll,
                              PERMISSIONS.patientsHistoryContactReason.viewAll
                            ]}
                          >
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.patientsHistory.patientsHistory.index}
                              title={intl.formatMessage({ id: 'admin.patientsHistory.contact' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faComments')}
                              permission={PERMISSIONS.patientsHistoryContact.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.patientsHistory.contactReason.index}
                              title={intl.formatMessage({ id: 'admin.patientsHistory.contactReasonPlural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faList')}
                              permission={PERMISSIONS.patientsHistoryContactReason.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.patientsHistory.statuses.index}
                              title={intl.formatMessage({ id: 'admin.common.contactStatuses' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faInfoCircle')}
                              permission={PERMISSIONS.statuses.viewAll}
                              small
                            />
                          </SidebarButtonCollapse>
                          <SidebarButtonCollapse
                            narrow={isNarrowMenu}
                            route={routes.admin.questionnaire.index}
                            title={intl.formatMessage({ id: 'admin.anamnesis.title.plural' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faClipboardList')}
                            permissions={[
                              PERMISSIONS.scripts.viewAll,
                              PERMISSIONS.categories.viewAll,
                              PERMISSIONS.typesCancer.viewAll,
                              PERMISSIONS.emails.viewAll,
                              PERMISSIONS.strategy.viewAll,
                              PERMISSIONS.exams.viewAll
                            ]}
                          >
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.scripts.index}
                              title={intl.formatMessage({ id: 'admin.script.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faRoad')}
                              permission={PERMISSIONS.scripts.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.scriptSend}
                              title={intl.formatMessage({ id: 'admin.scriptSend.title' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faPaperPlane'
                              )}
                              permission={PERMISSIONS.anamnesis.create}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.emails.index}
                              title={intl.formatMessage({ id: 'admin.common.emailContents' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faEnvelopeOpen')}
                              permission={PERMISSIONS.emails.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.layouts.index}
                              title={intl.formatMessage({ id: 'admin.common.emailLayouts' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faObjectGroup')}
                              permission={PERMISSIONS.layouts.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.genders.index}
                              title={intl.formatMessage({ id: 'admin.common.genders' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faVenusMars')}
                              permission={PERMISSIONS.genders.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.typesCancer.index}
                              title={intl.formatMessage({ id: 'admin.cancerTypes.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faPills')}
                              permission={PERMISSIONS.typesCancer.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.categories.index}
                              title={intl.formatMessage({ id: 'admin.categories.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faBook')}
                              permission={PERMISSIONS.categories.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.strategy.index}
                              title={intl.formatMessage({ id: 'admin.strategies.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faPrescriptionBottle')}
                              permission={PERMISSIONS.strategy.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.exams.index}
                              title={intl.formatMessage({ id: 'admin.exams.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faFlask')}
                              permission={PERMISSIONS.exams.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.questionnaire.justifications.index}
                              title={intl.formatMessage({ id: 'admin.justifications.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faClipboard')}
                              permission={PERMISSIONS.justifications.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.risks.index}
                              title={intl.formatMessage({ id: 'admin.common.risks' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faHeartbeat')}
                              permission={PERMISSIONS.risks.viewAll}
                              small
                            />
                          </SidebarButtonCollapse>
                          <SidebarButtonCollapse
                            narrow={isNarrowMenu}
                            route={routes.admin.site.index}
                            title={intl.formatMessage({ id: 'admin.site.title' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faSitemap')}
                            permissions={[
                              PERMISSIONS.testimonies.viewAll,
                              PERMISSIONS.specialities.viewAll,
                              PERMISSIONS.team.viewAll,
                              PERMISSIONS.customers.viewAll
                            ]}
                          >
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.site.testimonies.index}
                              title={intl.formatMessage({ id: 'admin.testimonies.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faCommentDots')}
                              permission={PERMISSIONS.testimonies.viewAll}
                              small
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.site.specialities.index}
                              title={intl.formatMessage({ id: 'admin.specialities.title.plural' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faBriefcaseMedical')}
                              small
                              permission={PERMISSIONS.specialities.viewAll}
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.site.team.index}
                              title={intl.formatMessage({ id: 'admin.common.staff' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUserMd')}
                              small
                              permission={PERMISSIONS.team.viewAll}
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.site.customers.index}
                              title={intl.formatMessage({ id: 'admin.common.customers' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUsers')}
                              small
                              permission={PERMISSIONS.customers.viewAll}
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.site.contactInfos.index}
                              title={intl.formatMessage({ id: 'admin.common.contactInfos' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faUsers')}
                              small
                              permission={PERMISSIONS.contactInfos.viewAll}
                            />
                          </SidebarButtonCollapse>
                          <SidebarButtonCollapse
                            narrow={isNarrowMenu}
                            route={routes.admin.blog.index}
                            title={intl.formatMessage({ id: 'admin.common.blog' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faPenSquare')}
                            permissions={[
                              PERMISSIONS.posts.viewAll,
                              PERMISSIONS.blogCategories.viewAll,
                              PERMISSIONS.tags.viewAll
                            ]}
                          >
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.blog.posts.index}
                              title={intl.formatMessage({ id: 'admin.common.posts' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faEdit')}
                              small
                              permission={PERMISSIONS.posts.viewAll}
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.blog.categories.index}
                              title={intl.formatMessage({ id: 'admin.common.categories' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faHashtag')}
                              small
                              permission={PERMISSIONS.blogCategories.viewAll}
                            />
                            <SidebarButton
                              narrow={isNarrowMenu}
                              to={routes.admin.blog.tags.index}
                              title={intl.formatMessage({ id: 'admin.common.tags' })}
                              icon={require('@fortawesome/fontawesome-free-solid/faTags')}
                              small
                              permission={PERMISSIONS.tags.viewAll}
                            />
                          </SidebarButtonCollapse>
                        </div>
                        <div>
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.myAccount}
                            title={intl.formatMessage({ id: 'admin.common.myAccount' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faCog')}
                          />
                          <SidebarButton
                            narrow={isNarrowMenu}
                            to={routes.admin.logout}
                            title={intl.formatMessage({ id: 'admin.common.signOut' })}
                            icon={require('@fortawesome/fontawesome-free-solid/faSignOutAlt')}
                          />
                        </div>
                      </Sidebar>
                      <Content narrow={isNarrowMenu}>
                        <VerticalSpacer>
                          <Notifications />
                          <Switch>
                            <Route exact path={routes.admin.index} component={Screens.DashboardScreen} />
                            <Route path={routes.admin.dashboard.index} component={Screens.DashboardScreen} />
                            <Route path={routes.admin.myAccount} component={Screens.MyAccountScreen} />
                            <Route path={routes.admin.users.users.index} component={Screens.UsersScreen} />
                            <Route path={routes.admin.users.deleting} component={Screens.UsersDeletingScreen} />
                            <Route path={routes.admin.companies.index} component={Screens.CompaniesScreen} />
                            <Route path={routes.admin.healthInsurances.index} component={Screens.HealthInsurancesScreen} />
                            <Route path={routes.admin.roles.index} component={Screens.RolesScreen} />
                            <Route path={routes.admin.permissions.index} component={Screens.PermissionsScreen} />
                            <Route path={routes.admin.languages.index} component={Screens.LanguagesScreen} />
                            <Route path={routes.admin.patients.patients.index} component={Screens.PatientsScreen} />
                            <Route path={routes.admin.patients.importing} component={Screens.PatientsImportingScreen} />
                            <Route path={routes.admin.patients.deleting} component={Screens.PatientsDeletingScreen} />
                            <Route path={routes.admin.patientsHistory.patientsHistory.index} component={Screens.PatientsHistoryScreen} />
                            <Route path={routes.admin.patientsHistory.contactReason.index} component={Screens.ContactReasonScreen} />
                            <Route path={routes.admin.patientsHistory.statuses.index} component={Screens.ContactStatusesScreen} />
                            <Route path={routes.admin.site.customers.index} component={Screens.CustomersScreen} />
                            <Route path={routes.admin.site.testimonies.index} component={Screens.TestimoniesScreen} />
                            <Route path={routes.admin.site.specialities.index} component={Screens.SpecialitiesScreen} />
                            <Route path={routes.admin.site.contactInfos.index} component={Screens.ContactInfosScreen} />
                            <Route path={routes.admin.site.team.index} component={Screens.TeamScreen} />
                            <Route path={routes.admin.blog.categories.index} component={Screens.BlogCategoriesScreen} />
                            <Route path={routes.admin.blog.tags.index} component={Screens.TagsScreen} />
                            <Route path={routes.admin.blog.posts.index} component={Screens.PostsScreen} />
                            <Route path={routes.admin.questionnaire.categories.index} component={Screens.CategoriesScreen} />
                            <Route path={routes.admin.questionnaire.scripts.index} component={Screens.ScriptsScreen} />
                            <Route path={routes.admin.questionnaire.typesCancer.index} component={Screens.TypesCancerScreen} />
                            <Route path={routes.admin.questionnaire.scriptSend} component={Screens.ScriptSendScreen} />
                            <Route path={routes.admin.questionnaire.emails.index} component={Screens.EmailsScreen} />
                            <Route path={routes.admin.questionnaire.layouts.index} component={Screens.LayoutsScreen} />
                            <Route path={routes.admin.questionnaire.justifications.index} component={Screens.JustificationsScreen} />
                            <Route path={routes.admin.risks.index} component={Screens.RisksScreen} />
                            <Route path={routes.admin.genders.index} component={Screens.GendersScreen} />
                            <Route path={routes.admin.strategy.index} component={Screens.StrategyScreen} />
                            <Route path={routes.admin.exams.index} component={Screens.ExamsScreen} />
                            <Route path={routes.admin.reports} component={Screens.ReportsScreen} />
                            <Route component={NotFoundScreen} />
                          </Switch>
                        </VerticalSpacer>
                      </Content>
                    </Container>
                  </div>
                )}
              </Toggle>
            )}
          </Toggle>
        )
      }}
    </Auth>
  )
}

const checkIndexDash = (match, location) => {
  const current = location && location.pathname

  return startsWith(routes.admin.dashboard.index, current) || current === routes.admin.index
}

export default injectIntl(Dashboard)
