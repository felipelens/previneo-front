import styled from 'styled-components'
import media from 'utils/mediaQuery'

export const Container = styled.div`
  background-color: ${props => props.theme.colors.background};
  min-height: calc(100vh - 70px);
`

export const Content = styled.div`
  padding: 40px;
  width: 100%;
  box-sizing: border-box;
  font-family: ${props => props.theme.fonts.primary};

  ${media.greaterThan('large')`
    margin-top: 70px;
    padding-left: ${props => props.narrow ? '110px' : '310px'};
  `}
`

export const Sidebar = styled.aside`
  border-right: 1px solid #e5e5e5;
  background-color: ${props => props.theme.dashboard.colors.primary};
  box-shadow: -3px 0 20px 0 rgba(0, 0, 0, 0.07);
  padding-top: 20px;
   
  ::-webkit-scrollbar {
    width: .4em;
  }
   
  ::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  }
   
  ::-webkit-scrollbar-thumb {
    background-color: darkgrey;
    outline: 1px solid slategrey;
  }

  ${media.greaterThan('large')`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: fixed;
    top: 70px;
    width: ${props => props.narrow ? '80px' : '280px'};
    height: calc(100vh - 90px);
    overflow: auto;
  `}

  ${media.lessThan('large')`
    display: ${props => props.isMenuOpened ? 'block' : 'none'};
  `}
`
export const HorizontalCollapseArrow = styled.a`
  margin-left: ${props => props.narrow ? '80px' : '280px'};
  margin-top: 2px;
  position: fixed;
  z-index: 998;
  background-color: #f4f4f4;
  height: 30px;
  line-height: 30px;
  color: #2C3E50;
  text-align: center;
  width: 20px;
  box-shadow: 0 3px 7px 0 rgba(0,0,0,0.2);
  border-top-right-radius: 2px;
  border-bottom-right-radius: 2px;
  cursor: pointer;
  font-size: 20px;
  
  color: ${props => props.theme.dashboard.colors.primary};
 
    :hover {
      color: #39acb5;
    }
  
    ${media.lessThan('large')`
      display: none;
    `}
`
