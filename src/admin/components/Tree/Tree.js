import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { isEmpty } from 'ramda'
import Node from './Node'
import { FormattedMessage } from 'react-intl'

export default class Tree extends Component {
  static propTypes = {
    /** Nós disponíveis */
    nodes: PropTypes.object.isRequired,

    /** ID do nó atual */
    parentId: PropTypes.number,

    /** Função para requisitar o carregamento de nós */
    onRequestNodes: PropTypes.func.isRequired,

    /** Função para requisitar a criação de um novo nó */
    onRequestNodeCreation: PropTypes.func.isRequired,

    /** Função para requisitar a edição de um nó */
    onRequestNodeEdition: PropTypes.func.isRequired,

    /** Função para requisitar a remoção de um nó */
    onRequestNodeDeletion: PropTypes.func.isRequired,

    /** Permite a criação de novos nós */
    canCreate: PropTypes.bool.isRequired,

    /** Permite a edição de nós existentes */
    canUpdate: PropTypes.bool.isRequired,

    /** Permite a exclusão de nós */
    canDelete: PropTypes.bool.isRequired
  }

  static defaultProps = {
    parentId: 0
  }

  parentNode = this.props.nodes[this.props.parentId]

  componentDidMount () {
    if (!this.parentNode.loaded && !this.parentNode.loading) {
      this.props.onRequestNodes(this.parentNode.id)
    }
  }

  render () {
    const ids = this.props.nodes[this.props.parentId].children

    if (isEmpty(ids)) {
      return <FormattedMessage id={'admin.common.noResultsFound'} />
    }

    return (
      <div>
        {ids.map(id => (
          <Node
            key={id}
            nodes={this.props.nodes}
            parentNode={this.parentNode}
            node={this.props.nodes[id]}
            onRequestNodes={this.props.onRequestNodes}
            onRequestNodeCreation={this.props.onRequestNodeCreation}
            onRequestNodeEdition={this.props.onRequestNodeEdition}
            onRequestNodeDeletion={this.props.onRequestNodeDeletion}
            canCreate={this.props.canCreate}
            canUpdate={this.props.canUpdate}
            canDelete={this.props.canDelete}
          />
        ))}
      </div>
    )
  }
}
