import styled from 'styled-components'

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border: 1px solid ${props => props.theme.colors.formBorder};
  box-sizing: border-box;
  background-color: ${props => props.theme.colors.background};
`

export const Container = styled.div`
  & + & {
    margin-top: 5px;
  }
`

export const Content = styled.div`
  padding-left: 40px;
  margin: 5px 0;
  margin-bottom: 15px;
`

export const HeaderButton = styled.button`
  display: block;
  flex-grow: 1;
  box-sizing: border-box;
  margin: 0;
  border: none;
  padding: 10px;
  background-color: transparent;
  cursor: pointer;
`

export const Controls = styled.div`
  padding: 0 10px;
  > * + * {
    margin-left: 5px;
  }
`
