import React from 'react'
import PropTypes from 'prop-types'
import Toggle from 'components/Toggle'
import Text from 'components/Text'
import Button from 'components/Button'
import Icon from '@fortawesome/react-fontawesome'
import Tree from './Tree'
import {
  Container,
  Header,
  HeaderButton,
  Controls,
  Content
} from './styles'

export default function Node ({ node, nodes, parentNode, canCreate, canUpdate, canDelete, ...props }) {
  return (
    <Container>
      <Toggle>
        {({ toggle, value }) => (
          <React.Fragment>
            <Header>
              <HeaderButton type='button' onClick={toggle}>
                <Text component='span'>
                  <span style={{ marginRight: 5 }}>{value ? '▼' : '▶'}</span> {node.name}
                </Text>
              </HeaderButton>
              <Controls>
                {canCreate && (
                  <Button
                    type='button'
                    size='small'
                    color='success'
                    onClick={() => props.onRequestNodeCreation(node, parentNode)}
                    disabled={node.removing}
                  >
                    <Icon icon={require('@fortawesome/fontawesome-free-solid/faPlus')} />
                  </Button>
                )}
                {canUpdate && (
                  <Button
                    type='button'
                    size='small'
                    color='warning'
                    onClick={() => props.onRequestNodeEdition(node, parentNode)}
                    disabled={node.removing}
                  >
                    <Icon icon={require('@fortawesome/fontawesome-free-solid/faEdit')} />
                  </Button>
                )}
                {canDelete && (
                  <Button
                    type='button'
                    size='small'
                    color='danger'
                    onClick={() => props.onRequestNodeDeletion(node, parentNode)}
                    disabled={node.removing}
                  >
                    <Icon icon={require('@fortawesome/fontawesome-free-solid/faMinus')} />
                  </Button>
                )}
              </Controls>
            </Header>
            {value && (
              <Content>
                {node.loading ? (
                  <Text>Carregando</Text>
                ) : (
                  <Tree
                    nodes={nodes}
                    parentId={node.id}
                    canCreate={canCreate}
                    canUpdate={canUpdate}
                    canDelete={canDelete}
                    {...props}
                  />
                )}
              </Content>
            )}
          </React.Fragment>
        )}
      </Toggle>
    </Container>
  )
}

Node.propTypes = {
  /** Objeto que representa o Node */
  node: PropTypes.shape({
    id: PropTypes.any.isRequired,
    name: PropTypes.string.isRequired
  }),

  /** Permite a criação de novos nós */
  canCreate: PropTypes.bool.isRequired,

  /** Permite a edição de nós existentes */
  canUpdate: PropTypes.bool.isRequired,

  /** Permite a exclusão de nós */
  canDelete: PropTypes.bool.isRequired,

  /** Objeto que contém todos os nodes */
  nodes: PropTypes.object.isRequired,

  /** Nó pai */
  parentNode: PropTypes.object.isRequired,

  /** Função para requisitar o carregamento de nós */
  onRequestNodes: PropTypes.func.isRequired,

  /** Função para requisitar a criação de um novo nó */
  onRequestNodeCreation: PropTypes.func.isRequired,

  /** Função para requisitar a edição de um nó */
  onRequestNodeEdition: PropTypes.func.isRequired,

  /** Função para requisitar a remoção de um nó */
  onRequestNodeDeletion: PropTypes.func.isRequired
}
