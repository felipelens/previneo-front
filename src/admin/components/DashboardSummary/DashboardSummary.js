import React from 'react'
import { injectIntl } from 'react-intl'
import PropTypes from 'prop-types'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import CardChartInfo from 'admin/components/CardChartInfo'
import CardAgeRange from 'admin/components/CardAgeRange'
import CardChartRisk from 'admin/components/CardChartRisk'
import CardChartExams from 'admin/components/CardChartExams'
import CardChartCancerSummary from 'admin/components/CardChartCancerSummary'
import CardChartGender from 'admin/components/CardChartGender'
import { Grid, GridRisk } from './styles'

const ChartResource = props =>
  <Resource
    resource='Charts'
    autoFetch
    spinner
    {...props}
  />

function DashboardSummary ({ company, name, intl }) {
  return (
    <VerticalSpacer space={10}>
      <ChartResource
        namespace='planned'
        params={{ empresa_id: company, request: '/previsto' }}
      >
        {({ records }) => (
          <CardChartInfo data={records} name={name} />
        )}
      </ChartResource>
      <Grid spacing={10}>
        <ChartResource
          namespace='male'
          params={{ empresa_id: company, sexo: 'M', request: '/faixa-etaria' }}
        >
          {({ records }) => (
            <CardAgeRange data={records} />
          )}
        </ChartResource>
        <ChartResource
          namespace='gender'
          params={{ empresa_id: company, request: '/realizado' }}
        >
          {({ records }) => (
            <CardChartGender data={records} />
          )}
        </ChartResource>
        <ChartResource
          namespace='female'
          params={{ empresa_id: company, sexo: 'F', request: '/faixa-etaria' }}
        >
          {({ records }) => (
            <CardAgeRange data={records} right female />
          )}
        </ChartResource>
      </Grid>
      <ChartResource
        namespace='risk'
        params={{ empresa_id: company, request: '/avaliacao-risco' }}
      >
        {({ records }) => (
          <CardChartRisk data={records} />
        )}
      </ChartResource>
      <GridRisk>
        <ChartResource
          namespace='exams'
          params={{ empresa_id: company, request: '/exames-faltantes' }}
        >
          {({ records }) => (
            <CardChartExams data={records} />
          )}
        </ChartResource>
        <ChartResource
          namespace='cancer'
          params={{ empresa_id: company, request: '/risco-alto' }}
        >
          {({ records }) => (
            <CardChartCancerSummary data={records} />
          )}
        </ChartResource>
      </GridRisk>
    </VerticalSpacer>
  )
}

DashboardSummary.propTypes = {
  /** ID da empresa */
  company: PropTypes.number
}

export default injectIntl(DashboardSummary)
