import React from 'react'
import { injectIntl } from 'react-intl'
import CustomSelect from 'admin/components/CustomSelect'

function FilterSelect ({ valueKey = 'id', options, onChange, filter, intl, ...props }) {
  return (
    <CustomSelect
      onChange={option => option && option[valueKey] ? onChange(option[valueKey]) : onChange('')}
      value={filter ? filter.value : ''}
      valueKey='id'
      labelKey='name'
      options={[
        { id: '', [props.labelKey || 'name']: intl.formatMessage({ id: 'admin.common.all' }) },
        ...options
      ]}
      {...props}
    />
  )
}

export default injectIntl(FilterSelect)
