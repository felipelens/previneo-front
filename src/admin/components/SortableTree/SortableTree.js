import React from 'react'
import styled from 'styled-components'
import ReactSortableTree from 'react-sortable-tree'
import 'react-sortable-tree/style.css'

const Container = styled.div`
  display: flex;
  padding: 20px 0;
  overflow: auto;
  font-family: ${props => props.theme.fonts.primary};

  .rst__row {
    border: 1px solid ${props => props.theme.colors.formBorder};
  }

  .rst__rowContents {
    border: none;
    box-shadow: none;
    padding-left: 0;
  }

  .rst__moveHandle {
    border: none;
    box-shadow: none;
    background-color: transparent;
    filter: invert(100%);
  }
`

export default function SortableTree (props) {
  return (
    <Container>
      <ReactSortableTree
        isVirtualized={false}
        {...props}
      />
    </Container>
  )
}
