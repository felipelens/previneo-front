import React from 'react'
import CustomField from 'components/CustomField'
import { SingleDatePicker, formatDate, parseDate } from 'admin/components/Dates'
import { injectIntl } from 'react-intl'

function FormFieldDate ({ intl, numberOfMonths = 1, ...props }) {
  return (
    <SingleDatePicker
      date={parseDate(props.value)}
      onDateChange={date => props.onChange(formatDate(date, props.formatDefault))}
      focused={props.meta.active}
      onFocusChange={({ focused }) => focused ? props.onFocus() : props.onBlur()}
      placeholder={intl.formatMessage({ id: 'admin.common.dateInsert' })}
      displayFormat='DD/MM/YYYY'
      isOutsideRange={() => false}
      block
      numberOfMonths={numberOfMonths}
    />
  )
}

export default injectIntl(props =>
  <CustomField
    component={FormFieldDate}
    {...props}
  />
)
