import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { groupBy, uniq } from 'ramda'
import shallowEqual from 'shallow-equal/arrays'
import VerticalSpacer from 'components/VerticalSpacer'
import Text from 'components/Text'
import { Container, Option } from './styles'

class PermissionsManager extends Component {
  permissions = groupBy(obj => obj.entity, this.props.permissions)

  shouldComponentUpdate ({ value }) {
    return !shallowEqual(value, this.props.value)
  }

  render () {
    const { permissions } = this
    const { value, onChange } = this.props

    return (
      <Container>
        <VerticalSpacer space={15}>
          {Object.keys(permissions).map(key => (
            <div key={key}>
              <Text size='18px'><strong>{key}</strong></Text>
              <VerticalSpacer space={5} style={{ paddingLeft: 10 }}>
                {permissions[key].map(p => (
                  <Option key={p.id}>
                    <input
                      type='checkbox'
                      value={p.id}
                      checked={value.includes(p.id)}
                      onChange={({ target: { checked } }) => {
                        const newValue = checked
                          ? value.concat(p.id)
                          : value.filter(id => id !== p.id)
                        onChange(uniq(newValue))
                      }}
                    />
                    <Text component='span'>{p.description}</Text>
                  </Option>
                ))}
              </VerticalSpacer>
            </div>
          ))}
        </VerticalSpacer>
      </Container>
    )
  }
}

PermissionsManager.propTypes = {
  /** Permissões */
  permissions: PropTypes.array.isRequired,

  /** IDs selecionados */
  value: PropTypes.array.isRequired,

  /** Callback para alterar os IDs selecionados */
  onChange: PropTypes.func.isRequired
}

export default PermissionsManager
