import styled from 'styled-components'

export const Container = styled.div`
  max-height: 300px;
  overflow: auto;
  border: 1px solid ${props => props.theme.colors.formBorder};
  padding: 10px;
`

export const Option = styled.label`
  display: flex;
  align-items: center;
  cursor: pointer;
  user-select: none;

  input {
    cursor: pointer;
  }
`
