import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import { Grid, Column, Image } from './styles'
import { FormattedNumber, injectIntl } from 'react-intl'

function CardChartCancer ({ data, intl }) {
  return (
    <Box color='highRisk' arrow>
      <Text
        size='18px'
        lineHeight={1.2}
        color='highRisk'
        style={{ marginBottom: 20 }}
      >
        <strong>{intl.formatMessage({ id: 'admin.dashboard.charts.highRisk' })}</strong>{' - '}
        <span style={{ fontSize: '16px' }}>
          {intl.formatMessage({ id: 'admin.dashboard.charts.highRiskSummary' })}
        </span>
      </Text>
      <Grid>
        {data.map((item, index) => (
          <Column key={index} color={item.cor}>
            <Image src={item.icone.url} alt='Cancer' />
            <Text
              size='14px'
              lineHeight={1.2}
              style={{ opacity: 0.8 }}
            >
              {item.nome}
            </Text>
            <Text
              size='25px'
              lineHeight={1.2}
              weight='bold'
            >
              <FormattedNumber value={item.qntd} />
            </Text>
            <Text
              size='14px'
              lineHeight={1.2}
              style={{ opacity: 0.6 }}
            >
              {item.porcentagem}%
            </Text>
          </Column>
        ))}
      </Grid>
    </Box>
  )
}

export default injectIntl(CardChartCancer)
