import React from 'react'
import ReactDatePicker from 'react-datepicker'
import 'moment/locale/pt-br'
import 'react-datepicker/dist/react-datepicker.css'
import Input from 'components/Input'
import { Container } from './styles'

class InputWrapper extends React.Component {
  render () {
    const { inputProps, ...props } = this.props
    return (
      <Input
        {...props}
        {...inputProps}
      />
    )
  }
}

export default function DatePicker ({ inputProps, ...props }) {
  return (
    <Container>
      <ReactDatePicker
        dateFormat='DD/MM/YYYY'
        customInput={<InputWrapper inputProps={inputProps} />}
        {...props}
      />
    </Container>
  )
}
