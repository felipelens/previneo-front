import styled from 'styled-components'

export const Container = styled.div`
  .react-datepicker-wrapper,
   .react-datepicker__input-container {
    display: block;
    width: 100%;
    height: 36px;

    > input {
      height: 100%;
      border: 1px solid #ccc !important;
    }
  }
`
