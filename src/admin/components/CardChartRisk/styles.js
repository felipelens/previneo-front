import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Grid = styled.div`
  display: flex;
  ${mediaQuery.lessThan('small')`
    flex-wrap: wrap;
  `}
`

export const Column = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px 0;
  box-sizing: border-box;
  border-radius: 5px;
  transition: .2s;
  position: relative;

  &:not(:last-child) {
    &::after {
      content: '';
      width: 1px;
      height: 65%;
      background: #e1e1e1;
      position: absolute;
      top: 10%;
      right: 0;
    }
  }
  ${mediaQuery.lessThan('small')`
    flex-basis: 50%;
  `}
`
