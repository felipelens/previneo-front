import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import { Grid, Column } from './styles'
import { FormattedNumber, injectIntl } from 'react-intl'

function CardChartInfo ({ data, intl }) {
  const highRisks = data.filter(item => item.nivel !== 'HIGH')
  const normalRisks = data.filter(item => item.nivel === 'HIGH')

  return (
    <Box width='100%' arrow>
      <Text
        size='18px'
        lineHeight={1.2}
        style={{ marginBottom: 20 }}
      >
        <strong>{intl.formatMessage({ id: 'admin.dashboard.charts.riskAssessment' })}</strong>{' - '}
        <span style={{ fontSize: '16px' }}>
          {intl.formatMessage({id: 'admin.dashboard.charts.riskAssessmentSufix'})}
        </span>
      </Text>
      <Grid>
        {[...highRisks, ...normalRisks].map((item, index) => {
          const name = item.nivel === 'HIGH'
            ? intl.formatMessage({id: 'admin.common.high'})
            : intl.formatMessage({id: 'admin.common.normal'})
          const exams = item.exames > 0
          const smoker = item.tabagista > 0
          const danger = item.nivel === 'HIGH'

          return (
            <Column key={index}>
              <Text
                size='14px'
                sizeMobile='14px'
                lineHeight={1.2}
                color={danger ? 'danger' : 'black'}
                style={(!exams && !smoker) ? { marginBottom: 13 } : {}}
                align='center'
              >
                {name}
              </Text>
              {exams && (
                <Text
                  size='11px'
                  sizeMobile='11px'
                  lineHeight={1.2}
                  color={danger ? 'danger' : 'black'}
                  align='center'
                >
                  {`(${intl.formatMessage({id: 'admin.dashboard.charts.examsPending'})})`}
                </Text>
              )}
              {smoker && (
                <Text
                  size='11px'
                  sizeMobile='11px'
                  lineHeight={1.2}
                  color={danger ? 'danger' : 'black'}
                  align='center'
                >
                  {`(${intl.formatMessage({id: 'admin.dashboard.charts.smoker'})})`}
                </Text>
              )}
              <Text
                size='18px'
                weight='bold'
                lineHeight={1.2}
                color={danger ? 'danger' : 'black'}
              >
                <FormattedNumber value={item.qntd} />
              </Text>
              <Text
                size='12px'
                sizeMobile='12px'
                style={{ opacity: 0.6 }}
                lineHeight={1.2}
              >
                {item.porcentagem}%
              </Text>
            </Column>
          )
        })}
      </Grid>
    </Box>
  )
}

export default injectIntl(CardChartInfo)
