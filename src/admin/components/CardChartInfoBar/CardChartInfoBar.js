import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import withAuth from 'store/utils/withAuth'
import { Column } from './styles'
import { Bar, ResponsiveContainer, Tooltip, XAxis, YAxis, ComposedChart } from 'recharts'
import PatternCandyBar from 'admin/components/PatternCandyBar'
import { FormattedMessage, FormattedNumber } from 'react-intl'

function CardChartInfo ({ data, name, ...props }) {
  return (
    <Box width='100%'>
      <Column center>
        <Text
          size='18px'
          weight='bold'
          color='#2C3E50'
          lineHeight={1.2}
        >
          Total <FormattedMessage id={'admin.dashboard.chart.foreseen'} />: <FormattedNumber value={data.total} />
        </Text>
        <Text
          size='50px'
          sizeMobile='30px'
          weight='bold'
          color='#2C3E50'
          lineHeight={1.2}
        >
          <FormattedNumber value={data.realizado} />
        </Text>
        <Text
          size='20px'
          sizeMobile='14px'
          style={{ opacity: 0.4 }}
          lineHeight={1.2}
        >
          {`(${data.porcentagem_realizado}%)`}
        </Text>
      </Column>
      <ResponsiveContainer width='100%' height={90}>
        <ComposedChart layout='vertical' data={[{name: 'Realizado', realizado: data.realizado, nao_realizado: data.nao_realizado}]}>
          <pattern
            id='pattern-stripe'
            width='4' height='4'
            patternUnits='userSpaceOnUse'
            patternTransform='rotate(45)'>
            <rect width='1' height='4' transform='translate(0,0)' fill='white' />
          </pattern>
          <mask id='mask-stripe'>
            <rect x='0' y='0' width='100%' height='100%' fill='url(#pattern-stripe)' />
          </mask>
          <XAxis type='number' domain={[0, data.total]} />
          <YAxis dataKey='name' type='category' hide />
          <Tooltip
            content={props => (
              <TooltipContent
                {...props}
                donePercent={data.porcentagem_realizado || 0}
                notDonePercent={data.porcentagem_nao_realizado || 0}
              />
            )}
          />
          <Bar dataKey='realizado' stackId='a' barSize={30} fill={'#60a495'} />
          <Bar dataKey='nao_realizado' stackId='a' barSize={30} shape={<PatternCandyBar fill='#60a495' />} />
        </ComposedChart>
      </ResponsiveContainer>
    </Box>
  )
}

const TooltipContent = (props) => {
  if (!props.payload[0]) return null

  const done = props.payload[0].payload['realizado']
  const notDone = props.payload[0].payload['nao_realizado']
  const donePercent = props.donePercent
  const notDonePercent = props.notDonePercent

  return (
    <Box style={{ padding: 10 }}>
      <Text
        color='#2C3E50'
      >
        <strong>{done}</strong> <span style={{ color: '#636363', fontSize: '12px' }}>{` (${donePercent}%) `}</span> <span style={{ fontSize: '14px' }}>{<FormattedMessage id={'admin.dashboard.chart.fulfilled'} />}</span>
      </Text>
      <div style={{ width: '100%', backgroundColor: '#f4f4f4', height: '2px' }} />
      <Text
        color='#2C3E50'
      >
        <strong>{notDone}</strong> <span style={{ color: '#636363', fontSize: '12px' }}>{` (${notDonePercent}%)`}</span> <span style={{ fontSize: '14px' }}>{<FormattedMessage id={'admin.dashboard.chart.not.fulfilled'} />}</span>
      </Text>
    </Box>
  )
}

export default withAuth(CardChartInfo)
