import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  ${props => props.center && `
    align-items: center;
  `}
  ${props => props.total && `
    padding: 0 40px 0 70px;
  `}
  ${mediaQuery.lessThan('medium')`
    padding: 0;
  `}
`
