import styled from 'styled-components'
import {
  SCRIPT_NODE_TYPES as TYPES,
  TYPE_QUESTIONS
} from 'config/constants'

export const TreeContainer = styled.div`
  .type-${TYPES.ANSWER} .rst__rowTitle {
    font-weight: normal;
  }

  .type-${TYPES.QUESTION},
  .type-${TYPES.ANSWER},
  .type-${TYPES.CATEGORY} {
    position: relative;

    &::after {
      position: absolute;
      top: 50%;
      left: 100%;
      transform: translateY(-50%);
      background-color: ${props => props.theme.colors.primary};
      color: #fff;
      font-size: 10px;
      text-transform: uppercase;
      padding: 5px;
    }
  }

  .type-${TYPES.QUESTION} {
    border: 2px solid ${props => props.theme.colors.primary};
    &::after {
      background-color: ${props => props.theme.colors.primary};
    }
  }

  .type-${TYPES.ANSWER} {
    border: 2px solid ${props => props.theme.colors.formBorder};
    &::after {
      content: '${props => props.intl.formatMessage({ id: 'admin.script.answer' })}';
      background-color: ${props => props.theme.colors.formBorder};
    }
  }
  
  .type-${TYPES.CATEGORY} {
    border: 2px solid ${props => props.theme.colors.success};
    &::after {
      content: '${props => props.intl.formatMessage({ id: 'admin.script.category' })}';
      background-color: ${props => props.theme.colors.success};
    }
  }

  .question-type-${TYPE_QUESTIONS.DISCURSIVA.id}::after {
    content: '${props => props.intl.formatMessage({ id: 'admin.script.questionTypes.discursive' })}';
  }

  .question-type-${TYPE_QUESTIONS.OBJETIVA.id}::after {
    content: '${props => props.intl.formatMessage({ id: 'admin.script.questionTypes.objective' })}';
  }
  
  .question-type-${TYPE_QUESTIONS.GROUP.id}::after {
    content: '${props => props.intl.formatMessage({ id: 'admin.common.group' })}';
  }
`
