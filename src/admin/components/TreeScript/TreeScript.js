import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import classNames from 'classnames'
import Icon from '@fortawesome/react-fontawesome'
import Button from 'components/Button'
import SortableTree from 'admin/components/SortableTree'
import { SCRIPT_NODE_TYPES as TYPES, TYPE_QUESTIONS } from 'config/constants'
import { TreeContainer } from './styles'

function TreeScript ({ treeData, onChange, includesCategory, intl, ...props }) {
  const {
    onAddCategoryRequest,
    onAddRequest,
    onEditRequest,
    onRemoveRequest,
    isFetching,
    disabled,
    ...rest
  } = props
  return (
    <React.Fragment>
      {includesCategory && !disabled &&
        <Button type='button' size='small' color='success' onClick={onAddCategoryRequest} disabled={isFetching}>
          {isFetching
            ? intl.formatMessage({ id: 'admin.common.loading' })
            : intl.formatMessage({ id: 'admin.script.addCategory' })
          }
        </Button>
      }
      {!includesCategory && !disabled &&
        <Button type='button' size='small' color='success' onClick={onAddRequest} disabled={isFetching}>
          {intl.formatMessage({ id: 'admin.script.addQuestion' })}
        </Button>
      }
      <TreeContainer intl={intl}>
        <SortableTree
          treeData={treeData}
          onChange={onChange}
          getNodeKey={({ node }) => node.key}
          generateNodeProps={({ node, path }) => {
            return {
              title: node.type === TYPES.CATEGORY && node.historico_medico
                ? `${node.title} (${node.genero.nome})`
                : node.title,
              className: classNames({
                [`type-${node.type}`]: true,
                [`question-type-${node.typeQuestion}`]: node.type === TYPES.QUESTION
              }),
              buttons: disabled
                ? []
                : [
                  <Button
                    type='button'
                    size='small'
                    color='success'
                    onClick={() => onAddRequest({ node, path })}
                    disabled={node.typeQuestion === TYPE_QUESTIONS.DISCURSIVA.id}
                    style={{ marginRight: 5 }}
                  >
                    <Icon icon={require('@fortawesome/fontawesome-free-solid/faPlus')} />
                  </Button>,
                  node.type !== TYPES.CATEGORY ? (
                    <Button
                      type='button'
                      size='small'
                      color='warning'
                      onClick={() => onEditRequest({ node, path })}
                      style={{ marginRight: 5 }}
                    >
                      <Icon icon={require('@fortawesome/fontawesome-free-solid/faEdit')} />
                    </Button>
                  ) : null,
                  !node.obrigatoria ? (
                    <Button
                      type='button'
                      size='small'
                      color='danger'
                      onClick={() => onRemoveRequest({ node, path })}
                    >
                      <Icon icon={require('@fortawesome/fontawesome-free-solid/faMinus')} />
                    </Button>
                  ) : null
                ]
            }
          }}
          canDrag={disabled}
          canDrop={({ node, nextParent }) => {
            const nextParentType = nextParent ? nextParent.type : ''

            if (node.type === TYPES.ANSWER && nextParentType === TYPES.QUESTION) {
              return nextParent.typeQuestion === TYPE_QUESTIONS.OBJETIVA.id
            }

            if (includesCategory && node.type === TYPES.QUESTION && !nextParentType) {
              return false
            }

            if (nextParentType === TYPES.CATEGORY && node.type !== TYPES.QUESTION) {
              return false
            }

            if (includesCategory && node.type === TYPES.CATEGORY && !!nextParentType) {
              return false
            }

            return node.type !== nextParentType
          }}
          {...rest}
        />
      </TreeContainer>
    </React.Fragment>
  )
}

TreeScript.propTypes = {
  /** Dados da árvore */
  treeData: PropTypes.array.isRequired,

  /** Inclui categoria */
  includesCategory: PropTypes.bool,

  /** Callback onChange */
  onChange: PropTypes.func.isRequired,

  /** Callback para adicionar uma categoria */
  onAddCategoryRequest: PropTypes.func.isRequired,

  /** Callback para adicionar um novo nó */
  onAddRequest: PropTypes.func.isRequired,

  /** Callback para editar um nó */
  onEditRequest: PropTypes.func.isRequired,

  /** Callback para remover um nó */
  onRemoveRequest: PropTypes.func.isRequired
}

export default injectIntl(TreeScript)
