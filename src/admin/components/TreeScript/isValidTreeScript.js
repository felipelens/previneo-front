import { getFlatDataFromTree } from 'react-sortable-tree'
import { TYPE_QUESTIONS } from 'config/constants'

export default function isValidTreeScript (tree) {
  const values = getFlatDataFromTree({
    treeData: tree,
    getNodeKey: ({ node }) => node.key,
    ignoreCollapsed: false
  })

  const questionsWithoutAnswers = values.filter(({ node }) => {
    const children = node.children || []
    return node.typeQuestion === TYPE_QUESTIONS.OBJETIVA.id && children.length === 0
  })

  if (questionsWithoutAnswers.length) {
    window.alert('Questões objetivas não podem ficar sem respostas')
    return false
  }

  return true
}
