import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'react-final-form'
import { FieldArray } from 'react-final-form-arrays'
import Icon from '@fortawesome/react-fontawesome'
import CustomField from 'components/CustomField'
import Label from 'components/Label'
import Button from 'components/Button'
import VerticalSpacer from 'components/VerticalSpacer'
import { componentType } from 'types'
import withResource from 'store/utils/withResource'
import Section from 'admin/components/Section'
import Text from 'components/Text'

const SimpleWrapper = props => (
  <React.Fragment>
    <Text weight='bold' style={{ marginBottom: '5px' }}>{props.title}</Text> {props.children}
  </React.Fragment>)

const BoxWrapper = props => <Section {...props} titleSize='20px' />

function FieldTranslations (props) {
  const {
    name: key,
    values,
    label,
    resourceProps: resource,
    ...rest
  } = props

  const shouldDisplayButton = !(
    values[key] && values[key].length === resource.records.length
  )

  const usedLanguages = values[key] && values[key].length
    ? values[key].map(r => r.idioma.id)
    : []

  return (
    <VerticalSpacer space={15}>
      <Label>{label}</Label>
      <div style={{ padding: '0 10px' }}>
        <FieldArray name={key}>
          {({ fields }) => {
            const Wrapper = props.wrapped ? BoxWrapper : SimpleWrapper

            return (
              <VerticalSpacer>
                {fields.map((name, index) => (
                  <div key={name}>
                    <Field
                      name={`${name}.idioma`}
                      component='input'
                      type='hidden'
                    />
                    {props.additional
                      ? (
                        <Wrapper title={values[key][index] && values[key][index].idioma.nome}>
                          <VerticalSpacer>
                            {props.additional.map((item, i) => {
                              return (
                                <Field
                                  key={`${item.name}${i}`}
                                  name={`${name}.${item.name}`}
                                  id={`${name}.${item.name}`}
                                  component={item.component}
                                  label={item.label}
                                  disabled={props.disabled}
                                />
                              )
                            })}
                          </VerticalSpacer>
                        </Wrapper>)
                      : (<Field
                        name={`${name}.descricao`}
                        id={`${name}.descricao`}
                        label={values[key][index] && values[key][index].idioma.nome}
                        {...rest}
                      />)}
                  </div>
                ))}
                {!props.disabled && shouldDisplayButton && (
                  <div style={{ textAlign: 'right' }}>
                    <Button
                      type='button'
                      color='success'
                      size='small'
                      onClick={() => {
                        const language = resource.records.find(r => (
                          !usedLanguages.includes(r.id)
                        ))

                        fields.push({
                          descricao: '',
                          idioma: language
                        })
                      }}
                    >
                      <Icon icon={require('@fortawesome/fontawesome-free-solid/faPlus')} />
                    </Button>
                  </div>
                )}
              </VerticalSpacer>
            )}}
        </FieldArray>
      </div>
    </VerticalSpacer>
  )
}

FieldTranslations.propTypes = {
  name: PropTypes.string,
  component: componentType,
  values: PropTypes.object,
  label: PropTypes.string
}

FieldTranslations.defaultProps = {
  name: 'traducoes',
  component: CustomField
}

export default withResource(FieldTranslations, {
  resource: 'Languages',
  namespace: 'field-translations',
  autoFetch: true,
  spinner: true,
  cached: true
})
