import React from 'react'
import DataTable from 'admin/components/DataTable'
import CustomField from 'components/CustomField'

function CustomCheckDataTable ({ records, loading, columns, onChange, value, ...props }) {
  const selectedIds = value || []
  return (
    <DataTable
      manual
      loading={loading}
      data={records}
      sortable={false}
      columns={[
        {
          id: 'checkbox',
          width: 50,
          filterable: false,
          resizable: false,
          Cell: ({ original }) => {
            const checked = selectedIds.includes(original.id)
            return (
              <input
                type='checkbox'
                checked={checked}
                onChange={() => onChange(
                  checked ? selectedIds.filter(id => id !== original.id) : selectedIds.concat(original.id)
                )}
              />
            )
          },
          Header: () => {
            const isAllChecked = selectedIds.length === records.length
            const someChecked = !isAllChecked && selectedIds.length > 0
            return (
              <input
                type='checkbox'
                checked={isAllChecked}
                ref={el => {
                  if (el) {
                    el.indeterminate = someChecked
                  }
                }}
                onChange={() => {
                  if (isAllChecked || someChecked) {
                    return onChange([])
                  } else {
                    return onChange(records.map(r => r.id))
                  }
                }}
              />
            )
          }
        },
        ...columns
      ]}
      {...props}
    />
  )
}

export default props =>
  <CustomField component={CustomCheckDataTable} {...props} />
