import React from 'react'
import { Route } from 'react-router-dom'
import styled from 'styled-components'
import Auth from 'admin/containers/Auth'
import Toggle from 'components/Toggle'
import SidebarButton from 'admin/components/SidebarButton'
import Collapse from 'react-smooth-collapse'
import { hoverColor } from 'utils/colors'
import ReactTooltip from 'react-tooltip'
import Component from '@reach/component-component'

const Container = styled.div`
  background-color: ${props => hoverColor(props.theme.dashboard.colors.primary)};
  border-top: 3px solid black;
  border-bottom: 3px solid black;
`

export default function SidebarButtonCollapse ({ route, title, icon, children, permissions = [], narrow }) {
  return (
    <Auth>
      {props => {
        if (permissions.filter(p => props.permissions.includes(p)).length === 0) {
          return null
        }

        return (
          <Route>
            {({ location }) => (
              <Toggle defaultValue={location.pathname.startsWith(route)}>
                {({ toggle, value }) => {
                  return (
                    <div>
                      <SidebarButton
                        component='button'
                        title={title}
                        icon={icon}
                        onClick={toggle}
                        className={value && 'active'}
                        collapse
                        data-tip={title}
                        data-for='dashboard-tooltip'
                        narrow={narrow}
                      />
                      <Collapse expanded={value}>
                        <Component didMount={ReactTooltip.rebuild}>
                          <Container>
                            {children}
                          </Container>
                        </Component>
                      </Collapse>
                    </div>
                  )
                }}
              </Toggle>
            )}
          </Route>
        )
      }}
    </Auth>
  )
}
