import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { isEmpty } from 'ramda'
import nanoId from 'nanoid'
import WithModal from 'admin/components/WithModal'
import Section from 'admin/components/Section'
import FormCategorySelect from 'admin/forms/FormCategorySelect'
import FormQuestion from 'admin/forms/FormQuestion'
import FormAnswer from 'admin/forms/FormAnswer'
import ConfirmModal from 'admin/components/ConfirmModal/ConfirmModal'
import TreeScript from 'admin/components/TreeScript'
import withResource from 'store/utils/withResource'
import { SCRIPT_NODE_TYPES as TYPES, TYPE_QUESTIONS } from 'config/constants'
import {
  addNodeUnderParent,
  removeNode,
  changeNodeAtPath,
  getFlatDataFromTree
} from 'react-sortable-tree'

const getNodeKey = ({ node }) => node.key

class ScriptEditor extends Component {
  static propTypes = {
    /** Valor inicial */
    value: PropTypes.array.isRequired,

    /** Callback para cuidar das mudanças */
    onChange: PropTypes.func.isRequired,

    /** Exibe a árvore em modo leitura */
    readOnly: PropTypes.bool,

    /** Inclui nós de categorias */
    includesCategory: PropTypes.bool
  }

  state = {
    categoryId: Number()
  }

  addCategoryNode (record) {
    const flatTree = getFlatDataFromTree({
      treeData: this.props.value,
      getNodeKey,
      ignoreCollapsed: true
    }).map(item => item.node)

    if (flatTree.find(node => node.id === record.id)) {
      return window.alert(this.props.intl.formatMessage({ id: 'admin.script.categoryAlreadyAdded' }))
    }

    const { nome: title, script: children, ...rest } = record

    const { treeData } = addNodeUnderParent({
      treeData: this.props.value,
      newNode: {
        key: nanoId(),
        type: TYPES.CATEGORY,
        title,
        children,
        ...rest
      },
      getNodeKey
    })

    this.props.onChange(treeData)
  }

  addNode = (data, type, node = {}) => {
    const { treeData } = addNodeUnderParent({
      treeData: this.props.value,
      newNode: {
        key: nanoId(),
        type,
        children: [],
        ...data
      },
      parentKey: node.key,
      getNodeKey
    })

    this.props.onChange(treeData)
  }

  removeNode = path => {
    const { treeData } = removeNode({
      treeData: this.props.value,
      path,
      getNodeKey
    })

    this.props.onChange(treeData)
  }

  updateNode = (path, newNode) => {
    const treeData = changeNodeAtPath({
      treeData: this.props.value,
      path,
      newNode,
      getNodeKey
    })

    this.props.onChange(treeData)
  }

  componentDidUpdate ({ resourceProps }) {
    const id = this.state.categoryId
    if (
      isEmpty(this.props.resourceProps.error) &&
      resourceProps.isFetching &&
      !this.props.resourceProps.isFetching &&
      this.props.resourceProps.detailedRecords[id] !== resourceProps.detailedRecords[id]
    ) {
      const record = this.props.resourceProps.detailedRecords[id]
      this.addCategoryNode(record)
    }
  }

  renderModal = ({ closeModal, action, node = {}, path }) => {
    if (action === 'ADD_CATEGORY') {
      return (
        <Section
          title={this.props.intl.formatMessage({ id: 'admin.script.addCategory' })}
          wrapped={false}
        >
          <FormCategorySelect
            categories={this.props.value.filter(c => c.type === TYPES.CATEGORY)}
            onSubmit={({ categoria }) => {
              this.setState({ categoryId: categoria })
              this.props.resourceProps.fetchOne(categoria)
              closeModal()
            }}
          />
        </Section>
      )
    }

    if (action === 'ADD') {
      if (node.type === TYPES.QUESTION && node.typeQuestion === TYPE_QUESTIONS.OBJETIVA.id) {
        return (
          <Section
            title={this.props.intl.formatMessage({ id: 'admin.script.addAnswer' })}
            wrapped={false}
          >
            <FormAnswer
              onSubmit={data => {
                this.addNode(data, TYPES.ANSWER, node)
                closeModal()
              }}
            />
          </Section>
        )
      } else {
        return (
          <Section
            title={this.props.intl.formatMessage({ id: 'admin.script.addQuestion' })}
            wrapped={false}
          >
            <FormQuestion
              onSubmit={data => {
                if (node.typeQuestion === TYPE_QUESTIONS.GROUP.id && data.typeQuestion === TYPE_QUESTIONS.GROUP.id) {
                  return window.alert(this.props.intl.formatMessage({ id: 'admin.script.questionGroupError' }))
                }

                this.addNode(data, TYPES.QUESTION, node)
                closeModal()
              }}
            />
          </Section>
        )
      }
    }

    if (action === 'EDIT') {
      if (node.type === TYPES.QUESTION) {
        return (
          <Section
            title={this.props.intl.formatMessage({ id: 'admin.script.editQuestion' })}
            wrapped={false}
          >
            <FormQuestion
              initialValues={node}
              onSubmit={data => {
                this.updateNode(path, data)
                closeModal()
              }}
            />
          </Section>
        )
      } else {
        return (
          <Section
            title={this.props.intl.formatMessage({ id: 'admin.script.editAnswer' })}
            wrapped={false}
          >
            <FormAnswer
              initialValues={node}
              onSubmit={data => {
                this.updateNode(path, data)
                closeModal()
              }}
            />
          </Section>
        )
      }
    }

    if (action === 'REMOVE') {
      return (
        <ConfirmModal
          onConfirm={() => {
            this.removeNode(path)
            closeModal()
          }}
          onCancel={closeModal}
          title={this.props.intl.formatMessage({ id: 'admin.common.warningMessage' })}
        />
      )
    }

    return null
  }

  render () {
    if (!Array.isArray(this.props.value)) {
      return null
    }

    return (
      <WithModal modal={this.renderModal}>
        {({ toggleModal }) => (
          <TreeScript
            isFetching={this.props.resourceProps.isFetching}
            treeData={this.props.value}
            onChange={treeData => this.props.onChange(treeData)}
            onAddCategoryRequest={() => toggleModal({ action: 'ADD_CATEGORY' })}
            onAddRequest={data => toggleModal({ action: 'ADD', ...data })}
            onEditRequest={data => toggleModal({ action: 'EDIT', ...data })}
            onRemoveRequest={data => toggleModal({ action: 'REMOVE', ...data })}
            includesCategory={this.props.includesCategory}
            disabled={this.props.readOnly}
          />
        )}
      </WithModal>
    )
  }
}

export default withResource(injectIntl(ScriptEditor), {
  resource: 'Categories'
})
