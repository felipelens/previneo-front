import React from 'react'
import { injectIntl } from 'react-intl'
import PropTypes from 'prop-types'
import Resource from 'containers/Resource'
import VerticalSpacer from 'components/VerticalSpacer'
import ChartAnswersByAgeAndTypeCancer from 'admin/components/ChartAnswersByAgeAndTypeCancer'
import Grid from 'admin/components/Grid'
import CardChartCancer from 'admin/components/CardChartCancer'

const ChartResource = props =>
  <Resource
    resource='Charts'
    autoFetch
    spinner
    {...props}
  />

function DashboardRisk ({ highRisk, company, name, params, intl }) {
  const riskType = highRisk ? '/risco-alto' : '/risco-normal'
  const riskTypeParam = highRisk ? { high: 'null' } : {}

  return (
    <VerticalSpacer space={20}>
      <ChartResource
        namespace='cancer'
        params={{ empresa_id: company, request: riskType }}
      >
        {({ records }) => (
          <CardChartCancer highRisk={highRisk} data={records} />
        )}
      </ChartResource>
      <Grid>
        <ChartResource
          namespace='answersRiskMale'
          params={
            {
              empresa_id: company,
              request: '/faixa-etaria-preenchidos',
              sexo: 'M',
              por_risco: 'null',
              ...params,
              ...riskTypeParam
            }
          }
        >
          {({ records, params }) => {
            return (
              <ChartAnswersByAgeAndTypeCancer
                highRisk={highRisk}
                tipoCancer={records.tipo_cancer}
                data={transform(records.resultado)}
                categories={records.riscos}
              />
            )
          }}
        </ChartResource>
        <ChartResource
          namespace='answersRiskFemale'
          params={
            {
              empresa_id: company,
              request: '/faixa-etaria-preenchidos',
              sexo: 'F',
              por_risco: 'null',
              ...params,
              ...riskTypeParam
            }
          }
        >
          {({ records, params }) => {
            return (
              <ChartAnswersByAgeAndTypeCancer
                highRisk={highRisk}
                tipoCancer={records.tipo_cancer}
                data={transform(records.resultado)}
                categories={records.riscos}
                female
              />
            )
          }}
        </ChartResource>
      </Grid>
    </VerticalSpacer>
  )
}

DashboardRisk.propTypes = {
  /** ID da empresa */
  company: PropTypes.number
}

export default injectIntl(DashboardRisk)

function transform (data) {
  return data.map(obj => ({
    nome: obj.nome,
    ...sort(obj.riscos).reduce((memo, value) => {
      memo[value.name] = value.qntd
      return memo
    }, {})
  }))
}

function sort (array) {
  if (array.length < 1) return []

  return array.sort(function (a, b) {
    return b.qntd - a.qntd
  })
}
