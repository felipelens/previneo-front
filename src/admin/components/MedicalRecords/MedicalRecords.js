import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import VerticalSpacer from 'components/VerticalSpacer'
import FormPatient from 'admin/forms/FormPatient'
import { Table, TableVersions } from './Tables'
import HistoryLogs from '../HistoryLogs/HistoryLogs'

export default function MedicalRecords ({ data, patientId, includeDetails = false, ...props }) {
  return (
    <VerticalSpacer>
      <Table
        minRows={1}
        data={data}
        columns={[
          {
            accessor: 'nome',
            Header: <FormattedMessage id='admin.common.script' />
          }
        ]}
        SubComponent={({ original }) => <TableVersions patientId={patientId} original={original} {...props} />}
      />
      { !includeDetails && (
        <HistoryLogs readOnly patientId={patientId} />
      )}
      { includeDetails && (
        <Table
          minRows={1}
          data={[props.resource.detailedRecord.paciente]}
          columns={[
            {
              accessor: 'name',
              Header: <FormattedMessage id='admin.common.patientInfo' />
            }
          ]}
          SubComponent={FormPatientInfo}
        />
      )}
    </VerticalSpacer>
  )
}

const FormPatientInfo = ({ original }) => (
  <FormPatient
    isInfoOnly
    onSubmit={() => null}
    wrapped={false}
    title=''
    readOnly
    initialValues={original}
  />
)

MedicalRecords.propTypes = {
  /** Dados das anmeneses */
  data: PropTypes.array.isRequired,

  /** Incluir dados do paciente */
  includeDetails: PropTypes.bool
}
