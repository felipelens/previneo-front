import VerticalSpacer from 'components/VerticalSpacer'
import Text from 'components/Text'
import { FormattedMessage } from 'react-intl'
import { isEmpty } from 'ramda'
import { CANCER_RISKS } from 'config/constants'
import WithModal from 'admin/components/WithModal'
import Button from 'components/Button'
import React from 'react'
import DataTable from 'admin/components/DataTable'
import Box from 'admin/components/Box'
import ConfirmModal from '../ConfirmModal/ConfirmModal'

export const Table = props =>
  <DataTable
    filterable={false}
    showPagination={false}
    pageSize={1000}
    {...props}
  />

export const Wrapper = props =>
  <div
    style={{ padding: 15 }}
    {...props}
  />

export const TableQuestions = ({ original }) => (
  <Wrapper>
    <VerticalSpacer>
      {original.estrategia && (
        <Box>
          <VerticalSpacer space={10}>
            <FormattedMessage id='admin.strategies.title' />
            <Text
              component='div'
              size='14px'
              lineHeight={1}
              dangerouslySetInnerHTML={{__html: original.estrategia.conteudo}}
            />
          </VerticalSpacer>
        </Box>
      )}
      <Table
        minRows={1}
        data={original.perguntas}
        columns={[
          {
            Header: <FormattedMessage id='admin.common.question' />,
            accessor: 'pergunta'
          },
          {
            Header: <FormattedMessage id='admin.common.answer' />,
            accessor: 'resposta',
            width: 200
          }
        ]}
      />
      {original.hasOwnProperty('mama_risco_5anos') && (
        <Table
          data={[
            {
              name: <FormattedMessage id='admin.medicalRecords.gail.risk1' />,
              value: original.mama_risco_5anos
            },
            {
              name: <FormattedMessage id='admin.medicalRecords.gail.risk2' />,
              value: original.mama_media_5anos
            },
            {
              name: <FormattedMessage id='admin.medicalRecords.gail.risk3' />,
              value: original.mama_risco_90anos
            },
            {
              name: <FormattedMessage id='admin.medicalRecords.gail.risk4' />,
              value: original.mama_media_90anos
            }
          ]}
          columns={[
            {
              Header: <FormattedMessage id='admin.common.risk' />,
              accessor: 'name'
            },
            {
              Header: <FormattedMessage id='admin.common.calculation' />,
              accessor: 'value',
              Cell: ({ value }) => value + '%',
              width: 100
            }
          ]}
        />
      )}
      {!isEmpty(original.examesRecomendados) && (
        <Table
          minRows={1}
          data={original.examesRecomendados}
          columns={[
            {
              accessor: 'nome',
              Header: 'Exames Recomendados'
            }
          ]}
        />
      )}
      {!isEmpty(original.justificativas) && (
        <Table
          minRows={1}
          data={original.justificativas}
          columns={[
            {
              accessor: 'justificativa',
              Header: 'Justificativas'
            }
          ]}
        />
      )}
    </VerticalSpacer>
  </Wrapper>
)

export const TableCategories = ({ original }) => (
  <Wrapper>
    <Table
      minRows={3}
      data={original.categorias}
      columns={[
        {
          accessor: 'nome',
          Header: <FormattedMessage id='admin.common.name' />
        },
        {
          accessor: 'risco',
          Header: <FormattedMessage id='admin.common.risk' />,
          Cell: ({ row: { risco } }) => {
            if (!risco) return ''

            const color = CANCER_RISKS.hasOwnProperty(risco.nivel)
              ? CANCER_RISKS[risco.nivel].color
              : 'black'

            return (
              <Text color={color}>{risco.nome}</Text>
            )
          }
        }
      ]}
      SubComponent={TableQuestions}
    />
  </Wrapper>
)

export const TableVersions = ({ original, patientId, ...props }) => (
  <Wrapper>
    <Table
      minRows={3}
      data={original.versoes}
      columns={[
        {
          Header: <FormattedMessage id='admin.common.version' />,
          accessor: 'data'
        },
        {
          accessor: 'data',
          width: 150,
          Cell: (row) => {
            if (!props.resource) return null
            return (
              <WithModal
                modal={({ closeModal }) => (
                  <ConfirmModal
                    onConfirm={() => {
                      props.resource.create({
                        id: patientId,
                        roteiro: row.original.roteiro_versao_id,
                        session: row.original.sessao
                      }).then(() => props.resource.fetchOne(patientId).then(() => closeModal()))
                    }
                    }
                    onCancel={closeModal}
                  />
                )}
              >
                {({ toggleModal }) => (
                  <Button
                    component='button'
                    color='primary'
                    block
                    size='small'
                    type='button'
                    onClick={toggleModal}
                  >
                    <FormattedMessage id='admin.common.reSendEmail' />
                  </Button>
                )}
              </WithModal>
            )
          }
        },
        {
          accessor: 'logEmails',
          width: 150,
          Cell: ({ row: { logEmails } }) => {
            if (!logEmails) return ''
            if (isEmpty(logEmails.conteudo)) return ''
            return (
              <WithModal width={700} modal={() => (
                <div
                  dangerouslySetInnerHTML={{__html: logEmails.conteudo}}
                />
              )} >
                {({ toggleModal }) => (
                  <Button
                    onClick={toggleModal}
                    size='small'
                    block
                    color='white'
                    textColor='primary'
                  >
                    <FormattedMessage id='admin.common.emailSent' />
                  </Button>
                )}
              </WithModal>
            )
          }
        }
      ]}
      SubComponent={TableCategories}
    />
  </Wrapper>
)
