import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import Icon from '@fortawesome/react-fontawesome'
import PermissionMatch from 'admin/containers/PermissionMatch'
import { ButtonContainer, IconWrapper, CollapseArrow, TitleWrapper } from './styles'

export default function SidebarButton ({ title, icon, collapse, permission, narrow, tooltip, ...props }) {
  return (
    <PermissionMatch permission={permission}>
      {() => (
        <ButtonContainer
          narrow={narrow}
          component={NavLink}
          data-tip={title}
          data-for='dashboard-tooltip'
          {...props}
        >
          {icon && <IconWrapper><Icon icon={icon} /></IconWrapper>}
          <TitleWrapper narrow={narrow}>{title}</TitleWrapper>
          {collapse && (
            <CollapseArrow>
              {props && props.className && props.className === 'active' ? '▼' : '▶'}
            </CollapseArrow>
          )}
        </ButtonContainer>
      )}
    </PermissionMatch>
  )
}

SidebarButton.propTypes = {
  /** Título do botão */
  title: PropTypes.string.isRequired,

  /** Ícone */
  icon: PropTypes.object.isRequired,

  /** Exibe tamanho pequeno */
  small: PropTypes.bool,

  /** Oculta o texto do Título */
  narrow: PropTypes.bool
}
