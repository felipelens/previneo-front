import React from 'react'
import styled, { css } from 'styled-components'
import media from 'utils/mediaQuery'
import Base from 'components/Base'
import { lighten } from 'polished'

const lightenNormal = lighten(0.15)
const lightenSmall = lighten(0.05)

export const IconWrapper = styled.span`
  display: block;
  width: 45px;
  text-align: center;
`

export const TitleWrapper = styled.span`
  display: block;
  
  ${media.greaterThan('large')`
    display: ${props => props.narrow ? 'none' : 'block'};
  `}
`

export const ButtonContainer = styled(({ small, narrow, ...rest }) => <Base {...rest} />)`
  display: flex;
  align-items: center;
  width: 100%;
  padding: 15px;
  padding-left: 5px;
    
  ${media.greaterThan('large')`
    padding-left: ${props => props.narrow ? '15px' : '5px'};
  `}
  
  font-size: 16px;
  color: #fff;
  background-color: transparent;
  border: none;
  border-left: 5px solid transparent;
  text-decoration: none;
  font-family: ${props => props.theme.fonts.primary};
  cursor: pointer;
  box-sizing: border-box;

  ${props => props.small && css`
    padding: 10px 15px;
    padding-left: 15px;
    font-size: 14px;
  `}

  &.active,
  &:hover {
    background-color: ${props => {
    const color = props.theme.dashboard.colors.primary
    return props.small ? lightenSmall(color) : lightenNormal(color)
  }};
    color: #fff;
  }

  &.active {
    border-left-color: ${props => props.theme.colors.primary};
  }

  > ${IconWrapper} {
    color: #fff;
  }
`

export const CollapseArrow = styled.span`
  margin-left: auto;
  font-size: 12px;
`
