import React from 'react'
import {
  SingleDatePicker as ReactSingleDatePicker,
  DateRangePicker as ReactDateRangePicker
} from 'react-dates'
import { isValid, parse } from 'date-fns'
import moment from 'moment'
import 'moment/locale/pt-br'
import 'moment/locale/es'
import 'moment/locale/en-gb'
import { Container } from './styles'
import { injectIntl } from 'react-intl'
import Storage from 'modules/Storage'
import { LANGUAGES } from 'config/constants'

function setMomentLocale (lang) {
  if (lang === LANGUAGES.pt) return 'pt-br'
  if (lang === LANGUAGES.en) return 'en-gb'
  return lang
}

moment.locale(setMomentLocale(Storage.get('lang')))

export const parseDate = value => {
  const date = parse(value)
  return isValid(date) ? moment(date) : null
}

export const formatDate = (date, formatDefault = 'YYYY-MM-DD hh:mm:ss') => date ? date.format(formatDefault) : ''

export const SingleDatePicker = props =>
  <Container>
    <ReactSingleDatePicker {...props} />
  </Container>

export const DateRangePicker = injectIntl(({ intl, ...props }) =>
  <Container>
    <ReactDateRangePicker
      startDatePlaceholderText={intl.formatMessage({ id: 'admin.common.start' })}
      endDatePlaceholderText={intl.formatMessage({ id: 'admin.common.today' })}
      {...props}
    />
  </Container>
)
