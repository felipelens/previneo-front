import styled from 'styled-components'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'

export const Container = styled.div`
  font-family: ${props => props.theme.fonts.primary};

  .SingleDatePickerInput__withBorder,
  .DateRangePickerInput {
    border: none;

    &__focused {
      border-color: ${props => props.theme.colors.primary};
    }
  }

  .DateRangePicker,
  .SingleDatePicker,
  .DateInput {
    &, * {
      box-sizing: border-box;
    }

    input {
      border: 2px solid ${props => props.theme.colors.formBorder};
      height: 40px;
      line-height: 1;
      padding: 0 5px;
      font-size: 16px;
      font-family: ${props => props.theme.fonts.primary};
      border-radius: 3px;
      color: ${props => props.theme.colors.text};
    }

    input:focus {
      border-color: ${props => props.theme.colors.primary};
    }
  }
  
  .DateRangePickerInput__block {
    display: flex;
    align-items: center;
    
    > .DateInput {
      flex-grow: 1;
    }
  }
`
