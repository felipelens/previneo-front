import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  margin: 0 -5px;
  
  > * {
    width: 100%;
    margin: 0 5px;
  }
`
