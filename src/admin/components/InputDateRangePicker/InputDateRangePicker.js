import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import DatePicker from 'admin/components/DatePicker'
import { Container } from './styles'

export default function InputDateRangePicker ({ value, onChange, ...props }) {
  const startDate = toMoment(value.start_date)
  const endDate = toMoment(value.end_date)
  return (
    <div>
      <Container>
        <DatePicker
          selectsStart
          selected={startDate}
          startDate={startDate}
          endDate={endDate}
          onChange={date => onChange({
            ...value,
            start_date: date ? date.format('YYYY-MM-DD') : ''
          })}
          inputProps={{
            placeholder: 'Início'
          }}
          {...props}
        />
        <DatePicker
          selectsEnd
          selected={endDate}
          startDate={startDate}
          endDate={endDate}
          onChange={date => onChange({
            ...value,
            end_date: date ? date.format('YYYY-MM-DD') : ''
          })}
          inputProps={{
            placeholder: 'Fim'
          }}
          {...props}
        />
      </Container>
    </div>
  )
}

InputDateRangePicker.propTypes = {
  /** DateRange's value */
  value: PropTypes.shape({
    start_date: PropTypes.string,
    end_date: PropTypes.string
  }).isRequired,

  /** Change handler */
  onChange: PropTypes.func.isRequired
}

const toMoment = date => date
  ? moment(date)
  : undefined
