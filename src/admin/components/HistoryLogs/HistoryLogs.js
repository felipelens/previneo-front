import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, injectIntl } from 'react-intl'
import Resource from 'containers/Resource'
import FormMedicalRecordObservations from 'admin/forms/FormMedicalRecordObservations'
import VerticalSpacer from 'components/VerticalSpacer'
import Button from 'components/Button'
import WithModal from 'admin/components/WithModal'
import Text from 'components/Text'
import { isEmpty } from 'ramda'
import { format } from 'modules/Date'
import { DateColumn } from 'admin/columns'
import ResourceDataTable from 'admin/containers/ResourceDataTable'

export default function HistoryLogs ({ readOnly, patientId }) {
  return (
    <VerticalSpacer>
      { !readOnly && (
        <Resource resource='PatientLogs'>
          {props => (
            <FormMedicalRecordObservations
              initialValues={{
                contato_realizado: false,
                tres_tentativas_contato: false,
                nao_requer_novo_contato: false
              }}
              onSubmit={async (data, methods) => {
                await props.create(patientId, data)
                props.fetchAll({
                  page: 1,
                  limit: 20,
                  id: patientId
                }).then(methods.reset())
              }}
              isSubmitting={props.isSubmitting}
              patientId={patientId}
              readOnly={readOnly}
            />
          )}
        </Resource>
      )
      }
      <ResourceDataTable
        autoFetch
        canEdit={false}
        canShowDetails={false}
        canRemove={false}
        resource={'PatientLogs'}
        params={{ id: patientId }}
        columns={[
          {
            accessor: 'causer.name',
            Header: <FormattedMessage id='admin.common.author' />,
            width: 200,
            filterable: false
          },
          {
            accessor: 'contato_realizado',
            Header: <FormattedMessage id='admin.common.contactMade' />,
            width: 100,
            filterable: false,
            Cell: (row) => {
              const message = row.original.contato_realizado
                ? 'admin.common.contactMadeOk'
                : 'admin.common.contactMadeNo'
              return <FormattedMessage id={message} />
            }
          },
          {
            accessor: 'descricao',
            Header: <FormattedMessage id='admin.common.description' />,
            filterable: false
          },
          DateColumn({dateAccessor: 'created_at', message: 'admin.common.createdAt', dateFormat: 'DD/MM/YYYY H:m:s'}),
          DateColumn({dateAccessor: 'proximo_contato', message: 'admin.common.nextContact'}),
          {
            Header: '',
            width: 50,
            filterable: false,
            Cell: (row) => {
              const data = row.original

              return (
                <WithModal width={800} modal={() => (
                  <ModalDetailsWithIntl data={data} />
                )} >
                  {({ toggleModal }) => (
                    <Button
                      onClick={toggleModal}
                      size='small'
                      icon={require('@fortawesome/fontawesome-free-solid/faInfoCircle')}
                      color='white'
                      textColor='primary'
                      block
                    />
                  )}
                </WithModal>
              )
            }
          }
        ]}
      />
    </VerticalSpacer>
  )
}

function ModalDetailsList (props) {
  const accessor = props.accessor
  return (
    <div>
      <Text size='16px' weight='bold'>{props.title}</Text>
      <ul>
        {
          props.data[props.list].map((item, index) => <li key={index}>{item[accessor]}</li>)
        }
      </ul>
    </div>
  )
}

function ModalDetails ({intl, ...props}) {
  const contactMessage = props.data.contato_realizado
    ? 'admin.common.contactMadeOk'
    : 'admin.common.contactMadeNo'

  const threeContactsMessage = props.data.tres_tentativas_contato
    ? 'admin.common.contactMadeOk'
    : 'admin.common.contactMadeNo'

  const noMoreContact = props.data.nao_requer_novo_contato
    ? 'admin.common.contactMadeNo'
    : 'admin.common.contactMadeOk'

  return (
    <VerticalSpacer space={10}>
      <Text size='35px'>Detalhes</Text>
      {props.data.causer && (
        <div>
          <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.common.author' })}</Text>
          <Text>{props.data.causer.name}</Text>
        </div>
      )}
      {props.data.proximo_contato && (
        <div>
          <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.common.nextContact' })}</Text>
          <Text>{format(props.data.proximo_contato, 'DD/MM/YYYY')}</Text>
        </div>
      )}
      {!isEmpty(props.data.examesRealizados) &&
      <ModalDetailsList
        title={intl.formatMessage({ id: 'admin.common.patientHistoryExams' })}
        list='examesRealizados'
        accessor='nome'
        data={props.data}
      />
      }
      {!isEmpty(props.data.motivosContato) &&
      <ModalDetailsList
        title={intl.formatMessage({ id: 'admin.common.contactReason' })}
        list='motivosContato'
        accessor='motivo'
        data={props.data}
      />
      }
      {!isEmpty(props.data.tiposCancer) &&
      <ModalDetailsList
        title={intl.formatMessage({ id: 'admin.common.typesCancer' })}
        list='tiposCancer'
        accessor='nome'
        data={props.data}
      />
      }
      {props.data.prontuario && (
        <div>
          <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.common.category' })}</Text>
          <Text>{props.data.prontuario.categoria.nome}</Text>
        </div>
      )}
      {props.data.prontuario && props.data.prontuario.risco && (
        <div>
          <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.common.risk' })}</Text>
          <Text>{props.data.prontuario.risco.risco}</Text>
        </div>
      )}
      <div>
        <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.medicalRecords.noMoreContactQuestion' })}</Text>
        <Text>{intl.formatMessage({ id: noMoreContact })}</Text>
      </div>
      <div>
        <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.medicalRecords.madeContact' })}</Text>
        <Text>{intl.formatMessage({ id: contactMessage })}</Text>
      </div>
      <div>
        <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.medicalRecords.threeContactsMessage' })}</Text>
        <Text>{intl.formatMessage({ id: threeContactsMessage })}</Text>
      </div>
      <Text size='16px' weight='bold'>{intl.formatMessage({ id: 'admin.common.observations' })}</Text>
      <textarea
        disabled
        rows={15}
        style={{ width: '100%', border: '2px solid #clclcl', borderRadius: '3px' }}
        defaultValue={props.data.descricao}
      />
    </VerticalSpacer>
  )
}

const ModalDetailsWithIntl = injectIntl(ModalDetails)

HistoryLogs.propTypes = {
  /** Dados das anmeneses */
  patientId: PropTypes.number.isRequired,

  /** Dados das anmeneses */
  readOnly: PropTypes.bool
}
