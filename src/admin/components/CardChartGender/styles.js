import styled from 'styled-components'

export const Container = styled.div`
  position: relative;
`

export const Donut = styled.svg`
  margin: 0 auto;
  border-radius: 50%;
  display: block;
  transform: rotate(-15deg);
`

export const Track = styled.circle`
  fill: transparent;
  stroke: ${props => props.theme.colors.primary};
  stroke-width: 15;
`

export const Indicator = styled.circle`
  fill: transparent;
  stroke: ${props => props.theme.colors.highRisk};
  stroke-width: 15;
  stroke-dasharray: 0 10000;
  transition: stroke-dasharray .3s ease;
`

export const Info = styled.div`
  width: 190px;
  height: ${props => props.total ? '' : '180px'};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Gender = styled.div`
  width: 90px;
  padding: 10px;
  border-radius: 2px;
  background: white;
  display: flex;
  justify-content: space-between;
`

export const Square = styled.div`
  width: 20px;
  height: 25px;
  border-radius: 4px;
  background: ${props => props.theme.colors.primary};
  ${props => props.female && `
    background: ${props.theme.colors.highRisk};
  `}
`

export const Female = styled.span`
  font-weight: 'bold';
  font-size: 14px;
  color: white;
  position: absolute;
  top: 5px;
  left: 50%;
  transform: translateX(-50%);
`

export const Male = styled.span`
  font-size: 14px;
  font-weight: 'bold';
  color: white;
  position: absolute;
  bottom: 5px;
  left: 50%;
  transform: translateX(-50%);
`
