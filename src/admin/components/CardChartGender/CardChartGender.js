import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl'
import Text from 'components/Text'
import { Donut, Container, Track, Indicator, Info, Gender, Square, Female, Male } from './styles'

class CardChartGender extends React.Component {
  state = {
    value: 0
  }

  static propTypes = {
    value: PropTypes.number,
    size: PropTypes.number,
    strokewidth: PropTypes.number
  }

  static defaultProps = {
    value: 0,
    size: 260,
    strokewidth: 25
  }

  componentDidMount () {
    this.setState({ value: this.props.data.mulheres_porcentagem })
  }

  render () {
    const halfsize = (this.props.size * 0.5)
    const radius = halfsize - (this.props.strokewidth * 0.5)
    const circumference = 2 * Math.PI * radius
    const strokeval = ((this.state.value * circumference) / 100)
    const dashval = (strokeval + ' ' + circumference)

    const trackstyle = { strokeWidth: this.props.strokewidth }
    const indicatorstyle = { strokeWidth: this.props.strokewidth, strokeDasharray: dashval }
    const rotateval = 'rotate(-90 ' + halfsize + ',' + halfsize + ')'
    const { data } = this.props

    return (
      <Container>
        <Donut width={this.props.size} height={this.props.size}>
          <Track
            r={radius}
            cx={halfsize}
            cy={halfsize}
            transform={rotateval}
            style={trackstyle}
            className='donutchart-track'
          />
          <Indicator
            r={radius}
            cx={halfsize}
            cy={halfsize}
            transform={rotateval}
            style={indicatorstyle}
            className='donutchart-indicator'
          />
        </Donut>
        {this.props.total
          ? (
            <Info total>
              <Text
                size='16px'
                weight='bold'
                color='#2C3E50'
                lineHeight={1.2}
              >
                {this.props.intl.formatMessage({ id: 'admin.dashboard.charts.total' })}
              </Text>
              <Text
                size='48px'
                sizeMobile='40px'
                weight='bold'
                color='#2C3E50'
                lineHeight={1}
              >
                <FormattedNumber value={data.realizado} />
              </Text>
              <Text
                size='16px'
                style={{ opacity: 0.4 }}
                lineHeight={1.2}
              >
                {`(${data.porcentagem_realizado}%)`}
              </Text>
            </Info>
          )
          : (<Info>
            <Text
              size='15px'
              lineHeight={1.1}
            >
              <FormattedMessage id={'admin.dashboard.chart.fulfilled'} />
            </Text>
            <Text
              size='24px'
              weight='bold'
              lineHeight={1.1}
              style={{ marginBottom: 10 }}
            >
              <FormattedNumber value={data.total} />
            </Text>
            <Gender style={{ marginBottom: 10 }}>
              <Square />
              <div>
                <Text
                  size='12px'
                  lineHeight={1.2}
                >
                  <FormattedMessage id={'admin.common.gender.male'} />
                </Text>
                <Text
                  size='14px'
                  weight='bold'
                  lineHeight={1.2}
                  style={{ textAlign: 'right' }}
                >
                  <FormattedNumber value={data.homens_total} />
                </Text>
              </div>
            </Gender>
            <Gender>
              <Square female />
              <div>
                <Text
                  size='12px'
                  lineHeight={1.2}
                >
                  <FormattedMessage id={'admin.common.gender.female'} />
                </Text>
                <Text
                  size='14px'
                  weight='bold'
                  lineHeight={1.2}
                  style={{ textAlign: 'right' }}
                >
                  <FormattedNumber value={data.mulheres_total} />
                </Text>
              </div>
            </Gender>
          </Info>)
        }
        <Female>{data.mulheres_porcentagem}%</Female>
        <Male>{data.homens_porcentagem}%</Male>
      </Container>
    )
  }
}

export default injectIntl(CardChartGender)
