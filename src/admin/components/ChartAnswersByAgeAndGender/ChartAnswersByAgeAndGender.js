import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import Card from 'admin/components/Card'
import Box from 'admin/components/Box'
import { COLORS } from 'config/constants'
import { Info } from './styles'
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  LabelList,
  Tooltip
} from 'recharts'

const TooltipContent = props => {
  if (!props.payload[0]) return null

  const data = props.payload[0].payload[`${props.type}_riscos`]

  return (
    <Box style={{ padding: 10 }}>
      {data.map(obj => (
        <Info key={obj.nome}>
          <strong>{obj.qntd}%</strong> <span>{obj.nome}</span>
        </Info>
      ))}
    </Box>
  )
}

class ChartsAnswersByAgeAndGender extends Component {
  state = {
    isTooltipVisible: false,
    type: ''
  }

  handleMouseEnter (type) {
    this.setState({
      isTooltipVisible: true,
      type
    })
  }

  handleMouseLeave () {
    this.setState({
      isTooltipVisible: false,
      type: ''
    })
  }

  render () {
    const data = this.props.data.map(item => ({
      ...item,
      feminino_total_value: item.feminino_total - item.feminino_qntd,
      masculino_total_value: item.masculino_total - item.masculino_qntd
    }))

    return (
      <Card title={this.props.intl.formatMessage({ id: 'admin.dashboard.charts.answersByAgeAndGender' })}>
        <ResponsiveContainer width='100%' height={400}>
          <BarChart data={data} stackOffset='sign'>
            <XAxis tick={false} />
            <YAxis />
            {this.state.isTooltipVisible && (
              <Tooltip
                content={props => (
                  <TooltipContent
                    {...props}
                    type={this.state.type}
                  />
                )}
              />
            )}
            {['masculino', 'feminino'].map((type, index) => (
              [
                <Bar
                  key={`${type}-value`}
                  dataKey={`${type}_qntd`}
                  fill={COLORS[index]}
                  stackId={`stack${index}`}
                  label={({ value }) => this.props.intl.formatNumber(value)}
                  isAnimationActive={false}
                  onMouseEnter={() => this.handleMouseEnter(type)}
                  onMouseLeave={() => this.handleMouseLeave()}
                >
                  <LabelList
                    dataKey={`${type}_nome`}
                    position='bottom'
                    fill={COLORS[index]}
                  />
                </Bar>,
                <Bar
                  key={`${type}-total_value`}
                  dataKey={`${type}_total_value`}
                  fill='#efefef'
                  stackId={`stack${index}`}
                  isAnimationActive={false}
                  onMouseEnter={() => this.handleMouseEnter(type)}
                  onMouseLeave={() => this.handleMouseLeave()}
                >
                  <LabelList
                    dataKey={`${type}_total`}
                    position='center'
                    fill='#000'
                  />
                </Bar>
              ]
            ))}
          </BarChart>
        </ResponsiveContainer>
      </Card>
    )
  }

  static propTypes = {
    /** Dados */
    data: PropTypes.array.isRequired
  }
}

export default injectIntl(ChartsAnswersByAgeAndGender)
