import styled from 'styled-components'

export const Info = styled.div`
  display: flex;
  align-items: center;

  strong {
    font-size: 16px;
    margin-right: 10px;
  }

  span {
    font-size: 14px;
    color: ${props => props.theme.textGray}
  }

  & + & {
    border-top: 1px solid ${props => props.theme.colors.border};
    margin-top: 5px;
    padding-top: 5px;
  }
`
