import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import theme from 'theme'
import Media from 'react-media'
import Logo from 'components/Logo'
import Button from 'components/Button'
import HeaderButton from 'admin/components/HeaderButton'
import routes from 'routes'
import media from 'utils/mediaQuery'

const DashboardHeaderStyled = styled.header`
  background-color: ${props => props.theme.dashboard.colors.primary};
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 70px;
  padding: 0 20px;
  box-sizing: border-box;
  box-shadow: 0 3px 7px 0 rgba(0, 0, 0, 0.2);

  ${media.greaterThan('large')`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;    
    z-index: 999;
  `}
`

export default function DashboardHeader ({ onMenuButtonClick, intl, ...props }) {
  return (
    <DashboardHeaderStyled {...props}>
      <Logo to={routes.admin.index} width={300} white />
      <Media query={`(max-width: ${theme.breakpoints.large})`}>
        {matches => matches
          ? (
            <Button
              type='button'
              onClick={onMenuButtonClick}
              style={{ marginLeft: 30 }}
            >
              Menu
            </Button>
          ) : (
            <div style={{ display: 'flex', height: 70 }}>
              <HeaderButton
                to={routes.admin.myAccount}
                title={intl.formatMessage({ id: 'admin.common.myAccount' })}
                icon={require('@fortawesome/fontawesome-free-solid/faCog')}
              />
              <HeaderButton
                to={routes.admin.logout}
                title={intl.formatMessage({ id: 'admin.common.signOut' })}
                icon={require('@fortawesome/fontawesome-free-solid/faSignOutAlt')}
              />
            </div>
          )
        }
      </Media>
    </DashboardHeaderStyled>
  )
}

DashboardHeader.propTypes = {
  /** Callback do botão de menu */
  onMenuButtonClick: PropTypes.func.isRequired
}
