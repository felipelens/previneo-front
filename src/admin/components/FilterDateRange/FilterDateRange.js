import React from 'react'
import PropTypes from 'prop-types'
import InputDateRangePicker from 'admin/components/InputDateRangePicker'

export default function DataTableFilterDateRange ({ filter, onChange }) {
  return (
    <InputDateRangePicker
      onChange={onChange}
      value={filter ? filter.value : {}}
    />
  )
}

DataTableFilterDateRange.propTypes = {
  /** DataTable's filter object */
  filter: PropTypes.object,

  /** Change handler */
  onChange: PropTypes.func.isRequired
}
