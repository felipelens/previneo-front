import React, { Component } from 'react'
import CustomField from 'components/CustomField'
import { DateRangePicker, parseDate, formatDate } from 'admin/components/Dates'

class FormFieldDateRange extends Component {
  state = {
    focusedInput: null
  }

  render () {
    const dateObj = this.props.value || {}
    return (
      <DateRangePicker
        startDate={parseDate(dateObj.start_date)}
        startDateId={`${this.props.id}_start_date`}
        endDate={parseDate(dateObj.end_date)}
        endDateId={`${this.props.id}_end_date`}
        onDatesChange={({ startDate, endDate }) => {
          this.props.onChange({
            start_date: formatDate(startDate),
            end_date: formatDate(endDate)
          })
        }}
        focusedInput={this.state.focusedInput}
        onFocusChange={focusedInput => {
          if (focusedInput) {
            this.props.onFocus()
          } else {
            this.props.onBlur()
          }

          this.setState({ focusedInput })
        }}
        displayFormat='DD/MM/YYYY'
        isOutsideRange={() => false}
        block
      />
    )
  }
}

export default props =>
  <CustomField
    component={FormFieldDateRange}
    {...props}
  />
