import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormattedNumber, injectIntl } from 'react-intl'
import Text from 'components/Text'
import Box from 'admin/components/Box'
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  Legend
} from 'recharts'

const TooltipContent = (props) => {
  if (!props.payload[0]) return null
  const reversed = props.payload.reverse()

  return (
    <Box style={{ padding: 10 }}>
      <Text
        size='15px'
        sizeMobile='15px'
      >
        {props.label}
      </Text>
      {reversed.map((item, index) => {
        return (
          <Text
            key={`label${index}`}
            color={item.color}
            size='14px'
            sizeMobile='12px'
          >
            <span style={{ paddingBottom: '5px', borderBottom: '1pt solid #f4f4f4' }}><FormattedNumber value={item.value} /> {item.name}</span>
          </Text>
        )
      })}
    </Box>
  )
}

const LegendContent = (props) => {
  if (!props.payload[0]) return null

  return (
    <ul style={{ padding: '0', margin: '0', textAlign: 'left' }}>
      {props.payload.map((item, index) => {
        return (
          <li key={index} style={{ display: 'inline-block', marginRight: '10px', fontSize: '12px' }}>
            <svg className='recharts-surface' width='14' height='14'
              viewBox='0 0 32 32'
              version='1.1'
              style={{display: 'inline-block', verticalAlign: 'middle', marginRight: '4px'}}
            >
              <path stroke='none' fill={item.color} d='M0,4h32v24h-32z' className='recharts-legend-icon' />
            </svg>
            <span> {item.dataKey} </span>
          </li>
        )
      })}
    </ul>
  )
}

class ChartAnswersByAgeAndTypeCancer extends Component {
  state = {
    isTooltipVisible: false,
    type: ''
  }

  handleMouseEnter (type) {
    this.setState({
      isTooltipVisible: true,
      type
    })
  }

  handleMouseLeave () {
    this.setState({
      isTooltipVisible: false,
      type: ''
    })
  }

  render () {
    const data = this.props.data
    const chartColors = {
      F: ['#c95e5f', '#d07172', '#d68485', '#dc9798', '#e3aaab', '#e9bdbe', '#efd0d1'],
      M: ['#339aa2', '#39acb5', '#44bbc4', '#57c2ca', '#6ac9d0', '#7dd0d6', '#91d7dc']
    }
    const labelRisks = this.props.categories.reduce((memo, item) => {
      return [...memo, item.name]
    }, [])
    const currentGender = this.props.female ? 'feminino' : 'masculino'
    const currentGenderInitial = this.props.female ? 'F' : 'M'
    const genderTitle = this.props.female
      ? this.props.intl.formatMessage({ id: 'admin.common.genderFemale' })
      : this.props.intl.formatMessage({ id: 'admin.common.genderMale' })
    const cancerType = this.props.tipoCancer ? `${this.props.tipoCancer} - ` : ''
    const riskType = this.props.highRisk
      ? this.props.intl.formatMessage({ id: 'admin.dashboard.charts.highRiskChart' })
      : this.props.intl.formatMessage({ id: 'admin.dashboard.charts.normalRiskChart' })

    return (
      <Box
        arrow
        color={this.props.female ? 'highRisk' : 'primary'}
      >
        <Text
          size='18px'
          sizeMobile='18px'
          color={this.props.female ? 'highRisk' : 'primary'}
          lineHeight={1.2}
          weight='bold'
          alignMobile='center'
          style={{ marginBottom: 20 }}
        >
          {`${cancerType}${genderTitle}` }
          <span style={{ float: 'right' }}>{riskType}</span>
        </Text>
        <ResponsiveContainer width='100%' height={250}>
          <BarChart data={data} stackOffset='sign'>
            <CartesianGrid
              strokeDasharray='4 2'
              stroke='#aab8c2'
            />
            <XAxis type='category' dataKey={`nome`} />
            <YAxis allowDecimals={false} interval={1} />
            {this.state.isTooltipVisible && (
              <Tooltip
                content={props => (
                  <TooltipContent
                    {...props}
                    type={currentGender}
                  />
                )}
              />
            )}
            {
              labelRisks.map((item, index) => {
                return (
                  <Bar
                    key={`bar${index}`}
                    dataKey={item}
                    fill={chartColors[currentGenderInitial][index]}
                    stackId={`nome`}
                    isAnimationActive={false}
                    onMouseEnter={() => this.handleMouseEnter(currentGender)}
                    onMouseLeave={() => this.handleMouseLeave()}
                  />
                )
              })
            }
            <Legend
              content={props => (
                <LegendContent
                  {...props}
                  type={currentGender}
                />
              )}
            />
          </BarChart>
        </ResponsiveContainer>
      </Box>
    )
  }

  static propTypes = {
    /** Dados */
    data: PropTypes.array.isRequired
  }
}

export default injectIntl(ChartAnswersByAgeAndTypeCancer)
