Um componente capaz de efetuar upload de imagens e outros tipos de arquivos.

```
<Uploader
  onUpload={file => console.log('Upload file:', file)}
  onRemove={image => console.log('Remover imagem:', image)}
/>
```

```
<Uploader
  onUpload={file => console.log('Upload file:', file)}
  onRemove={image => console.log('Remover imagem:', image)}
  file={{
    nome: 'nome_da_imagem.jpg',
    url: 'http://placehold.it/150x150'
  }}
/>
```

```
<Uploader
  onUpload={file => console.log('Upload file:', file)}
  onRemove={image => console.log('Remover imagem:', image)}
  file={{
    nome: 'nome-do-arquivo.pdf',
    url: 'http://previnelson.com.br/storage/arquivo.pdf'
  }}
/>
```


```
<Uploader
  onUpload={file => console.log('Upload file:', file)}
  onRemove={image => console.log('Remover imagem:', image)}
  file={{
    nome: 'nome_da_imagem.jpg',
    url: 'http://placehold.it/150x150'
  }}
  isLoading
/>
```

```
<Uploader
  onUpload={file => console.log('Upload file:', file)}
  onRemove={file => console.log('Remover arquivo:', image)}
  isLoading
/>
```
