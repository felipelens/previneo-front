import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'

export const Chart = styled.div`
  display: flex;
  flex-direction: column;
`

export const ChartHeader = styled.div`
  border-bottom: 1px solid #e1e1e1;
  display: flex;
`

export const CellHeader = styled.div`
  flex: 1;
  line-height: 2.5;
  text-align: center;
  opacity: .8;
  &:not(:last-child) {
    border-right: 1px solid #e1e1e1;
  }
`

export const ChartBody = styled.div`
  display: flex;
`

export const Cell = styled.div`
  flex: 1;
  height: 160px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  &:not(:last-child) {
    border-right: 1px solid #e1e1e1;
  }
`

export const Bar = styled.div`
  width: 40px;
  background: ${props => props.female ? props.theme.colors.highRisk : props.theme.colors.primary};
`

export const Detail = styled.div`
  width: 250px;
  height: 280px;
  background: ${props => props.theme.colors.background};
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  border-radius: 50%;
  left: calc(100% - 30px);
  box-shadow: inset 7px 0 9px -7px #0000000c;
  ${props => props.right && `
    left: auto;
    right: calc(100% - 30px);
    box-shadow: inset -7px 0 9px -7px #0000000c;
  `}
  ${mediaQuery.lessThan('medium')`
    display: none;
  `}
`
