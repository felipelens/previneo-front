import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import { Chart, ChartHeader, CellHeader, ChartBody, Cell, Bar, Detail } from './styles'
import { FormattedNumber, injectIntl } from 'react-intl'

function CardAgeRange ({ data, female, intl, ...props }) {
  const maleText = intl.formatMessage({ id: 'admin.common.gender.male' })
  const femaleText = intl.formatMessage({ id: 'admin.common.gender.female' })
  return (
    <Box arrow style={props.right ? { padding: '20px 15px 0 40px', borderWidth: '1px 1px 1px 0' } : { padding: '20px 40px 0 40px', borderWidth: '1px 0 1px 1px' }} color={female ? 'highRisk' : 'primary'}>
      <Text
        size='18px'
        color={female ? 'highRisk' : 'primary'}
        weight='bold'
        style={{ marginBottom: 20 }}
      >
        {`${intl.formatMessage({ id: 'admin.common.ageRange' })} ${female ? femaleText : maleText}`}
      </Text>
      <Chart>
        <ChartHeader>
          {data.map((item, index) => (
            <CellHeader key={index}>{item.faixa_nome}</CellHeader>
          ))}
        </ChartHeader>
        <ChartBody>
          {data.map((item, index) => (
            <Cell key={index}>
              <Text
                size='18px'
                weight='bold'
              >
                <FormattedNumber value={item.qntd} />
              </Text>
              <Bar style={{ height: item.porcentagem + '%' }} female={female} />
            </Cell>
          ))}
        </ChartBody>
      </Chart>
      <Detail right={props.right} />
    </Box>
  )
}

export default injectIntl(CardAgeRange)
