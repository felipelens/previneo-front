import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import { lighten } from 'polished'

export const IconWrapper = styled.span`
  display: block;
  width: 30px;
  text-align: left;
  font-size: 20px;
`

export const HeaderButtonStyled = styled(NavLink)`
  display: flex;
  align-items: center;
  padding: 15px;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 16px;
  text-decoration: none;
  color: #fff;
  height: inherit;
  box-sizing: border-box;

  &.active,
  &:hover {
    background-color: ${props => lighten(0.15, props.theme.dashboard.colors.primary)};
    color: #fff;
  }

  > ${IconWrapper} {
    color: #fff;
  }
`
