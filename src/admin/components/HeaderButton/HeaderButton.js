import React from 'react'
import PropTypes from 'prop-types'
import Icon from '@fortawesome/react-fontawesome'
import PermissionMatch from 'admin/containers/PermissionMatch'
import { HeaderButtonStyled, IconWrapper } from './styles'

export default function HeaderButton ({ title, icon, permission, ...props }) {
  return (
    <PermissionMatch permission={props.permission}>
      {() => (
        <HeaderButtonStyled {...props}>
          {icon && <IconWrapper><Icon icon={icon} /></IconWrapper>}
          <span>{title}</span>
        </HeaderButtonStyled>
      )}
    </PermissionMatch>
  )
}

HeaderButton.propTypes = {
  /** Título do botão */
  title: PropTypes.string.isRequired,

  /** Ícone */
  icon: PropTypes.object.isRequired
}
