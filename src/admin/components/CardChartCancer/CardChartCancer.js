import React from 'react'
import Box from 'admin/components/Box'
import Text from 'components/Text'
import { Grid, Column, Image } from './styles'
import { FormattedNumber, injectIntl } from 'react-intl'
import { NavLink } from 'react-router-dom'
import getRoute from 'utils/getRoute'
import routes from '../../../routes'

function CardChartCancer ({ highRisk, data, intl }) {
  const url = highRisk ? routes.admin.dashboard.highRisk.type : routes.admin.dashboard.normalRisk.type

  return (
    <Box color='highRisk' >
      <Grid>
        {data.map((item, index) => (
          <Column
            key={index}
            color={item.cor}
            component={NavLink}
            to={getRoute(url, { tipo_cancer_id: item.id })}
          >
            <Text
              size='18px'
              style={{ marginBottom: 25, opacity: 0.8 }}
            >
              {item.nome}
            </Text>
            <Image src={item.icone.url} alt='Cancer' />
            <Text
              size='25px'
              lineHeight={1.2}
              weight='bold'
            >
              <FormattedNumber value={item.qntd} />
            </Text>
            <Text
              size='14px'
              lineHeight={1.2}
              style={{ opacity: 0.6 }}
            >
              {item.porcentagem}%
            </Text>
          </Column>
        ))}
      </Grid>
    </Box>
  )
}

export default injectIntl(CardChartCancer)
