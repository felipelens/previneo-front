import styled from 'styled-components'
import mediaQuery from 'utils/mediaQuery'
import Base from 'components/Base'
import React from 'react'

export const Grid = styled.div`
  display: flex;
  ${mediaQuery.lessThan('small')`
    flex-wrap: wrap;
  `}
`

export const BoxHeader = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`

export const BackButton = styled(({ color, ...rest }) => <Base {...rest} />)`
  padding: 5px;
  padding-left: 20px;
  padding-right: 20px;
  height: 15px;
  line-height: 15px;
  width: auto;
  margin-bottom: 20px;
   
  color: ${props => props.theme.colors.textGray};
  background-color: white;
  border: 1px solid ${props => props.theme.colors.textGray};
  border-radius: 2px;
  text-decoration: none;
  font-family: ${props => props.theme.fonts.primary};
  cursor: pointer;

  &:hover {
    background-color: ${props => props.theme.dashboard.colors.grayLight};
    border: 1px solid ${props => props.theme.colors.success};
    color: ${props => props.theme.colors.success};
  }

  &.active {
    background-color: ${props => props.theme.colors.success};
    border: 1px solid ${props => props.theme.colors.success};
    font-weight: bold;
    color: #fff;
  }

 `

export const Column = styled(({ color, ...rest }) => <Base {...rest} />)`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 0;
  box-sizing: border-box;
  border-radius: 5px;
  transition: .2s;
  cursor: pointer;
  position: relative;
  text-decoration: none;
  
  &:not(.active) {
    &:hover {
      opacity: .5;
      background: ${props => props.color};
      > * {
        color: white;
      }
      > img {
        filter: brightness(0) invert(1);
      }
    }
  }
  
  &.active {
    background: ${props => props.color};
    > * {
      color: white;
    }
    > img {
      filter: brightness(0) invert(1);
    }
  }
  
  &:not(.active) {
    &:not(:last-child) {
      &::after {
        content: '';
        width: 1px;
        height: 65%;
        background: #e1e1e1;
        position: absolute;
        top: 10%;
        right: 0;
      }
    }
  }
  
  ${mediaQuery.lessThan('small')`
    flex-basis: 50%;
  `}
`

export const Image = styled.img`
  height: 65px;
  margin-bottom: 25px;
`
