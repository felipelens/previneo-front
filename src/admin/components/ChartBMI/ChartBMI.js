import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import Card from 'admin/components/Card'
import { COLORS } from 'config/constants'
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  LabelList,
  XAxis,
  YAxis,
  Tooltip,
  Cell
} from 'recharts'

function ChartBMI ({ data, intl }) {
  return (
    <Card title={intl.formatMessage({ id: 'admin.dashboard.charts.bmi' })}>
      <ResponsiveContainer width='100%' height={300}>
        <BarChart data={data}>
          <XAxis dataKey='nome' />
          <YAxis />
          <Tooltip />
          <Bar
            dataKey='qntd'
            name='Quantidade'
            fill={COLORS[0]}
            isAnimationActive={false}
          >
            <LabelList
              dataKey='porcentagem'
              position='center'
              fill='black'
              formatter={value => `${value}%`}
            />
            {data.map((entry, index) => (
              <Cell
                key={index}
                fill={COLORS[index % COLORS.length]}
              />
            ))}
          </Bar>
        </BarChart>
      </ResponsiveContainer>
    </Card>
  )
}

ChartBMI.propTypes = {
  /** Dados obrigatórios */
  data: PropTypes.array.isRequired
}

export default injectIntl(ChartBMI)
