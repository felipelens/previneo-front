import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import VerticalSpacer from 'components/VerticalSpacer'
import FormGrid from 'components/FormGrid'
import CustomField from 'components/CustomField'
import ButtonSubmit from 'components/ButtonSubmit'
import * as forms from 'utils/forms'
import { injectIntl } from 'react-intl'
import Resource from 'containers/Resource'

function FormContact ({ intl, ...props }) {
  return (
    <Resource resource='FormContact'>
      {props => (
        <Form
          onSubmit={data => {
            props.create(data)
          }}
          {...props}
        >
          {({ handleSubmit, pristine, isSubmitting, form }) => (
            <form onSubmit={handleSubmit}>
              <Resource
                resource='FormContact'
                onSuccess={() => form.reset()}
              />
              <VerticalSpacer>
                <Field
                  name='nome'
                  label={intl.formatMessage({id: 'form.contact.name'})}
                  id='form_contact_nome'
                  component={CustomField}
                  {...forms.required}
                />
                <FormGrid>
                  <Field
                    name='telefone'
                    label={intl.formatMessage({ id: 'form.contact.phone' })}
                    id='form_contact_telefone'
                    component={CustomField}
                    {...forms.phone}
                    {...forms.required}
                  />
                  <Field
                    name='email'
                    label={intl.formatMessage({ id: 'form.contact.email' })}
                    id='form_contact_telefone'
                    component={CustomField}
                    {...forms.emailRequired}
                  />
                </FormGrid>
                <Field
                  name='assunto'
                  label={intl.formatMessage({ id: 'form.contact.subject' })}
                  id='form_contact_assunto'
                  component={CustomField}
                  {...forms.required}
                />
                <Field
                  name='mensagem'
                  label={intl.formatMessage({ id: 'form.contact.message' })}
                  id='form_contact_message'
                  component={CustomField.Textarea}
                  rows={5}
                  {...forms.required}
                />
                <ButtonSubmit
                  id='submit_contact'
                  disabled={pristine}
                  isSubmitting={isSubmitting}
                />
              </VerticalSpacer>
            </form>
          )}
        </Form>
      )}
    </Resource>
  )
}

FormContact.propTypes = {
  /** Submit Handler */
  onSubmit: PropTypes.func.isRequired,

  /** Formulário ndo processado */
  isSubmitting: PropTypes.bool
}

export default injectIntl(FormContact)
