/**
 * Module dependencies.
 */

import updateObj from 'immutability-helper'
import objectPath from 'object-path'
import createReducer from 'store/utils/createReducer'
import createActionTypes from 'store/utils/createActionTypes'
import { Companies } from 'store/api'

/**
 * Action Types.
 */

export const actionTypes = createActionTypes('tree', [
  'REGISTER_RESOURCE',

  'RESET',

  'CREATE',
  'CREATE_PENDING',
  'CREATE_FULFILLED',
  'CREATE_REJECTED',

  'FETCH',
  'FETCH_PENDING',
  'FETCH_FULFILLED',
  'FETCH_REJECTED',

  'UPDATE',
  'UPDATE_PENDING',
  'UPDATE_FULFILLED',
  'UPDATE_REJECTED',

  'REMOVE',
  'REMOVE_PENDING',
  'REMOVE_FULFILLED',
  'REMOVE_REJECTED'
])

/**
 * Initial State.
 */

export const treeState = {
  nodes: {
    0: {
      id: 0,
      name: 'Root',
      children: [],
      loaded: false,
      loading: false,
      removing: false
    }
  },
  error: {},
  isSubmitting: false,
  isFetching: false,
  isRemoving: false
}

/**
 * Reducer.
 */

export default createReducer({}, {
  [actionTypes.REGISTER_RESOURCE] (state, { payload: { resource } }) {
    return updateObj(state, {
      [resource]: { $set: { ...treeState } }
    })
  },

  /**
   * Reset.
   */

  [actionTypes.RESET] (state, { payload: { resource } }) {
    return updateObj(state, {
      [resource]: { $set: { ...treeState } }
    })
  },

  /**
   * Fetch Nodes.
   */

  [actionTypes.FETCH_PENDING] (state, { meta: { parentId } }) {
    return updateObj(state, {
      nodes: {
        [parentId]: {
          loading: { $set: true }
        }
      },
      isFetching: { $set: true }
    })
  },

  [actionTypes.FETCH_FULFILLED] (state, { payload, meta: { parentId } }) {
    return updateObj(state, {
      nodes: {
        [parentId]: {
          children: { $set: payload.map(n => n.id) },
          loading: { $set: false },
          loaded: { $set: true }
        },
        $merge: payload.reduce((memo, value) => {
          memo[value.id] = {
            ...value,
            loading: false,
            loaded: false,
            removing: false
          }
          return memo
        }, {})
      },
      isFetching: { $set: false },
      error: { $set: {} }
    })
  },

  [actionTypes.FETCH_REJECTED] (state, { payload, meta: { parentId } }) {
    return updateObj(state, {
      nodes: {
        [parentId]: {
          loading: { $set: false }
        },
        isFetching: { $set: false },
        error: { $set: payload }
      }
    })
  },

  /**
   * Create.
   */

  [actionTypes.CREATE_PENDING] (state) {
    return updateObj(state, {
      isSubmitting: { $set: true }
    })
  },

  [actionTypes.CREATE_FULFILLED] (state, { payload, meta: { parentId } }) {
    return updateObj(state, {
      nodes: {
        [parentId]: {
          children: { $push: [ payload.id ] }
        },
        $merge: {
          ...payload,
          children: [],
          loading: false,
          loaded: true
        },
        isSubmitting: { $set: false },
        error: { $set: {} }
      }
    })
  },

  [actionTypes.CREATE_PENDING] (state, { payload }) {
    return updateObj(state, {
      isSubmitting: { $set: false },
      error: { $set: payload }
    })
  },

  /**
   * Remove.
   */

  [actionTypes.REMOVE_PENDING] (state, { meta: { id } }) {
    return updateObj(state, {
      nodes: {
        [id]: {
          removing: { $set: true }
        }
      },
      isRemoving: { $set: true }
    })
  },

  [actionTypes.REMOVE_FULFILLED] (state, { meta: { id, parentId } }) {
    return updateObj(state, {
      nodes: {
        [parentId]: {
          children: { $apply: values => values.filter(i => i !== id) }
        },
        [id]: { $set: undefined }
      },
      isRemoving: { $set: false },
      error: { $set: {} }
    })
  },

  [actionTypes.REMOVE_REJECTED] (state, { payload, meta: { id } }) {
    return updateObj(state, {
      nodes: {
        [id]: {
          removing: { $set: false }
        }
      },
      isRemoving: { $set: false },
      error: { $set: payload }
    })
  }
})

/**
 * Action Creators.
 */

export const registerResource = resource => (dispatch, getState) => {
  const state = getState()
  if (state.tree.hasOwnProperty(resource)) return
  return dispatch({
    type: actionTypes.REGISTER_RESOURCE,
    payload: { resource }
  })
}

export const reset = resource => ({
  type: actionTypes.RESET,
  payload: { resource }
})

export const fetchNodes = parentId => ({
  type: actionTypes.FETCH,
  payload: Companies.fetchNodes(parentId),
  meta: {
    parentId
  }
})

export const create = (config, data) => ({
  type: actionTypes.CREATE,
  payload: Companies.create(data),
  meta: {
    resource: config.resource,
    parentId: data.parent.id
  }
})

export const remove = (id, parentId) => ({
  type: actionTypes.REMOVE,
  payload: Companies.remove(id),
  meta: {
    id,
    parentId
  }
})

/**
 * Selectors.
 */

export const getNodes = (resource, state) =>
  objectPath.get(state.tree, `${resource}.nodes`, {})

export const isSubmitting = (resource, state) =>
  objectPath.get(state.tree, `${resource}.isSubmitting`, false)

export const getError = (resource, state) =>
  objectPath.get(state.tree, `${resource}.error`, {})
