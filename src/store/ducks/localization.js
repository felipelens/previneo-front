/**
 * Module dependencies.
 */

import createReducer from 'store/utils/createReducer'
import createActionTypes from 'store/utils/createActionTypes'
import Storage from 'modules/Storage'
import { actionTypes as actionTypesAuth } from './auth'
import { actionTypes as actionTypesQuestionnaire } from './questionnaire'

/**
 * Action Types.
 */

export const actionTypes = createActionTypes('localization', [
  'SET_LANG'
])

const defaultLanguage = Storage.get('lang') || 'pt'

/**
 * Initial State.
 */

export const initialState = {
  lang: Storage.set('lang', defaultLanguage)
}

/**
 * Reducer.
 */

export default createReducer(initialState, {
  [actionTypes.SET_LANG] (state, { payload }) {
    return { lang: payload.lang }
  },

  [actionTypesAuth.LOGIN_FULFILLED] (state, { payload }) {
    return { lang: Storage.set('lang', payload.data.language) }
  },

  [actionTypesQuestionnaire.FETCH_DATA_FULFILLED] (state, { payload }) {
    return { lang: payload.data.roteiro.idioma.slug }
  }
})

/**
 * Action Creators.
 */

export const setLang = lang => console.log(lang) || ({
  type: actionTypes.SET_LANG,
  payload: { lang: Storage.set('lang', lang) }
})
