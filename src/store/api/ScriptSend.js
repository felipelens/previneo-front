import Request from 'modules/Request'

export default {
  create (data, params) {
    return Request.auth({
      method: 'POST',
      url: `/api/roteiro/${data.script}/send`,
      body: data,
      params: params
    }).then(res => res.body)
  }
}
