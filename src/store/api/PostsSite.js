import createApi from 'store/utils/createApi'

export default createApi('/api/posts', null, true)
