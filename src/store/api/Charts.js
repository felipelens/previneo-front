import Request from 'modules/Request'

export default {
  fetchAll ({ request = '', ...params }) {
    return Request.auth({
      method: 'GET',
      url: '/api/dashboard' + request,
      query: params
    }).then(res => res.body)
  }
}
