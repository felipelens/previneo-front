import Request from '../../modules/Request'

export default {
  fetchAll (params) {
    return Request.auth({
      method: 'GET',
      url: `/api/prontuarios/${params.id}/logs`,
      query: params
    }).then(res => res.body)
  },
  fetchOne (id, params) {
    return Request.auth({
      method: 'GET',
      url: `/api/prontuarios/${id}/logs`,
      query: params
    }).then(res => res.body)
  },
  create (id, data, params) {
    return Request.auth({
      method: 'POST',
      url: `/api/prontuarios/${id}`,
      body: data,
      query: params
    }).then(res => res.body)
  }
}
