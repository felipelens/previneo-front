import Request from 'modules/Request'

export default {
  create ({id, roteiro, session}) {
    return Request.auth({
      method: 'POST',
      url: `/api/emails/send/${id}/${roteiro}/${session}`
    }).then(res => res.body)
  }
}
