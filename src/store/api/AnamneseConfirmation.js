import Request from 'modules/Request'

export default {
  fetchAll ({ script, patient, session }) {
    return Request.api({
      method: 'GET',
      url: `/api/roteiros/confirmation/${script}/${patient}/${session}`
    }).then(res => ({ data: [res.body] }))
  },

  create ({ script, patient }) {
    return Request.api({
      method: 'GET',
      url: `/api/roteiros/confirmation-reset/${script}/${patient}`
    }).then(res => res.body)
  }
}
