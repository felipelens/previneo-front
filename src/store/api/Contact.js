import Request from 'modules/Request'

export default {
  create (data) {
    return Request.api({
      url: '/api/contatos',
      body: data
    })
  }
}
