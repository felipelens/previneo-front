import Request from 'modules/Request'

export default {
  create (data, params) {
    return Request.auth({
      method: 'POST',
      url: '/api/pacientes/export',
      body: data,
      query: params
    }).then(res => res.body)
  },

  fetchAll (params) {
    return Request.auth({
      method: 'GET',
      url: '/api/pacientes/export',
      query: params
    }).then(res => res.body)
  },

  remove (id) {
    return Request.auth({
      method: 'DELETE',
      url: `/api/pacientes/export/${id}`
    }).then(res => res.body)
  }
}
