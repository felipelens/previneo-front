import Request from 'modules/Request'

export default {
  create (id, action) {
    const actions = {
      hold: 'aguardar',
      start: 'iniciar',
      finish: 'finalizar'
    }

    return Request.auth({
      method: 'POST',
      url: `/api/notificacoes/${actions[action]}/${id}`
    }).then(res => res.body)
  },

  fetchOne (id, params) {
    return Request.auth({
      method: 'GET',
      url: `/api/notificacoes/${id}`,
      query: params
    }).then(res => res.body)
  }
}
