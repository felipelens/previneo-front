import Request from 'modules/Request'
import createApi from 'store/utils/createApi'

export default createApi('/api/usuarios', {
  update (id, data, params = {}) {
    return Request.auth({
      method: 'PUT',
      url: params.myAccount ? `/api/usuarios/${id}/minha-conta` : `/api/usuarios/${id}`,
      body: data,
      query: params
    }).then(res => res.body)
  },

  updatePassword (id, data, params) {
    return Request.auth({
      method: 'PUT',
      url: `/api/usuarios/${id}/password`,
      body: data,
      query: params
    }).then(res => res.body)
  }
})
