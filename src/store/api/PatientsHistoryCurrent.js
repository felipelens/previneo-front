import Request from 'modules/Request'

export default {
  fetchOne (id, params) {
    return Request.auth({
      method: 'GET',
      url: `/api/prontuarios/${id}/situacao-atual`,
      query: params
    }).then(res => res.body)
  }
}
