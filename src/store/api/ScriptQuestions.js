import Request from 'modules/Request'

export default {
  fetchOne (id, params) {
    return Request.auth({
      method: 'GET',
      url: `/api/roteiros/${id}/roteiro-versao`,
      query: params
    }).then(res => res.body)
  }
}
