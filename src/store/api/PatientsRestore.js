import Request from 'modules/Request'
import createApi from 'store/utils/createApi'

export default createApi('/api/pacientes', {
  remove (data, params) {
    return Request.auth({
      method: 'PUT',
      url: `/api/pacientes/restaurar`,
      body: data,
      query: params
    }).then(res => res.body)
  }
})
