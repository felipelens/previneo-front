import Request from '../../modules/Request'

export default {
  fetchOne (id, params) {
    return Request.auth({
      method: 'GET',
      url: `/api/prontuarios/${id}`,
      query: params
    }).then(res => res.body)
  },
  create ({id, roteiro, session}) {
    return Request.auth({
      method: 'POST',
      url: `/api/emails/send/${id}/${roteiro}/${session}`
    }).then(res => res.body)
  }
}
