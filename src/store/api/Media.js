import Request from 'modules/Request'
import { form } from 'popsicle'

export default {
  create (file, params = {}) {
    return Request.auth({
      method: 'POST',
      url: '/api/media',
      body: form({ file }),
      query: params
    }).then(res => res.body)
  },

  remove (file) {
    return Promise.resolve({ data: file })
  }
}
