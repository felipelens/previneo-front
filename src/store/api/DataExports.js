import Request from 'modules/Request'

export default {
  create (data, params) {
    return Request.auth({
      method: 'POST',
      url: `/api/exportar`,
      body: data,
      query: params
    }).then(res => res.body)
  }
}
