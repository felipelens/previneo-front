import Request from 'modules/Request'
import { CLIENT_ID, CLIENT_SECRET, GRANT_TYPE } from 'config/constants'
import Storage from 'modules/Storage'

export const getTokenKey = type => `token.${type}`

async function requestToken (username, password, type) {
  const response = await Request.api({
    method: 'POST',
    url: 'api/oauth/token',
    body: {
      username,
      password,
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: GRANT_TYPE,
      provider: type
    }
  })

  const { access_token: token } = response.body

  return Storage.set(getTokenKey(type), token)
}

export default {
  async login (email, password, type) {
    const key = getTokenKey(type)

    if (!Storage.get(key)) {
      await requestToken(email, password, type)
    }

    Storage.set('authType', type)

    return Request.auth({
      url: '/api/usuario'
    })
      .then(res => res.body)
      .catch(err => {
        Storage.remove(key)
        throw err
      })
  }
}
