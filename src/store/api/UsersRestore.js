import Request from 'modules/Request'
import createApi from 'store/utils/createApi'

export default createApi('/api/usuarios', {
  remove (data, params) {
    return Request.auth({
      method: 'PUT',
      url: `/api/usuarios/restaurar`,
      body: data,
      query: params
    }).then(res => res.body)
  }
})
