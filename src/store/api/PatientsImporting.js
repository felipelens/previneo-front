import Request from 'modules/Request'

export default {
  create (data, params) {
    return Request.auth({
      method: 'POST',
      url: `/api/pacientes/import`,
      body: data,
      query: params
    }).then(res => res.body)
  }
}
