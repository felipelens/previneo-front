import Request from 'modules/Request'
import createApi from 'store/utils/createApi'

export default createApi('/api/anamnese', methods => ({
  fetchOne: methods.fetchOne,

  create (hash, data) {
    if (!data) {
      return Request.auth({
        method: 'GET',
        url: `/api/roteiros/${hash}/anamnese`
      }).then(res => res.body)
    }

    return Request.api({
      method: 'POST',
      url: `/api/roteiros/${hash}/pacientes-anamneses`,
      body: data
    }).then(res => res.body)
  }
}))
