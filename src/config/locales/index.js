import en from './en'
import es from './es'
import pt from './pt'

export default {
  'en': en,
  'es': es,
  'pt': pt
}
