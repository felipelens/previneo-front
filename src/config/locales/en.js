export default {
  'pages.home.title': 'PreviNEO - Revolution in preventing and fighting cancer',
  'pages.home.description': 'PreviNEO brings together technology and scientific knowledge in early diagnosis and prevention of the main types of cancer found in Brazil.',
  'pages.about.title': 'Who we are - PreviNEO',
  'pages.about.description': 'We work to prevent, diagnose and reduce the risks of incidence of the main types of cancer found in Brazil, by way of an oncology program and state of the art technology.',
  'pages.specialities.title': 'Specialities - PreviNEO',
  'pages.specialities.description': 'At PreviNEO, we undertake specialist work in preventing, diagnosing and reducing the incidence risk in Prostate, Breast, Lung, Colon and Cervix cancer.',
  'pages.specialities.titleDescription': 'In order to raise awareness about cancer prevention, you will find more information regarding this type of cancer below.<br />',
  'pages.customers.title': 'Customers - PreviNEO',
  'pages.customers.description': 'At PreviNEO, we count on partner companies that make our benefits available to employees and their families. Access and meet who is with us in this fight!',
  'pages.blog.title': 'Blog - PreviNEO',
  'pages.blog.description': 'Information is a decisive factor in early cancer diagnostic and effective prevention . Our Blog makes available data and news about our cause, do visit!',
  'pages.contact.title': 'Contact data - PreviNEO',
  'pages.contact.description': 'In order to settle doubts, send in suggestions or join us and become a partner, send us a message. Our team will be pleased to address you!',

  /* Títulos dos links do menu */
  'site.menu.home': 'Home',
  'site.menu.about': 'Who we are',
  'site.menu.specialities': 'Specialities',
  'site.menu.customers': 'Customers',
  'site.menu.blog': 'Blog',
  'site.menu.contact': 'Contact data',

  /* 404 */
  'site.notFound.title': 'Page not found',
  'site.notFound.message': '404 Page not found!',

  /* Login */
  'admin.forms.login.title': 'Login',
  'admin.forms.login.email': 'Email',
  'admin.forms.login.password': 'Password',
  'admin.forms.login.button': 'Login',
  'admin.forms.login.passwordRecovery': 'I forgot my password',

  /* Password recovery */
  'admin.forms.passwordRecovery.title': 'Password recovery',
  'admin.forms.passwordRecovery.button': 'Recover',

  /* Hero Home */
  'section.home.hero.title': 'Revolution in <strong> preventing </strong> and <strong> fighting </strong> cancer.',
  'section.home.hero.button': 'Learn more',

  /* Section Services Home */
  'section.home.services.title': 'Technology and scientific knowledge in favor of a greater asset - <strong> life. </strong>',
  'section.home.services.description': '<p>PreviNEO acts through prevention and <strong> execution of early diagnostic </strong> for the 5 main types of cancer occurring in Brazil, and that correspond to a significant volume of breast, prostate, lung, colon and cervix cancer cases.</p><p><strong> For us, people come first</strong>, we offer solutions focused in each patient with the fundamental objective of promoting health for a growing number of people.</p>',

  /* Section Steps Home */
  'section.home.steps.title': '<strong>PreviNEO\'s</strong> work is done in the following steps',
  'section.home.steps.stepone.step': 'Step 1',
  'section.home.steps.stepone.title': 'Identification',
  'section.home.steps.stepone.description': 'Online or on site anamnesis.',
  'section.home.steps.steptwo.step': 'Step 2',
  'section.home.steps.steptwo.title': 'Tracking',
  'section.home.steps.steptwo.description': 'Use of algorithms in stratifying risk populations.',
  'section.home.steps.stepthree.step': 'Step 3',
  'section.home.steps.stepthree.title': 'Guidance',
  'section.home.steps.stepthree.description': 'Getting in touch and guidance.',
  'section.home.steps.stepfour.step': 'Step 4',
  'section.home.steps.stepfour.title': 'Monitoring',
  'section.home.steps.stepfour.description': 'Drafting of risk reduction strategies.',

  /* Section customers Home */
  'section.home.customers.title': 'Customers',
  'section.home.customers.description': 'Our prevention programs already make a difference on the day to day of companies that, like us, place people\'s health and well-being in first place.',
  'section.home.customers.button': 'Check out which',

  /* Section Depoimentos Home */
  'section.home.testmonies.title': 'Our customers say it better than we do. ',
  'section.home.testmonies.internal.title': 'Nothing speaks louder than the <strong> approval </strong> of people who already know',
  'section.home.testmonies.internal.subtitle': 'Next to this, there are testimonials from <strong> our partners </strong>.',

  /* Section Blog Home */
  'section.home.blog.title': 'Our Blog',
  'section.home.blog.button': 'Check it all out',

  /* Section Parceiros */
  'section.partner.title': 'Be a PreviNEO partner',
  'section.partner.description': 'Let\'s join forces? Together, we can continually reduce mortality from cancer in Brazil.',
  'section.partner.button': 'Speak to us',

  /* Hero About */
  'section.about.hero.title': 'An oncology program to <strong> prevent </strong> and <strong> diagnose </strong>.',
  'section.about.hero.description': 'At PreviNEO, we work in preventing, diagnosing and reducing risks of occurrence of the main types of cancer occurring in Brazil, through an oncology program and cutting edge technology. Our mission is a simple one: promote health and reduce the incidence and cancer related mortality in the country. To this end, we act on the types of cancer with higher incidence in Brazil, with a view to achieving a positive and deep impact on the national quality of life.',
  'section.about.hero.button': 'Learn more',

  /* Section Steps About */
  'section.about.steps.title': 'How does the <strong> oncology </strong> program work?',
  'section.about.steps.description': 'Our action method consists in three main steps: data analysis, risk calculation and guidance. Thus, you will be able to access a fully personalized, complete service and diagnosis range. <strong> Check out the step by step </strong>.',
  'section.about.steps.stepone.step': 'Step 1',
  'section.about.steps.stepone.description': 'On site or off site execution of the anamnesis.',
  'section.about.steps.steptwo.step': 'Step 2',
  'section.about.steps.steptwo.description': 'Stratification of the risk population through the oncology program.',
  'section.about.steps.stepthree.step': 'Step 3',
  'section.about.steps.stepthree.description': 'Contact and guidance in establishing risk reduction strategies.',
  'section.about.steps.stepfour.step': 'Step 4',
  'section.about.steps.stepfour.description': 'Scheduling exams and appointments with clinicians.',
  'section.about.steps.stepfive.step': 'Step 5',
  'section.about.steps.stepfive.description': 'Monitoring of the risk reduction strategy.',

  /* Section Map About */
  'section.about.map.title': 'The <strong> first </strong> and only in Brazil!',
  'section.about.map.description': '<p>Established in <strong>2015</strong>, PreviNEO is Brazil\'s <strong>first</strong> and only company to work with a prevention program in oncology for prevention and diagnosis of cancer. Throughout our trajectory, we address over 48 thousand people in 22 companies.</p><p> Among the people reached through our partners, we have identified over <strong>5 thousand cases of patients at risk</strong>. We want to continue growing and taking information and quality of life to as <strong>many people as possible</strong>.</p>',

  /* Section Frase About */
  'section.phrase.title': 'Together in <strong>reducing cancer-related <br />deaths</strong> in Brazil.',

  /* Hero Specialities */
  'section.specialities.hero.title': '<strong>Prevention</strong> that saves lives!',
  'section.specialities.hero.description': '<p>In fighting cancer, we work mainly with prevention and <strong>early diagnostic, resulting in customized strategies to reduce the incidence, late diagnosis and death from the disease.</strong></p><p> The objective is always the same: promote health throughout Brazil. For this reason, we act in the five main types of cancer present in Brazilians with a view to <strong>reducing mortality and costs through these diagnostics in Brazil.</strong></p>',

  /* Section Specialities */
  'section.specialities.types.title': 'The 5 main types of cancer',
  'section.specialities.types.subtitle': 'At PreviNEO, we specialize in working with Prostate, Breast, Lung, Colon and Uterus cancer.',

  /* Hero Customers */
  'section.customers.hero.title': 'We are not alone!',
  'section.customers.hero.description': 'In order to take advantage of the benefits of prevention and early diagnostic of cancer, we count on our partner companies that place our benefits at the disposal of their staff and family members. Thus, peace of mind and health reach increasingly farther, reinforcing the battle for life!',

  /* Section customers */
  'section.customers.list.title': 'Meet our <strong>customers</strong>!',

  /* Contact data */
  'section.contact.title': 'Contact us',
  'section.contact.subtitle': 'In order to clarify doubts, send suggestions or praise, and become a partner, send us a message',

  /* Form Contact data */
  'form.contact.name': 'Name',
  'form.contact.phone': 'Telephone',
  'form.contact.email': 'Email',
  'form.contact.subject': 'Topic',
  'form.contact.message': 'Message',

  /* Admin Usuarios */
  'admin.users.title': 'User',
  'admin.users.title.plural': 'Users',
  'admin.users.form.sendPasswordEmail': 'Send Password by email',
  'admin.users.searchForUsers': 'Search by User',
  'admin.users.deletedUsers': 'Users deleted',
  'admin.common.restore': 'Recover',
  'admin.common.restoreSelected': 'Recover selected',

  /* Names comuns admin */
  'admin.common.name': 'Name',
  'admin.common.email': 'Email',
  'admin.common.emails': 'Emails',
  'admin.common.emailLayout': 'Email Layout',
  'admin.common.emailLayouts': 'Email Layouts',
  'admin.common.emailContent': 'Email content',
  'admin.common.emailContents': 'Email contents',
  'admin.common.password': 'Password',
  'admin.common.confirmPassword': 'Confirm Password',
  'admin.common.oldPassword': 'Former Password',
  'admin.common.company': 'Company',
  'admin.common.accessLevels': 'Access Levels',
  'admin.common.save': 'Save',
  'admin.common.listOf': 'List of',
  'admin.common.register': 'Register',
  'admin.common.edit': 'Edit',
  'admin.common.details': 'Details',
  'admin.common.remove': 'Remove',
  'admin.common.removePermanently': 'Remove permanently',
  'admin.common.back': 'Go back',
  'admin.common.actions': 'Actions',
  'admin.common.warningMessage': 'Do you really want to remove this record?',
  'admin.common.AreYouSureABoutIt': 'Are you sure?',
  'admin.common.confirm': 'Confirm',
  'admin.common.cancel': 'Cancel',
  'admin.common.processing': 'Processing...',
  'admin.common.previous': 'Previous',
  'admin.common.next': 'Next',
  'admin.common.loading': 'Loading...',
  'admin.common.loadMore': 'Load More',
  'admin.common.noResultsFound': 'No results found',
  'admin.common.page': 'Page',
  'admin.common.Of': 'of',
  'admin.common.results': 'Results',
  'admin.common.idNumber': 'Registration number',
  'admin.common.gender': 'Gender',
  'admin.common.genders': 'Genders',
  'admin.common.gender.male': 'Male',
  'admin.common.gender.female': 'Female',
  'admin.common.gender.unisex': 'Unissex',
  'admin.common.genderMale': 'Male',
  'admin.common.genderFemale': 'Female',
  'admin.common.dateOfBirth': 'Date of birth',
  'admin.common.companyPlaceHolder': 'Search Company',
  'admin.common.birthDatePlaceHolder': 'DD/MM/YYYY',
  'admin.common.cpf': 'CPF (Brazilian Taxpayer ID) ',
  'admin.common.weight': 'Weight (kg)',
  'admin.common.height': 'Height (cm)',
  'admin.common.phone': 'Telephone',
  'admin.common.cellphone': 'Mobile',
  'admin.common.acceptContact': 'Accepts contact',
  'admin.common.yes': 'Yes',
  'admin.common.no': 'No',
  'admin.common.cep': 'CEP (Postal Code)',
  'admin.common.cepTooltip': 'Insert your CEP (Postal Code) (Ex 73015-132)',
  'admin.common.country': 'Country',
  'admin.common.state': 'State',
  'admin.common.city': 'City',
  'admin.common.address': 'Address',
  'admin.common.addressNumber': 'Number',
  'admin.common.addressComplement': 'Complement',
  'admin.common.editCompany': 'Edit Company',
  'admin.common.registerCompany': 'Register Company',
  'admin.common.removeMessage': 'Do you really want to remove this company?',
  'admin.common.rootNohNotFound': 'Root node not found!',
  'admin.common.companyName': 'Company Name',
  'admin.common.cnpj': 'CNPJ (Brazilian Corporate Tax ID)',
  'admin.common.parentCompany': 'Parent Company',
  'admin.common.expirationDate': 'Expiry Date',
  'admin.common.logo': 'Logo',
  'admin.common.logoTopRight': 'Logo upper right',
  'admin.common.logoTopLeft': 'Logo upper left',
  'admin.common.logoBottomRight': 'Logo lower right',
  'admin.common.logoBottomLeft': 'Logo lower left',
  'admin.common.openURL': 'URL Open',
  'admin.common.restrictedURL': 'URL with login',
  'admin.common.slugURL': 'Slug (Name after URL)',
  'admin.common.select': 'Select',
  'admin.common.typeSomethingToSearch': 'Type in to search',
  'admin.common.dateInsert': 'Insert a date',
  'admin.common.createdAt': 'Created on',
  'admin.common.fileImport': 'Import file',
  'admin.common.downloadModel': 'Download template',
  'admin.common.import': 'Import',
  'admin.common.export': 'Export',
  'admin.common.reportQueue': 'Report queue',
  'admin.common.title': 'Name',
  'admin.common.status': 'Status',
  'admin.common.lastUpdate': 'Latest update',
  'admin.common.download': 'Download',
  'admin.common.searchForCompany': 'Search Company',
  'admin.common.men': 'Men',
  'admin.common.women': 'Women',
  'admin.common.script': 'Script',
  'admin.common.version': 'Version',
  'admin.common.risk': 'Risk',
  'admin.common.risks': 'Risks',
  'admin.common.question': 'Question',
  'admin.common.answer': 'Answer',
  'admin.common.calculation': 'Calculation',
  'admin.common.interval': 'Interval',
  'admin.common.start': 'Beginning',
  'admin.common.end': 'End',
  'admin.common.selectPatient': 'Select patients manually',
  'admin.common.high': 'High',
  'admin.common.medium': 'Medium',
  'admin.common.low': 'Low',
  'admin.common.description': 'Description',
  'admin.common.riskSelect': 'Select a risk',
  'admin.common.observations': 'Observations',
  'admin.common.all': 'All',
  'admin.common.icon': 'Icon',
  'admin.common.categoryColor': 'Category color',
  'admin.common.tuss': 'TUSS',
  'admin.common.today': 'Today',
  'admin.common.visible': 'Visible',
  'admin.common.subtitle': 'Caption',
  'admin.common.specialtyColor': 'Color of specialty',
  'admin.common.image': 'Image',
  'admin.common.requiredFields': 'Mandatory fields',
  'admin.common.optionalFields': 'Optional fields',
  'admin.common.minimumAge': 'Minimum age',
  'admin.common.maximumAge': 'Maximum age',
  'admin.common.active': 'Active',
  'admin.common.inactive': 'Inactive',
  'admin.common.draft': 'Draft version',
  'admin.common.language': 'Language',
  'admin.common.link': 'Link',
  'admin.common.search': 'Search',
  'admin.common.send': 'Send',
  'admin.common.subject': 'Topic',
  'admin.common.content': 'Content',
  'admin.common.staff': 'Team',
  'admin.common.customer': 'Customer',
  'admin.common.customers': 'Customers',
  'admin.common.value': 'Value',
  'admin.common.contactInfo': 'Contact data information',
  'admin.common.contactInfos': 'Contact data information',
  'admin.common.site': 'Site',
  'admin.common.blog': 'Blog',
  'admin.common.post': 'Post',
  'admin.common.posts': 'Posts',
  'admin.common.filter': 'Filter',
  'admin.common.continueReading': 'Read more',
  'admin.common.permalink': 'Permanent Link ',
  'admin.common.category': 'Category',
  'admin.common.categories': 'Categories',
  'admin.common.blogCategory': 'Blog category',
  'admin.common.blogCategories': 'Blog categories',
  'admin.common.tag': 'Tag',
  'admin.common.tags': 'Tags',
  'admin.common.myAccount': 'My account',
  'admin.common.signOut': 'Exit',
  'admin.common.personalData': 'Personal data',
  'admin.common.editPassword': 'Change Password',
  'admin.common.report': 'Report',
  'admin.common.reports': 'Reports',
  'admin.common.patientsFilter': 'Patient filter',
  'admin.common.unavailable': 'Unavailable',
  'admin.common.queue': 'Queue',
  'admin.common.personalInformations': 'Personal data',
  'admin.common.continue': 'Continue',
  'admin.common.dontKnow': 'I don\'t know',
  'admin.common.didNotDoIt': 'I did not do it',
  'admin.common.ethnicity.white': 'White',
  'admin.common.ethnicity.black': 'African descendant',
  'admin.common.ethnicity.brown': 'Mixed race',
  'admin.common.ethnicity.yellow': 'Oriental',
  'admin.common.ethnicity.indigenous': 'Native American indian',
  'admin.common.others': 'Others',
  'admin.common.clickHere': 'Click here',
  'admin.common.result': 'Result',
  'admin.common.recommendations': 'Recommendations',
  'admin.common.recommendedExams': 'Recommended exams',
  'admin.common.justifications': 'Justifications',
  'admin.common.max': 'Maximum',
  'admin.common.min': 'Minimum',
  'admin.common.format': 'Format',
  'admin.common.integer': 'Whole number',
  'admin.common.none': 'None',
  'admin.common.displayBeforeAnamnesis': 'Display before anamnesis',
  'admin.common.group': 'Group',
  'admin.common.author': 'Author',
  'admin.common.date': 'Date',
  'admin.common.selectRiskForEmails': 'Select level of risk desired to trigger alert',
  'admin.common.requiredHistoryMedicalCategories': 'The categories you have selected require all medical records categories',
  'admin.common.emailSent': 'Email sent',
  'admin.common.age': 'Age',

  /* Dashboard */
  'admin.dashboard.charts.total': 'Total',
  'admin.dashboard.charts.ofTotal': 'of the total',
  'admin.dashboard.charts.historyCancer': 'Users with history of cancer events',
  'admin.dashboard.charts.patientsNeedContact': 'Users requiring contact data',
  'admin.dashboard.charts.anamnesisFulfillments': 'Number of anamnesis completed',
  'admin.dashboard.charts.answersByAgeAndGender': 'Volume of respondents by age and gender',
  'admin.dashboard.charts.highRiskByCancerType': 'Volume of High Risk by Type of Cancer',
  'admin.dashboard.charts.bmi': 'BMI',
  'admin.dashboard.charts.examsPending': 'Exams missing',
  'admin.dashboard.charts.riskAssessment': 'Risk Assessment',
  'admin.dashboard.charts.riskAssessmentSufix': 'Number of people identified with high risk',
  'admin.dashboard.charts.highRiskSummary': 'Number of high risks identified',
  'admin.dashboard.charts.highRisk': 'High Risk',
  'admin.dashboard.charts.normalRisk': 'Normal Risk',
  'admin.dashboard.charts.summary': 'Summary',
  'admin.dashboard.charts.totalFilled': 'Number of anamnesis completed',
  'admin.dashboard.charts.totalFilledByGender': 'Number of anamnesis completed by gender',
  'admin.dashboard.charts.totalFilledByAge': 'Number performed by age bracket',
  'admin.dashboard.charts.totalDone': 'Number completed',
  'admin.dashboard.charts.totalForeseen': 'Foreseen',
  'admin.dashboard.charts.highRiskChart': 'HIGH RISK',
  'admin.dashboard.charts.normalRiskChart': 'NORMAL RISK',
  'admin.dashboard.charts.smoker': 'Smoker',

  /* Companys */
  'admin.company.title': 'Company',
  'admin.company.title.plural': 'Companies',

  /* Plano de Saúde */
  'admin.healthInsurance.title': 'Health insurance',
  'admin.healthInsurance.title.plural': 'Health insurance',

  /* Perfil de Acesso */
  'admin.roles.title': 'Access profile',
  'admin.roles.title.plural': 'Access profiles',
  'admin.formRole.permissions': 'Permissions',

  /* Idiomas */
  'admin.languages.title': 'Language',
  'admin.languages.title.plural': 'Languages',
  'admin.languages.slug': 'Slug',

  /* Patients */
  'admin.patients.title': 'Patient',
  'admin.patients.title.plural': 'Patients',
  'admin.patients.listOfPatients': 'List of Patients',
  'admin.patients.importPatients': 'Import Patients',
  'admin.patients.exportPatients': 'Export Patients',
  'admin.patients.exportText': 'Select the filters of your preference and click on \'Export\' to request Patient reports. The time for the document to be ready depends on the queue in the system and the export criteria applied.',
  'admin.patients.fields.gender': 'M or F',
  'admin.patients.fields.cpf': 'Only numbers (05376483649) or formatted (768.884.253-04)',
  'admin.patients.fields.phone': 'Only Numbers (41993241556) or formatted ((41) 99324-1556)',
  'admin.patients.fields.birthday': 'YYYY-MM-DD (Example 1990-06-20)',
  'admin.patients.fields.cep': 'Only numbers (93530280) or formatted (93530-280)',

  /* Prontuário */
  'admin.medicalRecords.title': 'Medical records',
  'admin.medicalRecords.title.plural': 'Medical records',
  'admin.medicalRecords.open': 'Open patient records',
  'admin.medicalRecords.close': 'Close patient records',
  'admin.medicalRecords.noInformation': 'No information available',
  'admin.medicalRecords.gail.risk1': 'Estimated risk of patient developing breast cancer in the next 5 years.',
  'admin.medicalRecords.gail.risk2': 'Risk of general population developing breast cancer in the next 5 years',
  'admin.medicalRecords.gail.risk3': 'Mean risk of patient developing breast cancer by the age of 90 years old',
  'admin.medicalRecords.gail.risk4': 'Mean risk of population developing breast cancer by the age of 90 years old',
  'admin.medicalRecords.madeContact': 'Contact successfully established',
  'admin.medicalRecords.noMoreContact': 'Does not require new contact',
  'admin.medicalRecords.noMoreContactQuestion': 'Requires new contact?',
  'admin.medicalRecords.threeContactsMessage': '3 contact attempts',

  /* Tipos de Câncer */
  'admin.cancerTypes.title': 'Type of Cancer',
  'admin.cancerTypes.title.plural': 'Types of Cancer',

  /* Estratégias */
  'admin.strategies.title': 'Strategy',
  'admin.strategies.title.plural': 'Strategies',

  /* Exames Recomendados */
  'admin.exams.title': 'Exam',
  'admin.exams.title.plural': 'Recommended Exams',

  /* Site */
  'admin.site.title': 'Site',
  'admin.testimonies.title': 'Testimonial',
  'admin.testimonies.title.plural': 'Testimonials',
  'admin.testimonies.author': 'Author of testimonial',
  'admin.specialities.title': 'Specialty',
  'admin.specialities.title.plural': 'Specialties',

  /* Anamnesis, */
  'admin.anamnesis.title': 'Anamnesis',
  'admin.anamnesis.title.plural': 'Anamnesis',

  /* Categories */
  'admin.categories.title': 'Category',
  'admin.categories.title.plural': 'Categories',

  /* Script */
  'admin.script.title': 'Script',
  'admin.script.title.plural': 'Scripts',
  'admin.script.categoryAlreadyAdded': 'Category already registered',
  'admin.script.addCategory': 'Add Category',
  'admin.script.addAnswer': 'Add Answer',
  'admin.script.addQuestion': 'Add Question',
  'admin.script.editQuestion': 'Edit Question',
  'admin.script.editAnswer': 'Edit Answer',
  'admin.script.valueAnswer': 'Value of Answer',
  'admin.script.questionType': 'Type of Question',
  'admin.script.selectTheQuestionType': 'Select type of Question',
  'admin.script.questionTitle': 'Question name',
  'admin.script.questionDescription': 'Description of Question',
  'admin.script.questionTypes.objective': 'Objective',
  'admin.script.questionTypes.discursive': 'Discursive',
  'admin.script.answer': 'Answer',
  'admin.script.question': 'Question',
  'admin.script.category': 'Category',
  'admin.script.status': 'Status of Script',
  'admin.script.requiredRegistration': 'Mandatory registration number',
  'admin.script.requiredPhone': 'Mandatory telephone number',
  'admin.script.requiredCellphone': 'Mandatory mobile number',
  'admin.script.requiredHealthcareInsurance': 'Mandatory health insurance information',
  'admin.script.minimumPeriod': 'Minimum períod to answer this script again (in months)',
  'admin.script.displayResultPageWithRisks': 'Show page with risk results',
  'admin.script.sendResultForEmail': 'Send result by email',
  'admin.script.sendResultForEmailPeriod': 'Timeframe to send results (in days)',
  'admin.script.sendReminderEmail': 'Send reminder by email',
  'admin.script.sendReminderEmailPeriod': 'Timeframe for sending (in days)',
  'admin.script.sendReminderEmailQuantity': 'Number of sends',
  'admin.script.resultPageContent': 'Content of result page',
  'admin.script.initialPageContent': 'Content of landing page',
  'admin.script.displayFooter': 'Show footnotes',
  'admin.script.questionGroupError': 'One group cannot be registered inside other groups',
  'admin.script.additionalInfoPageContent': 'Additional information',
  'admin.script.displayAdditionalInfoPageContent': 'Show additional information',

  /* Disparo de Script */
  'admin.scriptSend.title': 'Trigger Script',
  'admin.scriptSend.exportCsvFile': 'Download CSV file',
  'admin.scriptSend.sendEmail': 'Send email',
  'admin.scriptSend.sentScripts': 'Scripts Triggered',

  /* Membros da Equipe */
  'admin.team.title': 'Team member',
  'admin.team.title.plural': 'Team members',

  /* ValidActions */
  'validations.required': 'Mandatory field',
  'validations.integer': 'Number must be a whole number',
  'validations.integerBetween': 'Insert a number between {min} and {max}',
  'validations.email': 'Not a valid email',
  'validations.phone': 'Not a valid telephone number',
  'validations.cpf': 'CPF (Brazilian Taxpayer ID)  not valid',
  'validations.cnpj': 'CNPJ (Brazilian Corporate Tax ID) not valid',
  'validations.cep': 'CEP (Postal Code) not valid',
  'validations.date': 'Date not valid',
  'validations.password': 'Must contain at least 8 characters including numbers, capital and lower case letters',
  'validations.passwordConfirm': 'Passwords must be the same',
  'validations.fileType': 'Type of file not supported',

  /* Feedback */
  'feedback.success': 'Action successfully executed!',
  'feedback.error': 'Oops! There has been a problem!',

  /* Field Hints */
  'hints.cpf': 'Insert your CPF (Brazilian Taxpayer ID) with or without punctuation',
  'hints.cpfNumberOnly': 'Insert your CPF (Brazilian Taxpayer ID)  without punctuation (numbers only)',
  'hints.number': 'Only numbers',
  'hints.email': 'Insert your Email (Ex: example@example.com)',
  'hints.height': 'Inform your height in centimeters (cm)',
  'hints.weight': 'Insert your weight em kilograms (kg)',
  'hints.dateOfBirth': 'The date must be in format DD/MM/YYYY (example 25/10/1950)',

  /* Anamnesis, */
  'q.startPrevention': 'Start filling in',
  'q.backToAnamnese': 'Return to anamnesis',
  'q.wizardStep': 'Step {progress} of {count}',
  'q.acceptContact': 'I accept contact by Telephone or Email',
  'q.termsAccept': 'I accept the ',
  'q.termsOfService': 'Service Terms',
  'q.termsAndConditions': 'Terms and Conditions',
  'q.acceptRequired': 'The terms must be accepted',
  'q.brca.question': 'Have you ever done a genetic exam (known as BRCA1 or BRCA) to know whether there a change/mutation that might increase your risk of developing cancer and, if so, was the result positive?',
  'q.brca.description': 'Mutation in genes BRCA1 or BRCA2 significatly increase the risk for some types of cancer (breast, ovary, prostate, colon or pancreas). This genetic exam may be performed through blood or saliva samples to find out whether the person has this mutation. This mutation usually occurs in people with cancer history in close family members. It is estimated that approximately 5% to 10% of all breast and ovary cancer cases are caused by this mutation',
  'q.ethnicity.question': 'Ethnic group',
  'q.ethnicity.description': 'People of African descent are known to have an increased risk for prostate cancer ',
  'q.cancerHistory.question': 'Do you have a personal history of cancer?',
  'q.cancerHistory.description': 'It is known that people with prior cancer events have a high risk of relapse or eventually a new tumor of the same type. Because of this, for the purpose of calculating your risk, it is important to establish whether you have had this disease before in your life.',
  'q.cancerHistory.whatCancer': 'What type of cancer?',
  'q.cancerHistory.specifyCancer': 'Specify the type of cancer',
  'q.helloPatient': 'Hello, {name}',
  'q.patientIntroText': 'The time has come to learn about you and your medical history. Please, complete the fields accurately in order for your assessment to be as accurate as possible. Shall we start?',
  'q.startAvaliation': 'Start Assessment',
  'q.profileUnmatch': 'The anamnesis in this campaign do not match your profile. Thank you for helping us this far!',
  'q.finishAvaliation': 'End assessment',
  'q.toUpdateYourData': 'in order to update your personal data',
  'q.stepFinished': 'One more step concluded!',
  'q.youAreDoingRight': 'You are correctly filling in the anamnesis data. Keep it up!',
  'q.continueAvaliation': 'Continue assessment',
  'q.toAnswerTheQuestions': 'in order to answer the questions',
  'q.thanks': 'Thank you for undertaking this assessment!',
  'q.sentForEmail': 'The result has been sent to your email.',
  'q.welcomeUser': 'Welcome, {name}',

  'admin.dashboard.chart.foreseen': 'Foreseen',
  'admin.dashboard.chart.fulfilled': 'Done',
  'terms': '' +
    '<p>' +
    '   <strong>Privacy Policy and Use Authorization</strong>' +
    '</p>' +
    '<p>' +
    '   We are a company dedicated to the promotion of medical information, targeted at the prevention of some types of cancer in Brazil. We do this because we believe that the more information, the greater the chances of an early diagnosis, which is fundamental for cancer prevention.' +
    '</p>' +
    '<p>' +
    '   The form in which we disclose the information is a preformatted form, based on medical literature and nationally and internationally recognized methods to verify the presence of one or more risk factors that increase the estimated risk of developing a pathology. To this end, we require some personal information from you, including your identification data, family history of pathologies as well as data in connection with your daily life.' +
    '</p>' +
    '<p>' +
    '   For example, we will need to know if you smoke or have ever smoked, if you have had sexual relations, and if you have already had any type of preventative testing. These and other information are essential to achieve a correct estimate of the risk of developing the types of cancer applicable to the forms.' +
    '</p>' +
    '<p>' +
    '   <strong>How will your data be used?</strong>' +
    '</p>' +
    '<p>' +
    '   All personal data you provide will be kept in absolute secrecy and will be used only for the purpose of obtaining the result regarding your risk of developing the pathologies tested. This applies both to identification data (such as your name, email address, age, etc.) as well as to the answers to the forms, i.e. your medical records or daily life data. We will use your email exclusively for the purpose of sending the results and will not pass this information on to any third parties or use it for any other purpose.' +
    '</p>' +
    '<p>' +
    '   <strong>Who has access to my data?</strong>' +
    '</p>' +
    '<p>' +
    '   In order to obtain the results, your answers will be analyzed automatically and checked manually by employees of Previneo, linked to the healthcare area. The tool was developed by doctors, but the analysis of the data is not done by them, since we do not provide healthcare services, but only disclosure of information. Our employees are committed to the confidentiality of the data and our privacy policy.        ' +
    '</p>' +
    '<p>' +
    '   If you are participating in this evaluation as part of your employer\'s health program, the responsible physicians (and only the physicians) of your employer may have access to the results in full and absolute respect to the Code of Medical Ethics.' +
    '</p>' +
    '<p>' +
    '   <strong>What if I authorize the use of the data in the form of statistics?</strong>' +
    '</p>' +
    '<p>' +
    '   By completing our form, we ask that in addition to accepting our terms of use, you also agree that the respective results may constitute a database for the establishment of prevention policies. After all, in addition to providing adequate information, our tool can also serve to identify flaws or necessary corrections in cancer diagnosis methods in Brazil.' +
    '</p>' +
    '<p>' +
    '   In this case, if you agree, the results obtained in completing your form can be used to comprise statistics without any personal identification or sensitive data. We will maintain absolute secrecy in relation to these data.</p>' +
    '<p>' +
    '   For example, if you are 20 years old, do not smoke and live in Curitiba-PR, your risk of developing lung cancer will be considered normal for your age and condition. In this case, your result will compose the statistic as a normal risk person for the development of this pathology, without any form of identification, only linked to the region where you reside.' +
    '</p>' +
    '<p>' +
    '   Any doubts or clarifications can be previously settled through our email <strong>contato@previneo.com.br</strong>.        ' +
    '</p>',
  'admin.dashboard.chart.not.fulfilled': 'Not done',

  'admin.patientsHistory.currentSituation': 'Current Situation',
  'admin.common.nextContact': 'Next Contact',
  'admin.common.contactReason': 'Contact Reason',
  'admin.common.typesCancer': 'Types of Cancer',
  'admin.common.patientHistoryExams': 'Exams Performed',
  'admin.common.saveContact': 'Save Contact',
  'admin.patientsHistory.status.pending': 'Start',
  'admin.patientsHistory.status.inProgress': 'Pause',
  'admin.patientsHistory.status.done': 'Done',
  'admin.common.contactMade': 'Contacted?',
  'admin.common.contactMadeOk': 'Yes',
  'admin.common.contactMadeNo': 'No',
  'admin.common.contactStatus': 'Status',
  'admin.common.contactStatuses': 'Status',
  'admin.common.managePatientHistory': 'Contact',
  'admin.common.ethnicity': 'Ethnic data',
  'admin.common.patientInfo': 'Patient data',
  'admin.patients.deletedPatients': 'Patients removed',
  'admin.common.motivo': 'Reason',
  'admin.common.hidePatientInfo': 'Hide Contact Information',
  'admin.common.displayPatientInfo': 'Display Contact Information',
  'admin.common.noPermission': 'You don\'t have permission to access this page',
  'admin.patientsHistory.titlePlural': 'Medical Records',
  'admin.patientsHistory.contact': 'Contact',
  'admin.patientsHistory.contactReasonPlural': 'Reasons for contact',

  'admin.common.logoRight': 'Logo right',
  'admin.common.logoLeft': 'Logo left',
  'admin.common.justifications.title': 'Justification',
  'admin.common.justifications.title.plural': 'Justifications',
  'admin.common.file': 'File',
  'admin.common.fileFront': 'Front file',
  'admin.common.roles': 'Access profiles',
  'admin.common.ageRange': 'Age Bracket',
  'admin.permissions.title.plural': 'Permissions',
  'admin.permissions.title': 'Permission',
  'admin.justifications.title': 'Justification',
  'admin.justifications.title.plural': 'Justifications',
  'admin.faixasimc.abaixo': 'Below weight',
  'admin.faixasimc.normal': 'Normal weight',
  'admin.faixasimc.acima': 'Above weight',
  'admin.faixasimc.obesidadeI': 'Obesity I',
  'admin.faixasimc.obesidadeII': 'Obesity II',
  'admin.faixasimc.obesidadeIII': 'Obesity III',
  'admin.common.lastAnamnesis': 'Date of anamnesis',
  'admin.common.reSendEmail': 'Resend Email',
  'admin.common.normal': 'Normal',
  'admin.common.entity': 'Entity',
  'admin.common.entities': 'Entities',
  'admin.common.numberOfEmployees': 'Number of Employees',
  'admin.common.confirmation': 'Confirm',
  'admin.common.confirmationNotFound': 'Record not found. Click the button bellow to receive another confirmation link',

  'admin.common.editUser': 'Edit User'

}
