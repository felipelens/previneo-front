export default {
  'pages.home.title': 'PreviNEO - Revolución en la prevención y lucha contra el cáncer',
  'pages.home.description': 'PreviNEO reúne la tecnología y el conocimiento científico en el diagnóstico precoz y la prevención de los principales tipos de cáncer encontrados en Brasil',
  'pages.about.title': 'Quienes somos - PreviNEO',
  'pages.about.description': 'Trabajamos para prevenir, diagnosticar y reducir los riesgos de incidencia de los principales tipos de cáncer encontrados en Brasil, a través de un programa de oncología y tecnología de punta.',
  'pages.specialities.title': 'Especialidades - PreviNEO',
  'pages.specialities.titleDescription': 'Abaixo, você encontra mais informações em torno das causas e sintomas deste tipo de câncer, com o objetivo de conscientizar sobre a prevenção dessa doença.<br />',
  'pages.specialities.description': 'En PreviNEO, llevamos a cabo un trabajo especializado para prevenir, diagnosticar y reducir el riesgo de incidencia en cáncer de próstata, mama, pulmón, colon y cuello uterino',
  'pages.customers.title': 'Clientes - PreviNEO',
  'pages.customers.description': 'En PreviNEO, contamos con compañías asociadas que ponen nuestros beneficios a disposición de los empleados y sus familias. ¡Acceda y conozca quién está con nosotros en esta lucha!',
  'pages.blog.title': 'Blog - PreviNEO',
  'pages.blog.description': 'La información es un factor decisivo en el diagnóstico precoz y la prevención efectiva del cáncer . Nuestro blog pone a disposición datos y noticias sobre nuestra causa, ¡visite!',
  'pages.contact.title': 'Contacto - PreviNeo',
  'pages.contact.description': 'Para resolver dudas, enviar sugerencias o unirse y hacerse socio, envíenos un mensaje. ¡Nuestro equipo estará encantado de dirigirnos a usted!',

  /* Títulos dos links do menu */
  'site.menu.home': 'Home',
  'site.menu.about': 'Quienes somos ',
  'site.menu.specialities': 'Especialidades',
  'site.menu.customers': 'Clientes',
  'site.menu.blog': 'Blog',
  'site.menu.contact': 'Contacto',

  /* 404 */
  'site.notFound.title': 'Página no encontrada',
  'site.notFound.message': '404 ¡Página no encontrada!',

  /* Login */
  'admin.forms.login.title': 'Login',
  'admin.forms.login.email': 'Email',
  'admin.forms.login.password': 'Contraseña',
  'admin.forms.login.button': 'Login',
  'admin.forms.login.passwordRecovery': 'Me olvidé mi contraseña',

  /* Password recovery */
  'admin.forms.passwordRecovery.title': 'Recuperación de contraseña',
  'admin.forms.passwordRecovery.button': 'Recuperar',

  /* Hero Home */
  'section.home.hero.title': 'Revolución en la <strong>prevención</strong> y <strong>lucha</strong> contra el cáncer',
  'section.home.hero.button': 'Sepa más',

  /* Section Services Home */
  'section.home.services.title': 'Tecnología y conocimiento científico a favor de un activo mayor - <strong>la vida.</strong>',
  'section.home.services.description': '<p>PreviNEO actúa a través de la prevención y la <strong>ejecución del diagnóstico</strong> precoz para los 5 tipos principales de cáncer que se producen en Brasil, y que corresponden a un volumen significativo de casos de cáncer de mama, próstata, pulmón, colon y cuello uterino.</p><p><strong> Para nosotros, las personas son lo primero</strong>, ofrecemos soluciones enfocadas en cada paciente con el objetivo fundamental de promover la salud para un número creciente de personas.</p>',

  /* Section Steps Home */
  'section.home.steps.title': 'El trabajo de <strong>PreviNEO</strong> se realiza en los siguientes pasos',
  'section.home.steps.stepone.step': 'Paso 1',
  'section.home.steps.stepone.title': 'Identificación',
  'section.home.steps.stepone.description': 'Anamnesis en el sitio o por internet.',
  'section.home.steps.steptwo.step': 'Paso 2',
  'section.home.steps.steptwo.title': 'Acompañamiento',
  'section.home.steps.steptwo.description': 'Uso de algoritmos para estratificar poblaciones de riesgo.',
  'section.home.steps.stepthree.step': 'Paso 3',
  'section.home.steps.stepthree.title': 'Orientación',
  'section.home.steps.stepthree.description': 'Contacto y orientación.',
  'section.home.steps.stepfour.step': 'Paso 4',
  'section.home.steps.stepfour.title': 'Monitoreo.',
  'section.home.steps.stepfour.description': 'Redacción de estrategias de reducción de riesgos.',

  /* Section customers Home */
  'section.home.customers.title': 'Clientes',
  'section.home.customers.description': 'Nuestros programas de prevención ya marcan la diferencia en el día a día de las empresas que, como nosotros, colocan la salud y el bienestar de las personas en primer lugar.',
  'section.home.customers.button': 'Vea quales',

  /* Section Depoimentos Home */
  'section.home.testmonies.title': 'Nuestros clientes lo dicen mejor que nosotros.',
  'section.home.testmonies.internal.title': 'Nada habla más fuerte que la <strong>aprobación</strong> de personas que ya saben',
  'section.home.testmonies.internal.subtitle': 'Junto a esto, hay testimonios de <strong>nuestros asociados</strong>.',

  /* Section Blog Home */
  'section.home.blog.title': 'Nuestro Blog',
  'section.home.blog.button': 'Vea lo todo',

  /* Section Parceiros */
  'section.partner.title': 'Sé un asociado de PreviNEO',
  'section.partner.description': 'Vamos a unir fuerzas? Juntos, podemos reducir continuamente la mortalidad por cáncer en Brasil.',
  'section.partner.button': 'Hable con nosotros',

  /* Hero About */
  'section.about.hero.title': 'Un programa de oncología para <strong>prevenir</strong> y <strong>diagnosticar</strong>.',
  'section.about.hero.description': 'En PreviNEO, trabajamos para prevenir, diagnosticar y reducir los riesgos de incidencia de los principales tipos de cáncer que ocurren en Brasil, a través de un programa de oncología y tecnología de punta. Nuestra misión es simple: promover la salud y reducir la incidencia y la mortalidad relacionada con el cáncer en el país. Para ello, actuamos sobre los tipos de cáncer con mayor incidencia en Brasil, con el objetivo de lograr un impacto positivo y profundo en la calidad de vida nacional.',
  'section.about.hero.button': 'Sepa más',

  /* Section Steps About */
  'section.about.steps.title': '¿Cómo funciona el programa de <strong>oncología</strong>?',
  'section.about.steps.description': 'Nuestro método de acción consiste en tres pasos principales: análisis de datos, cálculo de riesgos y orientación. Por lo tanto, usted podrá acceder a un servicio y rango de diagnóstico personalizado y completo. <strong>Mira el paso a paso</strong>.',
  'section.about.steps.stepone.step': 'Paso 1',
  'section.about.steps.stepone.description': 'Anamnesis en el sitio o por internet.',
  'section.about.steps.steptwo.step': 'Paso 2',
  'section.about.steps.steptwo.description': 'Estratificación de las poblaciones de riesgo por el programa de oncología.',
  'section.about.steps.stepthree.step': 'Paso 3',
  'section.about.steps.stepthree.description': 'Contacto y orientación em establecimiento de las estrategias de reducción de riesgos.',
  'section.about.steps.stepfour.step': 'Paso 4',
  'section.about.steps.stepfour.description': 'Programar exámenes y citas con los médicos.',
  'section.about.steps.stepfive.step': 'Paso 5',
  'section.about.steps.stepfive.description': 'Monitoreo de las estrategias de reducción de riesgos.',

  /* Section Map About */
  'section.about.map.title': '¡El <strong>primer y único</strong> em Brasil!',
  'section.about.map.description': '<p>Establecida en <strong>2015</strong>, PreviNEO es la <strong>primera y única</strong> empresa de Brasil a trabajar con un programa en oncología para la prevención y el diagnóstico del cáncer. A lo largo de nuestra trayectoria, tratamos más de 48 mil personas en 22 compañías.</p><p> Entre las personas a las que se llegó a través de nuestros asociados, hemos identificado más de <strong>5 mil casos de pacientes en riesgo</strong>. Queremos seguir creciendo y llevando información y calidad del <strong>mayor número de personas posible</strong>.',

  /* Section Frase About */
  'section.phrase.title': 'Juntos en la <strong>reducción</strong> de las <br /> muertes relacionadas con el cáncer en Brasil.',

  /* Hero Specialities */
  'section.specialities.hero.title': '¡<strong>Prevención</strong> que salva vidas!',
  'section.specialities.hero.description': '<p>En la lucha contra el cáncer, trabajamos principalmente con la prevención y el <strong>diagnóstico precoz, lo que resulta en estrategias personalizadas para reducir la incidencia, diagnósticos tardíos o la muerte por la enfermedad.</strong></p><p> El objetivo es siempre el mismo: promover la salud en todo Brasil. Por esta razón, actuamos en los cinco tipos principales de cáncer presentes en los brasileños con el objetivo de <strong>reducir la mortalidad y los costos a través de estos diagnósticos en Brasil.</strong></p>',

  /* Section Specialities */
  'section.specialities.types.title': 'Los cinco tipos principales de cáncer',
  'section.specialities.types.subtitle': 'En PreviNEO, nos especializamos en trabajar com cáncer de próstata, mama, pulmón, colon y útero.',

  /* Hero Customers */
  'section.customers.hero.title': '¡<strong>No</strong> estamos <strong>solos</strong>!',
  'section.customers.hero.description': 'Para aprovechar los beneficios de la prevención y el <strong>diagnóstico precoz</strong> del cáncer, contamos con nuestras compañías asociadas que otorgan los beneficios a su personal y sus familiares. ¡Por lo tanto, la paz mental y la salud se extienden cada vez más, reforzando la batalla por la vida!',

  /* Section customers */
  'section.customers.list.title': '¡Conozca a nuestros <strong>clientes</strong>!',

  /* Contact data */
  'section.contact.title': 'Contáctenos',
  'section.contact.subtitle': 'Para aclarar dudas, enviar sugerencias o elogios, y asociarse, envíenos un mensaje',

  /* Form Contact data */
  'form.contact.name': 'Nombre',
  'form.contact.phone': 'Teléfono',
  'form.contact.email': 'Correo electrónico',
  'form.contact.subject': 'Tema',
  'form.contact.message': 'Mensaje',

  /* Admin Usuarios */
  'admin.users.title': 'Usuario',
  'admin.users.title.plural': 'Usuarios',
  'admin.users.form.sendPasswordEmail': 'Enviar contraseña por correo electrónico',
  'admin.users.searchForUsers': 'Búsqueda por usuario',
  'admin.users.deletedUsers': 'Usuarios eliminados',
  'admin.common.restore': 'Recuperar',
  'admin.common.restoreSelected': 'Recuperar seleccionado',

  /* Names comuns admin */
  'admin.common.name': 'Nombre',
  'admin.common.email': 'Correo electrónico',
  'admin.common.emails': 'Correo electrónicos',
  'admin.common.emailLayout': 'Diseño de correo electrónico',
  'admin.common.emailLayouts': 'Diseños de correo electrónico',
  'admin.common.emailContent': 'Contenido de correo electrónico',
  'admin.common.emailContents': 'Contenidos de correo electrónico',
  'admin.common.password': 'Contraseña',
  'admin.common.confirmPassword': 'Confirmar contraseña',
  'admin.common.oldPassword': 'Contraseña anterior',
  'admin.common.company': 'Empresa',
  'admin.common.accessLevels': 'Niveles de acceso',
  'admin.common.save': 'Salvar',
  'admin.common.listOf': 'Lista de',
  'admin.common.register': 'Registro',
  'admin.common.edit': 'Editar',
  'admin.common.details': 'Detalles',
  'admin.common.remove': 'Retirar',
  'admin.common.removePermanently': 'Retirar permanentemente',
  'admin.common.back': 'Volver',
  'admin.common.actions': 'Acciones',
  'admin.common.warningMessage': '¿Realmente quieres eliminar este registro?',
  'admin.common.AreYouSureABoutIt': '¿Estás seguro?',
  'admin.common.confirm': 'Confirmar',
  'admin.common.cancel': 'Cancelar',
  'admin.common.processing': 'Procesando',
  'admin.common.previous': 'Anterior',
  'admin.common.next': 'Próximo',
  'admin.common.loading': 'Cargando...',
  'admin.common.loadMore': 'Cargar más',
  'admin.common.noResultsFound': 'No se han encontrado resultados',
  'admin.common.page': 'Página',
  'admin.common.Of': 'de',
  'admin.common.results': 'Resultados',
  'admin.common.idNumber': 'Número de registro',
  'admin.common.gender': 'Género',
  'admin.common.genders': 'Géneros',
  'admin.common.gender.male': 'Masculino',
  'admin.common.gender.female': 'Femenino',
  'admin.common.gender.unisex': 'Unissex',
  'admin.common.genderMale': 'Masculino',
  'admin.common.genderFemale': 'Femenino',
  'admin.common.dateOfBirth': 'Fecha de nacimiento',
  'admin.common.companyPlaceHolder': 'Búsqueda de empresa',
  'admin.common.birthDatePlaceHolder': 'DD/MM/AAAA',
  'admin.common.cpf': 'CPF',
  'admin.common.weight': 'Peso (kg)',
  'admin.common.height': 'Altura (cm)',
  'admin.common.phone': 'Teléfono',
  'admin.common.cellphone': 'Móvil',
  'admin.common.acceptContact': 'Aceptar contacto',
  'admin.common.yes': 'Sí',
  'admin.common.no': 'No',
  'admin.common.cep': 'CEP',
  'admin.common.cepTooltip': 'Inserir su Codigo Postal (ex - 73015-132)',
  'admin.common.country': 'País',
  'admin.common.state': 'Estado',
  'admin.common.city': 'Ciudad',
  'admin.common.address': 'Dirección',
  'admin.common.addressNumber': 'Número',
  'admin.common.addressComplement': 'Complemento',
  'admin.common.editCompany': 'Editar empresa',
  'admin.common.registerCompany': 'Registrar empresa',
  'admin.common.removeMessage': '¿De verdad quieres eliminar esta empresa?',
  'admin.common.rootNohNotFound': '¡No se encontró el nodo raíz!',
  'admin.common.companyName': 'Nombre Empresa',
  'admin.common.cnpj': 'CNPJ',
  'admin.common.parentCompany': 'Empresa matriz',
  'admin.common.expirationDate': 'Fecha de caducidad',
  'admin.common.logo': 'Logo',
  'admin.common.logoTopRight': 'Logo arriba a la derecha',
  'admin.common.logoTopLeft': 'Logo arriba a la izquierda',
  'admin.common.logoBottomRight': 'Logo abajo a la derecha',
  'admin.common.logoBottomLeft': 'Logo abajo a la izquierda',
  'admin.common.openURL': 'URL Abrir',
  'admin.common.restrictedURL': 'URL com login',
  'admin.common.slugURL': 'Slug (Nombre después de la URL)',
  'admin.common.select': 'Seleccionar',
  'admin.common.typeSomethingToSearch': 'Insertar una consulta para buscar',
  'admin.common.dateInsert': 'Inserir una fecha',
  'admin.common.createdAt': 'Creado en',
  'admin.common.fileImport': 'Importar archivo',
  'admin.common.downloadModel': 'Descargar plantilla',
  'admin.common.import': 'Importar',
  'admin.common.export': 'Exportar',
  'admin.common.reportQueue': 'Cola informes',
  'admin.common.title': 'Nombre',
  'admin.common.status': 'Status',
  'admin.common.lastUpdate': 'Última actualización',
  'admin.common.download': 'Descarga',
  'admin.common.searchForCompany': 'Buscar Empresa',
  'admin.common.men': 'Hombres',
  'admin.common.women': 'Mujeres',
  'admin.common.script': 'Guión',
  'admin.common.version': 'Versión',
  'admin.common.risk': 'Riesgo',
  'admin.common.risks': 'Riesgos',
  'admin.common.question': 'Pregunta',
  'admin.common.answer': 'Contestar',
  'admin.common.calculation': 'Cálculo',
  'admin.common.interval': 'Intervalo',
  'admin.common.start': 'Comenzo',
  'admin.common.end': 'Fin',
  'admin.common.selectPatient': 'Seleccionar pacientes de forma manual',
  'admin.common.high': 'Alto',
  'admin.common.medium': 'Medio',
  'admin.common.low': 'Bajo',
  'admin.common.description': 'Descripción',
  'admin.common.riskSelect': 'Seleccionar un riesgo',
  'admin.common.observations': 'Observaciones',
  'admin.common.all': 'Todas',
  'admin.common.icon': 'Icono',
  'admin.common.categoryColor': 'Categoría étnica',
  'admin.common.tuss': 'TUSS',
  'admin.common.today': 'Hoy',
  'admin.common.visible': 'Visible',
  'admin.common.subtitle': 'Subtítulo',
  'admin.common.specialtyColor': 'Color de la especialidad',
  'admin.common.image': 'Imagen',
  'admin.common.requiredFields': 'Campos obligatorios',
  'admin.common.optionalFields': 'Campos opcionales',
  'admin.common.minimumAge': 'Edad mínima',
  'admin.common.maximumAge': 'Edad máxima',
  'admin.common.active': 'Activo',
  'admin.common.inactive': 'Inactivo',
  'admin.common.draft': 'Versión preliminar',
  'admin.common.language': 'Idioma',
  'admin.common.link': 'Enlaze',
  'admin.common.search': 'Buscar',
  'admin.common.send': 'Enviar',
  'admin.common.subject': 'Tema',
  'admin.common.content': 'Contenido',
  'admin.common.staff': 'Equipo',
  'admin.common.customer': 'Cliente',
  'admin.common.customers': 'Clientes',
  'admin.common.value': 'Valor',
  'admin.common.contactInfo': 'Información de contacto',
  'admin.common.contactInfos': 'Informaciones de contacto',
  'admin.common.site': 'Sitio',
  'admin.common.blog': 'Blog',
  'admin.common.post': 'Enviar',
  'admin.common.posts': 'Posts',
  'admin.common.filter': 'Filtrar',
  'admin.common.continueReading': 'Lee mas',
  'admin.common.permalink': 'Enlace Permanente',
  'admin.common.category': 'Categoría',
  'admin.common.categories': 'Categorías',
  'admin.common.blogCategory': 'Categoría de blog',
  'admin.common.blogCategories': 'Categorías de blog',
  'admin.common.tag': 'Etiqueta',
  'admin.common.tags': 'Etiquetas',
  'admin.common.myAccount': 'Mi cuenta',
  'admin.common.signOut': 'Salir',
  'admin.common.personalData': 'Información personal',
  'admin.common.editPassword': 'Cambiar la contraseña',
  'admin.common.report': 'Informe',
  'admin.common.reports': 'Informes',
  'admin.common.patientsFilter': 'Filtro de paciente',
  'admin.common.unavailable': 'Indisponible',
  'admin.common.queue': 'Cola',
  'admin.common.personalInformations': 'Información personal',
  'admin.common.continue': 'Continuar',
  'admin.common.dontKnow': 'No lo sé',
  'admin.common.didNotDoIt': 'No lo hice',
  'admin.common.ethnicity.white': 'Blanco',
  'admin.common.ethnicity.black': 'Descendiente de africanos',
  'admin.common.ethnicity.brown': 'Raza mixta',
  'admin.common.ethnicity.yellow': 'Oriental',
  'admin.common.ethnicity.indigenous': 'Indio nativo americano',
  'admin.common.others': 'Otros',
  'admin.common.clickHere': 'Haga clic aquí',
  'admin.common.result': 'Resultado',
  'admin.common.recommendations': 'Recomendaciones',
  'admin.common.recommendedExams': 'Exámenes recomendados',
  'admin.common.justifications': 'Justificaciones',
  'admin.common.max': 'Máximo',
  'admin.common.min': 'Mínimo',
  'admin.common.format': 'Formato',
  'admin.common.integer': 'Número entero',
  'admin.common.none': 'Ninguna',
  'admin.common.displayBeforeAnamnesis': 'Mostrar antes de anamnesis',
  'admin.common.group': 'Grupo',
  'admin.common.author': 'Autor',
  'admin.common.date': 'Fecha',
  'admin.common.selectRiskForEmails': 'Seleccione el nivel de riesgo deseado para activar la alerta',
  'admin.common.requiredHistoryMedicalCategories': 'Las categorías que ha seleccionado requieren todas las categorías de registros médicos',
  'admin.common.emailSent': 'Email enviado',
  'admin.common.age': 'Años',

  /* Dashboard */
  'admin.dashboard.charts.total': 'Total',
  'admin.dashboard.charts.ofTotal': 'del total',
  'admin.dashboard.charts.historyCancer': 'Usuarios con antecedentes de eventos de cáncer',
  'admin.dashboard.charts.patientsNeedContact': 'Usuarios que requieren datos de contacto',
  'admin.dashboard.charts.anamnesisFulfillments': 'Número de anamnesis completadas',
  'admin.dashboard.charts.answersByAgeAndGender': 'Volumen de encuestados por edad y sexo',
  'admin.dashboard.charts.highRiskByCancerType': 'Volumen de alto riesgo por tipo de cáncer',
  'admin.dashboard.charts.bmi': 'IMC',
  'admin.dashboard.charts.examsPending': 'Faltan exámenes',
  'admin.dashboard.charts.riskAssessment': 'Evaluación de riesgos',
  'admin.dashboard.charts.riskAssessmentSufix': 'Número de personas identificadas con alto riesgo',
  'admin.dashboard.charts.highRiskSummary': 'Número de altos riesgos identificados',
  'admin.dashboard.charts.highRisk': 'Alto Riesgo',
  'admin.dashboard.charts.normalRisk': 'Riesgo Normal',
  'admin.dashboard.charts.summary': 'Resumen',
  'admin.dashboard.charts.totalFilled': 'Número de anamnesis completadas',
  'admin.dashboard.charts.totalFilledByGender': 'Número de anamnesis completadas por género',
  'admin.dashboard.charts.totalFilledByAge': 'Número realizado por tramos de edad',
  'admin.dashboard.charts.totalDone': 'Número completado',
  'admin.dashboard.charts.totalForeseen': 'Cantidad Prevista',
  'admin.dashboard.charts.highRiskChart': 'ALTO RIESGO',
  'admin.dashboard.charts.normalRiskChart': 'RIESGO NORMAL',
  'admin.dashboard.charts.smoker': 'Fumador',

  /* Companys */
  'admin.company.title': 'Empresa',
  'admin.company.title.plural': 'Compañías',

  /* Plano de Saúde */
  'admin.healthInsurance.title': 'Seguro de salud',
  'admin.healthInsurance.title.plural': 'Seguro de salud',

  /* Perfil de Acesso */
  'admin.roles.title': 'Perfil de acceso',
  'admin.roles.title.plural': 'Perfiles de acceso',
  'admin.formRole.permissions': 'Permisos',

  /* Idiomas */
  'admin.languages.title': 'Idioma',
  'admin.languages.title.plural': 'Idiomas',
  'admin.languages.slug': 'Slug',

  /* Patients */
  'admin.patients.title': 'Paciente',
  'admin.patients.title.plural': 'Pacientes',
  'admin.patients.listOfPatients': 'Lista de pacientes',
  'admin.patients.importPatients': 'Importar pacientes',
  'admin.patients.exportPatients': 'Exportar pacientes',
  'admin.patients.exportText': 'Seleccione los filtros de su preferencia y haga clic en \'Exportar\' para solicitar informes de pacientes. El tiempo para que el documento esté listo depende de la cola en el sistema y de los criterios de exportación aplicados.',
  'admin.patients.fields.gender': 'M o F',
  'admin.patients.fields.cpf': 'Sólo números (05376483649) o formateado (768.884.253-04)',
  'admin.patients.fields.phone': 'Solo números (41993241556) o formateados ((41) 99324-1556)',
  'admin.patients.fields.birthday': 'AAAA-MM-DD (Ejemplo 1990-06-20)',
  'admin.patients.fields.cep': 'Sólo números (93533280) o formateado (93530-280)',

  /* Prontuário */
  'admin.medicalRecords.title': 'Registros médicos',
  'admin.medicalRecords.title.plural': 'Registros médicos',
  'admin.medicalRecords.open': 'Abrir registros de pacientes',
  'admin.medicalRecords.close': 'Cerrar registros de pacientes',
  'admin.medicalRecords.noInformation': 'No hay información disponible',
  'admin.medicalRecords.gail.risk1': 'Riesgo estimado de desarrollar cáncer de mama en los próximos 5 años.',
  'admin.medicalRecords.gail.risk2': 'Riesgo de que la población general desarrolle cáncer de mama en los próximos 5 años',
  'admin.medicalRecords.gail.risk3': 'Riesgo promedio de cáncer de mama en desarrollo del paciente a la edad de 90 años',
  'admin.medicalRecords.gail.risk4': 'Riesgo promedio de población que desarrolla cáncer de mama a la edad de 90 años',
  'admin.medicalRecords.madeContact': 'Contacto establecido con éxito',
  'admin.medicalRecords.noMoreContact': 'No requiere nuevo contacto',
  'admin.medicalRecords.noMoreContactQuestion': '¿Requiere nuevo contacto?',
  'admin.medicalRecords.threeContactsMessage': '3 intentos de contacto',

  /* Tipos de Câncer */
  'admin.cancerTypes.title': 'Tipo de cáncer',
  'admin.cancerTypes.title.plural': 'Tipos de Cáncer',

  /* Estratégias */
  'admin.strategies.title': 'Estrategia',
  'admin.strategies.title.plural': 'Estrategias',

  /* Exames Recomendados */
  'admin.exams.title': 'Examen',
  'admin.exams.title.plural': 'Exámenes recomendados',

  /* Site */
  'admin.site.title': 'Sitio',
  'admin.testimonies.title': 'Testimonial',
  'admin.testimonies.title.plural': 'Testimonios',
  'admin.testimonies.author': 'Autor del testimonio',
  'admin.specialities.title': 'Especialidad',
  'admin.specialities.title.plural': 'Especialidades',

  /* Anamnesis, */
  'admin.anamnesis.title': 'Anamnesis',
  'admin.anamnesis.title.plural': 'Anamnesis',

  /* Categories */
  'admin.categories.title': 'Categoría',
  'admin.categories.title.plural': 'Categorías',

  /* Script */
  'admin.script.title': 'Guión',
  'admin.script.title.plural': 'Guiones',
  'admin.script.categoryAlreadyAdded': 'Categoría ya registrada',
  'admin.script.addCategory': 'Añadir categoría',
  'admin.script.addAnswer': 'Agregar respuesta',
  'admin.script.addQuestion': 'Agregar pregunta',
  'admin.script.editQuestion': 'Editar pregunta',
  'admin.script.editAnswer': 'Editar respuesta',
  'admin.script.valueAnswer': 'Valor de la respuesta',
  'admin.script.questionType': 'Tipo de pregunta',
  'admin.script.selectTheQuestionType': 'Seleccione el tipo de pregunta',
  'admin.script.questionTitle': 'Nombre de la pregunta',
  'admin.script.questionDescription': 'Descripción de la pregunta',
  'admin.script.questionTypes.objective': 'Objetiva',
  'admin.script.questionTypes.discursive': 'Discursiva',
  'admin.script.answer': 'Contestar',
  'admin.script.question': 'Pregunta',
  'admin.script.category': 'Categoría',
  'admin.script.status': 'Estado del guión',
  'admin.script.requiredRegistration': 'Número de registro obligatorio',
  'admin.script.requiredPhone': 'Número de teléfono obligatorio',
  'admin.script.requiredCellphone': 'Número de móvil obligatorio',
  'admin.script.requiredHealthcareInsurance': 'Información obligatoria del seguro de salud',
  'admin.script.minimumPeriod': 'Período mínimo para responder este guión nuevamente (en meses)',
  'admin.script.displayResultPageWithRisks': 'Mostrar página con resultados de riesgo',
  'admin.script.sendResultForEmail': 'Enviar el resultado por correo electrónico',
  'admin.script.sendResultForEmailPeriod': 'Plazo para enviar resultados (en días)',
  'admin.script.sendReminderEmail': 'Enviar recordatorio por correo electrónico',
  'admin.script.sendReminderEmailPeriod': 'Plazo de envío (en días)',
  'admin.script.sendReminderEmailQuantity': 'Cantidad de envíos',
  'admin.script.resultPageContent': 'Contenido de la página de resultados',
  'admin.script.initialPageContent': 'Contenido de la página de inicio',
  'admin.script.displayFooter': 'Mostrar notas al pie de la página',
  'admin.script.questionGroupError': 'Un grupo no puede registrarse dentro de otros grupos',
  'admin.script.additionalInfoPageContent': 'Información Adicional',
  'admin.script.displayAdditionalInfoPageContent': 'Mostrar información adicional',

  /* Disparo de Script */
  'admin.scriptSend.title': 'Guión de disparo',
  'admin.scriptSend.exportCsvFile': 'Descargar archivo CSV',
  'admin.scriptSend.sendEmail': 'Enviar correo electrónico',
  'admin.scriptSend.sentScripts': 'Guiones activados',

  /* Membros da Equipe */
  'admin.team.title': 'Miembro del equipo',
  'admin.team.title.plural': 'Miembros del equipo',

  /* ValidActions */
  'validations.required': 'Campo obligatorio',
  'validations.integer': 'El número debe ser un número entero',
  'validations.integerBetween': 'Inserir un número entre {min} y {max}',
  'validations.email': 'No es un correo electrónico válido',
  'validations.phone': 'No es un número de teléfono válido',
  'validations.cpf': 'CPF (identificación del contribuyente brasileño) no válido',
  'validations.cnpj': 'CNPJ (identificación fiscal brasileña) no válida',
  'validations.cep': 'CEP (Código postal) no válido',
  'validations.date': 'Fecha no válida',
  'validations.password': 'Debe contener al menos 8 caracteres, incluidos números, mayúsculas y minúsculas',
  'validations.passwordConfirm': 'Las contraseñas deben ser iguales',
  'validations.fileType': 'Tipo de archivo no soportado',

  /* Feedback */
  'feedback.success': '¡Acción ejecutada con éxito!',
  'feedback.error': 'Oops! ¡Ha habido un problema!',

  /* Field Hints */
  'hints.cpf': 'Inserir su CPF (identificación del contribuyente brasileño) con o sin puntuación',
  'hints.cpfNumberOnly': 'Inserir su CPF (identificación del contribuyente brasileño) sin puntuación (solo números)',
  'hints.number': 'Sólo números',
  'hints.email': 'Inserir su Correo Electrónico (Ej: ejemplo@example.com)',
  'hints.height': 'Informe su altura en centímetros (cm)',
  'hints.weight': 'Inserta tu peso en kilogramos (kg)',
  'hints.dateOfBirth': 'La fecha debe estar en formato DD / MM / AAAA (ejemplo 25/10/1950)',

  /* Anamnesis, */
  'q.startPrevention': 'Comience a contestar',
  'q.backToAnamnese': 'Regreso a la anamnesis.',
  'q.wizardStep': 'Paso {progress} de {count}',
  'q.acceptContact': 'Acepto contacto por teléfono o correo electrónico',
  'q.termsAccept': 'Acepto los ',
  'q.termsOfService': 'Términos del servicio',
  'q.termsAndConditions': 'Términos y Condiciones',
  'q.acceptRequired': 'Los términos deben ser aceptados',
  'q.brca.question': '¿Alguna vez ha realizado un examen genético (conocido como BRCA1 o BRCA) para saber si hay un cambio / mutación que podría aumentar su riesgo de desarrollar cáncer y, se sí, el resultado fue positivo?',
  'q.brca.description': 'La mutación en los genes BRCA1 o BRCA2 aumenta significativamente el riesgo de algunos tipos de cáncer (mama, ovario, próstata, colon o páncreas). Este examen genético puede realizarse a través de muestras de sangre o saliva para determinar si la persona tiene esta mutación. Esta mutación generalmente ocurre en personas con antecedentes de cáncer en familiares cercanos. Se estima que aproximadamente del 5% al 10% de todos los casos de cáncer de mama y ovario son causados por esta mutación',
  'q.ethnicity.question': 'Grupo étnico',
  'q.ethnicity.description': 'Se sabe que las personas de ascendencia africana tienen un mayor riesgo de cáncer de próstata.',
  'q.cancerHistory.question': '¿Tienes un historial personal de cáncer?',
  'q.cancerHistory.description': 'Se sabe que las personas con antecedentes de cáncer tienen un alto riesgo de recaída o, eventualmente, un nuevo tumor del mismo tipo. Debido a esto, con el propósito de calcular su riesgo, es importante establecer si ya ha tenido esta enfermedad en su vida.',
  'q.cancerHistory.whatCancer': '¿Qué tipo de cáncer?',
  'q.cancerHistory.specifyCancer': 'Especifique el tipo de cáncer',
  'q.helloPatient': 'Hola, {name}',
  'q.patientIntroText': 'Ha llegado el momento de aprender sobre usted y su historial médico. Complete los campos con precisión para que su evaluación sea lo más precisa posible. ¿Empezamos?',
  'q.startAvaliation': 'Comenzar evaluación',
  'q.profileUnmatch': 'Las anamneses en esta campaña no coinciden con su perfil. ¡Gracias por ayudarnos hasta aquí!',
  'q.finishAvaliation': 'Encerrar evaluación',
  'q.toUpdateYourData': 'para actualizar tus datos personales',
  'q.stepFinished': '¡Un paso más concluyido!',
  'q.youAreDoingRight': 'Estás completando correctamente los datos de anamnesis. ¡Sigue así!',
  'q.continueAvaliation': 'Continuar la evaluación',
  'q.toAnswerTheQuestions': 'para responder las preguntas',
  'q.thanks': '¡Gracias por realizar esta evaluación!',
  'q.sentForEmail': 'El resultado ha sido enviado a su correo electrónico.',
  'q.welcomeUser': 'Bienvenido, {name}',

  'terms': '<p>' +
    '   <strong>Política de privacidad y autorización de uso</strong>' +
    '</p>' +
    '<p>' +
    '   Somos una empresa dedicada a la promoción de información médica, dirigida a la prevención de algunos tipos de cáncer en Brasil. Hacemos esto porque creemos que cuanta más información, mayores son las posibilidades de un diagnóstico temprano, que es fundamental en la prevención del cáncer.' +
    '</p>' +
    '<p>' +
    '   La forma en que divulgamos la información es una forma pre-formateada, basada en literatura médica y en métodos reconocidos nacional e internacionalmente para verificar la presencia de uno o más factores de riesgo que aumentan el riesgo estimado de desarrollar una patología. Para este fin, le solicitamos cierta información personal, incluidos sus datos de identificación, el historial familiar de patologías y datos relacionados con su vida diaria.' +
    '</p>' +
    '<p>' +
    '   Por ejemplo, necesitaremos saber si usted fuma o ya fue fumador, si ha tenido relaciones sexuales y si ya ha tenido algún tipo de prueba preventiva. Esta y otras informaciones son esenciales para lograr una estimación correcta del riesgo de desarrollar los tipos de cáncer aplicables a los formularios.' +
    '</p>' +
    '<p>' +
    '   <strong>¿Cómo se utilizarán sus datos?</strong>' +
    '</p>' +
    '<p>' +
    '   Todos los datos personales que proporcione se mantendrán en secreto y se utilizarán únicamente con el fin de obtener el resultado con respecto a su riesgo de desarrollar las patologías analizadas. Esto se aplica tanto a los datos de identificación (como su nombre, dirección de correo electrónico, edad, etc.) como a las respuestas a los formularios, es decir, sus registros médicos o datos de vida diaria. Usaremos su correo electrónico exclusivamente con el fin de enviar los resultados y no pasaremos esta información a terceros ni la utilizaremos para ningún otro fin.' +
    '</p>' +
    '<p>' +
    '   <strong>¿Quién tiene acceso a mis datos?</strong>' +
    '</p>' +
    '<p>' +
    '   Para obtener los resultados, sus respuestas serán analizadas automáticamente y verificadas manualmente por empleados de Previneo, vinculados al área de atención médica. La herramienta fue desarrollada por médicos, pero el análisis de los datos no es realizado por ellos, ya que no brindamos servicios de atención médica, sino solo divulgación de información. Nuestros empleados están comprometidos con la confidencialidad de los datos y nuestra política de privacidad.' +
    '</p>' +
    '<p>' +
    '   Si participa en esta evaluación como parte del programa de salud de su empleador, los médicos responsables (y solo los médicos) de su empleador pueden tener acceso a los resultados en total y absoluto respeto al Código de Ética Médica.' +
    '</p>' +
    '<p>' +
    '   <strong>¿Qué pasa si autorizo ​​el uso de los datos en forma de estadísticas?</strong>' +
    '</p>' +
    '<p>' +
    '   Al completar nuestro formulario, solicitamos que, además de aceptar nuestros términos de uso, usted también acepte que los respectivos resultados puedan constituir una base de datos para el establecimiento de políticas de prevención. Al final, además de proporcionar información adecuada, nuestra herramienta también puede servir para identificar fallas o correcciones necesarias en los métodos de diagnóstico de cáncer en Brasil.' +
    '</p>' +
    '<p>' +
    '   En este caso, si está de acuerdo, los resultados obtenidos al completar su formulario pueden utilizarse para incluir en estadísticas sin identificación personal o datos confidenciales. Mantendremos el secreto absoluto en relación con estos datos.</p>' +
    '<p>' +
    '   Por ejemplo, si tiene 20 años, no fuma y vive en Curitiba-PR, su riesgo de desarrollar cáncer de pulmón se considerará normal para su edad y condición. En este caso, su resultado compondrá la estadística como una persona de riesgo normal para el desarrollo de esta patología, sin ningún tipo de identificación, solo vinculada a la región donde reside.' +
    '</p>' +
    '<p>' +
    '   Cualquier duda o aclaración se puede resolver previamente a través de nuestro correo electrónico <strong>contato@previneo.com.br</strong>.        ' +
    '</p>',

  'admin.dashboard.chart.foreseen': 'Previsto',
  'admin.dashboard.chart.fulfilled': 'Hecho',
  'admin.dashboard.chart.not.fulfilled': 'No hecho',
  'admin.patientsHistory.currentSituation': 'Situación actual',
  'admin.common.nextContact': 'Próximo Contacto',
  'admin.common.contactReason': 'Motivo del contacto',
  'admin.common.typesCancer': 'Tipos de Cáncer',
  'admin.common.patientHistoryExams': 'Exámenes realizados',
  'admin.common.saveContact': 'Salvar el contacto',
  'admin.patientsHistory.status.pending': 'Inicio',
  'admin.patientsHistory.status.inProgress': 'Pausa',
  'admin.patientsHistory.status.done': 'Fin',
  'admin.common.contactMade': 'Contactado?',
  'admin.common.contactMadeOk': 'Sí',
  'admin.common.contactMadeNo': 'No',
  'admin.common.contactStatus': 'Situación',
  'admin.common.contactStatuses': 'Situaciones',
  'admin.common.managePatientHistory': 'Tratamiento',
  'admin.common.ethnicity': 'Etnicidad',
  'admin.common.patientInfo': 'Datos del paciente',
  'admin.patients.deletedPatients': 'Pacientes eliminados',
  'admin.common.motivo': 'Razón',
  'admin.common.hidePatientInfo': 'Ocultar información de contacto',
  'admin.common.displayPatientInfo': 'Mostrar información de contacto',
  'admin.common.noPermission': 'Usted no tiene permiso para acceder a esta página',
  'admin.patientsHistory.titlePlural': 'Prontuarios',
  'admin.patientsHistory.contact': 'Atendimiento',
  'admin.patientsHistory.contactReasonPlural': 'Motivos del contacto',

  'admin.common.logoRight': 'Logo derecho',
  'admin.common.logoLeft': 'Logo izquierdo',
  'admin.common.justifications.title': 'Justificación',
  'admin.common.justifications.title.plural': 'Justificaciones',
  'admin.common.file': 'Archivo',
  'admin.common.fileFront': 'Archivo frontal',
  'admin.common.roles': 'Perfiles de acceso',
  'admin.common.ageRange': 'Rango de edad',
  'admin.permissions.title': 'Permiso',
  'admin.permissions.title.plural': 'Permisos',
  'admin.justifications.title': 'Justificación',
  'admin.justifications.title.plural': 'Justificaciones',
  'admin.faixasimc.abaixo': 'Bajo peso',
  'admin.faixasimc.normal': 'Peso normal',
  'admin.faixasimc.acima': 'Por encima del peso',
  'admin.faixasimc.obesidadeI': 'Obesidad I',
  'admin.faixasimc.obesidadeII': 'Obesidad II',
  'admin.faixasimc.obesidadeIII': 'Obesidad III',
  'admin.common.lastAnamnesis': 'Fecha de la Anamnesis',
  'admin.common.reSendEmail': 'Reenviar correo electrónicol',
  'admin.common.normal': 'Normal',
  'admin.common.entity': 'Entidade',
  'admin.common.entities': 'Entidades',
  'admin.common.numberOfEmployees': 'Número de Empleados',
  'admin.common.confirmation': 'Confirmación',
  'admin.common.confirmationNotFound': 'Registro no encontrado, haga clic en el botón de abajo para intentarlo de nuevo.',
  'admin.common.editUser': 'Editar Usuario'
}
