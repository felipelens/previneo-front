export default {
  /* Páginas */
  'pages.home.title': 'PreviNEO - Revolução na prevenção e combate ao câncer',
  'pages.home.description': 'A PreviNEO une tecnologia e conhecimento científico para prevenir e realizar diagnóstico precoce dos principais tipos de câncer que ocorrem no Brasil.',
  'pages.about.title': 'Quem Somos - PreviNEO',
  'pages.about.description': 'Trabalhamos para prevenir, diagnosticar e reduzir os riscos de incidência dos principais tipos de câncer que ocorrem no Brasil, através de um programa de oncologia e tecnologia de ponta.',
  'pages.specialities.title': 'Especialidades - PreviNEO',
  'pages.specialities.titleDescription': 'Abaixo, você encontra mais informações em torno das causas e sintomas deste tipo de câncer, com o objetivo de conscientizar sobre a prevenção dessa doença.<br />',
  'pages.specialities.description': 'Na PreviNEO trabalhamos de forma especializada para prevenir, diagnosticar e reduzir os riscos de incidência dos cânceres de Próstata, Mama, Pulmão, Cólon e Útero.',
  'pages.customers.title': 'Clientes - PreviNEO',
  'pages.customers.description': 'Na PreviNEO contamos com empresas parceiras que colocam nossos benefícios à disposição de colaboradores e familiares. Acesse e conheça quem está conosco nessa luta!',
  'pages.blog.title': 'Blog - PreviNEO',
  'pages.blog.description': 'A informação é fator decisivo na prevenção e diagnóstico precoce do câncer. Através do nosso Blog são disponibilizados dados e notícias a respeito da nossa causa, acesse!',
  'pages.contact.title': 'Contato - PreviNEO',
  'pages.contact.description': 'Para sanar dúvidas, enviar sugestões ou se tornar um parceiro, envie-nos uma mensagem. Nossa equipe terá o maior prazer em atendê-lo!',

  /* Títulos dos links do menu */
  'site.menu.home': 'Home',
  'site.menu.about': 'Quem Somos',
  'site.menu.specialities': 'Especialidades',
  'site.menu.customers': 'Clientes',
  'site.menu.blog': 'Blog',
  'site.menu.contact': 'Contato',

  /* 404 */
  'site.notFound.title': 'Página não encontrada',
  'site.notFound.message': '404: Página não encontrada!',

  /* Login */
  'admin.forms.login.title': 'Login',
  'admin.forms.login.email': 'E-mail',
  'admin.forms.login.password': 'Senha',
  'admin.forms.login.button': 'Login',
  'admin.forms.login.passwordRecovery': 'Esqueci minha senha',
  'admin.patientsHistory.currentSituation': 'Situação Atual',
  'admin.common.nextContact': 'Próximo Contato',
  'admin.common.contactReason': 'Motivos do Contato',
  'admin.common.typesCancer': 'Tipos de Câncer',
  'admin.common.patientHistoryExams': 'Exames Realizados',
  'admin.common.saveContact': 'Salvar Contato',
  'admin.patientsHistory.status.pending': 'Iniciar',
  'admin.patientsHistory.status.inProgress': 'Pausar',
  'admin.patientsHistory.status.done': 'Finalizar',
  'admin.patientsHistory.titlePlural': 'Prontuários',
  'admin.patientsHistory.contact': 'Atendimento',
  'admin.patientsHistory.contactReasonPlural': 'Motivos de Contato',
  'admin.common.contactMade': 'Contatado?',
  'admin.common.contactMadeOk': 'Sim',
  'admin.common.contactMadeNo': 'Não',
  'admin.common.contactStatus': 'Status',
  'admin.common.contactStatuses': 'Status',
  'admin.common.managePatientHistory': 'Atendimento',
  'admin.common.ethnicity': 'Etnia',
  'admin.common.patientInfo': 'Dados do Paciente',

  /* Recuperação de Senha */
  'admin.forms.passwordRecovery.title': 'Recuperação de Senha',
  'admin.forms.passwordRecovery.button': 'Recuperar',

  /* Hero Home */
  'section.home.hero.title': 'Revolução na <strong>prevenção</strong><br /> e <strong>combate</strong> ao câncer.',
  'section.home.hero.button': 'Saiba Mais',

  /* Section Services Home */
  'section.home.services.title': 'Tecnologia e conhecimento científico em prol de um bem maior: <strong>a vida.</strong>',
  'section.home.services.description': '<p>A PreviNEO atua através da prevenção e da <strong>realização do diagnóstico precoce</strong> dos 5 principais tipos de câncer que ocorrem no Brasil, e que correspondem a um volume expressivo dos casos: mama, próstata, pulmão, cólon e colo de útero.</p><p> <strong>Colocamos o ser humano em primeiro lugar</strong>, oferecemos soluções centradas em cada paciente com um objetivo fundamental: promover a saúde para cada vez mais pessoas.</p>',

  /* Section Steps Home */
  'section.home.steps.title': 'O trabalho da <strong>PreviNEO</strong> é realizado nas seguintes etapas:',
  'section.home.steps.stepone.step': 'Etapa 1',
  'section.home.steps.stepone.title': 'Identificação',
  'section.home.steps.stepone.description': 'Anamnese online ou presencial.',
  'section.home.steps.steptwo.step': 'Etapa 2',
  'section.home.steps.steptwo.title': 'Rastreamento',
  'section.home.steps.steptwo.description': 'Utilização de algoritmos para estratificação da população de risco.',
  'section.home.steps.stepthree.step': 'Etapa 3',
  'section.home.steps.stepthree.title': 'Orientação',
  'section.home.steps.stepthree.description': 'Contato e orientação.',
  'section.home.steps.stepfour.step': 'Etapa 4',
  'section.home.steps.stepfour.title': 'Monitoramento',
  'section.home.steps.stepfour.description': 'Elaboração de estratégias para redução de risco.',

  /* Section Clientes Home */
  'section.home.customers.title': 'Clientes',
  'section.home.customers.description': 'Nossos programas de prevenção já fazem a diferença no dia a dia de diversas empresas que, assim como nós, colocam a saúde e bem estar das pessoas em primeiro lugar.',
  'section.home.customers.button': 'Veja todos',

  /* Section Depoimentos Home */
  'section.home.testmonies.title': 'Nossos clientes falam melhor que a gente',
  'section.home.testmonies.internal.title': 'Nada fala mais alto do que a <strong>aprovação</strong> de quem já conhece.',
  'section.home.testmonies.internal.subtitle': 'Ao lado, você encontra os depoimentos de <strong>nossos parceiros:</strong>',

  /* Section Blog Home */
  'section.home.blog.title': 'Nosso Blog',
  'section.home.blog.button': 'Veja tudo',

  /* Section Parceiros */
  'section.partner.title': 'Seja um parceiro PreviNEO',
  'section.partner.description': 'Vamos unir forças? Juntos, podemos reduzir cada vez mais a mortalidade por câncer no Brasil.',
  'section.partner.button': 'Fale conosco',

  /* Hero About */
  'section.about.hero.title': 'Um programa de oncologia para <strong>prevenir</strong> e <strong>diagnosticar</strong>.',
  'section.about.hero.description': 'Na PreviNEO, trabalhamos para prevenir, diagnosticar e reduzir os riscos de incidência dos principais tipos de câncer que ocorrem no Brasil, através de um programa de oncologia e tecnologia de ponta. Nossa missão é simples: promover saúde e reduzir a incidência e mortalidade de câncer no país. Para isso, atuamos com os tipos de câncer mais incidentes no Brasil, a fim de impactar positivamente e profundamente na qualidade de vida nacional.',
  'section.about.hero.button': 'Saiba mais',

  /* Section Steps About */
  'section.about.steps.title': 'Como o programa de <strong>oncologia</strong> trabalha?',
  'section.about.steps.description': 'Nosso método de atuação consiste em três etapas principais: análise de dados, cálculo de risco e orientações. Assim, você terá acesso a um atendimento e um diagnóstico completo e totalmente personalizado. <strong>Confira o passo a passo:</strong>',
  'section.about.steps.stepone.step': 'Etapa 1',
  'section.about.steps.stepone.description': 'Preenchimento da anamnese presencial ou online.',
  'section.about.steps.steptwo.step': 'Etapa 2',
  'section.about.steps.steptwo.description': 'Estratificação de população de risco através do programa de oncologia.',
  'section.about.steps.stepthree.step': 'Etapa 3',
  'section.about.steps.stepthree.description': 'Contato e orientação para criação de estratégias de redução de riscos.',
  'section.about.steps.stepfour.step': 'Etapa 4',
  'section.about.steps.stepfour.description': 'Agendamento de exames e consultas.',
  'section.about.steps.stepfive.step': 'Etapa 5',
  'section.about.steps.stepfive.description': 'Monitoramento da estratégia de redução de riscos.',

  /* Section Map About */
  'section.about.map.title': 'A <strong>primeira</strong> e <strong>única</strong> do Brasil!',
  'section.about.map.description': '<p>Fundada em <strong>2015</strong>, a PreviNEO é a <strong>primeira</strong> e <strong>única</strong> empresa brasileira a trabalhar com um programa de oncologia para a prevenção e diagnóstico de câncer. Ao longo de nossa trajetória, atendemos mais de 48 mil pessoas em 22 empresas.</p><p> Entre as pessoas alcançadas através de nossos parceiros, identificamos mais de <strong>5 mil casos de pacientes em risco.</strong> Queremos continuar crescendo e levando informação e qualidade de vida para <strong>a maior quantidade de pessoas possível.</strong></p>',

  /* Section Frase About */
  'section.phrase.title': 'Unidos para <strong>reduzir a <br />mortalidade</strong> por câncer no Brasil.',

  /* Hero Specialities */
  'section.specialities.hero.title': '<strong>Prevenção</strong><br />que salva vidas!',
  'section.specialities.hero.description': '<p>Para combater o câncer, trabalhamos principalmente com a prevenção e o <strong>diagnóstico precoce do câncer, resultando em estratégias personalizadas para diminuir a incidência, o diagnóstico tardio e a mortalidade para essa doença.</strong></p><p>O objetivo é sempre o mesmo: promover saúde em todo o Brasil. Por conta disso, atuamos nos cinco principais tipos de câncer presentes nos <strong>brasileiros para diminuir a mortalidade e o custo com esses diagnósticos no Brasil.<strong></p>',

  /* Section Specialities */
  'section.specialities.types.title': 'Os 5 principais tipos de câncer',
  'section.specialities.types.subtitle': 'Na PreviNEO, trabalhamos de forma especializada com câncer de Próstata, Mama, Pulmão, Cólon e Útero.',

  /* Hero Customers */
  'section.customers.hero.title': '<strong>Não</strong> estamos <br /><strong>sozinhos!</strong>',
  'section.customers.hero.description': 'Para levar os benefícios da prevenção e do <strong>diagnóstico precoce</strong> do câncer, nós contamos com empresas parceiras que colocam nossos benefícios à disposição de colaboradores e familiares. Assim, a tranquilidade e a saúde chegam cada vez mais longe, fortalecendo a luta pela vida!',

  /* Section Clientes */
  'section.customers.list.title': 'Conheça nossos <strong>clientes</strong>',

  /* Contato */
  'section.contact.title': 'Entre em contato conosco',
  'section.contact.subtitle': 'Para sanar dúvidas, enviar sugestões ou elogios e se tornar um parceiro, envie uma mensagem',

  /* Form Contato */
  'form.contact.name': 'Nome',
  'form.contact.phone': 'Telefone',
  'form.contact.email': 'E-mail',
  'form.contact.subject': 'Assunto',
  'form.contact.message': 'Mensagem',

  /* Admin Usuarios */
  'admin.users.title': 'Usuário',
  'admin.users.title.plural': 'Usuários',
  'admin.users.form.sendPasswordEmail': 'Enviar senha por email',
  'admin.users.searchForUsers': 'Pesquisar por usuário',
  'admin.users.deletedUsers': 'Usuários Removidos',
  'admin.common.restore': 'Recuperar',
  'admin.common.restoreSelected': 'Recuperar selecionados',

  /* Nomes comuns admin */
  'admin.common.name': 'Nome',
  'admin.common.email': 'E-mail',
  'admin.common.emails': 'E-mails',
  'admin.common.emailLayout': 'Layout de E-mail',
  'admin.common.emailLayouts': 'Layouts de E-mail',
  'admin.common.emailContent': 'Conteúdo de E-mail',
  'admin.common.emailContents': 'Conteúdos de E-mail',
  'admin.common.password': 'Senha',
  'admin.common.confirmPassword': 'Confirmar Senha',
  'admin.common.oldPassword': 'Senha antiga',
  'admin.common.company': 'Empresa',
  'admin.common.accessLevels': 'Níveis de Acesso',
  'admin.common.save': 'Salvar',
  'admin.common.listOf': 'Lista de',
  'admin.common.register': 'Cadastrar',
  'admin.common.edit': 'Editar',
  'admin.common.details': 'Detalhes',
  'admin.common.remove': 'Remover',
  'admin.common.removePermanently': 'Remover permanentemente',
  'admin.common.back': 'Voltar',
  'admin.common.actions': 'Ações',
  'admin.common.warningMessage': 'Deseja mesmo remover este registro?',
  'admin.common.AreYouSureABoutIt': 'Está certo disso?',
  'admin.common.confirm': 'Confirmar',
  'admin.common.cancel': 'Cancelar',
  'admin.common.processing': 'Processando...',
  'admin.common.previous': 'Anterior',
  'admin.common.next': 'Próximo',
  'admin.common.loading': 'Carregando...',
  'admin.common.loadMore': 'Carregar mais',
  'admin.common.noResultsFound': 'Nenhum resultado encontrado',
  'admin.common.page': 'Página',
  'admin.common.Of': 'de',
  'admin.common.results': 'Resultados',
  'admin.common.idNumber': 'Matrícula',
  'admin.common.gender': 'Gênero',
  'admin.common.genders': 'Gêneros',
  'admin.common.gender.male': 'Masculino',
  'admin.common.gender.female': 'Feminino',
  'admin.common.gender.unisex': 'Unissex',
  'admin.common.genderMale': 'Masculino',
  'admin.common.genderFemale': 'Feminino',
  'admin.common.dateOfBirth': 'Data de nascimento',
  'admin.common.companyPlaceHolder': 'Pesquisar Empresa',
  'admin.common.birthDatePlaceHolder': 'DD/MM/AAAA',
  'admin.common.cpf': 'CPF',
  'admin.common.weight': 'Peso (kg)',
  'admin.common.height': 'Altura (cm)',
  'admin.common.phone': 'Telefone',
  'admin.common.cellphone': 'Celular',
  'admin.common.acceptContact': 'Aceita Contato',
  'admin.common.yes': 'Sim',
  'admin.common.no': 'Não',
  'admin.common.cep': 'CEP',
  'admin.common.cepTooltip': 'Insira seu CEP (Ex: 73015-132)',
  'admin.common.country': 'País',
  'admin.common.state': 'Estado',
  'admin.common.city': 'Cidade',
  'admin.common.address': 'Endereço',
  'admin.common.addressNumber': 'Número',
  'admin.common.addressComplement': 'Complemento',
  'admin.common.editCompany': 'Editar Empresa',
  'admin.common.registerCompany': 'Cadastrar Empresa',
  'admin.common.removeMessage': 'Deseja mesmo remover esta empresa?',
  'admin.common.rootNohNotFound': 'Nó raiz não encontrado!',
  'admin.common.companyName': 'Nome da empresa',
  'admin.common.cnpj': 'CNPJ',
  'admin.common.parentCompany': 'Empresa Pai',
  'admin.common.expirationDate': 'Data de Expiração',
  'admin.common.logo': 'Logo',
  'admin.common.logoTopRight': 'Logo superior direita',
  'admin.common.logoTopLeft': 'Logo superior esquerda',
  'admin.common.logoBottomRight': 'Logo inferior direita',
  'admin.common.logoBottomLeft': 'Logo inferior esquerda',
  'admin.common.openURL': 'URL Aberta',
  'admin.common.restrictedURL': 'URL com login',
  'admin.common.slugURL': 'Slug (nome após URL)',
  'admin.common.select': 'Selecione',
  'admin.common.typeSomethingToSearch': 'Digite algo para pesquisar',
  'admin.common.dateInsert': 'Insira uma data',
  'admin.common.createdAt': 'Criado em',
  'admin.common.fileImport': 'Arquivo de importação',
  'admin.common.downloadModel': 'Baixar modelo',
  'admin.common.import': 'Importar',
  'admin.common.export': 'Exportar',
  'admin.common.reportQueue': 'Fila de Relatórios',
  'admin.common.title': 'Título',
  'admin.common.status': 'Status',
  'admin.common.lastUpdate': 'Última atualização',
  'admin.common.download': 'Baixar',
  'admin.common.searchForCompany': 'Pesquisar empresa',
  'admin.common.men': 'Homens',
  'admin.common.women': 'Mulheres',
  'admin.common.script': 'Roteiro',
  'admin.common.version': 'Versão',
  'admin.common.risk': 'Risco',
  'admin.common.risks': 'Riscos',
  'admin.common.question': 'Pergunta',
  'admin.common.answer': 'Resposta',
  'admin.common.calculation': 'Cálculo',
  'admin.common.interval': 'Intervalo',
  'admin.common.start': 'Início',
  'admin.common.end': 'Fim',
  'admin.common.selectPatient': 'Selecionar pacientes manualmente',
  'admin.common.high': 'Alto',
  'admin.common.medium': 'Médio',
  'admin.common.low': 'Baixo',
  'admin.common.description': 'Descrição',
  'admin.common.riskSelect': 'Selecione um risco',
  'admin.common.observations': 'Observações',
  'admin.common.all': 'Todos',
  'admin.common.icon': 'Ícone',
  'admin.common.categoryColor': 'Cor da categoria',
  'admin.common.tuss': 'TUSS',
  'admin.common.today': 'Hoje',
  'admin.common.visible': 'Visível',
  'admin.common.subtitle': 'Legenda',
  'admin.common.specialtyColor': 'Cor da especilidade',
  'admin.common.image': 'Imagem',
  'admin.common.requiredFields': 'Campos obrigatórios',
  'admin.common.optionalFields': 'Campos opcionais',
  'admin.common.minimumAge': 'Idade mínima',
  'admin.common.maximumAge': 'Idade máxima',
  'admin.common.active': 'Ativo',
  'admin.common.inactive': 'Inativo',
  'admin.common.draft': 'Rascunho',
  'admin.common.language': 'Idioma',
  'admin.common.link': 'Link',
  'admin.common.search': 'Pesquisar',
  'admin.common.send': 'Enviar',
  'admin.common.subject': 'Assunto',
  'admin.common.content': 'Conteúdo',
  'admin.common.staff': 'Equipe',
  'admin.common.customer': 'Cliente',
  'admin.common.customers': 'Clientes',
  'admin.common.value': 'Valor',
  'admin.common.contactInfo': 'Informação de Contato',
  'admin.common.contactInfos': 'Informações de Contato',
  'admin.common.site': 'Site',
  'admin.common.blog': 'Blog',
  'admin.common.post': 'Post',
  'admin.common.posts': 'Posts',
  'admin.common.filter': 'Filtrar',
  'admin.common.continueReading': 'Leia mais',
  'admin.common.permalink': 'Link Permanente',
  'admin.common.category': 'Categoria',
  'admin.common.categories': 'Categorias',
  'admin.common.blogCategory': 'Categoria do Blog',
  'admin.common.blogCategories': 'Categorias do Blog',
  'admin.common.tag': 'Tag',
  'admin.common.tags': 'Tags',
  'admin.common.myAccount': 'Minha conta',
  'admin.common.signOut': 'Sair',
  'admin.common.personalData': 'Dados Pessoais',
  'admin.common.editPassword': 'Alterar Senha',
  'admin.common.report': 'Relatório',
  'admin.common.reports': 'Relatórios',
  'admin.common.patientsFilter': 'Filtro de Pacientes',
  'admin.common.unavailable': 'Indisponível',
  'admin.common.queue': 'Fila',
  'admin.common.personalInformations': 'Informações pessoais',
  'admin.common.continue': 'Continuar',
  'admin.common.dontKnow': 'Não sei',
  'admin.common.didNotDoIt': 'Não fiz',
  'admin.common.ethnicity.white': 'Branca',
  'admin.common.ethnicity.black': 'Negra',
  'admin.common.ethnicity.brown': 'Parda',
  'admin.common.ethnicity.yellow': 'Amarela',
  'admin.common.ethnicity.indigenous': 'Indígena',
  'admin.common.others': 'Outros',
  'admin.common.clickHere': 'Clique aqui',
  'admin.common.result': 'Resultado',
  'admin.common.recommendations': 'Recomendações',
  'admin.common.recommendedExams': 'Exames Recomendados',
  'admin.common.justifications': 'Justificativas',
  'admin.common.max': 'Máximo',
  'admin.common.min': 'Mínimo',
  'admin.common.format': 'Formato',
  'admin.common.integer': 'Número inteiro',
  'admin.common.none': 'Nenhum',
  'admin.common.displayBeforeAnamnesis': 'Exibir antes da anamnese',
  'admin.common.group': 'Grupo',
  'admin.common.author': 'Autor',
  'admin.common.date': 'Data',
  'admin.common.selectRiskForEmails': 'Selecione o grau de risco que deseja alertar',
  'admin.common.requiredHistoryMedicalCategories': 'As categorias que você selecionou requerem todas as categorias de histórico médico',
  'admin.common.emailSent': 'E-mail enviado',
  'admin.common.motivo': 'Motivo',
  'admin.common.hidePatientInfo': 'Ocultar Informações de Contato',
  'admin.common.displayPatientInfo': 'Exibir Informações de Contato',
  'admin.common.noPermission': 'Você não tem permissão para acessar essa página',
  'admin.common.age': 'Idade',

  /* Dashboard */
  'admin.dashboard.charts.total': 'Total',
  'admin.dashboard.charts.ofTotal': 'do total',
  'admin.dashboard.charts.historyCancer': 'usuários que possuem histórico de casos de câncer',
  'admin.dashboard.charts.patientsNeedContact': 'usuários necessitam contato imediato',
  'admin.dashboard.charts.anamnesisFulfillments': 'Quantidade total de preenchimentos de anamnese',
  'admin.dashboard.charts.answersByAgeAndGender': 'Volume de Respondentes por Idade e Gênero',
  'admin.dashboard.charts.highRiskByCancerType': 'Volume de Alto Risco por Tipo de Câncer',
  'admin.dashboard.charts.bmi': 'IMC',
  'admin.dashboard.charts.examsPending': 'Faltam Exames',
  'admin.dashboard.charts.riskAssessment': 'Avaliação de Risco',
  'admin.dashboard.charts.riskAssessmentSufix': 'Quantidade de Pessoas Identificadas com Alto Risco',
  'admin.dashboard.charts.highRisk': 'Alto Risco',
  'admin.dashboard.charts.normalRisk': 'Risco Normal',
  'admin.dashboard.charts.summary': 'Sumário',
  'admin.dashboard.charts.totalFilled': 'Quantidade de Preenchimento de Anamnese',
  'admin.dashboard.charts.totalFilledByGender': 'Quantidade de Preenchimento de Anamnese por Gênero',
  'admin.dashboard.charts.totalFilledByAge': 'Quantidade Realizada por Faixa Etária',
  'admin.dashboard.charts.totalDone': 'Quantidade Realizada',
  'admin.dashboard.charts.totalForeseen': 'Quantidade Prevista',
  'admin.dashboard.charts.highRiskChart': 'ALTO RISCO',
  'admin.dashboard.charts.normalRiskChart': 'RISCO NORMAL',
  'admin.dashboard.charts.highRiskSummary': 'Quantidade de Altos Riscos Identificados, por Tipo de Câncer',
  'admin.dashboard.charts.smoker': 'Tagagista',

  /* Empresas */
  'admin.company.title': 'Empresa',
  'admin.company.title.plural': 'Empresas',

  /* Plano de Saúde */
  'admin.healthInsurance.title': 'Plano de Saúde',
  'admin.healthInsurance.title.plural': 'Planos de Saúde',

  /* Perfil de Acesso */
  'admin.roles.title': 'Perfis de Acesso',
  'admin.roles.title.plural': 'Perfis de Acesso',
  'admin.formRole.permissions': 'Permissões',

  /* Idiomas */
  'admin.languages.title': 'Idioma',
  'admin.languages.title.plural': 'Idiomas',
  'admin.languages.slug': 'Slug',

  /* Pacientes */
  'admin.patients.title': 'Paciente',
  'admin.patients.title.plural': 'Pacientes',
  'admin.patients.listOfPatients': 'Lista de Pacientes',
  'admin.patients.importPatients': 'Importar Pacientes',
  'admin.patients.exportPatients': 'Exportar Pacientes',
  'admin.patients.deletedPatients': 'Pacientes Removidos',
  'admin.patients.exportText': 'Selecione os filtros de sua preferência e clique em "Exportar" para solicitar o relatório de pacientes. O tempo para o documento ficar pronto depende da fila no sistema e os critérios de exportação usados.',
  'admin.patients.fields.gender': 'M ou F',
  'admin.patients.fields.cpf': 'Somente números (05376483649) ou formatado (768.884.253-04)',
  'admin.patients.fields.phone': 'Somente números (41993241556) ou formatado ((41) 99324-1556)',
  'admin.patients.fields.birthday': 'AAAA-MM-DD (Exemplo: 1990-06-20)',
  'admin.patients.fields.cep': 'Somente números (93530280) ou formatado (93530-280)',

  /* Prontuário */
  'admin.medicalRecords.title': 'Prontuário',
  'admin.medicalRecords.title.plural': 'Prontuários',
  'admin.medicalRecords.open': 'Abrir prontuário',
  'admin.medicalRecords.close': 'Fechar prontuário',
  'admin.medicalRecords.noInformation': 'Nenhuma informação disponível',
  'admin.medicalRecords.gail.risk1': 'Risco Estimado da Paciente de desenvolver Câncer de Mama nos próximos 05 anos',
  'admin.medicalRecords.gail.risk2': 'Risco de desenvolvimento de Câncer de Mama da População Geral nos próximos 05 anos',
  'admin.medicalRecords.gail.risk3': 'Risco Médio da Paciente de desenvolver Câncer de Mama até os 90 anos',
  'admin.medicalRecords.gail.risk4': 'Risco Médio da População Geral de desenvolver Câncer de Mama até os 90 anos',
  'admin.medicalRecords.madeContact': 'Contato Realizado',
  'admin.medicalRecords.noMoreContact': 'Não Requer Novo Contato',
  'admin.medicalRecords.noMoreContactQuestion': 'Requer Novo Contato?',
  'admin.medicalRecords.threeContactsMessage': '3 Tentativas de Contato',

  /* Tipos de Câncer */
  'admin.cancerTypes.title': 'Tipo de Câncer',
  'admin.cancerTypes.title.plural': 'Tipos de Câncer',

  /* Estratégias */
  'admin.strategies.title': 'Estratégia',
  'admin.strategies.title.plural': 'Estratégias',

  /* Exames Recomendados */
  'admin.exams.title': 'Exame',
  'admin.exams.title.plural': 'Exames Recomendados',

  /* Site */
  'admin.site.title': 'Site',
  'admin.testimonies.title': 'Depoimento',
  'admin.testimonies.title.plural': 'Depoimentos',
  'admin.testimonies.author': 'Autor do depoimento',
  'admin.specialities.title': 'Especialidade',
  'admin.specialities.title.plural': 'Especialidades',

  /* Anamnese */
  'admin.anamnesis.title': 'Anamnese',
  'admin.anamnesis.title.plural': 'Anamneses',

  /* Categorias */
  'admin.categories.title': 'Categoria',
  'admin.categories.title.plural': 'Categorias',

  /* Script */
  'admin.script.title': 'Roteiro',
  'admin.script.title.plural': 'Roteiros',
  'admin.script.categoryAlreadyAdded': 'Categoria já cadastrada',
  'admin.script.addCategory': 'Adicionar Categoria',
  'admin.script.addAnswer': 'Adicionar Resposta',
  'admin.script.addQuestion': 'Adicionar Pergunta',
  'admin.script.editQuestion': 'Editar Pergunta',
  'admin.script.editAnswer': 'Editar Resposta',
  'admin.script.valueAnswer': 'Valor da resposta',
  'admin.script.questionType': 'Tipo de pergunta',
  'admin.script.selectTheQuestionType': 'Selecione o tipo de pergunta',
  'admin.script.questionTitle': 'Título da pergunta',
  'admin.script.questionDescription': 'Descrição da pergunta',
  'admin.script.questionTypes.objective': 'Objetiva',
  'admin.script.questionTypes.discursive': 'Discursiva',
  'admin.script.answer': 'Resposta',
  'admin.script.question': 'Pergunta',
  'admin.script.category': 'Categoria',
  'admin.script.status': 'Status do Roteiro',
  'admin.script.requiredRegistration': 'Matrícula obrigatória',
  'admin.script.requiredPhone': 'Telefone obrigatório',
  'admin.script.requiredCellphone': 'Celular obrigatório',
  'admin.script.requiredHealthcareInsurance': 'Plano de saúde obrigatório',
  'admin.script.minimumPeriod': 'Período mínimo para responder novamente este roteiro (em meses)',
  'admin.script.displayResultPageWithRisks': 'Mostrar página de resultado com riscos',
  'admin.script.sendResultForEmail': 'Enviar resultado por e-mail',
  'admin.script.sendResultForEmailPeriod': 'Prazo para envio dos resultados (em dias)',
  'admin.script.sendReminderEmail': 'Enviar e-mail de lembrete',
  'admin.script.sendReminderEmailPeriod': 'Prazo para envio (em dias)',
  'admin.script.sendReminderEmailQuantity': 'Quantidade de envios',
  'admin.script.resultPageContent': 'Conteúdo da página de resultado',
  'admin.script.initialPageContent': 'Conteúdo página inicial',
  'admin.script.displayFooter': 'Mostrar rodapé',
  'admin.script.questionGroupError': 'Você não pode cadastrar um grupo dentro de outro grupo',
  'admin.script.additionalInfoPageContent': 'Informações Adicionais',
  'admin.script.displayAdditionalInfoPageContent': 'Mostrar Informações Adicionais',

  /* Disparo de Roteiro */
  'admin.scriptSend.title': 'Disparo de Roteiro',
  'admin.scriptSend.exportCsvFile': 'Baixar arquivo CSV',
  'admin.scriptSend.sendEmail': 'Enviar e-mail',
  'admin.scriptSend.sentScripts': 'Roteiros Disparados',

  /* Membros da Equipe */
  'admin.team.title': 'Membro da Equipe',
  'admin.team.title.plural': 'Membros da Equipe',

  /* Validações */
  'validations.required': 'Campo Obrigatório',
  'validations.integer': 'Campo precisa ser um número inteiro',
  'validations.integerBetween': 'Insira um número entre {min} e {max}',
  'validations.email': 'E-mail inválido',
  'validations.phone': 'Telefone inválido',
  'validations.cpf': 'CPF inválido',
  'validations.cnpj': 'CNPJ inválido',
  'validations.cep': 'CEP inválido',
  'validations.date': 'Data inválida',
  'validations.password': 'Precisa conter pelo menos 8 caracteres com números, letras maiúsculas e minúsculas',
  'validations.passwordConfirm': 'As senhas precisam ser iguais',
  'validations.fileType': 'Tipo de arquivo não suportado',

  /* Feedback */
  'feedback.success': 'Ação realizada com sucesso!',
  'feedback.error': 'Opa! Aconteceu algum problema!',

  /* Field Hints */
  'hints.cpf': 'Insira seu CPF com ou sem pontuação',
  'hints.cpfNumberOnly': 'Insira seu CPF sem pontuação (somente números)',
  'hints.number': 'Somente números',
  'hints.email': 'Insira seu e-mail (Ex: example@example.com)',
  'hints.height': 'Insira sua altura em centímetros (cm)',
  'hints.weight': 'Insira seu peso em kilogramas (kg)',
  'hints.dateOfBirth': 'A data deve estar no formado DD/MM/AAAA (exemplo: 25/10/1950)',

  /* Anamnese */
  'q.startPrevention': 'Iniciar Preenchimento',
  'q.backToAnamnese': 'Voltar à anamnese',
  'q.wizardStep': 'Etapa {progress} de {count}',
  'q.acceptContact': 'Eu aceito contato via telefone ou e-mail',
  'q.termsAccept': 'Eu aceito os',
  'q.termsOfService': 'Termos de Serviço',
  'q.termsAndConditions': 'Termos e Condições',
  'q.acceptRequired': 'É necessário aceitar os termos',
  'q.brca.question': 'Você já realizou um exame genético (conhecido como BRCA1 ou BRCA) para saber se tem uma alteração/mutação que aumenta seu risco para desenvolver câncer e este resultado foi positivo?',
  'q.brca.description': 'A mutação nos genes BRCA1 ou BRCA2 aumentam significativamente o risco para alguns tipos de câncer (mama, ovário, próstata, cólon e pâncreas). Esse é um exame genético que pode ser feito pelo sangue ou pela saliva para a pessoa saber se apresenta a mutação. Essa mutação ocorre geralmente em pessoas que têm vários familiares próximos acometidos por câncer. Estima-se que aproximadamente 5% a 10% dos casos de câncer de mama e ovários são ocasionados por essa mutação',
  'q.ethnicity.question': 'Etnia',
  'q.ethnicity.description': 'Sabe-se que pessoas de etnia negra apresentam risco aumentado para desenvolver câncer de próstata',
  'q.cancerHistory.question': 'Possui histórico pessoal de câncer?',
  'q.cancerHistory.description': 'Sabe-se que quem já foi acometido por câncer apresenta um risco elevado de apresentar recidiva ou eventualmente um novo tumor do mesmo tipo. Por isso é importante para o seu cálculo de risco saber se você já foi acometido por essa doença em sua vida.',
  'q.cancerHistory.whatCancer': 'Qual o tipo de câncer?',
  'q.cancerHistory.specifyCancer': 'Especifique o tipo de câncer:',
  'q.helloPatient': 'Olá, {name}',
  'q.patientIntroText': 'Chegou o momento de conhecermos você e um pouco do seu histórico médico. Preencha os campos de maneira precisa, para que a sua avaliação de risco seja feita da melhor forma. Vamos lá?',
  'q.startAvaliation': 'Iniciar Avaliação',
  'q.profileUnmatch': 'As anamneses desta campanha não se adequam ao seu perfil. Obrigado por nos ajudar até aqui!',
  'q.finishAvaliation': 'Finalizar avaliação',
  'q.toUpdateYourData': 'para atualizar seus dados pessoais',
  'q.stepFinished': 'Mais uma etapa concluída!',
  'q.youAreDoingRight': 'Você está preenchendo os dados da anamnese de maneira correta. Continue assim!',
  'q.continueAvaliation': 'Continuar Avaliação',
  'q.toAnswerTheQuestions': 'para responder às questões',
  'q.thanks': 'Obrigado por realizar a avaliação!',
  'q.sentForEmail': 'O resultado foi enviado para seu e-mail.',
  'q.welcomeUser': 'Bem-vindo(a), {name}',

  'terms': `<p>
          <strong>Antes de começar: quem somos e o que fazemos</strong>
        </p>
        <p>
          Somos uma empresa dedicada à promoção de informações médicas, visando a prevenção de alguns tipos de câncer no Brasil. Fazemos isso porque acreditamos que quanto maior a informação, maiores as chances de um diagnóstico precoce, o que é fundamental quando se fala de prevenção de câncer.</strong>
        </p>
        <p>
          A forma pela qual divulgamos as informações é um formulário pré-formatado, fundamentado na literatura médica e em métodos reconhecidos nacional e internacionalmente para verificação da presença de algum ou alguns fatores de risco que aumentem a estimativa do risco de desenvolvimento de uma patologia. Para isso, precisaremos de algumas informações pessoais suas, no que se incluem seus dados de identificação, histórico familiar de patologia bem como dados de sua vida cotidiana.
        </p>
        <p>
          Por exemplo, precisaremos saber se você fuma ou já fumou, se já teve relações sexuais e se já se submeteu a alguns tipos de exames preventivos. Estas e outras informações são essenciais para traçar um resultado correto da estimativa de risco de desenvolvimento dos tipos de câncer aplicados nos formulários.
        </p>
        <p>
          <strong>Como seus dados serão utilizados?</strong>
        </p>
        <p>
          Todos os dados pessoais preenchidos por você serão mantidos em absoluto sigilo e serão utilizados apenas para a finalidade de obter o resultado referente ao seu risco de desenvolvimento das patologias testadas. Isso vale tanto para dados de identificação (tais como seu nome, endereço eletrônico, idade, etc.) como também em relação às respostas aos formulários, ou seja, aos seus dados médicos ou cotidianos. Utilizaremos seu endereço eletrônico exclusivamente para envio dos resultados e não repassaremos estes dados a terceiros ou para qualquer outra finalidade.
        </p>
        <p>
          <strong>Quem tem acesso a meus dados?</strong>
        </p>
        <p>
          Para a obtenção do resultado, suas respostas serão analisadas automaticamente e conferidas manualmente por funcionários da Previneo, vinculados à área de saúde. A ferramenta foi desenvolvida por médicos, mas a análise dos dados não é feita por eles, já que não prestamos serviço de assistência à saúde, mas apenas de divulgação de informação. Nossos funcionários estão comprometidos com o sigilo dos dados e com nossa política de privacidade.
        </p>
        <p>
          Se você está participando desta avaliação como parte de um programa de saúde do seu empregador, os médicos (somente os médicos) responsáveis do seu empregador poderão ter acesso aos resultados, em total e absoluto respeito ao Código de Ética Médica.
        </p>
        <p>
          <strong>E se eu autorizar a utilização dos dados, em forma de estatística?</strong>
        </p>
        <p>
          DAo preencher nosso formulário, pedimos que além de aceitar nossos termos de uso, também concorde que os resultados respectivos possam compor uma base de dados para estabelecimento de políticas de prevenção. Afinal, além de oferecer informações adequadas, nossa ferramenta também pode servir para identificar falhas ou correções necessárias nos métodos de diagnóstico de câncer no Brasil.
        </p>
        <p>
          Neste caso, se estiver de acordo, os resultados obtidos no preenchimento do seu formulário poderão ser utilizados para composição de uma estatística, sem qualquer identificação pessoal ou de dados sensíveis. Manteremos o sigilo absoluto em relação a estes dados.
        </p>
        <p>
          Por exemplo, se você tem 20 anos, não fuma e mora em Curitiba-PR, seu risco de desenvolvimento de câncer de pulmão será considerado normal para sua idade e condição. Neste caso, o seu resultado comporá a estatística como uma pessoa de risco normal para desenvolvimento desta patologia, sem qualquer forma de identificação, apenas vinculado à região onde você reside.
        </p>
        <p>
          Quaisquer dúvidas ou esclarecimentos podem ser previamente sanados por nosso endereço eletrônico <strong>contato@previneo.com.br</strong>.
        </p>`,

  'admin.dashboard.chart.foreseen': 'Previsto',
  'admin.dashboard.chart.fulfilled': 'Realizado',
  'admin.dashboard.chart.not.fulfilled': 'Não Realizado',

  'admin.common.logoRight': 'Logo direita',
  'admin.common.logoLeft': 'Logo esquerda',
  'admin.common.justifications.title': 'Justificativa',
  'admin.common.justifications.title.plural': 'Justificativas',
  'admin.common.file': 'Arquivo',
  'admin.common.fileFront': 'Arquivo front',
  'admin.common.roles': 'Perfis de acesso',
  'admin.common.ageRange': 'Faixa Etária',
  'admin.permissions.title': 'Permissão',
  'admin.permissions.title.plural': 'Permissões',
  'admin.justifications.title': 'Justificativa',
  'admin.justifications.title.plural': 'Justificativas',
  'admin.faixasimc.abaixo': 'Abaixo do Peso',
  'admin.faixasimc.normal': 'Peso Normal',
  'admin.faixasimc.acima': 'Acima do Peso',
  'admin.faixasimc.obesidadeI': 'Obesidade I',
  'admin.faixasimc.obesidadeII': 'Obesidade II',
  'admin.faixasimc.obesidadeIII': 'Obesidade III',
  'admin.common.lastAnamnesis': 'Data da Anamnese',
  'admin.common.reSendEmail': 'Reenviar E-mail',
  'admin.common.normal': 'Normal',
  'admin.common.entity': 'Entidade',
  'admin.common.entities': 'Entidades',
  'admin.common.numberOfEmployees': 'Quantidade de Colaboradores',
  'admin.common.confirmation': 'Confirmação',
  'admin.common.confirmationNotFound': 'Registro não encontrado, clique no botão abaixo para receber um novo link.',
  'admin.common.editUser': 'Editar Usuário'
}
