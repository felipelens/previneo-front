export const API_URL = process.env.REACT_APP_API_URL || window.location.origin
export const APP_URL = process.env.REACT_APP_URL || '/app'

export const CLIENT_ID = 2
export const CLIENT_SECRET = process.env.REACT_APP_CLIENT_SECRET
export const GRANT_TYPE = 'password'

export const SCRIPT_NODE_TYPES = {
  QUESTION: 'question',
  ANSWER: 'answer',
  CATEGORY: 'category'
}

export const LANGUAGES = {
  pt: 'pt',
  en: 'en',
  es: 'es'
}

export const TYPE_QUESTIONS = {
  OBJETIVA: { id: 'Objetiva', name: 'admin.script.questionTypes.objective' },
  DISCURSIVA: { id: 'Discursiva', name: 'admin.script.questionTypes.discursive' },
  GROUP: { id: 'Group', name: 'admin.common.group' }
}

export const QUESTIONS_FORMATS = {
  none: { id: '', name: 'admin.common.none' },
  integer: { id: 'inteiro', name: 'admin.common.integer' }
}

export const PATIENTS_ETNIAS = {
  white: { label: 'white', value: 'Branco' },
  black: { label: 'black', value: 'Negro' },
  brown: { label: 'brown', value: 'Pardo' },
  yellow: { label: 'yellow', value: 'Amarelo' },
  indigenous: { label: 'indigenous', value: 'Indígena' }
}

export const CANCER_RISKS = {
  HIGH: { id: 'alto', name: 'Alto', color: 'danger' },
  MEDIUM: { id: 'medio', name: 'Médio', color: 'warning' },
  LOW: { id: 'baixo', name: 'Baixo', color: 'success' }
}

export const CONTACT_STATUS = {
  PENDING: { status: 'aguardando', action: 'start', label: 'admin.patientsHistory.status.pending', color: 'primary' },
  IN_PROGRESS: { status: 'em_atendimento', action: 'hold', label: 'admin.patientsHistory.status.inProgress', color: 'warning' },
  DONE: { status: 'finalizado', action: 'finish', label: 'admin.patientsHistory.status.done', color: 'success' }
}

export const GENDERS = {
  male: { name: 'admin.common.gender.male', id: 'M' },
  female: { name: 'admin.common.gender.female', id: 'F' },
  unisex: { name: 'admin.common.gender.unisex', id: 'U' }
}

export const COLORS = [
  '#66bbc3',
  '#f299b3',
  '#9f7f8b',
  '#d47b7a',
  '#e2d3b0',
  '#2ecc71',
  '#9b59b6'
]

export const ENABLE_MULTILANG = true
