import * as popsicle from 'popsicle'
import status from 'popsicle-status'
import prefix from 'popsicle-prefix'
import { API_URL } from 'config/constants'
import Storage from 'modules/Storage'
import { getTokenKey } from 'store/api/Auth'

const lang = (self, next) => {
  const lang = Storage.get('lang')
  if (lang) {
    self.set('Accept-Language', lang)
  }
  return next()
}

const auth = (self, next) => {
  const type = Storage.get('authType')
  const token = Storage.get(getTokenKey(type))
  if (token) {
    self.set('Authorization', `Bearer ${token}`)
  }
  return next()
}

const request = popsicle.defaults({
  headers: {
    Accept: 'application/json'
  },
  use: [
    status(),
    popsicle.plugins.stringify(),
    popsicle.plugins.headers(),
    popsicle.plugins.parse('json'),
    lang
  ]
})

request.api = popsicle.defaults({
  headers: {
    Accept: 'application/json'
  },
  use: [
    status(),
    popsicle.plugins.stringify(),
    popsicle.plugins.headers(),
    popsicle.plugins.parse('json'),
    prefix(API_URL),
    lang
  ]
})

request.auth = popsicle.defaults({
  headers: {
    Accept: 'application/json'
  },
  use: [
    status(),
    popsicle.plugins.stringify(),
    popsicle.plugins.headers(),
    popsicle.plugins.parse('json'),
    prefix(API_URL),
    lang,
    auth
  ]
})

export default request
