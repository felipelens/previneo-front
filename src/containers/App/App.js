import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Wrapper from 'containers/Wrapper'
import Site from 'components/Site'
import Admin from 'admin'
import Questionnaire from 'questionnaire'
import routes from 'routes'

export default function App () {
  return (
    <Wrapper>
      <Switch>
        <Route path={routes.admin.index} component={Admin} />
        <Route path={routes.questionnaire.index} component={Questionnaire} />
        <Route component={Site} />
      </Switch>
    </Wrapper>
  )
}
