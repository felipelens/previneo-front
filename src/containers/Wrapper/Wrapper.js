import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { injectGlobal, ThemeProvider } from 'styled-components'
import LocalizationProvider from 'containers/LocalizationProvider'
import theme from 'theme'
import configureStore from 'store'
import { APP_URL } from 'config/constants'

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Barlow+Condensed:400,500,700|Fira+Sans:400,400i,500,700');

  * {
    outline: 0;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: ${theme.fonts.primary};
  }
`

const store = configureStore()

export default function Wrapper (props) {
  return (
    <Provider store={store}>
      <LocalizationProvider>
        <Router basename={APP_URL}>
          <ThemeProvider theme={theme}>
            <div {...props} />
          </ThemeProvider>
        </Router>
      </LocalizationProvider>
    </Provider>
  )
}
