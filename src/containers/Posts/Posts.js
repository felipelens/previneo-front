import React from 'react'
import BlogList from 'components/BlogList'
import Button from 'components/Button'
import Spinner from 'components/Spinner'
import Resource from 'containers/Resource'
import { FormattedMessage } from 'react-intl'

export default function Posts ({ params }) {
  return (
    <Resource resource='PostsSite' autoFetch paginated params={{ limit: 12, per_page: 12, ...params }}>
      {props => {
        if (props.records.length === 0 && props.isFetching) {
          return <Spinner text='admin.common.loading' />
        }

        return (
          <React.Fragment>
            <BlogList posts={props.records} />
            {props.pagination.last_page > props.pagination.current_page && (
              <div style={{ textAlign: 'center' }}>
                <Button
                  type='button'
                  blockAtBreakpoint='large'
                  onClick={() => props.fetchAll({
                    ...props.params,
                    page: props.pagination.current_page + 1
                  })}
                  disabled={props.isFetching}
                >
                  {!props.isFetching ? <FormattedMessage id='admin.common.loadMore' /> : <FormattedMessage id='admin.common.loading' />}
                </Button>
              </div>
            )}
          </React.Fragment>
        )
      }}
    </Resource>
  )
}
