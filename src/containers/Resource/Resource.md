É um dos mais importantes componentes do App. Ele é capaz de gerenciar qualquer recurso
Restful com uma API estabelecida na pasta `store/api/`.

Com este componente, você consegue realizar todas as operações CRUD's necessárias.

### Listagem

Exemplo de listagem de usuários com `<Resource />`:

```jsx static
<Resource resource='Users' autoFetch params={{ page: 3 }}>
  {props => {
    // `isFetching` indica que a requisição está pendente
    if (props.isFetching) {
      return <Spinner text='Usuários estão sendo carregados...' />
    }

    // `records` é um array com os registros retornados pela API.
    console.log(props.records)

    // Podemos verificar se há algum registro retornado pela API.
    if (!props.records.length) {
      return 'Nenhum usuário retornado'
    }
    
    // Caso tenha registros, podemos então exibir esses registros na tela:
    return (
      <ul>
        {props.records.map(record => (
          <li key={record.id}>
            {record.name}
          </li>
        ))}
      </ul>
    )
  }}
</Resource>
```

### Detalhes

Caso precise extrair dados detalhados de algum registro, basta adicionar a prop `id` com
o ID deste registro:

```jsx static
// No exemplo abaixo, vou carregar os dados detalhados do post de id 2
<Resource resource='Posts' id={2} autoFetch> // A prop `spinner` adiciona automaticamente o spinner enquanto o registro estiver carregando.
  {props => {{
    // O registro detalhado fica na propriedade `detailedRecord`
    console.log(props.detailedRecord)
    
    return (
      <div>
        Dados detalhados do post:
        <pre>
          {JSON.stringify(props.detailedRecord, null, 4)}      
        </pre>
      </div>
    )
  }}
</Resource>
```

### Criação

No caso da criação, podemos omitir a prop `autoFetch` para evitar o carregamento automático.
Só precisamos dele se for preciso carregar a listagem ou registro detalhado.

```jsx static
// Importamos um formulário
import FormExample from 'forms/FormExample'

<Resource resource='Examples'>
  {props => {
     return (
       <FormExample
         onSubmit={data => props.create(data)} // `create` cria um registro
         isSubmitting={props.isSubmitting} // `isSubmitting` indica se o registro está sendo criado
       />
     )
  }}
</Resource>
```

### Atualização

No caso da atualização, teremos de usar as props `autoFetch` e `id` para carregar os dados detalhados
e adicioná-los ao form como valores iniciais.

E para a prop `onSubmit` do formulário, poderemos usar a action `update` que receberá o id e os dados
atualizado.

```jsx static
// Importamos um formulário
import FormExample from 'forms/FormExample'

// ID do recurso que eu quero atualizar
const id = 12

<Resource resource='Examples' id={id} autoFetch spinner> // Agora com autoFetch e spinner
  {props => {
     return (
       <FormExample
         initialValues={props.detailedRecord} // valor default para o formulário
         onSubmit={data => props.update(id, data)} // `update` com o ID e os dados que foram atualizados
         isSubmitting={props.isSubmitting} // `isSubmitting` indica se o registro está sendo atualizado
       />
     )
  }}
</Resource>
```

### Remoção

Para remover um registro, basta usar a action `remove` passando o ID do registro que você deseja deletar.

```jsx static
// ID do recurso que eu quero remover
const id = 70

<Resource resource='Examples'>
  {props => {
    if (props.isRemoving) { // `isRemoving` indica se algum registro está sendo removido
      return 'O registro está sendo removido'
    }

     return (
       <button
         type='button'
         onClick={() => props.remove(id)}
       >
         clique me para remover o registro {id}
       </button>
     )
  }}
</Resource>
```

### TUDO JUNTO!

Não precisamos usar um <Resource> pra cada action. Podemos trabalhar várias actions dentro de um
único <Resource />.

Abaixo, um exemplo mais completo com o Resource:

```jsx static
import FormExample from 'forms/FormExample'

// Vamos carregar uma listagem qualquer.
<Resource resource='Examples' autoFetch>
  {props => (
    <div>
      <Table
        data={props.records}
        isLoading={props.isFetching}
        onRemove={id => props.remove(id)}
      />
      <FormExample
        onSubmit={data => props.create(data)}
        isSubmitting={isSubmitting}
      />
    </div>
  )}
</Resource>
```
