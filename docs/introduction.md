Este projeto foi criado através do [create-react-app](https://github.com/facebook/create-react-app).

Na documentação oficial do create-react-app, você poderá tirar várias dúvidas sobre como rodar este projeto.

## Instalação

Instale as dependências:

`npm install`

Depois rode o projeto:

`npm run start`
