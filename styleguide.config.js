const path = require('path')

module.exports = {
  skipComponentsWithoutExample: true,
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/containers/Wrapper')
  },
  sections: [
    {
      name: 'Introdução',
      content: 'docs/introduction.md'
    },
    {
      name: 'UI Components',
      content: 'docs/ui-components.md',
      components: 'src/components/**/*.js'
    },
    {
      name: 'Containers',
      components: 'src/containers/**/*.js'
    },
    {
      name: 'Formulários',
      components: 'src/forms/**/*.js'
    },
    {
      name: 'Questionário',
      sections: [
        {
          name: 'UI Components',
          components: 'src/questionnaire/components/**/*.js'
        }
      ]
    },
    {
      name: 'Admin',
      sections: [
        {
          name: 'UI Components',
          components: 'src/admin/components/**/*.js'
        },
        {
          name: 'Forms',
          components: 'src/admin/forms/**/*.js'
        }
      ]
    }
  ]
}
